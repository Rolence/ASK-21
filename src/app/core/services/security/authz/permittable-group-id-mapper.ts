/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {FimsPermissionDescriptor} from './fims-permission-descriptor';
import {IdentityPermittableGroupIds} from '../../identity/domain/permittable-group-ids.model';
import {OfficePermittableGroupIds} from '../../office/domain/permittable-group-ids.model';
import {CustomerPermittableGroupIds} from '../../customer/domain/permittable-group-ids';
import {AccountingPermittableGroupIds} from '../../accounting/domain/permittable-group-ids';
import {PermissionId} from './permission-id.type';
import {Injectable} from '@angular/core';
import {DepositAccountPermittableGroupIds} from '../../depositAccount/domain/permittable-group-ids';
import {TellerPermittableGroupIds} from '../../teller/domain/permittable-group-ids';
import {ReportingPermittableGroupIds} from '../../reporting/domain/permittable-group-ids';
import {ChequePermittableGroupIds} from '../../cheque/domain/permittable-group-ids';
import {PayrollPermittableGroupIds} from '../../payroll/domain/permittable-group-ids';
import {PermittableGroupIds} from '../../loan/domain/permittable-group-ids.model';
import {CatalogPermittableGroupIds} from '../../catalog/domain/permittable-group-ids';

interface PermittableGroupMap {
  [s: string]: FimsPermissionDescriptor;
}

/**
 * Maps permittable group ids to internal keys
 */
@Injectable()
export class PermittableGroupIdMapper {

  private _permittableGroupMap: PermittableGroupMap = {};

  constructor() {
    this._permittableGroupMap[OfficePermittableGroupIds.EMPLOYEE_MANAGEMENT] = {id: 'office_employees', label: 'Employees'};
    this._permittableGroupMap[OfficePermittableGroupIds.OFFICE_MANAGEMENT] = {id: 'office_offices', label: 'Offices'};
    this._permittableGroupMap[OfficePermittableGroupIds.SELF_MANAGEMENT] = {
      id: 'office_self',
      label: 'User created resources(Offices & Employees)'
    };
    this._permittableGroupMap[OfficePermittableGroupIds.CONFIGURATION_MANAGEMENT] = { id: 'office_config', label: 'Configuration' };

    this._permittableGroupMap[IdentityPermittableGroupIds.IDENTITY_MANAGEMENT] = {id: 'identity_identities', label: 'Identities'};
    this._permittableGroupMap[IdentityPermittableGroupIds.ROLE_MANAGEMENT] = {id: 'identity_roles', label: 'Roles'};
    this._permittableGroupMap[IdentityPermittableGroupIds.SELF_MANAGEMENT] = {
      id: 'identity_self',
      label: 'User created resources(Identity & Roles)'
    };

    this._permittableGroupMap[CustomerPermittableGroupIds.CUSTOMER_MANAGEMENT] = {id: 'customer_customers', label: 'Members'};
    this._permittableGroupMap[CustomerPermittableGroupIds.TASK_MANAGEMENT] = {id: 'customer_tasks', label: 'Tasks'};
    this._permittableGroupMap[CustomerPermittableGroupIds.CATALOG_MANAGEMENT] = {id: 'catalog_catalogs', label: 'Custom fields'};
    this._permittableGroupMap[CustomerPermittableGroupIds.IDENTITY_CARD_MANAGEMENT] = {
      id: 'customer_identifications',
      label: 'Member identification cards'
    };

    this._permittableGroupMap[CatalogPermittableGroupIds.CATALOG_MANAGEMENT] = {
      id: 'catalog_catalog_management', label: 'Catalog management'
    };
    this._permittableGroupMap[CatalogPermittableGroupIds.CATALOG_VALUES] = {id: 'catalog_catalog_values', label: 'Catalog values'};

    this._permittableGroupMap[CustomerPermittableGroupIds.PORTRAIT_MANAGEMENT] = {id: 'customer_portrait', label: 'Member portrait'};
    this._permittableGroupMap[CustomerPermittableGroupIds.CUSTOMER_DOCUMENT] = {id: 'customer_documents', label: 'Member documents'};
    this._permittableGroupMap[CustomerPermittableGroupIds.SETTINGS] = {id: 'customer_settings', label: 'Settings'};

    this._permittableGroupMap[AccountingPermittableGroupIds.ACCOUNT_MANAGEMENT] = {id: 'accounting_accounts', label: 'Accounts'};
    this._permittableGroupMap[AccountingPermittableGroupIds.JOURNAL_MANAGEMENT] = {id: 'accounting_journals', label: 'Journal'};
    this._permittableGroupMap[AccountingPermittableGroupIds.LEDGER_MANAGEMENT] = {id: 'accounting_ledgers', label: 'Ledger'};
    this._permittableGroupMap[AccountingPermittableGroupIds.TRANSACTION_TYPES] = {id: 'accounting_tx_types', label: 'Transaction types'};
    this._permittableGroupMap[AccountingPermittableGroupIds.THOTH_TRANSACTION] = {
      id: 'accounting_tx',
      label: 'Manual journal entries'
    };

    this._permittableGroupMap[AccountingPermittableGroupIds.THOTH_TRANSACTION_PROCESSING] = {
      id: 'accounting_tx_processing',
      label: 'Manual journal entry processing'
    };

    this._permittableGroupMap[AccountingPermittableGroupIds.THOTH_BANK_INFOS] = {
      id: 'accounting_bank_infos',
      label: 'Bank infos'
    };

    this._permittableGroupMap[PermittableGroupIds.CONSUMER_LOAN_PRODUCT] = { id: 'loan_consumer', label: 'Consumer loan products' };
    this._permittableGroupMap[PermittableGroupIds.LOAN_AGREEMENT] = { id: 'loan_agreement', label: 'Loan agreements' };
    this._permittableGroupMap[PermittableGroupIds.LOAN_APPROVE_AGREEMENT] = {
      id: 'loan_approve_agreement', label: 'Approve loan agreements'
    };
    this._permittableGroupMap[PermittableGroupIds.LOAN_DISBURSEMENT] = {
      id: 'loan_disburse_agreement', label: 'Loans disbursement'
    };
    this._permittableGroupMap[PermittableGroupIds.LOAN_PAYMENT] = {
      id: 'loan_repay_agreement', label: 'Loan repayment'
    };

    this._permittableGroupMap[DepositAccountPermittableGroupIds.DEFINITION_MANAGEMENT] = {
      id: 'deposit_definitions',
      label: 'Deposit account management'
    };
    this._permittableGroupMap[DepositAccountPermittableGroupIds.INSTANCE_MANAGEMENT] = {
      id: 'deposit_instances',
      label: 'Deposit account for members'
    };
    this._permittableGroupMap[DepositAccountPermittableGroupIds.TRANSACTION_MANAGEMENT] = {
      id: 'deposit_transactions',
      label: 'Deposit account transactions'
    };

    this._permittableGroupMap[TellerPermittableGroupIds.TELLER_MANAGEMENT] = {id: 'teller_management', label: 'Teller management'};
    this._permittableGroupMap[TellerPermittableGroupIds.TELLER_OPERATION] = {id: 'teller_operations', label: 'Teller operations'};
    this._permittableGroupMap[TellerPermittableGroupIds.TELLER_RECEIPT] = {id: 'teller_receipt', label: 'Print receipt'};

    this._permittableGroupMap[ReportingPermittableGroupIds.REPORT_MANAGEMENT] = {id: 'reporting_management', label: 'Report management'};

    this._permittableGroupMap[ChequePermittableGroupIds.CHEQUE_TRANSACTION] = {id: 'cheque_transaction', label: 'Cheque transaction'};
    this._permittableGroupMap[ChequePermittableGroupIds.CHEQUE_MANAGEMENT] = {id: 'cheque_management', label: 'Cheque management'};
    this._permittableGroupMap[ChequePermittableGroupIds.CHEQUE_PRINT] = {id: 'cheque_print', label: 'Print'};

    this._permittableGroupMap[PayrollPermittableGroupIds.CONFIGURATION] = {id: 'payroll_configuration', label: 'Payroll configuration'};
    this._permittableGroupMap[PayrollPermittableGroupIds.DISTRIBUTION] = {id: 'payroll_distribution', label: 'Payroll distribution'};
  }

  public map(permittableGroupId: string): FimsPermissionDescriptor {
    const descriptor: FimsPermissionDescriptor = this._permittableGroupMap[permittableGroupId];
    if (!descriptor) {
      console.warn(`Could not find permission descriptor for permittable group id '${permittableGroupId}'`);
    }
    return descriptor;
  }

  public isValid(id: PermissionId): boolean {
    for (const key in this._permittableGroupMap) {
      if (this._permittableGroupMap.hasOwnProperty(key)) {
        const descriptor: FimsPermissionDescriptor = this._permittableGroupMap[key];
        if (descriptor.id === id) {
          return true;
        }
      }
    }
    return false;
  }

}
