/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * List of supported permission ids for fims
 */
export type PermissionId = 'identity_self' | 'identity_identities' | 'identity_roles' |
  'office_self' | 'office_offices' | 'office_employees' | 'office_config' |
  'customer_customers' | 'customer_tasks' | 'catalog_catalogs' | 'customer_identifications' | 'customer_portrait' | 'customer_documents' |
  'catalog_catalog_management' | 'catalog_catalog_values' | 'customer_settings' |
  'accounting_accounts' | 'accounting_ledgers' | 'accounting_journals' | 'accounting_tx_types' | 'accounting_tx' |
  'accounting_tx_processing' | 'accounting_bank_infos' |
  'loan_consumer' | 'loan_agreement' | 'loan_approve_agreement' | 'loan_disburse_agreement' | 'loan_repay_agreement' |
  'deposit_definitions' | 'deposit_instances' | 'deposit_transactions' |
  'teller_management' | 'teller_operations' | 'teller_receipt' |
  'reporting_management' |
  'cheque_management' | 'cheque_transaction' | 'cheque_print' |
  'payroll_configuration' | 'payroll_distribution';
