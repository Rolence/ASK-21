/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class PermittableGroupIds {
  public static CONSUMER_LOAN_PRODUCT = 'loan__v1__consumerproduct';
  public static LOAN_AGREEMENT = 'loan__v1__loanagreement';
  public static LOAN_APPROVE_AGREEMENT = 'loan__v1__approveagreement';
  public static LOAN_DISBURSEMENT = 'loan__v1__loandisbursement';
  public static LOAN_PAYMENT = 'loan__v1__loanpayment';
}
