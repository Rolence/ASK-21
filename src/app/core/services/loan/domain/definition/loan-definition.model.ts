/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Term} from './term.model';
import {Currency} from '../currency.model';
import {ApprovalLimit} from './approval-limit.model';
import {Charge} from './charge.model';
import {PaymentPolicy} from './payment-policy.model';
import {DTIConfig} from './DTI-config.model';
import {ExpectedCreditLoss} from './expected-credit-loss.model';
import {Collateral} from '../collateral.model';
import {LoanType} from '../loan-type.model';

export interface LoanDefinition {
  loanType: LoanType;
  shortName: string;
  name: string;
  description?: string;
  currency: Currency;
  fundingAccount: string;
  loanAppropriationsLedger: string;
  memberLoanLedger: string;
  arrearsLedger?: string;
  paymentPolicy: PaymentPolicy;
  dtiConfig?: DTIConfig;
  term: Term;
  approvalLimits: ApprovalLimit[];
  charges: Charge[];
  expectedCreditLoss?: ExpectedCreditLoss;
  purposes?: string[];
  collateral?: Collateral;
  stopAccrualAfterDays?: number;
  active: boolean;
  createdBy?: string;
  createdAt?: string;
  lastModifiedBy?: string;
  lastModifiedAt?: string;
}
