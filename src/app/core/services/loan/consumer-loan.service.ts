/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Inject, Injectable} from '@angular/core';
import {LoanDefinition} from './domain/definition/loan-definition.model';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '../http/http.service';
import {Action} from './domain/definition/action.model';
import {PaymentPolicy} from './domain/definition/payment-policy.model';
import {LoanProductOutline} from './domain/definition/product-outline.model';
import {ProductCommand} from './domain/definition/product-command.model';
import {AmortizationSchedule} from './domain/finance/amortization-schedule.model';
import {LoanChangeSet} from './domain/definition/loan-change-set.model';
import {Term} from './domain/agreement/term.model';

@Injectable()
export class ConsumerLoanService {

  private baseUrl: string;

  constructor(private http: HttpClient, @Inject('loanBaseUrl') private loanBaseUrl: string) {
    this.baseUrl = `${loanBaseUrl}/consumer`;
  }

  fetchActions(): Observable<Action[]> {
    return this.http.get(`${this.baseUrl}/actions`);
  }

  fetchPaymentPolicies(): Observable<PaymentPolicy[]> {
    return this.http.get(`${this.baseUrl}/paymentpolicies`);
  }

  createProduct(consumerLoanDefinition: LoanDefinition): Observable<void> {
    return this.http.post(`${this.baseUrl}/products`, consumerLoanDefinition);
  }

  updateProduct(shortName: string, changeSet: LoanChangeSet): Observable<void> {
    return this.http.put(`${this.baseUrl}/products/${shortName}`, changeSet);
  }

  fetchAllProducts(): Observable<LoanProductOutline[]> {
    return this.http.get(`${this.baseUrl}/products`);
  }

  findProduct(shortName: string): Observable<LoanDefinition> {
    return this.http.get(`${this.baseUrl}/products/${shortName}`);
  }

  deleteProduct(shortName: string): Observable<void> {
    return this.http.delete(`${this.baseUrl}/products/${shortName}`);
  }

  process(shortName: string, productCommand: ProductCommand): Observable<void> {
    return this.http.post(`${this.baseUrl}/products/${shortName}/commands`, productCommand);
  }

  fetchCommands(shortName: string): Observable<ProductCommand[]> {
    return this.http.get(`${this.baseUrl}/products/${shortName}/commands`);
  }

  calculateAmortizationSchedule(shortName: string, term: Term): Observable<AmortizationSchedule> {
    return this.http.post(`${this.baseUrl}/products/${shortName}/amortizations`, term);
  }

}
