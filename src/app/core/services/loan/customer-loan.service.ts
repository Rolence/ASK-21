/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {HttpClient} from '../http/http.service';
import {Inject, Injectable} from '@angular/core';
import {LoanAgreement} from './domain/agreement/loan-agreement.model';
import {LoanAgreementOutline} from './domain/agreement/loan-agreement-outline.model';
import {Observable} from 'rxjs/Observable';
import {Document} from './domain/agreement/document-meta-data.model';
import {DocumentOutline} from './domain/agreement/document-outline.model';
import {DownloadService} from '../download/download.service';
import {Assessment} from './domain/agreement/assessment.model';
import {RequestOptionsArgs, URLSearchParams} from '@angular/http';
import {Confirmation} from './domain/agreement/confirmation.model';
import {Restructuring} from './domain/agreement/restructuring.model';
import {RestructureDetail} from './domain/agreement/restructure-detail.model';

@Injectable()
export class CustomerLoanService {

  private baseUrl: string;

  constructor(private http: HttpClient, @Inject('loanBaseUrl') private loanBaseUrl: string, private downloadService: DownloadService) {
    this.baseUrl = `${loanBaseUrl}/customers`;
  }

  createLoanAgreement(customerNumber: string, loanAgreement: LoanAgreement): Observable<string> {
    return this.http.post(`${this.baseUrl}/${customerNumber}/agreements`, loanAgreement);
  }

  modifyLoanAgreement(customerNumber: string, loanAgreement: LoanAgreement): Observable<string> {
    return this.http.put(`${this.baseUrl}/${customerNumber}/agreements/${loanAgreement.loanAccount}`, loanAgreement);
  }

  deleteLoanAgreement(customerNumber: string, loanAccount: string): Observable<string> {
    return this.http.delete(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}`);
  }

  fetchAllLoanAgreements(customerNumber: string): Observable<LoanAgreementOutline[]> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements`);
  }

  findLoanAgreement(customerNumber: string, loanAccount: string): Observable<LoanAgreement> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}`)
      .map(agreement => Object.assign({}, agreement, {
        loanAccount
      }));
  }

  attachDocuments(customerNumber: string, loanAccount: string, documents: Document[]): Observable<void> {
    return this.http.post(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/documents`, documents);
  }

  fetchDocuments(customerNumber: string, loanAccount: string): Observable<DocumentOutline[]> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/documents`);
  }

  streamDocument(customerNumber: string, loanAccount: string, fileName: string): Observable<Blob> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/documents/${fileName}`);
  }

  downloadDocument(customerNumber: string, loanAccount: string, fileName: string): Observable<Blob> {
    return this.downloadService.downloadFile(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/documents/${fileName}`, fileName);
  }

  deleteDocument(customerNumber: string, loanAccount: string, fileName: string): Observable<void> {
    return this.http.delete(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/documents/${fileName}`);
  }

  requestAssessment(customerNumber: string, loanAccount: string, assessment: Assessment): Observable<void> {
    return this.http.post(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/assessments`, assessment);
  }

  fetchAssessments(customerNumber: string, loanAccount: string, pending?: boolean): Observable<Assessment[]> {
    const params = new URLSearchParams();

    if (pending != null) {
      params.append('pending', pending ? 'true' : 'false');
    }

    const requestOptions: RequestOptionsArgs = {
      params
    };

    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/assessments`, requestOptions);
  }

  getAssessment(customerNumber: string, loanAccount: string, sequence: number): Observable<Assessment> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/assessments/${sequence}`);
  }

  confirmAssessment(customerNumber: string, loanAccount: string, sequence: number): Observable<void> {
    return this.http.post(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/assessments/${sequence}`, {});
  }

  confirm(customerNumber: string, loanAccount: string, confirmation: Confirmation): Observable<void> {
    return this.http.post(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/confirmations`, confirmation);
  }

  fetchConfirmations(customerNumber: string, loanAccount: string): Observable<Confirmation[]> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/confirmations`);
  }

  getConfirmation(customerNumber: string, loanAccount: string, sequence: number): Observable<Confirmation> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/confirmations/${sequence}`);
  }

  restructure(customerNumber: string, loanAccount: string, restructuring: Restructuring): Observable<void> {
    return this.http.post(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/restructurings`, restructuring);
  }

  fetchRestructurings(customerNumber: string, loanAccount: string): Observable<Restructuring[]> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/restructurings`);
  }

  getRestructuring(customerNumber: string, loanAccount: string, sequence: number): Observable<RestructureDetail> {
    return this.http.get(`${this.baseUrl}/${customerNumber}/agreements/${loanAccount}/restructurings/${sequence}`);
  }
}
