/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '../http/http.service';
import {Observable} from 'rxjs/Observable';
import {Action} from './domain/definition/action.model';
import {CheckingProductDefinition} from './domain/definition/checking-product-definition.model';
import {ProductDefinitionCommand} from './domain/definition/product-definition-command.model';
import {CheckingProductChangeSet} from './domain/definition/checking-product-change-set.model';
import {SavingsProductDefinition} from './domain/definition/savings-product-definition.model';
import {SavingsProductChangeSet} from './domain/definition/savings-product-change-set.model';
import {ShareProductDefinition} from './domain/definition/share-product-definition.model';
import {ShareProductChangeSet} from './domain/definition/share-product-change-set.model';
import {DividendDistribution} from './domain/definition/dividend-distribution.model';
import {TermProductDefinition} from './domain/definition/term-product-definition.model';
import {TermProductChangeSet} from './domain/definition/term-product-change-set.model';
import {ProductOutline} from './domain/definition/product-outline.model';
import {ProductInstanceOutline} from './domain/instance/product-instance-outline.model';
import {ProductInstanceAssignment} from './domain/instance/product-instance-assignment.model';
import {InstanceCommand} from './domain/instance/instance-command.model';
import {ProductInstanceChangeSet} from './domain/instance/product-instance-change-set.model';
import {ProductInstance} from './domain/instance/product-instance.model';
import {Type} from './domain/type.model';
import {StandingOrder} from './domain/instance/standing-order.model';
import {ACHTransaction} from './domain/ach-transaction.model';
import {RequestOptionsArgs, URLSearchParams} from '@angular/http';
import {DownloadService} from '../download/download.service';

@Injectable()
export class DepositAccountService {

  constructor(private http: HttpClient, @Inject('depositAccountBaseUrl') private baseUrl: string,
              private downloadService: DownloadService) {
  }

  createAction(action: Action): Observable<void> {
    return this.http.post(`${this.baseUrl}/actions`, action);
  }

  fetchActions(): Observable<Action[]> {
    return this.http.get(`${this.baseUrl}/actions`);
  }

  findProduct(identifier: string, type: Type): Observable<CheckingProductDefinition
    | SavingsProductDefinition | ShareProductDefinition | TermProductDefinition> {
    switch (type) {
      case 'CHECKING': {
        return this.findCheckingProduct(identifier);
      }
      case 'SAVINGS': {
        return this.findSavingsProduct(identifier);
      }
      case 'SHARE': {
        return this.findShareProduct(identifier);
      }
      case 'TERM': {
        return this.findTermProduct(identifier);
      }
      default:
        throw new Error(`Type ${type} not supported`);
    }
  }

  createCheckingProduct(checkingProductDefinition: CheckingProductDefinition): Observable<void> {
    return this.http.post(`${this.baseUrl}/checkings`, checkingProductDefinition);
  }

  findCheckingProduct(identifier: string): Observable<CheckingProductDefinition> {
    return this.http.get(`${this.baseUrl}/checkings/${identifier}`);
  }

  processCheckingCommand(identifier: string, command: ProductDefinitionCommand): Observable<void> {
    return this.http.post(`${this.baseUrl}/checkings/${identifier}/commands`, command);
  }

  changeCheckingProduct(identifier: string, checkingProductChangeSet: CheckingProductChangeSet): Observable<void> {
    return this.http.put(`${this.baseUrl}/checkings/${identifier}`, checkingProductChangeSet);
  }

  deleteCheckingProduct(identifier: string): Observable<void> {
    return this.http.delete(`${this.baseUrl}/checkings/${identifier}`);
  }

  createSavingsProduct(savingsProductDefinition: SavingsProductDefinition): Observable<void> {
    return this.http.post(`${this.baseUrl}/savings`, savingsProductDefinition);
  }

  findSavingsProduct(identifier: string): Observable<SavingsProductDefinition> {
    return this.http.get(`${this.baseUrl}/savings/${identifier}`);
  }

  processSavingsCommand(identifier: string, command: ProductDefinitionCommand): Observable<void> {
    return this.http.post(`${this.baseUrl}/savings/${identifier}/commands`, command);
  }

  changeSavingsProduct(identifier: string, savingsProductChangeSet: SavingsProductChangeSet): Observable<void> {
    return this.http.put(`${this.baseUrl}/savings/${identifier}`, savingsProductChangeSet);
  }

  deleteSavingsProduct(identifier: string): Observable<void> {
    return this.http.delete(`${this.baseUrl}/savings/${identifier}`);
  }

  createShareProduct(shareProductDefinition: ShareProductDefinition): Observable<void> {
    return this.http.post(`${this.baseUrl}/shares`, shareProductDefinition);
  }

  findShareProduct(identifier: string): Observable<ShareProductDefinition> {
    return this.http.get(`${this.baseUrl}/shares/${identifier}`);
  }

  processShareCommand(identifier: string, command: ProductDefinitionCommand): Observable<void> {
    return this.http.post(`${this.baseUrl}/shares/${identifier}/commands`, command);
  }

  changeShareProduct(identifier: string, shareProductChangeSet: ShareProductChangeSet): Observable<void> {
    return this.http.put(`${this.baseUrl}/shares/${identifier}`, shareProductChangeSet);
  }

  deleteShareProduct(identifier: string): Observable<void> {
    return this.http.delete(`${this.baseUrl}/shares/${identifier}`);
  }

  dividendDistribution(identifier: string, dividendDistribution: DividendDistribution): Observable<void> {
    return this.http.post(`${this.baseUrl}/shares/${identifier}/dividends`, dividendDistribution);
  }

  fetchDividendDistributions(identifier: string): Observable<DividendDistribution[]> {
    return this.http.get(`${this.baseUrl}/shares/${identifier}/dividends`);
  }

  createTermProduct(termProductDefinition: TermProductDefinition): Observable<void> {
    return this.http.post(`${this.baseUrl}/terms`, termProductDefinition);
  }

  findTermProduct(identifier: string): Observable<TermProductDefinition> {
    return this.http.get(`${this.baseUrl}/terms/${identifier}`);
  }

  processTermCommand(identifier: string, command: ProductDefinitionCommand): Observable<void> {
    return this.http.post(`${this.baseUrl}/terms/${identifier}/commands`, command);
  }

  changeTermProduct(identifier: string, termProductChangeSet: TermProductChangeSet): Observable<void> {
    return this.http.put(`${this.baseUrl}/terms/${identifier}`, termProductChangeSet);
  }

  deleteTermProduct(identifier: string): Observable<void> {
    return this.http.delete(`${this.baseUrl}/terms/${identifier}`);
  }

  fetchProductDefinitions(): Observable<ProductOutline[]> {
    return this.http.get(`${this.baseUrl}/definitions`);
  }

  fetchProductInstancesByProductDefinition(identifier: string): Observable<ProductInstanceOutline[]> {
    return this.http.get(`${this.baseUrl}/definitions/${identifier}`);
  }

  createProductInstance(productInstanceAssignment: ProductInstanceAssignment): Observable<string> {
    return this.http.post(`${this.baseUrl}/instances`, productInstanceAssignment);
  }

  processProductInstanceCommand(identifier: string, instanceCommand: InstanceCommand): Observable<void> {
    return this.http.post(`${this.baseUrl}/instances/${identifier}/commands`, instanceCommand);
  }

  changeProductInstance(identifier: string, productInstanceChangeSet: ProductInstanceChangeSet): Observable<void> {
    return this.http.put(`${this.baseUrl}/instances/${identifier}`, productInstanceChangeSet);
  }

  findProductInstance(identifier: string): Observable<ProductInstance> {
    return this.http.get(`${this.baseUrl}/instances/${identifier}`);
  }

  fetchProductInstancesByCustomer(identifier: string): Observable<ProductInstanceOutline[]> {
    return this.http.get(`${this.baseUrl}/customers/${identifier}/instances`);
  }

  fetchPossibleTransactionsForCustomer(identifier: string): Observable<string[]> {
    return this.http.get(`${this.baseUrl}/customers/${identifier}/transactions`);
  }

  createStandingOrder(identifier: string, standingOrder: StandingOrder): Observable<number> {
    return this.http.post(`${this.baseUrl}/instances/${identifier}/orders`, standingOrder);
  }

  fetchAllStandingOrders(identifier: string): Observable<StandingOrder[]> {
    return this.http.get(`${this.baseUrl}/instances/${identifier}/orders`);
  }

  getStandingOrder(identifier: string, sequence: number): Observable<StandingOrder> {
    return this.http.get(`${this.baseUrl}/instances/${identifier}/orders/${sequence}`);
  }

  deleteStandingOrder(identifier: string, sequence: number): Observable<void> {
    return this.http.delete(`${this.baseUrl}/instances/${identifier}/orders/${sequence}`);
  }

  updateStandingOrder(identifier: string, standingOrder: StandingOrder): Observable<void> {
    return this.http.put(`${this.baseUrl}/instances/${identifier}/orders/${standingOrder.sequence}`, standingOrder);
  }

  fetchACHTransactions(): Observable<ACHTransaction[]> {
    return this.http.get(`${this.baseUrl}/ach`);
  }

  downloadACHTransactions(bankAccount: string): Observable<string | {}> {
    const params = new URLSearchParams();
    params.append('account', bankAccount);

    const requestOptions: RequestOptionsArgs = {
      params
    };
    return this.downloadService.getText(`${this.baseUrl}/ach/export`, requestOptions);
  }

  probe(until?: string): Observable<StandingOrder[]> {
    const params = new URLSearchParams();
    params.append('until', until);

    const requestOptions: RequestOptionsArgs = {
      params
    };
    return this.http.get(`${this.baseUrl}/standingorders/probe`, requestOptions);
  }
}
