/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {createSelector} from 'reselect';
import {ActionReducer, ActionReducerMap, createFeatureSelector, MetaReducer} from '@ngrx/store';

import * as fromAuthentication from './security/authentication.reducer';
import * as fromAuthorization from './security/authorization.reducer';
import * as fromAccounts from './account/accounts.reducer';
import * as authenticationActions from './security/security.actions';
import {
  createSearchReducer,
  getSearchEntities,
  getSearchLoading,
  getSearchTotalElements,
  getSearchTotalPages,
  SearchState
} from './util/search.reducer';
import {InjectionToken} from '@angular/core';
import {localStorageSync} from 'ngrx-store-localstorage';

export interface State {
  authentication: fromAuthentication.State;
  authorization: fromAuthorization.State;
  officeSearch: SearchState;
  countrySearch: SearchState;
  employeeSearch: SearchState;
  roleSearch: SearchState;
  customerSearch: SearchState;
  accountSearch: SearchState;
  ledgerSearch: SearchState;
}

export const reducers: ActionReducerMap<State> = {
  authentication: fromAuthentication.reducer,
  authorization: fromAuthorization.reducer,

  officeSearch: createSearchReducer('Office'),
  countrySearch: createSearchReducer('Country'),
  employeeSearch: createSearchReducer('Employee'),
  roleSearch: createSearchReducer('Role'),
  customerSearch: createSearchReducer('Customer'),
  accountSearch: createSearchReducer('Account', fromAccounts.reducer),
  ledgerSearch: createSearchReducer('Ledger')
};

export const reducerToken = new InjectionToken<ActionReducerMap<State>>('Root reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export function resetStateOnLogout(reducer: ActionReducer<State>): ActionReducer<State> {
  return function(state: State, action: any): State {
    // Reset state
    if (action.type === authenticationActions.LOGOUT_SUCCESS) {
      return reducer(undefined, action);
    }

    return reducer(state, action);
  };
}

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({keys: ['authentication'], rehydrate: true})(reducer);
}

export const metaReducers: MetaReducer<State>[] = [resetStateOnLogout, localStorageSyncReducer];

/**
 * Office Search Selectors
 */

export const getOfficeSearchState = createFeatureSelector<SearchState>('officeSearch');

export const getSearchOffices = createSelector(getOfficeSearchState, getSearchEntities);
export const getOfficeSearchTotalElements = createSelector(getOfficeSearchState, getSearchTotalElements);
export const getOfficeSearchTotalPages = createSelector(getOfficeSearchState, getSearchTotalPages);
export const getOfficeSearchLoading = createSelector(getOfficeSearchState, getSearchLoading);

export const getOfficeSearchResults = createSelector(getSearchOffices, getOfficeSearchTotalPages, getOfficeSearchTotalElements,
  (offices, totalPages, totalElements) => {
  return {
    offices: offices,
    totalPages: totalPages,
    totalElements: totalElements
  };
});

/**
 * Country Search Selectors
 */

export const getCountrySearchState = createFeatureSelector<SearchState>('countrySearch');
export const getSearchCountry = createSelector(getCountrySearchState, getSearchEntities);

/**
 * Employee Search Selectors
 */
export const getEmployeeSearchState = createFeatureSelector<SearchState>('employeeSearch');

export const getSearchEmployees = createSelector(getEmployeeSearchState, getSearchEntities);
export const getEmployeeSearchTotalElements = createSelector(getEmployeeSearchState, getSearchTotalElements);
export const getEmployeeSearchTotalPages = createSelector(getEmployeeSearchState, getSearchTotalPages);
export const getEmployeeSearchLoading = createSelector(getEmployeeSearchState, getSearchLoading);

export const getEmployeeSearchResults = createSelector(getSearchEmployees, getEmployeeSearchTotalPages, getEmployeeSearchTotalElements,
  (employees, totalPages, totalElements) => {
  return {
    employees: employees,
    totalPages: totalPages,
    totalElements: totalElements
  };
});

/**
 * Role Search Selectors
 */
export const getRoleSearchState = createFeatureSelector<SearchState>('roleSearch');

export const getSearchRoles = createSelector(getRoleSearchState, getSearchEntities);
export const getRoleSearchTotalElements = createSelector(getRoleSearchState, getSearchTotalElements);
export const getRoleSearchTotalPages = createSelector(getRoleSearchState, getSearchTotalPages);
export const getRoleSearchLoading = createSelector(getRoleSearchState, getSearchLoading);

export const getRoleSearchResults = createSelector(getSearchRoles, getRoleSearchTotalPages, getRoleSearchTotalElements,
  (roles, totalPages, totalElements) => {
  return {
    roles: roles,
    totalPages: totalPages,
    totalElements: totalElements
  };
});

/**
 * Customer Search Selectors
 */
export const getCustomerSearchState = createFeatureSelector<SearchState>('customerSearch');

export const getSearchCustomers = createSelector(getCustomerSearchState, getSearchEntities);
export const getCustomerSearchTotalElements = createSelector(getCustomerSearchState, getSearchTotalElements);
export const getCustomerSearchTotalPages = createSelector(getCustomerSearchState, getSearchTotalPages);
export const getCustomerSearchLoading = createSelector(getCustomerSearchState, getSearchLoading);

export const getCustomerSearchResults = createSelector(getSearchCustomers, getCustomerSearchTotalPages, getCustomerSearchTotalElements,
  (customers, totalPages, totalElements) => {
  return {
    customers: customers,
    totalPages: totalPages,
    totalElements: totalElements
  };
});

/**
 * Account Search Selectors
 */
export const getAccountSearchState = createFeatureSelector<SearchState>('accountSearch');

export const getSearchAccounts = createSelector(getAccountSearchState, getSearchEntities);
export const getAccountSearchTotalElements = createSelector(getAccountSearchState, getSearchTotalElements);
export const getAccountSearchTotalPages = createSelector(getAccountSearchState, getSearchTotalPages);
export const getAccountSearchLoading = createSelector(getAccountSearchState, getSearchLoading);

export const getAccountSearchResults = createSelector(getSearchAccounts, getAccountSearchTotalPages, getAccountSearchTotalElements,
  (accounts, totalPages, totalElements) => {
  return {
    accounts: accounts,
    totalPages: totalPages,
    totalElements: totalElements
  };
});

/**
 * Ledger Search Selectors
 */
export const getLedgerSearchState = createFeatureSelector<SearchState>('ledgerSearch');

export const getSearchLedgers = createSelector(getLedgerSearchState, getSearchEntities);
export const getLedgerSearchTotalElements = createSelector(getLedgerSearchState, getSearchTotalElements);
export const getLedgerSearchTotalPages = createSelector(getLedgerSearchState, getSearchTotalPages);

export const getLedgerSearchResults = createSelector(getSearchLedgers, getLedgerSearchTotalElements, getLedgerSearchTotalPages,
  (ledgers, totalPages, totalElements) => {
  return {
    ledgers: ledgers,
    totalPages: totalPages,
    totalElements: totalElements
  };
});

export const getAuthenticationState = createFeatureSelector<fromAuthentication.State>('authentication');

export const getAuthentication = createSelector(getAuthenticationState, fromAuthentication.getAuthentication);
export const getAuthenticationError = createSelector(getAuthenticationState, fromAuthentication.getError);
export const getAuthenticationLoading = createSelector(getAuthenticationState, fromAuthentication.getLoading);
export const getUsername = createSelector(getAuthenticationState, fromAuthentication.getUsername);
export const getTenant = createSelector(getAuthenticationState, fromAuthentication.getTenant);
export const getPasswordError = createSelector(getAuthenticationState, fromAuthentication.getPasswordError);

export const getAuthorizationState = createFeatureSelector<fromAuthorization.State>('authorization');

export const getPermissions = createSelector(getAuthorizationState, fromAuthorization.getPermissions);

export const getPermissionsLoading = createSelector(getAuthorizationState, fromAuthorization.getLoading);
