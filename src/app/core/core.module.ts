/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {LOCALE_ID, NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpClient as FimsHttpClient} from './services/http/http.service';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {MissingTranslationHandler, TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {PayrollService} from './services/payroll/payroll.service';
import {ChequeService} from './services/cheque/cheque.service';
import {ReportingService} from './services/reporting/reporting.service';
import {NotificationService} from './services/notification/notification.service';
import {CurrencyService} from './services/currency/currency.service';
import {CountryService} from './services/country/country.service';
import {TellerService} from './services/teller/teller-service';
import {DepositAccountService} from './services/depositAccount/deposit-account.service';
import {CustomerLoanService} from './services/loan/customer-loan.service';
import {ConsumerLoanService} from './services/loan/consumer-loan.service';
import {AccountingService} from './services/accounting/accounting.service';
import {CatalogService} from './services/catalog/catalog.service';
import {CustomerService} from './services/customer/customer.service';
import {OfficeService} from './services/office/office.service';
import {IdentityService} from './services/identity/identity.service';
import {PermittableGroupIdMapper} from './services/security/authz/permittable-group-id-mapper';
import {AuthenticationService} from './services/security/authn/authentication.service';
import {DownloadService} from './services/download/download.service';
import {UpdateNotificationService} from './services/serviceWorker/update-notification.service';
import {getSelectedLanguage} from './services/i18n/translate';
import {ApplicationMissingTranslationHandler} from './services/i18n/ApplicationMissingTranslationHandler';
import {metaReducers, reducerProvider, reducerToken} from './store';
import {SecurityRouteEffects} from './store/security/effects/route.effects';
import {SecurityNotificationEffects} from './store/security/effects/notification.effects';
import {CustomerSearchApiEffects} from './store/customer/effects/service.effects';
import {EmployeeSearchApiEffects} from './store/employee/effects/service.effects';
import {OfficeSearchApiEffects} from './store/office/effects/service.effects';
import {SecurityApiEffects} from './store/security/effects/service.effects';
import {AccountSearchApiEffects} from './store/account/effects/service.effects';
import {RoleSearchApiEffects} from './store/role/effects/service.effects';
import {LedgerSearchApiEffects} from './store/ledger/effects/service.effects';
import {HttpModule} from '@angular/http';
import {environment} from '../../environments/environment';
import {ServiceWorkerModule} from '@angular/service-worker';
import {ExistsGuardService} from './services/guards/exists-guard';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {DialogService} from './services/dialog/dialog.service';
import {SettingsService} from './services/customer/settings.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    HttpModule,
    TranslateModule.forRoot({
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: ApplicationMissingTranslationHandler},
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    StoreModule.forRoot(reducerToken, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([
      SecurityApiEffects,
      SecurityRouteEffects,
      SecurityNotificationEffects,
      OfficeSearchApiEffects,
      EmployeeSearchApiEffects,
      CustomerSearchApiEffects,
      AccountSearchApiEffects,
      RoleSearchApiEffects,
      LedgerSearchApiEffects
    ]),
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    FimsHttpClient,
    AuthenticationService,
    PermittableGroupIdMapper,
    IdentityService,
    OfficeService,
    CustomerService,
    SettingsService,
    CatalogService,
    AccountingService,
    ConsumerLoanService,
    CustomerLoanService,
    DepositAccountService,
    TellerService,
    ReportingService,
    ChequeService,
    PayrollService,
    CountryService,
    CurrencyService,
    NotificationService,
    DownloadService,
    ExistsGuardService,
    DialogService,
    reducerProvider,
    {
      provide: LOCALE_ID, useFactory: getSelectedLanguage, deps: [TranslateService],
    },
    UpdateNotificationService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded');
    }
  }
}
