/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router, RouterState} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Store} from '@ngrx/store';
import * as fromRoot from './../core/store';
import {Observable} from 'rxjs/Observable';
import {TdMediaService} from '@covalent/core';
import {Subscription} from 'rxjs/Subscription';
import {MatSidenav} from '@angular/material';
import {CountryService} from '../core/services/country/country.service';
import {Action, HttpClient} from '../core/services/http/http.service';
import {PRE_LOGOUT} from '../core/store/security/security.actions';
import {NavigationItem} from '../shared/navigation/navigation.component';

@Component({
  templateUrl: './main.component.html',
  styleUrls: ['main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {

  private routerEventSubscription: Subscription;

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  navItems: NavigationItem[] = [
    {
      title: 'Quick access', icon: 'dashboard', routerLink: '/quickAccess'
    },
    {
      title: 'Offices',
      icon: 'store',
      routerLink: '/offices',
      permission: {id: 'office_offices', accessLevel: 'READ'},
      items: [
        {
          title: 'Headquarter',
          routerLink: '/offices',
          permission: {id: 'office_offices', accessLevel: 'READ'}
        }
      ]
    },
    {
      title: 'Employees',
      icon: 'group',
      routerLink: '/employees',
      permission: {id: 'office_employees', accessLevel: 'READ'},
      items: [
        {
          title: 'All employees',
          routerLink: '/employees',
          permission: {id: 'identity_roles', accessLevel: 'READ'}
        },
        {
          title: 'Roles',
          routerLink: '/employees/roles',
          permission: {id: 'identity_roles', accessLevel: 'READ'}
        }
      ]
    },
    {
      title: 'Accounting',
      icon: 'receipt',
      routerLink: '/accounting',
      permission: {id: 'accounting_ledgers', accessLevel: 'READ'},
      items: [
        {
          title: 'General ledger',
          routerLink: '/accounting',
          permission: {id: 'accounting_ledgers', accessLevel: 'READ'}
        },
        {
          title: 'Browse journal entries',
          routerLink: '/accounting/journalEntries',
          permission: {id: 'accounting_journals', accessLevel: 'READ'}
        },
        {
          title: 'Process journal entries',
          routerLink: '/accounting/transactions',
          permission: {id: 'accounting_tx', accessLevel: 'READ'}
        },
        {
          title: 'Transaction types',
          routerLink: '/accounting/transactiontypes',
          permission: {id: 'accounting_tx_types', accessLevel: 'READ'}
        },
        {
          title: 'Cheque clearing',
          routerLink: '/accounting/cheques',
          permission: {id: 'cheque_management', accessLevel: 'READ'}
        },
        {
          title: 'Accounts payable',
          routerLink: '/accounting/accountsPayable',
          permission: {id: 'cheque_management', accessLevel: 'READ'}
        },
        {
          title: 'Payrolls',
          routerLink: '/accounting/payrolls',
          permission: {id: 'payroll_distribution', accessLevel: 'READ'}
        },
        {
          title: 'Chart of accounts',
          routerLink: '/accounting/chartOfAccounts',
          permission: {id: 'accounting_ledgers', accessLevel: 'READ'}
        },
        {
          title: 'Bank infos',
          routerLink: '/accounting/bankInfos',
          permission: {id: 'accounting_bank_infos', accessLevel: 'READ'}
        }
      ]
    },
    {
      title: 'Member',
      icon: 'face',
      routerLink: '/customers',
      permission: {id: 'customer_customers', accessLevel: 'READ'},
      items: [
        {
          title: 'All members',
          routerLink: '/customers',
          permission: {id: 'customer_customers', accessLevel: 'READ'}
        },
        {
          title: 'Tasks',
          routerLink: '/customers/tasks',
          permission: {id: 'customer_tasks', accessLevel: 'READ'}
        },
        {
          title: 'Custom fields',
          routerLink: '/customers/catalog/detail',
          permission: {id: 'catalog_catalogs', accessLevel: 'READ'}
        },
        {
          title: 'Settings',
          routerLink: '/customers/settings',
          permission: {id: 'customer_settings', accessLevel: 'READ'}
        }
      ]
    },
    {
      title: 'Loan products',
      icon: 'credit_card',
      routerLink: '/loans',
      permission: {id: 'loan_consumer', accessLevel: 'READ'},
      items: [
        {
          title: 'All loan products',
          routerLink: '/loans',
          permission: {id: 'loan_consumer', accessLevel: 'READ'}
        }
      ]
    },
    {
      title: 'Deposit',
      icon: 'attach_money',
      routerLink: '/deposits',
      permission: {id: 'deposit_definitions', accessLevel: 'READ'},
      items: [
        {
          title: 'All deposit products',
          routerLink: '/deposits',
          permission: {id: 'deposit_definitions', accessLevel: 'READ'},
        },
        {
          title: 'Dry run standing orders',
          routerLink: '/deposits/dryRun',
          permission: {id: 'deposit_instances', accessLevel: 'READ'},
        },
        {
          title: 'ACH transactions',
          routerLink: '/deposits/ach',
          permission: {id: 'deposit_definitions', accessLevel: 'READ'}
        }
      ]
    },
    {
      title: 'Teller',
      icon: 'person',
      routerLink: '/teller',
      permission: {id: 'teller_operations', accessLevel: 'READ'}
    },
    {
      title: 'Reporting',
      icon: 'show_chart',
      routerLink: '/reporting',
      permission: {id: 'reporting_management', accessLevel: 'READ'},
      items: [
        {
          title: 'Member',
          routerLink: '/reporting',
          permission: {id: 'reporting_management', accessLevel: 'READ'}
        },
        {
          title: 'Deposit',
          routerLink: '/reporting/deposits',
          permission: {id: 'reporting_management', accessLevel: 'READ'}
        },
        {
          title: 'Loan',
          routerLink: '/reporting/loans',
          permission: {id: 'reporting_management', accessLevel: 'READ'}
        },
        {
          title: 'Finance',
          routerLink: '/reporting/finance',
          permission: {id: 'reporting_management', accessLevel: 'READ'}
        }
      ]
    }
  ];

  isLoading$: Observable<boolean>;
  isOpened$: Observable<boolean>;
  tenant$: Observable<string>;
  username$: Observable<string>;

  constructor(private router: Router, private titleService: Title, private httpClient: HttpClient, private countryService: CountryService,
              private store: Store<fromRoot.State>, private media: TdMediaService) {}

  ngOnInit(): void {
    this.routerEventSubscription = this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.getTitle(this.router.routerState, this.router.routerState.root))
      .subscribe(title => this.titleService.setTitle(title));

    this.tenant$ = this.store.select(fromRoot.getTenant);
    this.username$ = this.store.select(fromRoot.getUsername);
    this.isOpened$ = this.media.registerQuery('gt-sm');

    this.countryService.init();
  }

  ngOnDestroy(): void {
    this.routerEventSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.isLoading$ = this.httpClient.process
      .debounceTime(1000)
      .map((action: Action) => action === Action.QueryStart);

    this.media.broadcast();
  }

  private getTitle(state: RouterState, parent: ActivatedRoute): string {
    const titleChunks = this.getTitleChunks(state, parent);
    titleChunks.push('Kuelap Connect');
    return titleChunks.join(' - ');
  }

  private getTitleChunks(state: RouterState, parent: ActivatedRoute): string[] {
    const data = [];

    if (parent && parent.snapshot.data) {
      const dataProperty: any = parent.snapshot.data;

      if (dataProperty.title) {
        data.push(dataProperty.title);
      }
    }

    if (state && parent) {
      data.push(... this.getTitleChunks(state, parent.firstChild));
    }

    return data;
  }

  toggleSideNav(): void {
    this.sidenav.toggle(!this.sidenav.opened);
  }

  logout(): void {
    this.store.dispatch({ type: PRE_LOGOUT });
  }

  goToSettings(): void {
    this.router.navigate(['/user']);
  }
}
