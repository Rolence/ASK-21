/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {BankInfo} from '../../../../core/services/accounting/domain/bank-info.model';
import {CreateBankInfoAction} from '../../../store/bankInfo/bank-info.actions';
import * as fromAccounting from '../../../store';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './create.form.component.html'
})
export class BankInfoCreateComponent {

  bankInfo: BankInfo = {
    branchSortCode: '',
    bic: '',
    checkDigitMethod: 'METHOD_06'
  };

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromAccounting.State>) {}

  save(bankInfo: BankInfo): void {
    this.store.dispatch(new CreateBankInfoAction({
      bankInfo,
      activatedRoute: this.route
    }));
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
