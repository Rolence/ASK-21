/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {BankInfo} from '../../../core/services/accounting/domain/bank-info.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FimsValidators} from '../../../core/validator/validators';

@Component({
  selector: 'aten-bank-info-form',
  templateUrl: './bank-info-form.component.html'
})
export class BankInfoFormComponent implements OnChanges {

  form: FormGroup;

  @Input() bankInfo: BankInfo;
  @Input() editMode = false;

  @Output() save = new EventEmitter<BankInfo>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      branchSortCode: ['', [Validators.required, Validators.minLength(3),
        Validators.maxLength(34), FimsValidators.urlSafe]],
      bic: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(11)]],
      checkDigitMethod: ['', [Validators.required]]
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.bankInfo) {
      this.form.reset({
        branchSortCode: this.bankInfo.branchSortCode,
        bic: this.bankInfo.bic,
        checkDigitMethod: { value: this.bankInfo.checkDigitMethod, disabled: true }
      });
    }
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onSave(): void {
    const bankInfo: BankInfo = {
      branchSortCode: this.form.get('branchSortCode').value,
      bic: this.form.get('bic').value,
      checkDigitMethod: this.form.get('checkDigitMethod').value
    };
    this.save.emit(bankInfo);
  }

}
