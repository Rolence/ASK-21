/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromAccounting from '../store';
import {Store} from '@ngrx/store';
import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {SelectAction} from '../store/bankInfo/bank-info.actions';
import {BankInfo} from '../../core/services/accounting/domain/bank-info.model';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  templateUrl: './bank-info.index.component.html',
})
export class BankInfoIndexComponent implements OnDestroy {

  private actionsSubscription: Subscription;
  private bankInfoSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromAccounting.State>, private breadCrumbService: BreadCrumbService) {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction({
        branchSortCode: params['branchSortCode']
      }))
      .subscribe(this.store);

    this.bankInfoSubscription = store.select(fromAccounting.getSelectedBankInfo)
      .filter(bankInfo => !!bankInfo)
      .subscribe((bankInfo: BankInfo) =>
        this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, bankInfo.branchSortCode)
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.bankInfoSubscription.unsubscribe();
  }

}
