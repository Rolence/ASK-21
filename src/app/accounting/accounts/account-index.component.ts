/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {SelectAction} from '../store/account/account.actions';
import {Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';
import * as fromAccounting from '../store';
import {Subscription} from 'rxjs/Subscription';
import {Account} from '../../core/services/accounting/domain/account.model';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';

@Component({
  templateUrl: './account-index.component.html'
})
export class AccountIndexComponent implements OnDestroy {

  private actionsSubscription: Subscription;
  private accountSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromAccounting.State>, private breadCrumbService: BreadCrumbService) {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.accountSubscription = this.store.select(fromAccounting.getSelectedAccount)
      .filter(account => !!account)
      .subscribe((account: Account) => this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, account.identifier));

  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.accountSubscription.unsubscribe();
  }

}
