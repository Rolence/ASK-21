/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {MatDialog} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import * as fromAccounting from '../store';
import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {ValidateComponent} from './validate.component';
import {Transaction} from '../../core/services/accounting/domain/transaction.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './transactions.list.component.html',
})
export class TransactionsListComponent {

  transactions$: Observable<Transaction[]>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromAccounting.State>, private dialog: MatDialog) {
    this.transactions$ = store.select(fromAccounting.selectAllTransactions);
  }

  validate(data: Transaction): void {
    this.dialog.open(ValidateComponent, {
      data,
      width: '50%',
      height: '60%'
    }).afterClosed();
  }

  addJournalEntry(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
}
