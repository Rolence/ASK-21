/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromRoot from '../../../core/store/index';
import {Observable} from 'rxjs/Observable';
import * as fromAccounting from '../../store/index';
import {Store} from '@ngrx/store';
import {CreateTransactionAction} from '../../store/transaction/transaction.actions';
import {Transaction} from '../../../core/services/accounting/domain/transaction.model';
import {todayAsISOString} from '../../../core/services/domain/date.converter';

@Component({
  templateUrl: './create.form.component.html'
})
export class CreateTransactionFormComponent {

  transaction$: Observable<Transaction>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromAccounting.State>) {
    this.transaction$ = this.store.select(fromRoot.getUsername)
      .map(username => ({
        transactionDate: todayAsISOString(),
        transactionType: '',
        clerk: username,
        debtors: [
          { accountNumber: '', amount: '0' }
        ],
        creditors: [
          { accountNumber: '', amount: '0' }
        ]
      }));
  }

  onSave(transaction: Transaction): void {
    this.store.dispatch(new CreateTransactionAction({
      transaction,
      activatedRoute: this.route
    }));
  }

  onCancel() {
    this.navigateAway();
  }

  navigateAway(): void {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
