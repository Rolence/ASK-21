/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TransactionValidators} from './transaction.validator';
import {transactionTypeExists} from './transaction-type-select/validator/transaction-type-exists.validator';
import {Transaction} from '../../../core/services/accounting/domain/transaction.model';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {addCurrentTime, parseDate} from '../../../core/services/domain/date.converter';
import {Creditor} from '../../../core/services/accounting/domain/creditor.model';
import {Debtor} from '../../../core/services/accounting/domain/debtor.model';
import {FimsValidators} from '../../../core/validator/validators';
import {accountExists} from '../../../core/validator/account-exists.validator';

@Component({
  selector: 'aten-transaction-form',
  templateUrl: './form.component.html'
})
export class TransactionFormComponent implements OnChanges {

  @Input() transaction: Transaction;

  @Output() save = new EventEmitter<Transaction>();
  @Output() cancel = new EventEmitter<void>();

  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {
    this.form = this.formBuilder.group({
      transactionType: ['', [Validators.required], transactionTypeExists(this.accountingService)],
      transactionDate: ['', Validators.required],
      note: [''],
      message: [''],
      creditors: this.formBuilder.array([], TransactionValidators.minItems(1)),
      debtors: this.formBuilder.array([], TransactionValidators.minItems(1))
    }, { validator: TransactionValidators.equalSum('creditors', 'debtors') });

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transaction) {
      this.form.reset({
        transactionType: this.transaction.transactionType,
        transactionDate: this.transaction.transactionDate,
        note: this.transaction.note,
        message: this.transaction.message
      });

      this.transaction.debtors.forEach(debtor => this.addDebtor(debtor));
      this.transaction.creditors.forEach(creditor => this.addCreditor(creditor));
    }
  }

  onSave(): void {
    const date: Date = parseDate(this.form.get('transactionDate').value);
    const dateWithTime = addCurrentTime(date);

    const transaction: Transaction = {
      transactionType: this.form.get('transactionType').value,
      transactionDate: dateWithTime.toISOString(),
      clerk: this.transaction.clerk,
      note: this.form.get('note').value,
      message: this.form.get('message').value,
      creditors: this.form.get('creditors').value,
      debtors: this.form.get('debtors').value,
      state: 'INITIALIZED'
    };

    this.save.emit(transaction);
  }

  addCreditor(creditor?: Creditor): void {
    const control: FormArray = this.form.get('creditors') as FormArray;
    control.push(this.initCreditor(creditor));
  }

  removeCreditor(index: number): void {
    const control: FormArray = this.form.get('creditors') as FormArray;
    control.removeAt(index);
  }

  addDebtor(debtor?: Debtor): void {
    const control: FormArray = this.form.get('debtors') as FormArray;
    control.push(this.initDebtor(debtor));
  }

  removeDebtor(index: number): void {
    const control: FormArray = this.form.get('debtors') as FormArray;
    control.removeAt(index);
  }

  onCancel() {
    this.cancel.emit();
  }

  get debtors(): AbstractControl[] {
    const debtors: FormArray = this.form.get('debtors') as FormArray;
    return debtors.controls;
  }

  get creditors(): AbstractControl[] {
    const creditors: FormArray = this.form.get('creditors') as FormArray;
    return creditors.controls;
  }

  private initCreditor(creditor: Creditor = { accountNumber: '', amount: '0' }): FormGroup {
    return this.formBuilder.group({
      accountNumber: [creditor.accountNumber, [Validators.required], accountExists(this.accountingService)],
      amount: [creditor.amount, [Validators.required, FimsValidators.greaterThanValue(0)]]
    });
  }

  private initDebtor(debtor: Debtor = { accountNumber: '', amount: '0' }): FormGroup {
    return this.formBuilder.group({
      accountNumber: [debtor.accountNumber, [Validators.required], accountExists(this.accountingService)],
      amount: [debtor.amount, [Validators.required, FimsValidators.greaterThanValue(0)]]
    });
  }

}
