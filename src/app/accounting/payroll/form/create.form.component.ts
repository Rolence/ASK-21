/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CREATE} from '../../store/payroll/payroll-collection.actions';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromAccounting from '../../store';
import {PayrollPayment} from '../../../core/services/payroll/domain/payroll-payment.model';
import {PayrollService} from '../../../core/services/payroll/payroll.service';
import {PayrollCollectionSheet} from '../../../core/services/payroll/domain/payroll-collection-sheet.model';

@Component({
  templateUrl: './create.form.component.html'
})
export class CreatePayrollFormComponent implements OnInit {

  allpayrollPayments$: Observable<PayrollPayment[]>;

  constructor(private store: Store<fromAccounting.State>, private router: Router, private route: ActivatedRoute,
              private payrollService: PayrollService) {}

  ngOnInit(): void {
    this.allpayrollPayments$ = this.payrollService.fetchPayrollAllocations();
  }

  onSave(sheet: PayrollCollectionSheet): void {
    this.store.dispatch({ type: CREATE, payload: {
      sheet,
      activatedRoute: this.route
    }});
  }

  onCancel(): void {
    this.navigateAway();
  }

  navigateAway(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
