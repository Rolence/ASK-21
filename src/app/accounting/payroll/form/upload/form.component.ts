/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ITdDataTableColumn} from '@covalent/core';
import {ProcessData} from '../../../../core/services/payroll/domain/process-data.model';
import {FetchRequest} from '../../../../core/services/domain/paging/fetch-request.model';
import {DisplayFimsNumber} from '../../../../shared/number/fims-number.pipe';
import {PayrollPaymentPage} from '../../../../core/services/payroll/domain/payroll-payment-page.model';
import {FimsValidators} from '../../../../core/validator/validators';
import {AccountingValidationService} from '../../../../core/validator/services/accounting-validation.service';
import {Page} from '../../../../core/services/domain/paging/page.model';

export interface FetchEvent {
  identifier: string;
  fetchRequest: FetchRequest;
}

export interface CreateUploadEvent {
  identifier: string;
  processData: ProcessData;
}

@Component({
  selector: 'aten-upload-payroll-form',
  templateUrl: './form.component.html',
  providers: [DisplayFimsNumber]
})

export class UploadFormComponent {

  fileForm: FormGroup;
  form: FormGroup;

  columns: ITdDataTableColumn[] = [
    { name: 'customerIdentifier', label: 'Customer identifier' },
    { name: 'employer', label: 'Employer' },
    { name: 'salary', label: 'Salary', format: (value => this.displayFimsNumber.transform(value)) }
  ];

  @Input() identifier: string;
  @Input() payments: PayrollPaymentPage;

  @Output() save = new EventEmitter<CreateUploadEvent>();
  @Output() cancel = new EventEmitter<void>();
  @Output() fetch = new EventEmitter<FetchEvent>();
  @Output() upload = new EventEmitter<File>();

  constructor(private formBuilder: FormBuilder, private validationService: AccountingValidationService,
              private displayFimsNumber: DisplayFimsNumber) {
    this.fileForm = this.formBuilder.group({
      file: ['', [
        Validators.required,
        FimsValidators.mediaType(['text/csv', 'application/vnd.ms-excel'])
      ]]
    });

    this.form = this.formBuilder.group({
      sourceAccountNumber: ['', [Validators.required], validationService.accountExists()]
    });
  }

  onSave(): void {
    const processData: ProcessData = {
      sourceAccount: this.form.get('sourceAccountNumber').value
    };

    this.save.emit({
      identifier: this.identifier,
      processData
    });
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onUpload(): void {
    this.upload.emit(this.fileForm.get('file').value);
  }

  onFetch(page: Page): void {
    this.fetch.emit({
      identifier: this.identifier,
      fetchRequest: {
        page
      }
    });
  }

}
