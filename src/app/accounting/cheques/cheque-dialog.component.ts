/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {MatDialogRef} from '@angular/material';
import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountingService} from '../../core/services/accounting/accounting.service';
import {accountExists} from '../../core/validator/account-exists.validator';

@Component({
  templateUrl: './cheque-dialog.component.html'
})
export class ChequeDialogComponent {

  formGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<ChequeDialogComponent>, private formBuilder: FormBuilder,
              private accountingService: AccountingService) {
    this.formGroup = this.formBuilder.group({
      bankAccount: ['', [Validators.required], accountExists(this.accountingService)]
    });
  }

  approve(): void {
    this.dialogRef.close(this.formGroup.get('bankAccount').value);
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
