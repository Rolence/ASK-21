/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromAccounting from '../../store';
import {CreateTemplateFailAction, CreateTemplateSuccessAction} from '../../store/cheques/cheque-template.actions';
import {ProcessChequeEvent} from './form.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {ChequeTemplate} from '../../../core/services/cheque/domain/cheque-template.model';
import {Currency} from '../../../core/services/currency/domain/currency.model';
import {CurrencyService} from '../../../core/services/currency/currency.service';
import {ChequeService} from '../../../core/services/cheque/cheque.service';
import {NotificationService} from '../../../core/services/notification/notification.service';
import {PrintChequeInfo} from '../../../core/services/cheque/domain/print-cheque-info.model';

@Component({
  templateUrl: './create.component.html'
})
export class CreateChequeComponent {

  chequeTemplates$: Observable<ChequeTemplate[]>;
  currencies$: Observable<Currency[]>;
  chequeProcessed$: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute,
              private currencyService: CurrencyService, private store: Store<fromAccounting.State>,
              private chequeService: ChequeService, private notificationService: NotificationService) {
    this.chequeTemplates$ = store.select(fromAccounting.getChequeTemplateEntities);
    this.currencies$ = this.currencyService.fetchCurrencies();
  }

  onProcessCheque(event: ProcessChequeEvent): void {
    this.chequeProcessed$ = this.chequeService.postPayableCheque(event.chequePayable)
      .catch(() => {
        this.notificationService.sendAlert('There was an issue processing the cheque');
        return Observable.of(false);
      })
      .mergeMap(() => {
        if (event.chequeTemplate) {
          return this.chequeService.createTemplate(event.chequeTemplate)
            .do(() => {
              this.sendSuccess();
              this.store.dispatch(new CreateTemplateSuccessAction(event.chequeTemplate));
            })
            .map(() => true)
            .catch(() => {
              this.notificationService.sendAlert('There was an issue saving the template');
              this.store.dispatch(new CreateTemplateFailAction());
              return Observable.of(false);
            });
        } else {
          this.sendSuccess();
          return Observable.of(true);
        }
      });
  }

  private sendSuccess(): void {
    this.notificationService.sendMessage('Cheque successfully processed');
  }

  onPrintCheque(chequeInfo: PrintChequeInfo): void {
    this.chequeService.printCheque(chequeInfo).subscribe();
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
