/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromAccount from '../store';
import * as fromAccounting from '../store';
import {ActivatedRoute, Router} from '@angular/router';
import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DeleteTemplateAction} from '../store/cheques/cheque-template.actions';
import {TranslateService} from '@ngx-translate/core';
import {TdDialogService} from '@covalent/core';
import {Store} from '@ngrx/store';
import {ChequeTemplate} from '../../core/services/cheque/domain/cheque-template.model';

@Component({
  templateUrl: './cheque-templates.component.html'
})
export class ChequeTemplatesComponent {

  templates$: Observable<ChequeTemplate[]>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromAccounting.State>, private translate: TranslateService, private dialogService: TdDialogService) {
    this.templates$ = store.select(fromAccount.getChequeTemplateEntities);
  }

  confirmDeletion(): Observable<boolean> {
    const message = 'Do you want to delete this template?';
    const title = 'Confirm deletion';
    const button = 'DELETE TEMPLATE';

    return this.translate.get([title, message, button])
      .flatMap(result =>
        this.dialogService.openConfirm({
          message: result[message],
          title: result[title],
          acceptButton: result[button]
        }).afterClosed()
      );
  }

  deleteTemplate(name: string): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => {
        this.store.dispatch(new DeleteTemplateAction(name));
      });
  }

  addCheque(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
