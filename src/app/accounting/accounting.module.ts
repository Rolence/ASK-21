/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {AccountingRoutingModule} from './accounting-routing.module';
import {NgModule} from '@angular/core';
import {GeneralLedgerComponent} from './general-ledger.component';
import {SubLedgerDetailComponent} from './subLedger/sub-ledger.detail.component';
import {AccountDetailComponent} from './accounts/account.detail.component';
import {AccountStatusComponent} from './status/status.component';
import {AccountActivityComponent} from './activity/activity.component';
import {CommandsResolver} from './activity/commands.resolver';
import {AccountFormComponent} from './accounts/form/form.component';
import {CreateAccountFormComponent} from './accounts/form/create/create.form.component';
import {SubLedgerComponent} from './subLedger/sub-ledger.component';
import {EditAccountFormComponent} from './accounts/form/edit/edit.form.component';
import {JournalEntryListComponent} from './journalEntries/journal-entry.list.component';
import {AccountEntryListComponent} from './accounts/entries/account-entry.list.component';
import {LedgerFormComponent} from './form/form.component';
import {EditLedgerFormComponent} from './form/edit/edit.form.component';
import {CreateLedgerFormComponent} from './form/create/create.form.component';
import {LedgerExistsGuard} from './ledger-exists.guard';
import {AccountExistsGuard} from './accounts/account-exists.guard';
import {StoreModule} from '@ngrx/store';
import {AccountCommandNotificationEffects} from './store/account/task/effects/notification.effects';
import {EffectsModule} from '@ngrx/effects';
import {AccountCommandApiEffects} from './store/account/task/effects/service.effects';
import {AccountNotificationEffects} from './store/account/effects/notification.effects';
import {AccountEntryApiEffects} from './store/account/entries/effects/service.effect';
import {AccountRouteEffects} from './store/account/effects/route.effects';
import {AccountApiEffects} from './store/account/effects/service.effects';
import {JournalEntryApiEffects} from './store/ledger/journal-entry/effects/service.effects';
import {LedgerNotificationEffects} from './store/ledger/effects/notification.effects';
import {LedgerRouteEffects} from './store/ledger/effects/route.effects';
import {LedgerApiEffects} from './store/ledger/effects/service.effects';
import {AccountCommandRouteEffects} from './store/account/task/effects/route.effects';
import {ChartOfAccountComponent} from './reporting/chartOfAccounts/chart-of-accounts.component';
import {ChartOfAccountTableComponent} from './reporting/chartOfAccounts/chart-of-account-table.component';
import {SubLedgerListComponent} from './subLedger/sub-ledger.list.component';
import {TransactionTypeListComponent} from './transactionTypes/transaction-types.list.component';
import {TransactionTypeApiEffects} from './store/ledger/transaction-type/effects/service.effects';
import {TransactionTypeRouteEffects} from './store/ledger/transaction-type/effects/route.effects';
import {TransactionTypeNotificationEffects} from './store/ledger/transaction-type/effects/notification.effects';
import {TransactionTypeFormComponent} from './transactionTypes/form/transaction-type-form.component';
import {CreateTransactionTypeFormComponent} from './transactionTypes/form/create/create.form.component';
import {EditTransactionTypeFormComponent} from './transactionTypes/form/edit/edit.form.component';
import {TransactionTypeExistsGuard} from './transactionTypes/transaction-type-exists.guard';
import {TransactionTypeSelectComponent} from './transactions/form/transaction-type-select/transaction-type-select.component';
import {ChequeApiEffects} from './store/cheques/effects/service.effects';
import {ChequesListComponent} from './cheques/cheques.list.component';
import {PayrollCollectionApiEffects} from './store/payroll/effects/service.effects';
import {PayrollListComponent} from './payroll/payroll.list.component';
import {CreatePayrollFormComponent} from './payroll/form/create.form.component';
import {PayrollFormComponent} from './payroll/form/form.component';
import {PayrollCollectionRouteEffects} from './store/payroll/effects/route.effects';
import {PayrollCollectionNotificationEffects} from './store/payroll/effects/notification.effects';
import {PaymentsListComponent} from './payroll/payments.list.component';
import {ChequeDialogComponent} from './cheques/cheque-dialog.component';
import {UploadPayrollFormComponent} from './payroll/form/upload/upload.form.component';
import {UploadFormComponent} from './payroll/form/upload/form.component';
import {ChequeTemplatesComponent} from './accounts-payable/cheque-templates.component';
import {AccountsPayableComponent} from './accounts-payable/accounts-payable.component';
import {CreateChequeComponent} from './accounts-payable/form/create.component';
import {ChequeFormComponent} from './accounts-payable/form/form.component';
import {PayrollIndexComponent} from './payroll/payroll.index.component';
import {reducerProvider, reducerToken} from './store';
import {TransactionApiEffects} from './store/transaction/effects/service.effects';
import {TransactionRouteEffects} from './store/transaction/effects/route.effects';
import {TransactionNotificationEffects} from './store/transaction/effects/notification.effects';
import {CreateTransactionFormComponent} from './transactions/form/create.form.component';
import {TransactionFormComponent} from './transactions/form/form.component';
import {TransactionsListComponent} from './transactions/transactions.list.component';
import {TransactionsIndexComponent} from './transactions/transactions.index.component';
import {ValidateComponent} from './transactions/validate.component';
import {TransactionDetailComponent} from './components/transaction-detail.component';
import {SharedModule} from '../shared/shared.module';
import {AccountingValidationService} from '../core/validator/services/accounting-validation.service';
import {OfficeValidationService} from '../core/validator/services/office-validation.service';
import {AccountIndexComponent} from './accounts/account-index.component';
import {BankInformationApiEffects} from './store/bankInfo/effects/service.effects';
import {BankInformationRouteEffects} from './store/bankInfo/effects/route.effects';
import {BankInformationNotificationEffects} from './store/bankInfo/effects/notification.effects';
import {BankInfosIndexComponent} from './bankInfos/bank-infos-index.component';
import {BankInfoListComponent} from './bankInfos/bank-info.list.component';
import {BankInfoFormComponent} from './bankInfos/form/bank-info-form.component';
import {BankInfoCreateComponent} from './bankInfos/form/create/create.form.component';
import {BankInfoEditComponent} from './bankInfos/form/edit/edit.form.component';
import {BankInfoExistsGuard} from './bankInfos/bank-info-exists.guard';
import {BankInfoIndexComponent} from './bankInfos/bank-info.index.component';
import {BankInfoDetailComponent} from './bankInfos/bank-info.detail.component';

@NgModule({
  imports: [
    AccountingRoutingModule,
    SharedModule,

    StoreModule.forFeature('accounting', reducerToken),

    EffectsModule.forFeature([
      LedgerApiEffects,
      LedgerRouteEffects,
      LedgerNotificationEffects,

      JournalEntryApiEffects,

      TransactionApiEffects,
      TransactionRouteEffects,
      TransactionNotificationEffects,

      TransactionTypeApiEffects,
      TransactionTypeRouteEffects,
      TransactionTypeNotificationEffects,

      AccountApiEffects,
      AccountRouteEffects,
      AccountNotificationEffects,
      AccountEntryApiEffects,
      AccountCommandApiEffects,
      AccountCommandRouteEffects,
      AccountCommandNotificationEffects,

      ChequeApiEffects,

      PayrollCollectionApiEffects,
      PayrollCollectionRouteEffects,
      PayrollCollectionNotificationEffects,

      BankInformationApiEffects,
      BankInformationRouteEffects,
      BankInformationNotificationEffects
    ])
  ],
  declarations: [
    GeneralLedgerComponent,
    SubLedgerComponent,
    SubLedgerListComponent,
    SubLedgerDetailComponent,
    LedgerFormComponent,
    CreateLedgerFormComponent,
    EditLedgerFormComponent,
    ChartOfAccountComponent,
    ChartOfAccountTableComponent,
    AccountIndexComponent,
    AccountEntryListComponent,
    AccountDetailComponent,
    AccountStatusComponent,
    AccountActivityComponent,
    AccountFormComponent,
    CreateAccountFormComponent,
    EditAccountFormComponent,
    JournalEntryListComponent,
    TransactionsListComponent,
    TransactionsIndexComponent,
    CreateTransactionFormComponent,
    TransactionFormComponent,
    TransactionsListComponent,
    TransactionDetailComponent,
    ValidateComponent,
    TransactionTypeListComponent,
    TransactionTypeFormComponent,
    CreateTransactionTypeFormComponent,
    EditTransactionTypeFormComponent,
    TransactionTypeSelectComponent,
    ChequesListComponent,
    ChequeDialogComponent,
    AccountsPayableComponent,
    ChequeTemplatesComponent,
    CreateChequeComponent,
    ChequeFormComponent,
    PayrollIndexComponent,
    PayrollListComponent,
    CreatePayrollFormComponent,
    PayrollFormComponent,
    PaymentsListComponent,
    UploadPayrollFormComponent,
    UploadFormComponent,
    BankInfosIndexComponent,
    BankInfoListComponent,
    BankInfoFormComponent,
    BankInfoCreateComponent,
    BankInfoEditComponent,
    BankInfoIndexComponent,
    BankInfoDetailComponent
  ],
  providers: [
    CommandsResolver,
    LedgerExistsGuard,
    AccountExistsGuard,
    TransactionTypeExistsGuard,
    BankInfoExistsGuard,
    AccountingValidationService,
    OfficeValidationService,
    reducerProvider
  ],
  entryComponents: [
    ChequeDialogComponent,
    ValidateComponent
  ]
})
export class AccountingModule {}
