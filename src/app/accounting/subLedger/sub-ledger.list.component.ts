/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromAccounting from '../store';
import {Observable} from 'rxjs/Observable';
import {DELETE} from '../store/ledger/ledger.actions';
import {Store} from '@ngrx/store';
import {TableData} from '../../shared/data-table/data-table.component';
import {Ledger} from '../../core/services/accounting/domain/ledger.model';
import {DialogService} from '../../core/services/dialog/dialog.service';
import {DisplayFimsNumber} from '../../shared/number/fims-number.pipe';

@Component({
  templateUrl: './sub-ledger.list.component.html',
  providers: [DisplayFimsNumber]
})
export class SubLedgerListComponent implements OnInit, OnDestroy {

  private selectionSubscription: Subscription;

  private _ledger: Ledger;

  ledgerData: TableData = {
    totalElements: 0,
    totalPages: 1,
    data: []
  };

  columns: any[] = [
    { name: 'identifier', label: 'Id' },
    { name: 'name', label: 'Name' },
    { name: 'description', label: 'Description' },
    { name: 'totalValue', label: 'Balance', format: value => value ? this.displayFimsNumber.transform(value) : '-' }
  ];

  constructor(private route: ActivatedRoute, private router: Router,
              private store: Store<fromAccounting.State>, private  dialogService: DialogService,
              private displayFimsNumber: DisplayFimsNumber) {}

  ngOnInit(): void {
    this.selectionSubscription = this.store.select(fromAccounting.getSelectedLedger)
      .filter(ledger => !!ledger)
      .subscribe(ledger => this.ledger = ledger);
  }

  ngOnDestroy(): void {
    this.selectionSubscription.unsubscribe();
  }

  rowSelect(ledger: Ledger): void {
    this.router.navigate(['/accounting/ledgers/detail', ledger.identifier]);
  }

  set ledger(ledger: Ledger) {
    this._ledger = ledger;

    if (ledger.subLedgers) {
      this.ledgerData.data = ledger.subLedgers;
      this.ledgerData.totalElements = ledger.subLedgers.length;
    }
  }

  get ledger(): Ledger {
    return this._ledger;
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this ledger?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE LEDGER'
    });
  }

  deleteLedger(): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => {
        this.store.dispatch({ type: DELETE, payload: {
          ledger: this.ledger,
          activatedRoute: this.route
        }});
      });
  }

  addLedger(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

  editLedger(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

}
