/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {of} from 'rxjs/observable/of';
import {
  ApproveTransactionAction,
  ApproveTransactionFailAction,
  ApproveTransactionSuccessAction,
  CreateTransactionAction,
  CreateTransactionFailAction,
  CreateTransactionSuccessAction,
  DeclineTransactionAction,
  DeclineTransactionFailAction,
  DeclineTransactionSuccessAction,
  LoadAllAction,
  LoadAllCompleteAction,
  TransactionActionTypes
} from '../transaction.actions';
import {AccountingService} from '../../../../core/services/accounting/accounting.service';

@Injectable()
export class TransactionApiEffects {

  @Effect()
  loadAll$: Observable<Action> = this.actions$
    .ofType(TransactionActionTypes.LOAD_ALL)
    .map((action: LoadAllAction) => action.payload)
    .switchMap(payload => this.accountingService.fetchTransactions(payload.state)
      .map(transactions => new LoadAllCompleteAction({
        transactions
      }))
      .catch(() => of(new LoadAllCompleteAction({
        transactions: []
      })))
    );

  @Effect()
  createTransaction$: Observable<Action> = this.actions$
    .ofType(TransactionActionTypes.CREATE)
    .map((action: CreateTransactionAction) => action.payload)
    .mergeMap(payload => this.accountingService.createTransaction(payload.transaction)
      .map(identifier => new CreateTransactionSuccessAction({
        transaction: {
          identifier,
          ...payload.transaction
        },
        activatedRoute: payload.activatedRoute
      }))
      .catch(error => of(new CreateTransactionFailAction({
        error
      })))
    );

  @Effect()
  approveTransaction: Observable<Action> = this.actions$
    .ofType(TransactionActionTypes.APPROVE)
    .map((action: ApproveTransactionAction) => action.payload)
    .mergeMap(payload =>
      this.accountingService.processTransaction(payload.identifier, {
        action: 'APPROVE'
      })
      .map(journalEntry => new ApproveTransactionSuccessAction({
        identifier: payload.identifier,
        journalEntry
      }))
      .catch(error => of(new ApproveTransactionFailAction({
        error
      })))
    );

  @Effect()
  declineTransaction: Observable<Action> = this.actions$
    .ofType(TransactionActionTypes.DECLINE)
    .map((action: DeclineTransactionAction) => action.payload)
    .mergeMap(payload =>
      this.accountingService.processTransaction(payload.identifier, {
        action: 'DECLINE'
      })
        .map(() => new DeclineTransactionSuccessAction({
          identifier: payload.identifier
        }))
        .catch(error => of(new DeclineTransactionFailAction({
          error
        })))
    );

  constructor(private actions$: Actions, private accountingService: AccountingService) { }

}
