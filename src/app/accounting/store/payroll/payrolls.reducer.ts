/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as payrollActions from './payroll-collection.actions';
import {ResourceState} from '../../../core/store/util/resource.reducer';
import {PayrollCollectionHistory} from '../../../core/services/payroll/domain/payroll-collection-history.model';
import {idsToHashWithCurrentTimestamp, resourcesToHash} from '../../../core/store/util/reducer.helper';

export const initialState: ResourceState = {
  ids: [],
  entities: {},
  loadedAt: {},
  selectedId: null,
};

export function reducer(state = initialState, action: payrollActions.Actions): ResourceState {

  switch (action.type) {

    case payrollActions.LOAD_ALL_COLLECTIONS: {
      return initialState;
    }

    case payrollActions.LOAD_ALL_COLLECTIONS_COMPLETE: {
      const payrolls: PayrollCollectionHistory[] = action.payload;

      const ids = payrolls.map(payroll => payroll.identifier);

      const entities = resourcesToHash(payrolls);

      const loadedAt = idsToHashWithCurrentTimestamp(ids);

      return {
        ids: [ ...ids ],
        entities: entities,
        loadedAt: loadedAt,
        selectedId: state.selectedId
      };
    }

    default: {
      return state;
    }
  }
}
