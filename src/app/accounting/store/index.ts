/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromRoot from '../../core/store';
import * as fromLedgers from './ledger/ledgers.reducer';
import * as fromLedgerForm from './ledger/form.reducer';
import * as fromChartOfAccounts from './ledger/chart-of-account.reducer';
import * as fromJournalEntrySearch from './ledger/journal-entry/search.reducer';
import * as fromTransactions from './transaction/transactions.reducer';
import * as fromAccounts from './account/accounts.reducer';
import * as fromAccountEntrySearch from './account/entries/search.reducer';
import * as fromCheques from './cheques/cheques.reducer';
import * as fromChequeTemplates from './cheques/cheque-template.reducer';
import {getEntities} from './cheques/cheque-template.reducer';
import * as fromPayrolls from './payroll/payrolls.reducer';
import * as fromBankInfos from './bankInfo/bank-infos.reducer';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createSelector} from 'reselect';
import {
  createResourceReducer,
  getResourceAll,
  getResourceLoadedAt,
  getResourceSelected,
  ResourceState
} from '../../core/store/util/resource.reducer';
import {createFormReducer, FormState, getFormError} from '../../core/store/util/form.reducer';
import {
  createSearchReducer,
  getSearchEntities,
  getSearchLoading,
  getSearchTotalElements,
  getSearchTotalPages,
  SearchState
} from '../../core/store/util/search.reducer';
import {InjectionToken} from '@angular/core';
import {EntityState} from '@ngrx/entity';
import {Transaction} from '../../core/services/accounting/domain/transaction.model';

export interface AccountingState {
  accounts: ResourceState;
  accountForm: FormState;
  accountEntrySearch: fromAccountEntrySearch.State;

  ledgers: fromLedgers.State;
  ledgerForm: FormState;
  chartOfAccounts: fromChartOfAccounts.State;
  journalEntrySearch: fromJournalEntrySearch.State;

  transactions: EntityState<Transaction>;

  transactionTypes: ResourceState;
  transactionTypeSearch: SearchState;
  transactionForm: FormState;

  cheques: ResourceState;
  chequeTemplates: fromChequeTemplates.State;

  payrollCollections: ResourceState;
  payrollPayments: SearchState;

  bankInfos: fromBankInfos.State;
}

export interface State extends fromRoot.State {
  accounting: AccountingState;
}

export const reducers: ActionReducerMap<AccountingState> = {
  ledgers: fromLedgers.reducer,
  ledgerForm: createFormReducer('Ledger', fromLedgerForm.reducer),
  chartOfAccounts: fromChartOfAccounts.reducer,

  journalEntrySearch: fromJournalEntrySearch.reducer,

  transactions: fromTransactions.reducer,

  transactionTypes: createResourceReducer('Transaction Type', undefined, 'code'),
  transactionTypeSearch: createSearchReducer('Transaction Type'),
  transactionForm: createFormReducer('Transaction Type'),

  accounts: createResourceReducer('Account', fromAccounts.reducer),
  accountForm: createFormReducer('Account'),
  accountEntrySearch: fromAccountEntrySearch.reducer,

  cheques: createResourceReducer('Cheque', fromCheques.reducer),
  chequeTemplates: fromChequeTemplates.reducer,

  payrollCollections: createResourceReducer('Payroll Collection', fromPayrolls.reducer),
  payrollPayments: createSearchReducer('Payroll Payment'),

  bankInfos: fromBankInfos.reducer
};

export const reducerToken = new InjectionToken<ActionReducerMap<AccountingState>>('Accounting reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export const selectAccountingState = createFeatureSelector<AccountingState>('accounting');

/**
 * Ledger Selectors
 */
export const getLedgerState = createSelector(selectAccountingState, (state: AccountingState) => state.ledgers);

export const getLedgerFormState = createSelector(selectAccountingState, (state: AccountingState) => state.ledgerForm);
export const getLedgerFormError = createSelector(getLedgerFormState, getFormError);

export const getChartOfAccountsState = createSelector(selectAccountingState, (state: AccountingState) => state.chartOfAccounts);

export const getLedgerEntities = createSelector(getLedgerState, fromLedgers.getEntities);
export const getLedgersLoadedAt = createSelector(getLedgerState, fromLedgers.getLoadedAt);
export const getLedgerTopLevelIds = createSelector(getLedgerState, fromLedgers.getTopLevelIds);
export const getSelectedLedger = createSelector(getLedgerState, fromLedgers.getSelected);

export const getAllTopLevelLedgerEntities = createSelector(getLedgerTopLevelIds, getLedgerEntities, (topLevelIds, entities) => {
  return topLevelIds.map(id => entities[id]);
});

export const getChartOfAccountEntries = createSelector(getChartOfAccountsState, fromChartOfAccounts.getChartOfAccountEntries);
export const getChartOfAccountLoading = createSelector(getChartOfAccountsState, fromChartOfAccounts.getLoading);

/**
 * Journal Entries Selectors
 */
export const getJournalEntrySearchState = createSelector(selectAccountingState, (state: AccountingState) => state.journalEntrySearch);
export const getJournalEntryEntities = createSelector(getJournalEntrySearchState, fromJournalEntrySearch.getEntities);
export const getSearchJournalEntryIds = createSelector(getJournalEntrySearchState, fromJournalEntrySearch.getIds);

export const getJournalEntriesSearchResult = createSelector(getJournalEntryEntities, getSearchJournalEntryIds, (entities, ids) => {
  return ids.map(id => entities[id]);
});

/**
 * Transactions
 */

export const selectTransactionState = createSelector(selectAccountingState, (state: AccountingState) => state.transactions);
export const selectAllTransactions = createSelector(selectTransactionState, fromTransactions.selectAllTransactions);

/**
 * Accounts
 */
export const getAccountsState = createSelector(selectAccountingState, (state: AccountingState) => state.accounts);

export const getAccountFormState = createSelector(selectAccountingState, (state: AccountingState) => state.accountForm);
export const getAccountFormError = createSelector(getAccountFormState, getFormError);

export const getAccountsLoadedAt = createSelector(getAccountsState, getResourceLoadedAt);
export const getSelectedAccount = createSelector(getAccountsState, getResourceSelected);

export const getAccountEntrySearchState = createSelector(selectAccountingState, (state: AccountingState) => state.accountEntrySearch);
export const getAccountEntrySearchEntities = createSelector(getAccountEntrySearchState, fromAccountEntrySearch.getEntries);
export const getAccountEntrySearchTotalElements = createSelector(getAccountEntrySearchState, fromAccountEntrySearch.getTotalElements);
export const getAccountEntrySearchTotalPages = createSelector(getAccountEntrySearchState, fromAccountEntrySearch.getTotalPages);

export const getAccountEntrySearchResults = createSelector(
  getAccountEntrySearchEntities, getAccountEntrySearchTotalElements, getAccountEntrySearchTotalPages,
  (entities, totalElements, totalPages) => {
    return {
      entries: entities,
      totalPages: totalPages,
      totalElements: totalElements
    };
  });

/**
 * Transaction Types
 */
export const getTransactionTypesState = createSelector(selectAccountingState, (state: AccountingState) => state.transactionTypes);

export const getTransactionTypeLoadedAt = createSelector(getTransactionTypesState, getResourceLoadedAt);
export const getSelectedTransactionType = createSelector(getTransactionTypesState, getResourceSelected);

export const getTransactionTypeSearchState = createSelector(selectAccountingState, (state: AccountingState) => state.transactionTypeSearch);

export const getTransactionTypeFormState = createSelector(selectAccountingState, (state: AccountingState) => state.transactionForm);
export const getTransactionTypeFormError = createSelector(getTransactionTypeFormState, getFormError);


export const getSearchTransactionTypes = createSelector(getTransactionTypeSearchState, getSearchEntities);
export const getTransactionTypeSearchTotalElements = createSelector(getTransactionTypeSearchState, getSearchTotalElements);
export const getTransactionTypeSearchTotalPages = createSelector(getTransactionTypeSearchState, getSearchTotalPages);
export const getTransactionTypeSearchLoading = createSelector(getTransactionTypeSearchState, getSearchLoading);

export const getTransactionTypeSearchResults = createSelector(
  getSearchTransactionTypes, getTransactionTypeSearchTotalPages, getTransactionTypeSearchTotalElements,
  (transactionTypes, totalPages, totalElements) => {
    return {
      transactionTypes,
      totalPages,
      totalElements
    };
  });

/**
 * Cheques
 */
export const getChequesState = createSelector(selectAccountingState, (state: AccountingState) => state.cheques);

export const getChequeLoadedAt = createSelector(getChequesState, getResourceLoadedAt);
export const getSelectedCheque = createSelector(getChequesState, getResourceSelected);

export const getAllChequeEntities = createSelector(getChequesState, getResourceAll);

export const getChequeTemplatesState = createSelector(selectAccountingState, (state: AccountingState) => state.chequeTemplates);

export const getChequeTemplateEntities = createSelector(getChequeTemplatesState, getEntities);

/**
 * Payroll collections
 */
export const getPayrollCollectionsState = createSelector(selectAccountingState, (state: AccountingState) => state.payrollCollections);

export const getPayrollCollectionLoadedAt = createSelector(getPayrollCollectionsState, getResourceLoadedAt);
export const getSelectedPayrollCollection = createSelector(getPayrollCollectionsState, getResourceSelected);

export const getAllPayrollCollectionEntities = createSelector(getPayrollCollectionsState, getResourceAll);

export const getPayrollPaymentSearchState = createSelector(selectAccountingState, (state: AccountingState) => state.payrollPayments);

export const getSearchPayrollPayments = createSelector(getPayrollPaymentSearchState, getSearchEntities);
export const getPayrollPaymentsSearchTotalElements = createSelector(getPayrollPaymentSearchState, getSearchTotalElements);
export const getPayrollPaymentsSearchTotalPages = createSelector(getPayrollPaymentSearchState, getSearchTotalPages);

export const getPayrollPaymentSearchResults = createSelector(
  getSearchPayrollPayments, getPayrollPaymentsSearchTotalElements, getPayrollPaymentsSearchTotalPages,
  (data, totalElements, totalPages) => {
    return {
      data,
      totalPages,
      totalElements
    };
  });

/**
 * Bank infos
 */
export const selectBankInfoState = createSelector(selectAccountingState, (state: AccountingState) => state.bankInfos);
export const selectAllBankInfos = createSelector(selectBankInfoState, fromBankInfos.selectAllBankInfos);

export const getSelectedBankInfo = createSelector(selectBankInfoState, fromBankInfos.getSelectedBankInfo);
export const getBankInfoLoadedAt = createSelector(selectBankInfoState, fromBankInfos.getLoadedAt);
