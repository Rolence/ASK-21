/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {Error} from '../../../core/services/domain/error.model';
import {type} from '../../../core/store/util';
import {ChequeTemplate} from '../../../core/services/cheque/domain/cheque-template.model';

export const LOAD_ALL = type(`[Cheque Template] Load All`);
export const LOAD_ALL_COMPLETE = type(`[Cheque Template] Load All Complete`);

export const CREATE = type('[Cheque Template] Create');
export const CREATE_SUCCESS = type('[Cheque Template] Create Success');
export const CREATE_FAIL = type('[Cheque Template] Create Fail');

export const DELETE = type('[Cheque Template] Delete');
export const DELETE_SUCCESS = type('[Cheque Template] Delete Success');
export const DELETE_FAIL = type('[Cheque Template] Delete Fail');

export class LoadAllAction implements Action {
  readonly type = LOAD_ALL;

  constructor(public payload: any = null) {}
}

export class LoadAllCompleteAction implements Action {
  readonly type = LOAD_ALL_COMPLETE;

  constructor(public payload: ChequeTemplate[]) { }
}

export class CreateTemplateAction implements Action {
  readonly type = CREATE;

  constructor(public payload: ChequeTemplate) {}
}

export class CreateTemplateSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: ChequeTemplate) {}
}

export class CreateTemplateFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: any = null) {}
}

export class DeleteTemplateAction implements Action {
  readonly type = DELETE;

  constructor(public payload: string) {}
}

export class DeleteTemplateSuccessAction implements Action {
  readonly type = DELETE_SUCCESS;

  constructor(public payload: string) {}
}

export class DeleteTemplateFailAction implements Action {
  readonly type = DELETE_FAIL;

  constructor(public payload: Error) {}
}

export type Actions
  = LoadAllAction
  | LoadAllCompleteAction
  | CreateTemplateAction
  | CreateTemplateSuccessAction
  | CreateTemplateFailAction
  | DeleteTemplateAction
  | DeleteTemplateSuccessAction
  | DeleteTemplateFailAction;
