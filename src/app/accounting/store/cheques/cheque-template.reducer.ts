/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as chequeTemplate from './cheque-template.actions';
import * as cheque from './cheque.actions';
import {ChequeTemplate} from '../../../core/services/cheque/domain/cheque-template.model';

export interface State {
  entities: ChequeTemplate[];
}

export const initialState: State = {
  entities: []
};

export function reducer(state = initialState, action: chequeTemplate.Actions | cheque.Actions): State {

  switch (action.type) {

    case chequeTemplate.LOAD_ALL: {
      return initialState;
    }

    case chequeTemplate.LOAD_ALL_COMPLETE: {
      const entities: ChequeTemplate[] = action.payload;

      return {
        entities
      };
    }

    case chequeTemplate.CREATE_SUCCESS: {
      const template: ChequeTemplate = action.payload;

      return {
        entities: [...state.entities, template]
      };
    }

    case chequeTemplate.DELETE_SUCCESS: {
      const name: string = action.payload;

      const entities = state.entities.filter(entity => entity.name !== name);

      return {
        entities
      };
    }

    default: {
      return state;
    }

  }

}

export const getEntities = (state: State) => state.entities;

