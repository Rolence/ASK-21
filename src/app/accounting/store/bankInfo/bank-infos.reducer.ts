/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {BankInfo} from '../../../core/services/accounting/domain/bank-info.model';
import {BankInfoActions, BankInfoActionTypes} from './bank-info.actions';
import {createSelector} from 'reselect';

export const adapter: EntityAdapter<BankInfo> = createEntityAdapter<BankInfo>({
  selectId: bankInfo => bankInfo.branchSortCode
});

export interface State extends EntityState<BankInfo> {
  selectedId: string | null;
  loadedAt: { [id: string]: number };
}

export const initialState: State = adapter.getInitialState({
  selectedId: null,
  loadedAt: {}
});

export function reducer(state = initialState, action: BankInfoActions): State {

  switch (action.type) {

    case BankInfoActionTypes.LOAD: {
      const changes = action.payload.bankInfo;

      const updatedState: State = {
        ...state,
        loadedAt: {
          ...state.loadedAt,
          [changes.branchSortCode]: Date.now()
        }
      };

      return adapter.upsertOne({
        id: changes.branchSortCode,
        changes
      }, updatedState);
    }

    case BankInfoActionTypes.SELECT: {
      return {
        ...state,
        selectedId: action.payload.branchSortCode
      };
    }

    case BankInfoActionTypes.LOAD_ALL: {
      return adapter.removeAll(state);
    }

    case BankInfoActionTypes.LOAD_ALL_COMPLETE: {
      return adapter.addAll(action.payload.bankInfos, state);
    }

    case BankInfoActionTypes.CREATE_SUCCESS: {
      return adapter.addOne(action.payload.bankInfo, state);
    }

    case BankInfoActionTypes.UPDATE_SUCCESS: {
      const bankInfo = action.payload.bankInfo;
      return adapter.updateOne({
        id: bankInfo.branchSortCode,
        changes: {
          bic: bankInfo.bic,
          checkDigitMethod: bankInfo.checkDigitMethod
        }
      }, state);
    }

    case BankInfoActionTypes.DELETE_SUCCESS: {
      return adapter.removeOne(action.payload.bankInfo.branchSortCode, state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectEntities: selectAllBankInfoEntities,
  selectAll: selectAllBankInfos
} = adapter.getSelectors();

export const getSelectedId = (state: State) => state.selectedId;
export const getLoadedAt = (state: State) => state.loadedAt;

export const getSelectedBankInfo = createSelector(selectAllBankInfoEntities, getSelectedId, (entities, selectedId) => {
  return entities[selectedId];
});
