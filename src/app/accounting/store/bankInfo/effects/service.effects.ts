/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {AccountingService} from '../../../../core/services/accounting/accounting.service';
import {Action} from '@ngrx/store';
import {Injectable} from '@angular/core';
import {
  BankInfoActionTypes,
  CreateBankInfoAction,
  CreateBankInfoFailAction,
  CreateBankInfoSuccessAction,
  DeleteBankInfoAction,
  DeleteBankInfoFailAction,
  DeleteBankInfoSuccessAction,
  LoadAllCompleteAction,
  UpdateBankInfoAction,
  UpdateBankInfoFailAction,
  UpdateBankInfoSuccessAction
} from '../bank-info.actions';

@Injectable()
export class BankInformationApiEffects {

  @Effect()
  loadAll$: Observable<Action> = this.actions$
    .ofType(BankInfoActionTypes.LOAD_ALL)
    .switchMap(() => this.accountingService.fetchBankInformations()
      .map(bankInfos => new LoadAllCompleteAction({
        bankInfos
      }))
      .catch(() => of(new LoadAllCompleteAction({
        bankInfos: []
      })))
    );

  @Effect()
  createBankInfo$: Observable<Action> = this.actions$
    .ofType(BankInfoActionTypes.CREATE)
    .map((action: CreateBankInfoAction) => action.payload)
    .mergeMap(payload => this.accountingService.createBankInformation(payload.bankInfo)
      .map(() => new CreateBankInfoSuccessAction(payload))
      .catch(error => of(new CreateBankInfoFailAction({
        error
      })))
    );

  @Effect()
  updateBankInfo$: Observable<Action> = this.actions$
    .ofType(BankInfoActionTypes.UPDATE)
    .map((action: UpdateBankInfoAction) => action.payload)
    .mergeMap(payload => this.accountingService.changeBankInformation(payload.bankInfo)
      .map(() => new UpdateBankInfoSuccessAction(payload))
      .catch(error => of(new UpdateBankInfoFailAction({
        error
      })))
    );

  @Effect()
  deleteBankInfo$: Observable<Action> = this.actions$
    .ofType(BankInfoActionTypes.DELETE)
    .map((action: DeleteBankInfoAction) => action.payload)
    .mergeMap(payload => this.accountingService.deleteBankInformation(payload.bankInfo.branchSortCode)
      .map(() => new DeleteBankInfoSuccessAction(payload))
      .catch(error => of(new DeleteBankInfoFailAction({
        error
      })))
    );

  constructor(private actions$: Actions, private accountingService: AccountingService) { }

}
