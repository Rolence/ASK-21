/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {NotificationService} from '../../../../core/services/notification/notification.service';
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {BankInfoActionTypes} from '../bank-info.actions';

@Injectable()
export class BankInformationNotificationEffects {

  @Effect({ dispatch: false })
  createBankInfoSuccess$ = this.actions$
    .ofType(BankInfoActionTypes.CREATE_SUCCESS, BankInfoActionTypes.UPDATE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Bank information saved'));

  @Effect({ dispatch: false })
  createBankInfoError$ = this.actions$
    .ofType(BankInfoActionTypes.CREATE_FAIL)
    .do(() => this.notificationService.sendAlert('The branch sort code you entered was already taken.'));

  @Effect({ dispatch: false })
  deleteBankInfoSuccess$ = this.actions$
    .ofType(BankInfoActionTypes.DELETE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Bank information deleted'));

  constructor(private actions$: Actions, private notificationService: NotificationService) {}

}
