/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {RoutePayload} from '../../../core/store/util/route-payload';
import {BankInfo} from '../../../core/services/accounting/domain/bank-info.model';

export enum BankInfoActionTypes {
  LOAD_ALL = '[Bank Info] Load All',
  LOAD_ALL_COMPLETE = '[Bank Info] Load All Complete',

  LOAD = '[Bank Info] Load',
  SELECT = '[Bank Info] Select',

  CREATE = '[Bank Info] Create',
  CREATE_SUCCESS = '[Bank Info] Create Success',
  CREATE_FAIL = '[Bank Info] Create Fail',

  UPDATE = '[Bank Info] Update',
  UPDATE_SUCCESS = '[Bank Info] Update Success',
  UPDATE_FAIL = '[Bank Info] Update Fail',

  DELETE = '[Bank Info] Delete',
  DELETE_SUCCESS = '[Bank Info] Delete Success',
  DELETE_FAIL = '[Bank Info] Delete Fail',
}

export interface BankInfoPayload extends RoutePayload {
  bankInfo: BankInfo;
}

export class LoadAction implements Action {
  readonly type = BankInfoActionTypes.LOAD;

  constructor(public payload: { bankInfo: BankInfo }) { }
}

export class SelectAction implements Action {
  readonly type = BankInfoActionTypes.SELECT;

  constructor(public payload: { branchSortCode: string }) { }
}

export class LoadAllAction implements Action {
  readonly type = BankInfoActionTypes.LOAD_ALL;

  constructor() { }
}

export class LoadAllCompleteAction implements Action {
  readonly type = BankInfoActionTypes.LOAD_ALL_COMPLETE;

  constructor(public payload: { bankInfos: BankInfo[] }) { }
}

export class CreateBankInfoAction implements Action {
  readonly type = BankInfoActionTypes.CREATE;

  constructor(public payload: BankInfoPayload) { }
}

export class CreateBankInfoSuccessAction implements Action {
  readonly type = BankInfoActionTypes.CREATE_SUCCESS;

  constructor(public payload: BankInfoPayload) { }
}

export class CreateBankInfoFailAction implements Action {
  readonly type = BankInfoActionTypes.CREATE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export class UpdateBankInfoAction implements Action {
  readonly type = BankInfoActionTypes.UPDATE;

  constructor(public payload: BankInfoPayload) { }
}

export class UpdateBankInfoSuccessAction implements Action {
  readonly type = BankInfoActionTypes.UPDATE_SUCCESS;

  constructor(public payload: BankInfoPayload) { }
}

export class UpdateBankInfoFailAction implements Action {
  readonly type = BankInfoActionTypes.UPDATE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export class DeleteBankInfoAction implements Action {
  readonly type = BankInfoActionTypes.DELETE;

  constructor(public payload: BankInfoPayload) { }
}

export class DeleteBankInfoSuccessAction implements Action {
  readonly type = BankInfoActionTypes.DELETE_SUCCESS;

  constructor(public payload: BankInfoPayload) { }
}

export class DeleteBankInfoFailAction implements Action {
  readonly type = BankInfoActionTypes.DELETE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export type BankInfoActions
  = LoadAction
  | SelectAction
  | LoadAllAction
  | LoadAllCompleteAction
  | CreateBankInfoAction
  | CreateBankInfoSuccessAction
  | CreateBankInfoFailAction
  | UpdateBankInfoAction
  | UpdateBankInfoSuccessAction
  | UpdateBankInfoFailAction
  | DeleteBankInfoAction
  | DeleteBankInfoSuccessAction
  | DeleteBankInfoFailAction;
