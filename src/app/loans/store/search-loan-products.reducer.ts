/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as actions from './loan-product.actions';
import {LoanProductOutline} from '../../core/services/loan/domain/definition/product-outline.model';
import {LoanDefinition} from '../../core/services/loan/domain/definition/loan-definition.model';

export interface State {
  entities: LoanProductOutline[];
}

export const initialState: State = {
  entities: []
};

export function reducer(state = initialState, action: actions.Actions): State {
  switch (action.type) {

    case actions.SEARCH: {
      return initialState;
    }

    case actions.SEARCH_COMPLETE: {
      const entities: LoanProductOutline[] = action.payload;
      return {
        entities
      };
    }

    case actions.CREATE_SUCCESS:
    case actions.UPDATE_SUCCESS: {
      const definition: LoanDefinition = action.payload.resource;

      const newOutline: LoanProductOutline = {
        loanType: definition.loanType,
        shortName: definition.shortName,
        name: definition.name,
        currency: definition.currency,
        term: definition.term,
        active: definition.active
      };

      const entities = state.entities.filter(outline => outline.shortName !== newOutline.shortName);
      entities.push(newOutline);

      return {
        ...state,
        entities
      };
    }

    case actions.DELETE_SUCCESS: {
      const definition: LoanDefinition = action.payload.resource;
      const entities = state.entities.filter(entity => entity.shortName !== definition.shortName);

      return {
        ...state,
        entities
      };
    }

    case actions.ENABLE_SUCCESS: {
      const definition: LoanDefinition = action.payload.definition;

      const entities: LoanProductOutline[] = state.entities.map(entity => {
        if (entity.shortName !== definition.shortName) {
          return entity;
        }

        return {
          ...entity,
          active: true
        };
      });

      return {
        entities
      };
    }
    case actions.DISABLE_SUCCESS: {
      const definition: LoanDefinition = action.payload.definition;

      const entities: LoanProductOutline[] = state.entities.map(entity => {
        if (entity.shortName !== definition.shortName) {
          return entity;
        }

        return {
          ...entity,
          active: false
        };
      });

      return {
        entities
      };
    }

    default: {
      return state;
    }
  }
}

export const getEntities = (state: State) => state.entities;

