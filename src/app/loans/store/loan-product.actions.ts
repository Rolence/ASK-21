/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CreateResourceSuccessPayload,
  DeleteResourceSuccessPayload,
  LoadResourcePayload,
  SelectResourcePayload,
  UpdateResourceSuccessPayload
} from '../../core/store/util/resource.reducer';
import {Error} from '../../core/services/domain/error.model';
import {Action} from '@ngrx/store';
import {type} from '../../core/store/util';
import {LoanChangeSet} from '../../core/services/loan/domain/definition/loan-change-set.model';
import {LoanDefinition} from '../../core/services/loan/domain/definition/loan-definition.model';
import {RoutePayload} from '../../core/store/util/route-payload';
import {LoanProductOutline} from '../../core/services/loan/domain/definition/product-outline.model';

export const SEARCH = type('[Loan Product] Search');
export const SEARCH_COMPLETE = type('[Loan Product] Search Complete');

export const LOAD = type('[Loan Product] Load');
export const SELECT = type('[Loan Product] Select');

export const CREATE = type('[Loan Product] Create');
export const CREATE_SUCCESS = type('[Loan Product] Create Success');
export const CREATE_FAIL = type('[Loan Product] Create Fail');

export const UPDATE = type('[Loan Product] Update');
export const UPDATE_SUCCESS = type('[Loan Product] Update Success');
export const UPDATE_FAIL = type('[Loan Product] Update Fail');

export const DELETE = type('[Loan Product] Delete');
export const DELETE_SUCCESS = type('[Loan Product] Delete Success');
export const DELETE_FAIL = type('[Loan Product] Delete Fail');

export const ENABLE = type('[Loan Product] Enable');
export const ENABLE_SUCCESS = type('[Loan Product] Enable Success');
export const ENABLE_FAIL = type('[Loan Product] Enable Fail');

export const DISABLE = type('[Loan Product] Disable');
export const DISABLE_SUCCESS = type('[Loan Product] Disable Success');
export const DISABLE_FAIL = type('[Loan Product] Disable Fail');

export const RESET_FORM = type('[Loan Product] Reset Form');

export interface LoanProductRoutePayload extends RoutePayload {
  definition: LoanDefinition;
}

export interface ChangeProductRoutePayload extends RoutePayload {
  shortName: string;
  changeSet: LoanChangeSet;
}

export interface LoanProductPayload {
  definition: LoanDefinition;
}

export class SearchAction implements Action {
  readonly type = SEARCH;

  constructor() { }
}

export class SearchCompleteAction implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: LoanProductOutline[]) { }
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: LoadResourcePayload) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: SelectResourcePayload) { }
}

export class CreateProductAction implements Action {
  readonly type = CREATE;

  constructor(public payload: LoanProductRoutePayload) { }
}

export class CreateProductSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: CreateResourceSuccessPayload) { }
}

export class CreateProductFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: Error) { }
}

export class UpdateProductAction implements Action {
  readonly type = UPDATE;

  constructor(public payload: ChangeProductRoutePayload) { }
}

export class UpdateProductSuccessAction implements Action {
  readonly type = UPDATE_SUCCESS;

  constructor(public payload: UpdateResourceSuccessPayload) { }
}

export class UpdateProductFailAction implements Action {
  readonly type = UPDATE_FAIL;

  constructor(public payload: Error) { }
}

export class DeleteProductAction implements Action {
  readonly type = DELETE;

  constructor(public payload: LoanProductRoutePayload) { }
}

export class DeleteProductSuccessAction implements Action {
  readonly type = DELETE_SUCCESS;

  constructor(public payload: DeleteResourceSuccessPayload) { }
}

export class DeleteProductFailAction implements Action {
  readonly type = DELETE_FAIL;

  constructor(public payload: Error) { }
}

export class EnableProductAction implements Action {
  readonly type = ENABLE;

  constructor(public payload: LoanProductPayload) { }
}

export class EnableProductSuccessAction implements Action {
  readonly type = ENABLE_SUCCESS;

  constructor(public payload: LoanProductPayload) { }
}

export class EnableProductFailAction implements Action {
  readonly type = ENABLE_FAIL;

  constructor(public payload: Error) { }
}

export class DisableProductAction implements Action {
  readonly type = DISABLE;

  constructor(public payload: LoanProductPayload) { }
}

export class DisableProductSuccessAction implements Action {
  readonly type = DISABLE_SUCCESS;

  constructor(public payload: LoanProductPayload) { }
}

export class DisableProductFailAction implements Action {
  readonly type = DISABLE_FAIL;

  constructor(public payload: Error) { }
}

export class ResetProductFormAction implements Action {
  readonly type = RESET_FORM;

  constructor() {}
}

export type Actions
  = SearchAction
  | SearchCompleteAction
  | LoadAction
  | SelectAction
  | CreateProductAction
  | CreateProductSuccessAction
  | CreateProductFailAction
  | UpdateProductAction
  | UpdateProductSuccessAction
  | UpdateProductFailAction
  | DeleteProductAction
  | DeleteProductSuccessAction
  | DeleteProductFailAction
  | EnableProductAction
  | EnableProductSuccessAction
  | EnableProductFailAction
  | DisableProductAction
  | DisableProductSuccessAction
  | DisableProductFailAction;
