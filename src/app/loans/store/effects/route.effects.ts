/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Router} from '@angular/router';
import * as actions from '../loan-product.actions';

@Injectable()
export class LoanProductRouteEffects {

  @Effect({ dispatch: false })
  createLoanProductSuccess$ = this.actions$
    .ofType(actions.CREATE_SUCCESS)
    .map((action: actions.CreateProductSuccessAction) => action.payload)
    .do(payload => this.router.navigate(['../'], { relativeTo: payload.activatedRoute} ));

  @Effect({ dispatch: false })
  updateLoanProductSuccess$ = this.actions$
    .ofType(actions.UPDATE_SUCCESS)
    .map((action: actions.UpdateProductSuccessAction) => action.payload)
    .do(payload => this.router.navigate(['../'], { relativeTo: payload.activatedRoute} ));

  @Effect({ dispatch: false })
  deleteLoanProductSuccess$ = this.actions$
    .ofType(actions.DELETE_SUCCESS)
    .map((action: actions.DeleteProductSuccessAction) => action.payload)
    .do(payload => this.router.navigate(['../../../'], { relativeTo: payload.activatedRoute} ));

  constructor(private actions$: Actions, private router: Router) { }

}
