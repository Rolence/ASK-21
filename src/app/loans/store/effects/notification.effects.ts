/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as actions from '../loan-product.actions';
import {NotificationService, NotificationType} from '../../../core/services/notification/notification.service';

@Injectable()
export class LoanProductNotificationEffects {

  @Effect({ dispatch: false })
  createLoanProductSuccess$ = this.actions$
    .ofType(actions.CREATE_SUCCESS, actions.UPDATE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan product is going to be saved'));

  @Effect({ dispatch: false })
  createLoanProductFail$ = this.actions$
    .ofType(actions.CREATE_FAIL)
    .do(() => this.notificationService.send({
      type: NotificationType.ALERT,
      title: 'Loan product can\'t be created',
      message: 'The short name you entered was already taken.'
    }));

  @Effect({ dispatch: false })
  deleteProductSuccess$ = this.actions$
    .ofType(actions.DELETE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan product is going to be deleted'));

  @Effect({ dispatch: false })
  deleteProductFail$ = this.actions$
    .ofType(actions.DELETE_FAIL)
    .do(() => this.notificationService.send({
      type: NotificationType.ALERT,
      title: 'Loan product can\'t be deleted',
      message: 'Loan product is already assigned to a member.'
    }));

  @Effect({ dispatch: false })
  enableProductSuccess$ = this.actions$
    .ofType(actions.ENABLE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan product is going to be enabled'));

  @Effect({ dispatch: false })
  enableProductFail$ = this.actions$
    .ofType(actions.ENABLE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue enabling the product'));

  @Effect({ dispatch: false })
  disableProductSuccess$ = this.actions$
    .ofType(actions.DISABLE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan product is going to be disabled'));

  @Effect({ dispatch: false })
  disableProductFail$ = this.actions$
    .ofType(actions.DISABLE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue disabling the product'));

  constructor(private actions$: Actions, private notificationService: NotificationService) {}
}
