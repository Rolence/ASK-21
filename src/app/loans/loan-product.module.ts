/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {NgModule} from '@angular/core';
import {LoanProductRoutingModule} from './loan-product-routing.module';
import {LoanProductExistsGuard} from './loan-product-exists.guard';
import {StoreModule} from '@ngrx/store';
import {reducerProvider, reducerToken} from './store';
import {EffectsModule} from '@ngrx/effects';
import {LoanProductApiEffects} from './store/effects/service.effects';
import {LoanProductRouteEffects} from './store/effects/route.effects';
import {LoanProductNotificationEffects} from './store/effects/notification.effects';
import {LoanProductIndexComponent} from './loan-product.index.component';
import {LoanProductListComponent} from './loan-product.list.component';
import {LoanProductDetailComponent} from './loan-product.detail.component';
import {LoanProductCreateFormComponent} from './form/create/form.component';
import {LoanProductEditComponent} from './form/edit/edit.component';
import {LoanProductCreateComponent} from './form/create/create.component';
import {LoanProductChargesFormComponent} from './form/charges/charges.component';
import {LoanProductLimitsFormComponent} from './form/limits/limits.component';
import {ChargesFormService} from './form/services/charges-form.service';
import {LimitsFormService} from './form/services/limits-form.service';
import {LoanProductLossAllowanceFormComponent} from './form/loss-allowance/loss-allowance.component';
import {LossAllowanceFormService} from './form/services/loss-allowance-form.service';
import {LoanProductEditFormComponent} from './form/edit/form.component';
import {LoanProductCollateralFormComponent} from './form/collateral/collateral.component';
import {LoanProductsIndexComponent} from './loan-products.index.component';
import {LoanProductInterestFormComponent} from './form/interest/interest.component';
import {InterestFormService} from './form/services/interest-form.service';
import {CollateralFormService} from './form/services/collateral-form.service';
import {SharedModule} from '../shared/shared.module';
import {AccountingValidationService} from '../core/validator/services/accounting-validation.service';

@NgModule({
  imports: [
    LoanProductRoutingModule,
    SharedModule,

    StoreModule.forFeature('loans', reducerToken),

    EffectsModule.forFeature([
      LoanProductApiEffects,
      LoanProductRouteEffects,
      LoanProductNotificationEffects
    ])
  ],
  declarations: [
    LoanProductsIndexComponent,
    LoanProductListComponent,
    LoanProductIndexComponent,
    LoanProductDetailComponent,
    LoanProductCreateFormComponent,
    LoanProductEditFormComponent,
    LoanProductCreateComponent,
    LoanProductEditComponent,
    LoanProductChargesFormComponent,
    LoanProductLimitsFormComponent,
    LoanProductLossAllowanceFormComponent,
    LoanProductCollateralFormComponent,
    LoanProductInterestFormComponent
  ],
  providers: [
    LoanProductExistsGuard,
    ChargesFormService,
    LimitsFormService,
    LossAllowanceFormService,
    InterestFormService,
    CollateralFormService,
    AccountingValidationService,
    reducerProvider
  ]
})
export class LoanProductModule {}
