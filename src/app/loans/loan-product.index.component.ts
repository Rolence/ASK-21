/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {SelectAction} from './store/loan-product.actions';
import * as fromLoanProducts from './store';
import {Store} from '@ngrx/store';
import {BreadCrumbService} from '../shared/bread-crumbs/bread-crumb.service';
import {LoanDefinition} from '../core/services/loan/domain/definition/loan-definition.model';

@Component({
  templateUrl: './loan-product.index.component.html'
})
export class LoanProductIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private productSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromLoanProducts.State>, private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.productSubscription = this.store.select(fromLoanProducts.getSelectedProduct)
      .filter(product => !!product)
      .subscribe((loanProduct: LoanDefinition) =>
        this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, loanProduct.name)
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.productSubscription.unsubscribe();
  }
}
