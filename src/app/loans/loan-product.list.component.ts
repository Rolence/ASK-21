/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromLoanProducts from './store';
import {Store} from '@ngrx/store';
import {TableData} from '../shared/data-table/data-table.component';
import {LoanProductOutline} from '../core/services/loan/domain/definition/product-outline.model';

@Component({
  templateUrl: './loan-product.list.component.html'
})
export class LoanProductListComponent {

  productData$: Observable<TableData>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromLoanProducts.State>) {
    this.productData$ = this.store.select(fromLoanProducts.getSearchProducts)
      .map(products => ({
        data: products,
        totalElements: products.length,
        totalPages: 1
      }));
  }

  rowSelect(product: LoanProductOutline): void {
    this.router.navigate(['detail', product.shortName], { relativeTo: this.route });
  }

  addProduct(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
