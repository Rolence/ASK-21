/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {LoanProductListComponent} from './loan-product.list.component';
import {LoanProductExistsGuard} from './loan-product-exists.guard';
import {LoanProductDetailComponent} from './loan-product.detail.component';
import {LoanProductIndexComponent} from './loan-product.index.component';
import {LoanProductCreateComponent} from './form/create/create.component';
import {LoanProductEditComponent} from './form/edit/edit.component';
import {LoanProductsIndexComponent} from './loan-products.index.component';
import {NgModule} from '@angular/core';

const LoanProductRoutes: Routes = [
  {
    path: '',
    component: LoanProductsIndexComponent,
    data: {
      title: 'All loan products',
      hasPermission: { id: 'loan_consumer', accessLevel: 'READ' }
    },
    children: [
      {
        path: '',
        component: LoanProductListComponent,
      },
      {
        path: 'create',
        component: LoanProductCreateComponent,
        data: {
          title: 'Add product'
        }
      },
      {
        path: 'detail/:id',
        component: LoanProductIndexComponent,
        canActivate: [LoanProductExistsGuard],
        children: [
          {
            path: '',
            component: LoanProductDetailComponent
          },
          {
            path: 'edit',
            component: LoanProductEditComponent,
            data: {
              title: 'Edit',
              hasPermission: { id: 'loan_consumer', accessLevel: 'CHANGE' }
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(LoanProductRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LoanProductRoutingModule {}
