/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoanDefinition} from '../../../core/services/loan/domain/definition/loan-definition.model';
import {Currency} from '../../../core/services/currency/domain/currency.model';
import {interestPayableOptionList} from '../model/interest-payable-options';
import {Role} from '../../../core/services/identity/domain/role.model';
import {ChargesFormService} from '../services/charges-form.service';
import {Action} from '../../../core/services/loan/domain/definition/action.model';
import {LimitsFormService} from '../services/limits-form.service';
import {Term} from '../../../core/services/loan/domain/definition/term.model';
import {Duration} from '../../../core/services/loan/domain/definition/duration.model';
import {PrincipalRange} from '../../../core/services/loan/domain/definition/principal-range.model';
import {Interest} from '../../../core/services/loan/domain/definition/interest.model';
import {PaymentPolicy} from '../../../core/services/loan/domain/definition/payment-policy.model';
import {DTIConfig} from '../../../core/services/loan/domain/definition/DTI-config.model';
import {LossAllowanceFormService} from '../services/loss-allowance-form.service';
import {InterestFormService} from '../services/interest-form.service';
import {CollateralFormService} from '../services/collateral-form.service';
import {Field} from '../../../core/services/catalog/domain/field.model';
import {AccountingValidationService} from '../../../core/validator/services/accounting-validation.service';
import {FimsValidators} from '../../../core/validator/validators';
import {LoanType} from '../../../core/services/loan/domain/loan-type.model';
import {LoanPeriod} from '../../../core/services/loan/domain/definition/loan-period.model';

@Component({
  selector: 'aten-loan-product-create-form',
  templateUrl: './form.component.html'
})
export class LoanProductCreateFormComponent implements OnChanges {

  fields: Field[] = [];
  formGroup: FormGroup;
  chargesFormGroup: FormGroup;
  limitsFormGroup: FormGroup;
  lossAllowanceFormGroup: FormGroup;
  stopDaysFormGroup: FormGroup;
  collateralFormGroup: FormGroup;
  payableOptions = interestPayableOptionList;

  @Input() editMode: boolean;
  @Input() currencies: Currency[];
  @Input() roles: Role[];
  @Input() actions: Action[];
  @Input() paymentPolicies: PaymentPolicy[];

  @Output() save = new EventEmitter<LoanDefinition>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private accountingValidationService: AccountingValidationService,
              private chargesFormService: ChargesFormService, private limitsFormService: LimitsFormService,
              private lossAllowanceService: LossAllowanceFormService, private interestFormService: InterestFormService,
              private collateralFormService: CollateralFormService) {

    this.formGroup = this.formBuilder.group({
      type: ['CONSUMER', [Validators.required]],
      shortName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32), FimsValidators.urlSafe]],
      name: ['', [Validators.required, Validators.maxLength(256)]],
      description: ['', [Validators.maxLength(4096)]],
      currencyCode: ['', [Validators.required]],
      fundingAccount: ['', [Validators.required], accountingValidationService.accountExists()],
      loanAppropriationsLedger: ['', [Validators.required], accountingValidationService.ledgerExists()],
      memberLoanLedger: ['', [Validators.required], accountingValidationService.ledgerExists()],
      arrearsLedger: ['', [], accountingValidationService.ledgerExists()],
      paymentPolicy: ['', [Validators.required]],
      debtToIncomeRatioRequired: [false],
      debtToIncomePreferredRatio: ['35.00', [Validators.required, FimsValidators.greaterThanValue(0)]],
      debtToIncomeRiskedRatio: ['50.00', [Validators.required, FimsValidators.greaterThanValue(0)]],
      gracePeriodInDays: [''],
      purposes: [[]],
      principalRangeEnabled: [false],
      principalMin: ['0.00', [Validators.required]],
      principalMax: ['0.00', [Validators.required, FimsValidators.greaterThanValue(0)]],
      interestRangeEnabled: [false],
      interestMin: ['0.00', [Validators.required, FimsValidators.minValue(0)]],
      interestMax: ['0.00', [Validators.required, FimsValidators.minValue(0)]],
      interestType: ['DECLINING', [Validators.required]],
      interestPayable: ['MONTHS', [Validators.required]],
      interestRevenueAccount: ['', [Validators.required], accountingValidationService.accountExists()],
      interestAccrualAccount: ['', [], accountingValidationService.accountExists()],
      durationRangeEnabled: [false],
      durationMin: ['0', [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(1)]],
      durationMax: ['0', [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(1)]],
      durationPeriod: ['MONTHS', [Validators.required]],
      loanPeriodEnabled: [false],
      loanPeriodDuration: ['0', [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(1)]],
      loanPeriod: ['MONTHS', [Validators.required]],
    });

    this.chargesFormGroup = this.chargesFormService.initChargesForm();
    this.limitsFormGroup = this.limitsFormService.initLimitsForm();
    this.lossAllowanceFormGroup = this.lossAllowanceService.initLossAllowanceForm();

    this.stopDaysFormGroup = this.formBuilder.group({
      enabled: [false],
      stopAccrualAfterDays: ['', [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(1)]]
    });

    this.collateralFormGroup = this.collateralFormService.initCollateralForm();

    this.formGroup.get('durationRangeEnabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleDuration(enabled));

    this.formGroup.get('loanPeriodEnabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleLoanPeriod(enabled));

    this.formGroup.get('principalRangeEnabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.togglePrincipalRange(enabled));

    this.formGroup.get('debtToIncomeRatioRequired').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleDebtToIncomeRatio(enabled));

    this.formGroup.get('interestRangeEnabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleInterestRange(enabled));

    this.formGroup.get('type').valueChanges
      .startWith('CONSUMER')
      .subscribe(type => this.toggleType(type));

    this.stopDaysFormGroup.get('enabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleStopAccrual(enabled));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.paymentPolicies && this.paymentPolicies && this.paymentPolicies.length > 0) {
      this.formGroup.get('paymentPolicy').setValue(this.paymentPolicies[0]);
    }
  }

  private toggleDuration(enabled: boolean) {
    const control = this.formGroup.get('durationMin');

    if (enabled) {
      control.enable();
    } else {
      control.disable();
    }
  }

  private toggleLoanPeriod(enabled: boolean) {
    const loanPeriodDuration = this.formGroup.get('loanPeriodDuration');
    const loanPeriod = this.formGroup.get('loanPeriod');

    if (enabled) {
      loanPeriodDuration.enable();
      loanPeriod.enable();
    } else {
      loanPeriodDuration.disable();
      loanPeriod.disable();
    }
  }

  private togglePrincipalRange(enabled: boolean) {
    const control = this.formGroup.get('principalMin');

    if (enabled) {
      control.enable();
    } else {
      control.disable();
    }
  }

  private toggleDebtToIncomeRatio(enabled: boolean) {
    const preferredControl = this.formGroup.get('debtToIncomePreferredRatio');
    const riskedControl = this.formGroup.get('debtToIncomeRiskedRatio');

    if (enabled) {
      preferredControl.enable();
      riskedControl.enable();
    } else {
      preferredControl.disable();
      riskedControl.disable();
    }
  }

  private toggleInterestRange(enabled: boolean) {
    const interestMinControl = this.formGroup.get('interestMin');

    if (enabled) {
      interestMinControl.enable();
    } else {
      interestMinControl.disable();
    }
  }

  private toggleType(type: LoanType) {
    if (type === 'COLLATERAL') {
      this.collateralFormService.enable();
    } else {
      this.collateralFormService.disable();
    }
  }

  private toggleStopAccrual(enabled: boolean) {
    const stopAccrualControl = this.stopDaysFormGroup.get('stopAccrualAfterDays');

    if (enabled) {
      stopAccrualControl.enable();
    } else {
      stopAccrualControl.disable();
    }
  }

  onSave(): void {
    const foundCurrency: Currency = this.currencies.find(currency => currency.code === this.formGroup.get('currencyCode').value);

    const duration: Duration = {
      min: this.formGroup.get('durationRangeEnabled').value === true ? this.formGroup.get('durationMin').value : undefined,
      max: this.formGroup.get('durationMax').value,
      period: this.formGroup.get('durationPeriod').value
    };

    let loanPeriod: LoanPeriod;

    if (this.formGroup.get('loanPeriodEnabled').value === true) {
      loanPeriod = {
        duration: this.formGroup.get('loanPeriodDuration').value,
        period: this.formGroup.get('loanPeriod').value
      };
    }

    const principalRange: PrincipalRange = {
      min: this.formGroup.get('principalRangeEnabled').value === true ? this.formGroup.get('principalMin').value : undefined,
      max: this.formGroup.get('principalMax').value
    };

    const interest: Interest = this.interestFormService.getInterest(this.formGroup);

    const term: Term = {
      duration,
      loanPeriod,
      principalRange,
      interest,
      gracePeriodInDays: this.formGroup.get('gracePeriodInDays').value
    };

    const dtiConfig: DTIConfig = {
      preferred: this.formGroup.get('debtToIncomePreferredRatio').value,
      risked: this.formGroup.get('debtToIncomeRiskedRatio').value
    };

    const collateral = this.collateralFormService.getCollateral();
    collateral.fields = collateral.type !== 'DEPOSIT_SECURED' ? this.fields : [];

    const loanType: LoanType = this.formGroup.get('type').value;

    const loanDefinition: LoanDefinition = {
      loanType,
      shortName: this.formGroup.get('shortName').value,
      name: this.formGroup.get('name').value,
      description: this.formGroup.get('description').value,
      currency: {
        code: foundCurrency.code,
        name: foundCurrency.name,
        sign: foundCurrency.sign,
        scale: foundCurrency.digits
      },
      fundingAccount: this.formGroup.get('fundingAccount').value,
      loanAppropriationsLedger: this.formGroup.get('loanAppropriationsLedger').value,
      memberLoanLedger: this.formGroup.get('memberLoanLedger').value,
      arrearsLedger: this.formGroup.get('arrearsLedger').value ? this.formGroup.get('arrearsLedger').value : undefined,
      charges: this.chargesFormService.getCharges(),
      approvalLimits: this.limitsFormService.getLimits(),
      term,
      paymentPolicy: this.formGroup.get('paymentPolicy').value,
      dtiConfig: this.formGroup.get('debtToIncomeRatioRequired').value ? dtiConfig : undefined,
      collateral: loanType === 'COLLATERAL' ? collateral : null,
      stopAccrualAfterDays: this.stopDaysFormGroup.get('enabled').value ?
        this.stopDaysFormGroup.get('stopAccrualAfterDays').value : null,
      expectedCreditLoss: this.lossAllowanceService.getCreditLoss(),
      purposes: this.formGroup.get('purposes').value,
      active: false
    };

    this.save.emit(loanDefinition);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onAddCharge(): void {
    this.chargesFormService.addCharge();
  }

  onRemoveCharge(index: number): void {
    this.chargesFormService.removeCharge(index);
  }

  onAddLimit(): void {
    this.limitsFormService.addLimit();
  }

  onRemoveLimit(index: number): void {
    this.limitsFormService.removeLimit(index);
  }

  get isValid(): boolean {
    return this.formGroup.valid
      && this.limitsFormGroup.valid
      && this.chargesFormGroup.valid
      && this.collateralFormService.valid
      && this.lossAllowanceService.valid
      && this.stopDaysFormGroup.valid;
  }

  saveField(fieldToSave: Field): void {
    const fields = this.fields.filter(field => field.identifier !== fieldToSave.identifier);

    this.fields = [...fields, fieldToSave];
  }

  removeField(fieldToRemove: Field): void {
    this.fields = this.fields.filter(field => field.identifier !== fieldToRemove.identifier);
  }

  get isLossAllowanceStepValid(): boolean {
    return this.lossAllowanceFormGroup.valid && this.stopDaysFormGroup.valid;
  }

  get isLossAllowanceStepPristine(): boolean {
    return this.lossAllowanceFormGroup.pristine && this.stopDaysFormGroup.pristine;
  }

}
