/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TdStepComponent} from '@covalent/core';
import {ChargesFormService} from '../services/charges-form.service';
import {LimitsFormService} from '../services/limits-form.service';
import {LossAllowanceFormService} from '../services/loss-allowance-form.service';
import {InterestFormService} from '../services/interest-form.service';
import {CollateralFormService} from '../services/collateral-form.service';
import {LoanDefinition} from '../../../core/services/loan/domain/definition/loan-definition.model';
import {AccountingValidationService} from '../../../core/validator/services/accounting-validation.service';
import {Action} from '../../../core/services/loan/domain/definition/action.model';
import {Role} from '../../../core/services/identity/domain/role.model';
import {Field} from '../../../core/services/catalog/domain/field.model';
import {FimsValidators} from '../../../core/validator/validators';

@Component({
  selector: 'aten-loan-product-edit-form',
  templateUrl: './form.component.html'
})
export class LoanProductEditFormComponent implements OnChanges {

  fields: Field[] = [];
  termFormGroup: FormGroup;
  purposesFormGroup: FormGroup;
  chargesFormGroup: FormGroup;
  limitsFormGroup: FormGroup;
  lossAllowanceFormGroup: FormGroup;
  stopDaysFormGroup: FormGroup;
  collateralFormGroup: FormGroup;

  @Input() definition: LoanDefinition;
  @Input() roles: Role[];
  @Input() actions: Action[];

  @Output() save = new EventEmitter<LoanDefinition>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private accountingValidationService: AccountingValidationService,
              private chargesFormService: ChargesFormService, private limitsFormService: LimitsFormService,
              private lossAllowanceService: LossAllowanceFormService, private interestFormService: InterestFormService,
              private collateralFormService: CollateralFormService) {
    this.termFormGroup = this.formBuilder.group({
      interestRangeEnabled: [false],
      interestMin: ['', [Validators.required, FimsValidators.minValue(0)]],
      interestMax: ['', [Validators.required, FimsValidators.minValue(0)]],
      interestType: ['', [Validators.required]],
      interestPayable: ['', [Validators.required]],
      interestRevenueAccount: ['', [Validators.required], accountingValidationService.accountExists()],
      interestAccrualAccount: ['', [], accountingValidationService.accountExists()],
    });
    this.purposesFormGroup = this.formBuilder.group({
      purposes: [[]]
    });
    this.chargesFormGroup = this.chargesFormService.initChargesForm();
    this.limitsFormGroup = this.limitsFormService.initLimitsForm();
    this.lossAllowanceFormGroup = this.lossAllowanceService.initLossAllowanceForm();

    this.stopDaysFormGroup = this.formBuilder.group({
      enabled: [false],
      stopAccrualAfterDays: ['', [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(1)]]
    });

    this.collateralFormGroup = this.collateralFormService.initCollateralForm();

    this.termFormGroup.get('interestRangeEnabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleInterestRange(enabled));

    this.stopDaysFormGroup.get('enabled').valueChanges
      .startWith(false)
      .subscribe(enabled => this.toggleStopAccrual(enabled));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.definition && this.definition) {
      const interest = this.definition.term.interest;
      const currency = this.definition.currency;

      this.termFormGroup.reset({
        interestRangeEnabled: interest.min != null,
        interestMin: interest.min != null ? interest.min.toFixed(currency.scale) : interest.min,
        interestMax: interest.max.toFixed(currency.scale),
        interestType: { value: interest.interestType, disabled: true },
        interestPayable: interest.payable,
        interestRevenueAccount: interest.revenueAccount,
        interestAccrualAccount: interest.accrualAccount
      });

      this.stopDaysFormGroup.reset({
        enabled: this.definition.stopAccrualAfterDays != null,
        stopAccrualAfterDays: this.definition.stopAccrualAfterDays
      });

      if (this.definition.collateral) {
        this.collateralFormService.enable();
        this.collateralFormService.setCollateral(this.definition.collateral);
      } else {
        this.collateralFormService.disable();
      }

      this.purposesFormGroup.reset({
        purposes: this.definition.purposes ? this.definition.purposes : []
      });
      this.chargesFormService.setCharges(this.definition.charges);
      this.limitsFormService.setLimits(this.definition.approvalLimits);
      this.lossAllowanceService.setCreditLoss(this.definition.expectedCreditLoss);

      if (this.definition.collateral) {
        this.fields = [...this.definition.collateral.fields];
      }
    }
  }

  onSave(): void {
    const interest = this.interestFormService.getInterest(this.termFormGroup);

    const collateral = this.collateralFormService.getCollateral();
    collateral.fields = collateral.type !== 'DEPOSIT_SECURED' ? this.fields : [];

    this.save.emit({
      ...this.definition,
      term: {
        ...this.definition.term,
        interest
      },
      collateral: this.definition.loanType === 'COLLATERAL' ? collateral : null,
      stopAccrualAfterDays: this.stopDaysFormGroup.get('enabled').value ?
        this.stopDaysFormGroup.get('stopAccrualAfterDays').value : null,
      purposes: this.purposesFormGroup.get('purposes').value,
      charges: this.chargesFormService.getCharges(),
      approvalLimits: this.limitsFormService.getLimits(),
      expectedCreditLoss: this.lossAllowanceService.getCreditLoss()
    });
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onAddCharge(): void {
    this.chargesFormService.addCharge();
  }

  onRemoveCharge(index: number): void {
    this.chargesFormService.removeCharge(index);
  }

  onAddLimit(): void {
    this.limitsFormService.addLimit();
  }

  onRemoveLimit(index: number): void {
    this.limitsFormService.removeLimit(index);
  }

  get isValid(): boolean {
    return this.termFormGroup.valid
      && this.limitsFormGroup.valid
      && this.chargesFormGroup.valid
      && this.collateralFormService.valid
      && this.lossAllowanceService.valid
      && this.stopDaysFormGroup.valid;
  }

  private toggleInterestRange(enabled: boolean) {
    const interestMinControl = this.termFormGroup.get('interestMin');

    if (enabled) {
      interestMinControl.enable();
    } else {
      interestMinControl.disable();
    }
  }

  private toggleStopAccrual(enabled: boolean) {
    const stopAccrualControl = this.stopDaysFormGroup.get('stopAccrualAfterDays');

    if (enabled) {
      stopAccrualControl.enable();
    } else {
      stopAccrualControl.disable();
    }
  }

  saveField(fieldToSave: Field): void {
    const fields = this.fields.filter(field => field.identifier !== fieldToSave.identifier);

    this.fields = [...fields, fieldToSave];
  }

  removeField(fieldToRemove: Field): void {
    this.fields = this.fields.filter(field => field.identifier !== fieldToRemove.identifier);
  }

  get isLossAllowanceStepValid(): boolean {
    return this.lossAllowanceFormGroup.valid && this.stopDaysFormGroup.valid;
  }

  get isLossAllowanceStepPristine(): boolean {
    return this.lossAllowanceFormGroup.pristine && this.stopDaysFormGroup.pristine;
  }

}
