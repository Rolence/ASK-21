/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromLoanProducts from '../../store/index';
import {Observable} from 'rxjs/Observable';
import {ResetProductFormAction, UpdateProductAction} from '../../store/loan-product.actions';
import * as fromRoot from '../../../core/store/index';
import {SEARCH as SEARCH_ROLE} from '../../../core/store/role/role.actions';
import {Store} from '@ngrx/store';
import {LoanDefinition} from '../../../core/services/loan/domain/definition/loan-definition.model';
import {Role} from '../../../core/services/identity/domain/role.model';
import {Action} from '../../../core/services/loan/domain/definition/action.model';
import {ConsumerLoanService} from '../../../core/services/loan/consumer-loan.service';
import {CurrencyService} from '../../../core/services/currency/currency.service';
import {LoanChangeSet} from '../../../core/services/loan/domain/definition/loan-change-set.model';

@Component({
  templateUrl: './edit.component.html'
})
export class LoanProductEditComponent implements OnDestroy {

  definition$: Observable<LoanDefinition>;
  roles$: Observable<Role[]>;
  actions$: Observable<Action[]>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromLoanProducts.State>,
              private currencyService: CurrencyService, private loanService: ConsumerLoanService) {
    this.definition$ = this.store.select(fromLoanProducts.getSelectedProduct);
    this.roles$ = this.store.select(fromRoot.getSearchRoles);
    this.actions$ = this.loanService.fetchActions();

    this.store.dispatch({ type: SEARCH_ROLE });
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ResetProductFormAction());
  }

  onSave(changeSet: LoanChangeSet) {
    this.store.dispatch(new UpdateProductAction({
      shortName: changeSet.shortName,
      changeSet,
      activatedRoute: this.route
    }));
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
