/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountingValidationService} from '../../../core/validator/services/accounting-validation.service';
import {FimsValidators} from '../../../core/validator/validators';
import {Collateral, Type} from '../../../core/services/loan/domain/collateral.model';

@Injectable()
export class CollateralFormService {

  private _collateralFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private accountingValidationService: AccountingValidationService) {
  }

  initCollateralForm(): FormGroup {
    this._collateralFormGroup = this.buildCollateralForm();

    this._collateralFormGroup.get('type').valueChanges
      .startWith('DEPOSIT_SECURED')
      .subscribe(type => this.toggleType(type));

    return this._collateralFormGroup;
  }

  private buildCollateralForm(): FormGroup {
    return this.formBuilder.group({
      type: ['DEPOSIT_SECURED', [Validators.required]],
      percentage: ['0.00', [Validators.required, FimsValidators.minValue(0), FimsValidators.maxScale(5)]],
      collateralLedger: ['', [Validators.required], this.accountingValidationService.ledgerExists()]
    });
  }

  private toggleType(type: Type): void {
    const percentageControl = this.collateralFormGroup.get('percentage') as FormControl;
    const collateralLedgerControl = this.collateralFormGroup.get('collateralLedger') as FormControl;

    if (type === 'DEPOSIT_SECURED') {
      percentageControl.enable();
      collateralLedgerControl.enable();
    } else {
      percentageControl.disable();
      collateralLedgerControl.disable();
    }
  }

  get collateralFormGroup(): FormGroup {
    return this._collateralFormGroup;
  }

  getCollateral(): Collateral {
    const isSecured = this.collateralFormGroup.get('type').value === 'DEPOSIT_SECURED';

    return {
      type: this.collateralFormGroup.get('type').value,
      required: true,
      percentage: isSecured ? this.collateralFormGroup.get('percentage').value : null,
      collateralLedger: isSecured ? this.collateralFormGroup.get('collateralLedger').value : null
    };
  }

  setCollateral(collateral: Collateral): void {
    this.collateralFormGroup.reset({
      type: collateral.type,
      required: collateral.required,
      percentage: collateral.percentage,
      collateralLedger: collateral.collateralLedger
    });
  }

  get valid(): boolean {
    if (this.collateralFormGroup.enabled) {
      return this.collateralFormGroup.valid;
    }

    return true;
  }

  enable(): void {
    this.collateralFormGroup.enable();
  }

  disable(): void {
    this.collateralFormGroup.disable({
      emitEvent: false
    });
  }

}
