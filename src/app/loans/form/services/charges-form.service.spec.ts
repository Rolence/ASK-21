/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ReactiveFormsModule} from '@angular/forms';
import {TestBed} from '@angular/core/testing';
import {ChargesFormService} from './charges-form.service';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {Charge} from '../../../core/services/loan/domain/definition/charge.model';

describe('Test charge form spec', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      providers: [
        ChargesFormService,
        {
          provide: AccountingService,
          useValue: jasmine.createSpyObj('accountingService', ['findAccount'])
        }
      ]
    });
  });

  it('should not return amount, threshold and proportional when adhoc is active', () => {
    const chargesFormService: ChargesFormService = TestBed.get(ChargesFormService);
    chargesFormService.initChargesForm();

    const charges: Charge[] = [
      {
        description: 'description',
        action: 'action',
        adHoc: true,
        amount: '0.00',
        threshold: '0.00',
        revenueAccount: 'revenueAccount',
        proportional: true,
        chargeType: 'ONCE'
      }
    ];

    chargesFormService.setCharges(charges);

    const expectedResult: Charge = {
      description: 'description',
      action: 'action',
      adHoc: true,
      revenueAccount: 'revenueAccount',
      chargeType: undefined
    };

    expect(chargesFormService.getCharges()[0]).toEqual(expectedResult);
  });

  it('should return amount, threshold and proportional when adhoc is inactive', () => {
    const chargesFormService: ChargesFormService = TestBed.get(ChargesFormService);
    chargesFormService.initChargesForm();

    const charges: Charge[] = [
      {
        description: 'description',
        action: 'action',
        adHoc: false,
        amount: '0.00',
        threshold: '0.00',
        revenueAccount: 'revenueAccount',
        proportional: true,
        chargeType: 'ONCE'
      }
    ];

    chargesFormService.setCharges(charges);

    expect(chargesFormService.getCharges()[0]).toEqual(charges[0]);
  });
});
