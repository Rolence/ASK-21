/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {TellerService} from '../../../core/services/teller/teller-service';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import * as tellerActions from '../teller.actions';
import * as transactionActions from '../transaction.actions';
import {PrintChequePayload} from '../transaction.actions';
import {of} from 'rxjs/observable/of';
import {ChequeService} from '../../../core/services/cheque/cheque.service';
import {ConsumerLoanService} from '../../../core/services/loan/consumer-loan.service';
import {DepositAccountService} from '../../../core/services/depositAccount/deposit-account.service';
import {PrintChequeInfo} from '../../../core/services/cheque/domain/print-cheque-info.model';

@Injectable()
export class TellerApiEffects {

  @Effect()
  unlockDrawer$: Observable<Action> = this.actions$
    .ofType(tellerActions.UNLOCK_DRAWER)
    .map((action: tellerActions.UnlockDrawerAction) => action.payload)
    .mergeMap(payload =>
      this.tellerService.unlockDrawer(payload.tellerCode, {employeeIdentifier: payload.employeeId, password: payload.password})
        .map(teller => new tellerActions.UnlockDrawerSuccessAction(teller))
        .catch((error) => of(new tellerActions.UnlockDrawerFailAction(error)))
    );

  @Effect()
  lockDrawer$: Observable<Action> = this.actions$
    .ofType(tellerActions.LOCK_DRAWER)
    .map((action: tellerActions.LockDrawerAction) => action.payload)
    .mergeMap(payload =>
      this.tellerService.executeCommand(payload.tellerCode, 'PAUSE')
        .map(() => new tellerActions.LockDrawerSuccessAction())
        .catch((error) => of(new tellerActions.LockDrawerSuccessAction()))
    );

  @Effect()
  createTransaction$: Observable<Action> = this.actions$
    .ofType(transactionActions.CREATE_TRANSACTION)
    .map((action: transactionActions.CreateTransactionAction) => action.payload)
    .mergeMap(payload =>
      this.tellerService.createTransaction(payload.tellerCode, payload.tellerTransaction, true)
        .map((costs) => new transactionActions.CreateTransactionSuccessAction(costs))
        .catch((error) => of(new transactionActions.CreateTransactionFailAction(error)))
    );

  @Effect()
  confirmTransaction$: Observable<Action> = this.actions$
    .ofType(transactionActions.CONFIRM_TRANSACTION)
    .map((action: transactionActions.ConfirmTransactionAction) => action.payload)
    .mergeMap(payload =>
      this.tellerService.confirmTransaction(payload.tellerCode, payload.tellerTransactionIdentifier, payload.command,
        payload.chargesIncluded, payload.voucherNumber)
        .map(() => new transactionActions.ConfirmTransactionSuccessAction(payload))
        .catch((error) => of(new transactionActions.ConfirmTransactionFailAction(error)))
    );

  @Effect()
  printReceipt$: Observable<Action> = this.actions$
    .ofType(transactionActions.PRINT_RECEIPT)
    .map((action: transactionActions.PrintReceiptAction) => action.payload)
    .mergeMap(payload =>
      this.tellerService.confirmReceipt(payload.tellerCode, payload.tellerTransactionIdentifier)
        .map(() => new transactionActions.PrintReceiptSuccessAction())
        .catch((error) => of(new transactionActions.PrintReceiptFailAction(error)))
    );

  @Effect()
  printDepositCheque$: Observable<Action> = this.actions$
    .ofType(transactionActions.PRINT_CHEQUE_FOR_DEPOSIT)
    .map((action: transactionActions.PrintChequeForDepositAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.findProduct(payload.productShortName, payload.type)
        .map(product => this.mapPrintChequePayload(payload, product.currency.code))
        .switchMap(chequeInfo => this.printCheque(chequeInfo))
    );

  @Effect()
  printLoanCheque$: Observable<Action> = this.actions$
    .ofType(transactionActions.PRINT_CHEQUE_FOR_LOAN)
    .map((action: transactionActions.PrintChequeForLoanAction) => action.payload)
    .mergeMap(payload =>
      this.consumerLoanService.findProduct(payload.productShortName)
        .map(product => this.mapPrintChequePayload(payload, product.currency.code))
        .switchMap(chequeInfo => this.printCheque(chequeInfo))
    );

  private mapPrintChequePayload(payload: PrintChequePayload, currencyCode: string): PrintChequeInfo {
    return {
      branch: payload.branch,
      chequeNumber: payload.chequeNumber,
      chequeDate: payload.chequeDate,
      customerNumber: payload.customerNumber,
      transactionType: payload.transactionType,
      amount: payload.amount,
      tellerNumber: payload.tellerNumber,
      payeeName: payload.payeeName,
      currencyCode
    };
  }

  private printCheque(chequeInfo: PrintChequeInfo): Observable<Action> {
    return this.chequeService.printCheque(chequeInfo)
      .map(() => new transactionActions.PrintChequeSuccessAction())
      .catch((error) => of(new transactionActions.PrintChequeFailAction(error)));
  }

  constructor(private actions$: Actions, private tellerService: TellerService, private chequeService: ChequeService,
              private consumerLoanService: ConsumerLoanService, private depositService: DepositAccountService) {}
}
