/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Action} from '@ngrx/store';
import {type} from '../../core/store/util';
import {TellerTransaction, TransactionType} from '../../core/services/teller/domain/teller-transaction.model';
import {RoutePayload} from '../../core/store/util/route-payload';
import {Type} from '../../core/services/depositAccount/domain/type.model';
import {TellerTransactionCosts} from '../../core/services/teller/domain/teller-transaction-costs.model';

export const CREATE_TRANSACTION = type('[Teller Customer] Create Transaction');
export const CREATE_TRANSACTION_SUCCESS = type('[Teller Customer] Create Transaction Success');
export const CREATE_TRANSACTION_FAIL = type('[Teller Customer] Create Transaction Fail');

export const CONFIRM_TRANSACTION = type('[Teller Customer] Confirm Transaction');
export const CONFIRM_TRANSACTION_SUCCESS = type('[Teller Customer] Confirm Transaction Success');
export const CONFIRM_TRANSACTION_FAIL = type('[Teller Customer] Confirm Transaction Fail');

export const RESET_TRANSACTION_FORM = type('[Teller Customer] Reset Transaction Form');

export const PRINT_CHEQUE_FOR_LOAN = type('[Teller Customer] Print Cheque For Loan');
export const PRINT_CHEQUE_FOR_DEPOSIT = type('[Teller Customer] Print Cheque For Deposit');
export const PRINT_CHEQUE_SUCCESS = type('[Teller Customer] Print Cheque Success');
export const PRINT_CHEQUE_FAIL = type('[Teller Customer] Print Cheque Fail');

export const PRINT_RECEIPT = type('[Teller Customer] Print Receipt');
export const PRINT_RECEIPT_SUCCESS = type('[Teller Customer] Print Receipt Success');
export const PRINT_RECEIPT_FAIL = type('[Teller Customer] Print Receipt Fail');

export interface CreateTransactionPayload {
  tellerCode: string;
  tellerTransaction: TellerTransaction;
}

export interface ConfirmTransactionPayload extends RoutePayload {
  tellerCode: string;
  tellerTransactionIdentifier: string;
  command: string;
  chargesIncluded?: boolean;
  voucherNumber?: string;
}

export interface PrintChequePayload {
  branch: string;
  chequeNumber: string;
  chequeDate: string;
  customerNumber: string;
  transactionType: TransactionType;
  amount: string;
  tellerNumber: string;
  payeeName: string;
  productShortName: string;
}

export interface PrintDepositChequePayload extends PrintChequePayload {
  type: Type;
}

export interface PrintReceiptPayload {
  tellerCode: string;
  tellerTransactionIdentifier: string;
}

export class CreateTransactionAction implements Action {
  readonly type = CREATE_TRANSACTION;

  constructor(public payload: CreateTransactionPayload) {}
}

export class CreateTransactionSuccessAction implements Action {
  readonly type = CREATE_TRANSACTION_SUCCESS;

  constructor(public payload: TellerTransactionCosts) {}
}

export class CreateTransactionFailAction implements Action {
  readonly type = CREATE_TRANSACTION_FAIL;

  constructor(public payload: Error) {}
}

export class ConfirmTransactionAction implements Action {
  readonly type = CONFIRM_TRANSACTION;

  constructor(public payload: ConfirmTransactionPayload) {}
}

export class ConfirmTransactionSuccessAction implements Action {
  readonly type = CONFIRM_TRANSACTION_SUCCESS;

  constructor(public payload: ConfirmTransactionPayload) {}
}

export class ConfirmTransactionFailAction implements Action {
  readonly type = CONFIRM_TRANSACTION_FAIL;

  constructor(public payload: Error) {}
}

export class ResetTransactionFormAction implements Action {
  readonly type = RESET_TRANSACTION_FORM;

  constructor() {}
}

export class PrintChequeForDepositAction implements Action {
  readonly type = PRINT_CHEQUE_FOR_DEPOSIT;

  constructor(public payload: PrintDepositChequePayload) {}
}

export class PrintChequeForLoanAction implements Action {
  readonly type = PRINT_CHEQUE_FOR_LOAN;

  constructor(public payload: PrintChequePayload) {}
}

export class PrintChequeSuccessAction implements Action {
  readonly type = PRINT_CHEQUE_SUCCESS;

  constructor() {}
}

export class PrintChequeFailAction implements Action {
  readonly type = PRINT_CHEQUE_FAIL;

  constructor(public payload: Error) {}
}

export class PrintReceiptAction implements Action {
  readonly type = PRINT_RECEIPT;

  constructor(public payload: PrintReceiptPayload) {}
}

export class PrintReceiptSuccessAction implements Action {
  readonly type = PRINT_RECEIPT_SUCCESS;

  constructor() {}
}

export class PrintReceiptFailAction implements Action {
  readonly type = PRINT_RECEIPT_FAIL;

  constructor(public payload: Error) {}
}

export type Actions
  = CreateTransactionAction
  | CreateTransactionSuccessAction
  | CreateTransactionFailAction
  | ConfirmTransactionAction
  | ConfirmTransactionSuccessAction
  | ConfirmTransactionFailAction
  | ResetTransactionFormAction
  | PrintChequeForDepositAction
  | PrintChequeForLoanAction
  | PrintChequeSuccessAction
  | PrintChequeFailAction
  | PrintReceiptAction
  | PrintReceiptSuccessAction
  | PrintReceiptFailAction;
