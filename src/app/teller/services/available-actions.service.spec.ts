/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {AvailableActionService} from './available-actions.service';
import {DepositAccountService} from '../../core/services/depositAccount/deposit-account.service';
import {Observable} from 'rxjs/Observable';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {CustomerLoanService} from '../../core/services/loan/customer-loan.service';
import {LoanAgreementOutline} from '../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {State} from '../../core/services/loan/domain/agreement/loan-agreement.model';

describe('AvailableActionService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AvailableActionService,
        {
          provide: DepositAccountService,
          useValue: jasmine.createSpyObj('depositService', ['fetchPossibleTransactionsForCustomer'])
        },
        {
          provide: CustomerLoanService,
          useValue: jasmine.createSpyObj('customerLoanService', ['fetchAllLoanAgreements'])
        }
      ]
    });
  });

  function setupDeposit(transactionTypes: string[]) {
    const depositService = TestBed.get(DepositAccountService);
    depositService.fetchPossibleTransactionsForCustomer.and.returnValue(Observable.of(transactionTypes));
  }

  function setupLoan(outlines: LoanAgreementOutline[]) {
    const portfolioService = TestBed.get(CustomerLoanService);
    portfolioService.fetchAllLoanAgreements.and.returnValue(Observable.of(outlines));
  }

  function mockAgreement(state: State): LoanAgreementOutline {
    return {
      loanType: 'CONSUMER',
      loanAccount: 'test',
      productShortName: 'test',
      balance: '0.00',
      term: null,
      guarantor: false,
      hasPendingAssessments: false,
      hasCollateral: false,
      nextPayment: 'test',
      lastPayment: false,
      inRestructuring: false,
      amountToDisburse: '0.00',
      state
    };
  }

  it('should merge deposit, loan actions', fakeAsync(() => {
    setupDeposit(['ACCC']);

    setupLoan([mockAgreement('DISBURSED'), mockAgreement('PENDING')]);

    const actionService = TestBed.get(AvailableActionService);

    let result = null;

    actionService.getAvailableActions('test').subscribe(_result => result = _result);

    tick();

    // 1 deposit, 1 case
    expect(result.length).toBe(2);
  }));

  it('output deposit actions when deposit actions found', fakeAsync(() => {
    setupDeposit(['ACCC', 'CWDL']);

    const actionService = TestBed.get(AvailableActionService);

    let result = null;

    actionService.getAvailableDepositActions('test').subscribe(_result => result = _result);

    tick();

    expect(result.length).toBe(2);
  }));

  it('not output any deposit actions when no deposit actions found', fakeAsync(() => {
    setupDeposit([]);

    const actionService = TestBed.get(AvailableActionService);

    let result = null;

    actionService.getAvailableDepositActions('test').subscribe(_result => result = _result);

    tick();

    expect(result).toEqual([]);
  }));

  it('should output actions when active cases found', fakeAsync(() => {
    setupLoan([mockAgreement('DISBURSED')]);

    const actionService = TestBed.get(AvailableActionService);

    let result = null;

    actionService.getAvailableLoanActions('test').subscribe(_result => result = _result);

    tick();

    expect(result.length).toEqual(1);
  }));

  describe('should not output any loan actions', () => {
    it('when no cases found', fakeAsync(() => {
      setupLoan([]);

      const actionService = TestBed.get(AvailableActionService);

      let result = null;

      actionService.getAvailableLoanActions('test').subscribe(_result => result = _result);

      tick();

      expect(result).toEqual([]);
    }));

    it('when no repayable or disbursable agreements found', fakeAsync(() => {
      setupLoan([
        mockAgreement('PENDING'),
        mockAgreement('UNDERWRITING'),
        mockAgreement('DECLINED'),
        mockAgreement('REPAID')
      ]);

      const actionService = TestBed.get(AvailableActionService);

      let result = null;

      actionService.getAvailableLoanActions('test').subscribe(_result => result = _result);

      tick();

      expect(result).toEqual([]);
    }));
  });

});
