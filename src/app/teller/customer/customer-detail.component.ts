/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import * as fromTeller from '../store';
import {Customer} from '../../core/services/customer/domain/customer.model';
import {Observable} from 'rxjs/Observable';
import {CustomerService} from '../../core/services/customer/customer.service';
import {Action, AvailableActionService} from '../services/available-actions.service';
import {LoadAllDepositProductsAction, LoadAllLoanProductsAction} from '../store/teller.actions';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';
import {CustomerAccountOutline} from '../../shared/customer-account-list/domain/customer-account-outline.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class TellerCustomerDetailComponent implements OnDestroy {

  private loadDepositProductsSubscription: Subscription;
  private loadLoanProductsSubscription: Subscription;

  portrait$: Observable<Blob | {}>;
  customer$: Observable<Customer>;
  mainActions$: Observable<Action[]>;
  secondaryActions$: Observable<Action[]>;
  depositOutlines$: Observable<CustomerAccountOutline[]>;
  loanOutlines$: Observable<CustomerAccountOutline[]>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromTeller.State>, private customerService: CustomerService,
              private actionService: AvailableActionService) {
    this.customer$ = store.select(fromTeller.getTellerSelectedCustomer)
      .filter(customer => !!customer);

    this.portrait$ = this.customer$
      .flatMap(customer => this.customerService.getPortrait(customer.identifier));

    this.depositOutlines$ = store.select(fromTeller.getDepositAccountOutlines);
    this.loanOutlines$ = store.select(fromTeller.getLoanAccountOutlines);

    const availableActions = this.customer$
      .mergeMap(customer => this.actionService.getAvailableActions(customer.identifier))
      .share();

    this.mainActions$ = availableActions
      .map((actions: Action[]) => actions.filter(action => this.hasPrimaryAction(action)));

    this.secondaryActions$ = availableActions
      .map((actions: Action[]) => actions.filter(action => !this.hasPrimaryAction(action)));

    this.loadDepositProductsSubscription = this.customer$
      .map(customer => new LoadAllDepositProductsAction(customer.identifier))
      .subscribe(this.store);

    this.loadLoanProductsSubscription = this.customer$
      .map(customer => new LoadAllLoanProductsAction(customer.identifier))
      .subscribe(this.store);
  }

  hasPrimaryAction(action: Action): boolean {
    return action.transactionType === 'CDPT'
      || action.transactionType === 'CWDL'
      || action.transactionType === 'PPAY'
      || action.transactionType === 'CDIS'
      || action.transactionType === 'ACCO';
  }

  ngOnDestroy(): void {
    this.loadDepositProductsSubscription.unsubscribe();
    this.loadLoanProductsSubscription.unsubscribe();
  }

  goToTransaction(link: string): void {
    this.router.navigate([link], { relativeTo: this.route });
  }
}
