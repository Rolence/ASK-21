/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Observable} from 'rxjs/Observable';
import {TdStepComponent} from '@covalent/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CreateTransactionEvent} from '../../domain/create-transaction-event.model';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {ConfirmTransactionEvent} from '../../domain/confirm-transaction-event.model';
import {PaymentType} from '../../../../../core/services/teller/domain/teller-transaction.model';
import {ProductInstanceOutline} from '../../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {TellerTransactionCosts} from '../../../../../core/services/teller/domain/teller-transaction-costs.model';
import {ChequeFormService} from '../cheque-form/cheque-form.service';
import {defaultPaymentOptions} from '../../domain/payment-type.options';
import {FimsValidators} from '../../../../../core/validator/validators';

@Component({
  selector: 'aten-teller-deposit-form',
  templateUrl: './deposit-form.component.html'
})
export class TellerDepositFormComponent implements OnInit, OnChanges {

  _chargesIncludedDisabled: boolean;
  form: FormGroup;
  chequeForm: FormGroup;
  paymentOptions = defaultPaymentOptions.filter(option => option.type !== 'TRANSFER');
  micrResolutionError$: Observable<boolean>;
  currentBalance: number;
  voucherNumber: string;
  chargesIncluded = true;

  @Input('transactionCosts') transactionCosts: TellerTransactionCosts;
  @Input('payee') payee: string;
  @Input('productInstances') productInstances: ProductInstanceOutline[];
  @Input('transactionConfirmed') transactionConfirmed: boolean;
  @Input('minimumBalance') minimumBalance: number;
  @Input('chargesIncludedDisabled') set chargesIncludedDisabled(chargesIncludedDisabled: boolean) {
    this._chargesIncludedDisabled = chargesIncludedDisabled;
  }

  @Output('onCreateTransaction') onCreateTransaction = new EventEmitter<CreateTransactionEvent>();
  @Output('onConfirmTransaction') onConfirmTransaction = new EventEmitter<ConfirmTransactionEvent>();
  @Output('onCancelTransaction') onCancelTransaction = new EventEmitter<string>();
  @Output('onCancel') onCancel = new EventEmitter<void>();
  @Output('onPrintReceipt') onPrintReceipt = new EventEmitter<string>();
  @Output('onProductInstanceChanged') onProductInstanceChanged = new EventEmitter<ProductInstanceOutline>();

  @ViewChild('transactionStep') transactionStep: TdStepComponent;
  @ViewChild('confirmationStep') confirmationStep: TdStepComponent;

  constructor(private formBuilder: FormBuilder, private chequeFormService: ChequeFormService) {
    this.chequeFormService.init();
    this.chequeForm = chequeFormService.formGroup;

    this.form = this.formBuilder.group({
      productInstance: ['', Validators.required],
      amount: ['', [Validators.required, FimsValidators.greaterThanValue(0)]],
      paymentType: ['CASH', [Validators.required]],
    });

    this.form.get('paymentType').valueChanges
      .startWith('CASH')
      .subscribe(paymentType => this.togglePaymentType(paymentType));

    this.form.get('productInstance').valueChanges
      .subscribe(productInstance => this.toggleProductInstance(productInstance));
  }

  private togglePaymentType(paymentType: PaymentType): void {
    if (paymentType === 'CHEQUE') {
      this.chequeFormService.enable();
      this.chargesIncluded = false;
    } else {
      this.chequeFormService.disable();
      this.chargesIncluded = true;
    }
  }

  private toggleProductInstance(productInstance: ProductInstanceOutline): void {
    this.currentBalance = parseFloat(productInstance.balance);
    this.onProductInstanceChanged.emit(productInstance);
  }

  expandMICR(): void {
    this.micrResolutionError$ = this.chequeFormService.expandMICR();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transactionCosts && this.transactionCosts) {
      this.confirmationStep.open();
    }

    if (changes.payee && this.payee) {
      this.chequeFormService.formGroup.get('payee').setValue(this.payee);
    }
  }

  ngOnInit(): void {
    this.transactionStep.open();
  }

  createTransaction(): void {
    const productInstance: ProductInstanceOutline = this.form.get('productInstance').value;

    const formData: CreateTransactionEvent = {
      productIdentifier: productInstance.productShortCode,
      accountIdentifier: productInstance.accountIdentifier,
      paymentType: this.form.get('paymentType').value,
      amount: this.form.get('amount').value,
      cheque: this.chequeFormService.getCheque(this.form.get('amount').value)
    };

    this.onCreateTransaction.emit(formData);
  }

  confirmTransaction(chargesIncluded: boolean, voucherNumber: string): void {
    this.onConfirmTransaction.emit({
      chargesIncluded,
      voucherNumber,
      tellerTransactionIdentifier: this.transactionCosts.tellerTransactionIdentifier
    });
  }

  cancelTransaction(): void {
    this.onCancelTransaction.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  printReceipt(): void {
    this.onPrintReceipt.emit(this.transactionCosts.tellerTransactionIdentifier);
  }

  cancel(): void {
    this.onCancel.emit();
  }

  get createTransactionDisabled(): boolean {
    return this.form.invalid || this.chequeForm.invalid || this.transactionCreated;
  }

  get confirmTransactionDisabled(): boolean {
    return !this.transactionCreated || this.transactionConfirmed;
  }

  get printReceiptDisabled(): boolean {
    return !this.transactionConfirmed;
  }

  get transactionCreated(): boolean {
    return !!this.transactionCosts;
  }

  get chargesIncludedDisabled(): boolean {
    const paymentType: PaymentType = this.form.get('paymentType').value;
    if (paymentType === 'CHEQUE') {
      return true;
    }

    return this._chargesIncludedDisabled;
  }

}
