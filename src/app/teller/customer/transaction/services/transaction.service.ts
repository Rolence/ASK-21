/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {CreateTransactionEvent} from '../domain/create-transaction-event.model';
import {ConfirmTransactionEvent} from '../domain/confirm-transaction-event.model';
import * as fromTeller from '../../../store';
import {ConfirmTransactionAction, CreateTransactionAction, PrintReceiptAction} from '../../../store/transaction.actions';
import {Teller} from '../../../../core/services/teller/domain/teller.model';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute} from '@angular/router';
import {TransactionType} from '../../../../core/services/teller/domain/teller-transaction.model';
import {Store} from '@ngrx/store';

@Injectable()
export class TransactionService {

  private teller$: Observable<Teller>;

  constructor(private store: Store<fromTeller.State>) {
    this.teller$ = this.store.select(fromTeller.getAuthenticatedTeller);
  }

  createTransaction(event: CreateTransactionEvent, transactionType: TransactionType): void {
    this.store.select(fromTeller.getCreateTransactionData)
      .take(1)
      .map(data => new CreateTransactionAction({
        tellerCode: data.teller.code,
        tellerTransaction: {
          customerIdentifier: data.customer.identifier,
          productIdentifier: event.productIdentifier,
          customerAccountIdentifier: event.accountIdentifier,
          targetAccountIdentifier: event.targetAccountIdentifier,
          amount: event.amount,
          paymentType: event.paymentType,
          clerk: data.username,
          cheque: event.cheque,
          transactionType,
          transactionDate: new Date().toISOString()
        }
      }))
      .subscribe(this.store);
  }

  confirmTransaction(event: ConfirmTransactionEvent, activatedRoute: ActivatedRoute): void {
    this.teller$
      .take(1)
      .map(teller => new ConfirmTransactionAction({
        tellerCode: teller.code,
        tellerTransactionIdentifier: event.tellerTransactionIdentifier,
        command: 'CONFIRM',
        chargesIncluded: event.chargesIncluded,
        voucherNumber: event.voucherNumber,
        activatedRoute
      }))
      .subscribe(this.store);
  }

  cancelTransaction(tellerTransactionIdentifier: string, activatedRoute: ActivatedRoute): void {
    this.teller$
      .take(1)
      .map(teller => new ConfirmTransactionAction({
        tellerCode: teller.code,
        tellerTransactionIdentifier,
        command: 'CANCEL',
        activatedRoute
      }))
      .subscribe(this.store);
  }

  printReceipt(tellerTransactionIdentifier: string): void {
    this.teller$
      .take(1)
      .map(teller => new PrintReceiptAction({
        tellerCode: teller.code,
        tellerTransactionIdentifier
      }))
      .subscribe(this.store);
  }

}
