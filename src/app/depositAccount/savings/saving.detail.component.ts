/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, OnInit} from '@angular/core';
import * as fromDepositAccounts from '../store/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {DeleteProductDefinitionAction, ExecuteCommandAction} from '../store/savings/saving.actions';
import {Store} from '@ngrx/store';
import {SavingsProductDefinition} from '../../core/services/depositAccount/domain/definition/savings-product-definition.model';
import {DialogService} from '../../core/services/dialog/dialog.service';

@Component({
  templateUrl: './saving.detail.component.html'
})
export class DepositSavingDetailComponent implements OnInit {

  saving$: Observable<SavingsProductDefinition>;
  canDelete$: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromDepositAccounts.State>, private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.saving$ = this.store.select(fromDepositAccounts.getSelectedSavingProduct)
      .filter(saving => !!saving);

    this.canDelete$ = this.store.select(fromDepositAccounts.canDeleteSavingProduct);
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this product?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE PRODUCT',
    });
  }

  deleteProduct(saving: SavingsProductDefinition): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => this.store.dispatch(new DeleteProductDefinitionAction({
        saving,
        activatedRoute: this.route
      })));
  }

  enableProduct(definitionId: string): void {
    this.store.dispatch(new ExecuteCommandAction({
      definitionId,
      command: {
        action: 'ACTIVATE'
      },
      activatedRoute: this.route
    }));
  }

  disableProduct(definitionId: string): void {
    this.store.dispatch(new ExecuteCommandAction({
      definitionId,
      command: {
        action: 'DEACTIVATE'
      },
      activatedRoute: this.route
    }));
  }

  editProduct(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
