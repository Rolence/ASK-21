/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Observable} from 'rxjs/Observable';
import {MatDialog} from '@angular/material';
import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ITdDataTableColumn} from '@covalent/core';
import * as fromDepositAccounts from '../store';
import {Store} from '@ngrx/store';
import {DisplayFimsNumber} from '../../shared/number/fims-number.pipe';
import {TableData} from '../../shared/data-table/data-table.component';
import {DepositAccountService} from '../../core/services/depositAccount/deposit-account.service';
import {NotificationService} from '../../core/services/notification/notification.service';
import {DownloadTransactionsDialogComponent} from '../../shared/ach/download-transactions-dialog.component';

@Component({
  templateUrl: './ach-transaction.list.component.html',
  providers: [DisplayFimsNumber, DatePipe]
})
export class DepositACHTransactionListComponent {

  transactionData$: Observable<TableData>;

  columns: ITdDataTableColumn[] = [
    {name: 'payee', label: 'Payee'},
    {name: 'accountNumber', label: 'Account no.'},
    {name: 'createdOn', label: 'Created on', format: (v: any) => this.datePipe.transform(v, 'short')},
    {name: 'amount', label: 'Amount', format: (v: any) => this.displayFimsNumber.transform(v) }
  ];

  constructor(private depositService: DepositAccountService, private store: Store<fromDepositAccounts.State>,
              private dialog: MatDialog, private notificationService: NotificationService,
              private displayFimsNumber: DisplayFimsNumber, private datePipe: DatePipe) {
    this.transactionData$ = this.depositService.fetchACHTransactions()
      .map(transactions => ({
        data: transactions,
        totalElements: transactions.length,
        totalPages: 1
      }))
      .share();
  }

  downloadTransactions(): void {
    this.openDialog()
      .filter(bankAccount => !!bankAccount)
      .mergeMap(bankAccount =>
        this.depositService.downloadACHTransactions(bankAccount)
          .do(() => this.notificationService.sendMessage('Transactions successfully processed'))
          .catch(() => {
            this.notificationService.sendAlert('There was an issue processing the transactions');
            return Observable.of(false);
          })
      ).subscribe(() => {
          this.transactionData$ = Observable.of({
            data: [],
            totalElements: 0,
            totalPages: 0
          });
    });
  }

  private openDialog(): Observable<string | undefined> {
    return this.dialog.open(DownloadTransactionsDialogComponent, {
      width: '40%',
      height: '40%'
    }).afterClosed();
  }

}
