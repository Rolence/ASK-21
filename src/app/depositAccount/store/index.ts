/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createFormReducer, FormState, getFormError} from '../../core/store/util/form.reducer';
import {createResourceReducer, getResourceLoadedAt, getResourceSelected, ResourceState} from '../../core/store/util/resource.reducer';

import * as fromRoot from '../../core/store';
import * as fromDepositSearch from './search/deposit-search.reducer';
import {getEntities} from './search/deposit-search.reducer';
import * as fromDividends from './dividends/dividends.reducer';
import * as fromTerms from './terms/terms.reducer';
import * as fromCheckings from './checkings/checkings.reducer';
import * as fromSavings from './savings/savings.reducer';
import * as fromShares from './shares/shares.reducer';

import {createSelector} from 'reselect';
import {InjectionToken} from '@angular/core';
import {FimsPermission} from '../../core/services/security/authz/fims-permission.model';
import {ShareProductDefinition} from '../../core/services/depositAccount/domain/definition/share-product-definition.model';
import {SavingsProductDefinition} from '../../core/services/depositAccount/domain/definition/savings-product-definition.model';
import {CheckingProductDefinition} from '../../core/services/depositAccount/domain/definition/checking-product-definition.model';
import {TermProductDefinition} from '../../core/services/depositAccount/domain/definition/term-product-definition.model';

export interface DepositState {
  termProducts: ResourceState;
  checkingProducts: ResourceState;
  savingProducts: ResourceState;
  shareProducts: ResourceState;
  depositProductForm: FormState;
  depositProductSearch: fromDepositSearch.State;
  depositProductDividends: fromDividends.State;
}

export const reducers: ActionReducerMap<DepositState> = {
  termProducts: createResourceReducer('Deposit Term', fromTerms.reducer, 'shortCode'),
  checkingProducts: createResourceReducer('Deposit Checking', fromCheckings.reducer, 'shortCode'),
  savingProducts: createResourceReducer('Deposit Saving', fromSavings.reducer, 'shortCode'),
  shareProducts: createResourceReducer('Deposit Share', fromShares.reducer, 'shortCode'),
  depositProductForm: createFormReducer('Deposit Product Definition'),
  depositProductSearch: fromDepositSearch.reducer,
  depositProductDividends: fromDividends.reducer,
};

export const reducerToken = new InjectionToken<ActionReducerMap<DepositState>>('Deposit reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export interface State extends fromRoot.State {
  deposit: DepositState;
}

export const selectDeposit = createFeatureSelector<DepositState>('deposit');

/**
 * Form
 */
export const getProductFormState = createSelector(selectDeposit, (state: DepositState) => state.depositProductForm);
export const getProductFormError = createSelector(getProductFormState, getFormError);

/**
 * Product search selector
 */
export const getProductSearchState = createSelector(selectDeposit, (state: DepositState) => state.depositProductSearch);

export const getSearchProducts = createSelector(getProductSearchState, getEntities);

const getHasDeletePermission = createSelector(fromRoot.getPermissions, (permissions: FimsPermission[]) => {
  return permissions.filter(permission =>
    permission.id === 'deposit_definitions' &&
    permission.accessLevel === 'DELETE'
  ).length > 0;
});

/**
 * Term Products
 */
export const getTermProductsState = createSelector(selectDeposit, (state: DepositState) => state.termProducts);

export const getTermProductsLoadedAt = createSelector(getTermProductsState, getResourceLoadedAt);
export const getSelectedTermProduct = createSelector(getTermProductsState, getResourceSelected);

export const canDeleteTermProduct = createSelector(getHasDeletePermission, getSelectedTermProduct,
  (hasDeletePermission, selectedProduct: TermProductDefinition) => {
    if (!selectedProduct) {
      return false;
    }

    return hasDeletePermission && !selectedProduct.inUse;
  });

/**
 * Checking Products
 */
export const getCheckingProductsState = createSelector(selectDeposit, (state: DepositState) => state.checkingProducts);

export const getCheckingProductsLoadedAt = createSelector(getCheckingProductsState, getResourceLoadedAt);
export const getSelectedCheckingProduct = createSelector(getCheckingProductsState, getResourceSelected);

export const canDeleteCheckingProduct = createSelector(getHasDeletePermission, getSelectedCheckingProduct,
  (hasDeletePermission, selectedProduct: CheckingProductDefinition) => {
    if (!selectedProduct) {
      return false;
    }

    return hasDeletePermission && !selectedProduct.inUse;
  });

/**
 * Saving Products
 */
export const getSavingProductsState = createSelector(selectDeposit, (state: DepositState) => state.savingProducts);

export const getSavingProductsLoadedAt = createSelector(getSavingProductsState, getResourceLoadedAt);
export const getSelectedSavingProduct = createSelector(getSavingProductsState, getResourceSelected);

export const canDeleteSavingProduct = createSelector(getHasDeletePermission, getSelectedSavingProduct,
  (hasDeletePermission, selectedProduct: SavingsProductDefinition) => {
    if (!selectedProduct) {
      return false;
    }

    return hasDeletePermission && !selectedProduct.inUse;
  });

/**
 * Share Products
 */
export const getShareProductsState = createSelector(selectDeposit, (state: DepositState) => state.shareProducts);

export const getShareProductsLoadedAt = createSelector(getShareProductsState, getResourceLoadedAt);
export const getSelectedShareProduct = createSelector(getShareProductsState, getResourceSelected);

export const canDeleteShareProduct = createSelector(getHasDeletePermission, getSelectedShareProduct,
  (hasDeletePermission, selectedProduct: ShareProductDefinition) => {
    if (!selectedProduct) {
      return false;
    }

    return hasDeletePermission && !selectedProduct.inUse;
  });

/**
 * Dividends
 */
export const getProductDividendsState = createSelector(selectDeposit, (state: DepositState) => state.depositProductDividends);

export const getDividends = createSelector(getProductDividendsState, fromDividends.getDividends);
