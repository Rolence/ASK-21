/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {of} from 'rxjs/observable/of';
import * as termActions from '../term.actions';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';

@Injectable()
export class DepositTermProductApiEffects {

  @Effect()
  createProduct$: Observable<Action> = this.actions$
    .ofType(termActions.CREATE)
    .map((action: termActions.CreateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.createTermProduct(payload.term)
        .map(() => new termActions.CreateProductDefinitionSuccessAction({
          resource: payload.term,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new termActions.CreateProductDefinitionFailAction(error)))
    );

  @Effect()
  updateProduct$: Observable<Action> = this.actions$
    .ofType(termActions.UPDATE)
    .map((action: termActions.UpdateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.changeTermProduct(payload.term.shortCode, payload.term)
        .map(() => new termActions.UpdateProductDefinitionSuccessAction({
          resource: payload.term,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new termActions.UpdateProductDefinitionFailAction(error)))
    );

  @Effect()
  deleteProduct$: Observable<Action> = this.actions$
    .ofType(termActions.DELETE)
    .map((action: termActions.DeleteProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.deleteTermProduct(payload.term.shortCode)
        .map(() => new termActions.DeleteProductDefinitionSuccessAction({
          resource: payload.term,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new termActions.DeleteProductDefinitionFailAction(error)))
    );

  @Effect()
  executeCommand$: Observable<Action> = this.actions$
    .ofType(termActions.EXECUTE_COMMAND)
    .map((action: termActions.ExecuteCommandAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.processTermCommand(payload.definitionId, payload.command)
        .map(() => new termActions.ExecuteCommandSuccessAction(payload))
        .catch((error) => of(new termActions.ExecuteCommandFailAction(error)))
    );

  constructor(private actions$: Actions, private depositService: DepositAccountService) { }
}
