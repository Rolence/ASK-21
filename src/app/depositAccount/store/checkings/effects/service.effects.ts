/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {of} from 'rxjs/observable/of';
import * as checkingActions from '../checking.actions';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';

@Injectable()
export class DepositCheckingProductApiEffects {

  @Effect()
  createProduct$: Observable<Action> = this.actions$
    .ofType(checkingActions.CREATE)
    .map((action: checkingActions.CreateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.createCheckingProduct(payload.checking)
        .map(() => new checkingActions.CreateProductDefinitionSuccessAction({
          resource: payload.checking,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new checkingActions.CreateProductDefinitionFailAction(error)))
    );

  @Effect()
  updateProduct$: Observable<Action> = this.actions$
    .ofType(checkingActions.UPDATE)
    .map((action: checkingActions.UpdateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.changeCheckingProduct(payload.checking.shortCode, payload.checking)
        .map(() => new checkingActions.UpdateProductDefinitionSuccessAction({
          resource: payload.checking,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new checkingActions.UpdateProductDefinitionFailAction(error)))
    );

  @Effect()
  deleteProduct$: Observable<Action> = this.actions$
    .ofType(checkingActions.DELETE)
    .map((action: checkingActions.DeleteProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.deleteCheckingProduct(payload.checking.shortCode)
        .map(() => new checkingActions.DeleteProductDefinitionSuccessAction({
          resource: payload.checking,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new checkingActions.DeleteProductDefinitionFailAction(error)))
    );

  @Effect()
  executeCommand$: Observable<Action> = this.actions$
    .ofType(checkingActions.EXECUTE_COMMAND)
    .map((action: checkingActions.ExecuteCommandAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.processTermCommand(payload.definitionId, payload.command)
        .map(() => new checkingActions.ExecuteCommandSuccessAction(payload))
        .catch((error) => of(new checkingActions.ExecuteCommandFailAction(error)))
    );

  constructor(private actions$: Actions, private depositService: DepositAccountService) { }
}
