/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Action} from '@ngrx/store';
import {
  CreateResourceSuccessPayload,
  DeleteResourceSuccessPayload,
  LoadResourcePayload,
  SelectResourcePayload,
  UpdateResourceSuccessPayload
} from '../../../core/store/util/resource.reducer';
import {RoutePayload} from '../../../core/store/util/route-payload';
import {type} from '../../../core/store/util';
import {ShareProductDefinition} from '../../../core/services/depositAccount/domain/definition/share-product-definition.model';
import {ProductDefinitionCommand} from '../../../core/services/depositAccount/domain/definition/product-definition-command.model';

export const LOAD = type('[Deposit Share] Load');
export const SELECT = type('[Deposit Share] Select');

export const CREATE = type('[Deposit Share] Create');
export const CREATE_SUCCESS = type('[Deposit Share] Create Success');
export const CREATE_FAIL = type('[Deposit Share] Create Fail');

export const UPDATE = type('[Deposit Share] Update');
export const UPDATE_SUCCESS = type('[Deposit Share] Update Success');
export const UPDATE_FAIL = type('[Deposit Share] Update Fail');

export const DELETE = type('[Deposit Share] Delete');
export const DELETE_SUCCESS = type('[Deposit Share] Delete Success');
export const DELETE_FAIL = type('[Deposit Share] Delete Fail');

export const RESET_FORM = type('[Deposit Share] Reset Form');

export const EXECUTE_COMMAND = type('[Deposit Share] Execute Command');
export const EXECUTE_COMMAND_SUCCESS = type('[Deposit Share] Execute Command Success');
export const EXECUTE_COMMAND_FAIL = type('[Deposit Share] Execute Command Fail');

export interface CreateShareRoutePayload extends RoutePayload {
  share: ShareProductDefinition;
}

export interface UpdateShareRoutePayload extends RoutePayload {
  share: ShareProductDefinition;
}

export interface DeleteShareRoutePayload extends RoutePayload {
  share: ShareProductDefinition;
}

export interface ExecuteCommandPayload extends RoutePayload {
  definitionId: string;
  command: ProductDefinitionCommand;
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: LoadResourcePayload) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: SelectResourcePayload) { }
}

export class CreateProductDefinitionAction implements Action {
  readonly type = CREATE;

  constructor(public payload: CreateShareRoutePayload) { }
}

export class CreateProductDefinitionSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: CreateResourceSuccessPayload) { }
}

export class CreateProductDefinitionFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: Error) { }
}

export class UpdateProductDefinitionAction implements Action {
  readonly type = UPDATE;

  constructor(public payload: UpdateShareRoutePayload) { }
}

export class UpdateProductDefinitionSuccessAction implements Action {
  readonly type = UPDATE_SUCCESS;

  constructor(public payload: UpdateResourceSuccessPayload) { }
}

export class UpdateProductDefinitionFailAction implements Action {
  readonly type = UPDATE_FAIL;

  constructor(public payload: Error) { }
}

export class DeleteProductDefinitionAction implements Action {
  readonly type = DELETE;

  constructor(public payload: DeleteShareRoutePayload) { }
}

export class DeleteProductDefinitionSuccessAction implements Action {
  readonly type = DELETE_SUCCESS;

  constructor(public payload: DeleteResourceSuccessPayload) { }
}

export class DeleteProductDefinitionFailAction implements Action {
  readonly type = DELETE_FAIL;

  constructor(public payload: Error) { }
}

export class ExecuteCommandAction implements Action {
  readonly type = EXECUTE_COMMAND;

  constructor(public payload: ExecuteCommandPayload) { }
}

export class ExecuteCommandSuccessAction implements Action {
  readonly type = EXECUTE_COMMAND_SUCCESS;

  constructor(public payload: ExecuteCommandPayload) { }
}

export class ExecuteCommandFailAction implements Action {
  readonly type = EXECUTE_COMMAND_FAIL;

  constructor(public payload: Error) { }
}

export type Actions
  = LoadAction
  | SelectAction
  | CreateProductDefinitionAction
  | CreateProductDefinitionSuccessAction
  | CreateProductDefinitionFailAction
  | UpdateProductDefinitionAction
  | UpdateProductDefinitionSuccessAction
  | UpdateProductDefinitionFailAction
  | DeleteProductDefinitionAction
  | DeleteProductDefinitionSuccessAction
  | DeleteProductDefinitionFailAction
  | ExecuteCommandAction
  | ExecuteCommandSuccessAction
  | ExecuteCommandFailAction;
