/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {of} from 'rxjs/observable/of';
import * as shareActions from '../share.actions';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';

@Injectable()
export class DepositShareProductApiEffects {

  @Effect()
  createProduct$: Observable<Action> = this.actions$
    .ofType(shareActions.CREATE)
    .map((action: shareActions.CreateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.createShareProduct(payload.share)
        .map(() => new shareActions.CreateProductDefinitionSuccessAction({
          resource: payload.share,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new shareActions.CreateProductDefinitionFailAction(error)))
    );

  @Effect()
  updateProduct$: Observable<Action> = this.actions$
    .ofType(shareActions.UPDATE)
    .map((action: shareActions.UpdateProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.changeShareProduct(payload.share.shortCode, payload.share)
        .map(() => new shareActions.UpdateProductDefinitionSuccessAction({
          resource: payload.share,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new shareActions.UpdateProductDefinitionFailAction(error)))
    );

  @Effect()
  deleteProduct$: Observable<Action> = this.actions$
    .ofType(shareActions.DELETE)
    .map((action: shareActions.DeleteProductDefinitionAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.deleteShareProduct(payload.share.shortCode)
        .map(() => new shareActions.DeleteProductDefinitionSuccessAction({
          resource: payload.share,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new shareActions.DeleteProductDefinitionFailAction(error)))
    );

  @Effect()
  executeCommand$: Observable<Action> = this.actions$
    .ofType(shareActions.EXECUTE_COMMAND)
    .map((action: shareActions.ExecuteCommandAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.processTermCommand(payload.definitionId, payload.command)
        .map(() => new shareActions.ExecuteCommandSuccessAction(payload))
        .catch((error) => of(new shareActions.ExecuteCommandFailAction(error)))
    );

  constructor(private actions$: Actions, private depositService: DepositAccountService) { }
}
