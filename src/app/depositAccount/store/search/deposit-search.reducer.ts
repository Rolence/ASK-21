/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as searchActions from './search.actions';
import * as checkingActions from '../checkings/checking.actions';
import * as savingActions from '../savings/saving.actions';
import * as shareActions from '../shares/share.actions';
import * as termActions from '../terms/term.actions';
import {ProductOutline} from '../../../core/services/depositAccount/domain/definition/product-outline.model';
import {CheckingProductDefinition} from '../../../core/services/depositAccount/domain/definition/checking-product-definition.model';
import {SavingsProductDefinition} from '../../../core/services/depositAccount/domain/definition/savings-product-definition.model';
import {ShareProductDefinition} from '../../../core/services/depositAccount/domain/definition/share-product-definition.model';
import {TermProductDefinition} from '../../../core/services/depositAccount/domain/definition/term-product-definition.model';
import {ProductDefinitionCommand} from '../../../core/services/depositAccount/domain/definition/product-definition-command.model';

export interface State {
  entities: ProductOutline[];
}

export const initialState: State = {
  entities: [],
};

export function reducer(state = initialState,
                        action: searchActions.Actions
                          | checkingActions.Actions
                          | savingActions.Actions
                          | shareActions.Actions
                          | termActions.Actions): State {

  switch (action.type) {

    case searchActions.LOAD_ALL: {
      return initialState;
    }

    case searchActions.LOAD_ALL_COMPLETE: {
      const entities: ProductOutline[] = action.payload;
      return {
        entities
      };
    }

    case checkingActions.CREATE_SUCCESS: {
      const checkingProduct: CheckingProductDefinition = action.payload.resource;

      const interest = checkingProduct.interest;

      const outline: ProductOutline = {
        type: 'CHECKING',
        shortCode: checkingProduct.shortCode,
        name: checkingProduct.name,
        currency: checkingProduct.currency,
        interest: interest ? interest.amount : null,
        minimumBalance: checkingProduct.minimumBalance,
        active: checkingProduct.active
      };

      return {
        entities: [...state.entities, outline]
      };
    }

    case checkingActions.UPDATE_SUCCESS: {
      const checkingProduct: CheckingProductDefinition = action.payload.resource;
      const entities: ProductOutline[] = state.entities.map(entity => {
        if (entity.shortCode !== checkingProduct.shortCode) {
          return entity;
        }

        const interest = checkingProduct.interest;

        return {
          ...entity,
          name: checkingProduct.name,
          currency: checkingProduct.currency,
          interest: interest ? interest.amount : null,
          minimumBalance: checkingProduct.minimumBalance
        };
      });

      return {
        entities
      };
    }

    case savingActions.CREATE_SUCCESS: {
      const savingProduct: SavingsProductDefinition = action.payload.resource;

      const outline: ProductOutline = {
        type: 'SAVINGS',
        shortCode: savingProduct.shortCode,
        name: savingProduct.name,
        currency: savingProduct.currency,
        interest: savingProduct.interest.amount,
        minimumBalance: savingProduct.minimumBalance,
        active: savingProduct.active
      };

      return {
        entities: [...state.entities, outline]
      };
    }

    case savingActions.UPDATE_SUCCESS: {
      const savingProduct: SavingsProductDefinition = action.payload.resource;
      const entities: ProductOutline[] = state.entities.map(entity => {
        if (entity.shortCode !== savingProduct.shortCode) {
          return entity;
        }

        return {
          ...entity,
          name: savingProduct.name,
          currency: savingProduct.currency,
          interest: savingProduct.interest.amount,
          minimumBalance: savingProduct.minimumBalance
        };
      });

      return {
        entities
      };
    }

    case shareActions.CREATE_SUCCESS: {
      const shareProduct: ShareProductDefinition = action.payload.resource;

      const outline: ProductOutline = {
        type: 'SHARE',
        shortCode: shareProduct.shortCode,
        name: shareProduct.name,
        currency: shareProduct.currency,
        minimumBalance: shareProduct.minimumBalance,
        active: shareProduct.active
      };

      return {
        entities: [...state.entities, outline]
      };
    }

    case shareActions.UPDATE_SUCCESS: {
      const shareProduct: ShareProductDefinition = action.payload.resource;
      const entities: ProductOutline[] = state.entities.map(entity => {
        if (entity.shortCode !== shareProduct.shortCode) {
          return entity;
        }

        return {
          ...entity,
          name: shareProduct.name,
          currency: shareProduct.currency,
          minimumBalance: shareProduct.minimumBalance
        };
      });

      return {
        entities
      };
    }

    case termActions.CREATE_SUCCESS: {
      const termProduct: TermProductDefinition = action.payload.resource;

      const outline: ProductOutline = {
        type: 'TERM',
        shortCode: termProduct.shortCode,
        name: termProduct.name,
        currency: termProduct.currency,
        minimumBalance: termProduct.minimumBalance,
        active: termProduct.active
      };

      return {
        entities: [...state.entities, outline]
      };
    }

    case termActions.UPDATE_SUCCESS: {
      const termProduct: TermProductDefinition = action.payload.resource;
      const entities: ProductOutline[] = state.entities.map(entity => {
        if (entity.shortCode !== termProduct.shortCode) {
          return entity;
        }

        return {
          ...entity,
          name: termProduct.name,
          currency: termProduct.currency,
          minimumBalance: termProduct.minimumBalance
        };
      });

      return {
        entities
      };
    }

    case checkingActions.DELETE_SUCCESS:
    case savingActions.DELETE_SUCCESS:
    case shareActions.DELETE_SUCCESS:
    case termActions.DELETE_SUCCESS: {
      const shortCode = action.payload.resource.shortCode;

      const entities = state.entities.filter(entity => entity.shortCode !== shortCode);

      return {
        entities
      };
    }

    case checkingActions.EXECUTE_COMMAND_SUCCESS:
    case savingActions.EXECUTE_COMMAND_SUCCESS:
    case shareActions.EXECUTE_COMMAND_SUCCESS:
    case termActions.EXECUTE_COMMAND_SUCCESS: {
      const definitionId = action.payload.definitionId;
      const command: ProductDefinitionCommand = action.payload.command;

      const entities: ProductOutline[] = state.entities.map(entity => {
        if (entity.shortCode !== definitionId) {
          return entity;
        }

        return {
          ...entity,
          active: command.action === 'ACTIVATE'
        };
      });

      return {
        entities
      };
    }

    default: {
      return state;
    }
  }
}

export const getEntities = (state: State) => state.entities;
