/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {todayAsDateWithOffset, toShortISOString} from '../../../core/services/domain/date.converter';
import {DepositAccountService} from '../../../core/services/depositAccount/deposit-account.service';
import {TableData, TableFetchRequest} from '../../../shared/data-table/data-table.component';

@Component({
  templateUrl: './dry-run.component.html',
  providers: [DatePipe]
})
export class DryRunComponent {

  form: FormGroup;
  standingOrderData$: Observable<TableData>;

  columns: any[] = [
    {name: 'sequence', label: 'Sequence'},
    {name: 'payee', label: 'Payee'},
    {name: 'accountNumber', label: 'Account number'},
    {name: 'bankIdentifier', label: 'Bank identifier'},
    {name: 'amount', label: 'Amount'},
    {name: 'purpose', label: 'Purpose'},
    {name: 'interval', label: 'Interval'},
    {
      name: 'firstPaymentDate', label: 'First payment date', format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
    {
      name: 'lastPaymentDate', label: 'Last payment date', format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    }
  ];

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder,
              private depositAccountService: DepositAccountService, private datePipe: DatePipe) {
    const today = todayAsDateWithOffset(10);
    this.form = this.formBuilder.group({
      'endDate': [today, [Validators.required]]
    });
  }

  probe(fetchRequest?: TableFetchRequest): void {
    const endDate = toShortISOString(this.form.get('endDate').value);

    this.standingOrderData$ = this.depositAccountService.probe(endDate)
      .map(data => ({
        data,
        totalElements: data.length,
        totalPages: 1
      }));
  }
}
