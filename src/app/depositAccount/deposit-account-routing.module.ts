/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {DepositProductComponent} from './deposit-account.component';
import {DepositShareDetailComponent} from './shares/share.detail.component';
import {DepositShareIndexComponent} from './shares/share.index.component';
import {DepositProductDividendsComponent} from './shares/dividends/dividends.component';
import {CreateDividendFormComponent} from './shares/dividends/form/create.component';
import {TermProductExistsGuard} from './terms/term-product-exists.guard';
import {CheckingProductExistsGuard} from './checkings/checking-product-exists.guard';
import {SavingsProductExistsGuard} from './savings/savings-product-exists.guard';
import {ShareProductExistsGuard} from './shares/share-product-exists.guard';
import {DepositTermDetailComponent} from './terms/term.detail.component';
import {DepositCheckingDetailComponent} from './checkings/checking.detail.component';
import {DepositSavingDetailComponent} from './savings/saving.detail.component';
import {DepositCheckingIndexComponent} from './checkings/checking.index.component';
import {DepositTermIndexComponent} from './terms/term.index.component';
import {DepositSavingIndexComponent} from './savings/saving.index.component';
import {CreateDepositCheckingsProductFormComponent} from './checkings/form/create.form.component';
import {EditDepositCheckingsProductFormComponent} from './checkings/form/edit.form.component';
import {EditDepositSavingsProductFormComponent} from './savings/form/edit.form.component';
import {CreateDepositSavingsProductFormComponent} from './savings/form/create.form.component';
import {CreateDepositSharesProductFormComponent} from './shares/form/create.form.component';
import {EditDepositSharesProductFormComponent} from './shares/form/edit.form.component';
import {CreateDepositTermsProductFormComponent} from './terms/form/create.form.component';
import {EditDepositTermsProductFormComponent} from './terms/form/edit.form.component';
import {DepositProductsIndexComponent} from './deposit-accounts.index.component';
import {DepositACHTransactionListComponent} from './ach/ach-transaction.list.component';
import {DryRunComponent} from './standing-orders/dry-run/dry-run.component';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../shared/util/index.component';

const DepositAccountRoutes: Routes = [
  {
    path: '',
    component: DepositProductsIndexComponent,
    data: {
      hasPermission: { id: 'deposit_definitions', accessLevel: 'READ' },
      title: 'All deposit products'
    },
    children: [
      {
        path: '',
        component: DepositProductComponent,
      },
      {
        path: 'create/TERM',
        component: CreateDepositTermsProductFormComponent,
        data: {
          title: 'Add product',
          hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'create/CHECKING',
        component: CreateDepositCheckingsProductFormComponent,
        data: {
          title: 'Add product',
          hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'create/SAVINGS',
        component: CreateDepositSavingsProductFormComponent,
        data: {
          title: 'Add product',
          hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'create/SHARE',
        component: CreateDepositSharesProductFormComponent,
        data: {
          title: 'Add product',
          hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'type/TERM/:id',
        component: DepositTermIndexComponent,
        canActivate: [TermProductExistsGuard],
        children: [
          {
            path: '',
            component: DepositTermDetailComponent
          },
          {
            path: 'edit',
            component: EditDepositTermsProductFormComponent,
            data: {
              hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' },
              title: 'Edit'
            },
          }
        ]
      },
      {
        path: 'type/CHECKING/:id',
        component: DepositCheckingIndexComponent,
        canActivate: [CheckingProductExistsGuard],
        children: [
          {
            path: '',
            component: DepositCheckingDetailComponent
          },
          {
            path: 'edit',
            component: EditDepositCheckingsProductFormComponent,
            data: {
              hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' },
              title: 'Edit'
            },
          }
        ]
      },
      {
        path: 'type/SAVINGS/:id',
        component: DepositSavingIndexComponent,
        canActivate: [SavingsProductExistsGuard],
        children: [
          {
            path: '',
            component: DepositSavingDetailComponent,
          },
          {
            path: 'edit',
            component: EditDepositSavingsProductFormComponent,
            data: {
              hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' },
              title: 'Edit'
            },
          }
        ]
      },
      {
        path: 'type/SHARE/:id',
        component: DepositShareIndexComponent,
        canActivate: [ShareProductExistsGuard],
        children: [
          {
            path: '',
            component: DepositShareDetailComponent,
          },
          {
            path: 'edit',
            component: EditDepositSharesProductFormComponent,
            data: {
              hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' },
              title: 'Edit'
            },
          },
          {
            path: 'dividends',
            component: IndexComponent,
            data: {
              title: 'Dividends'
            },
            children: [
              {
                path: '',
                component: DepositProductDividendsComponent,
              },
              {
                path: 'distribute',
                component: CreateDividendFormComponent,
                data: { hasPermission: { id: 'deposit_definitions', accessLevel: 'CHANGE' } },
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: 'ach',
    component: DepositACHTransactionListComponent,
    data: { title: 'ACH transactions'}
  },
  {
    path: 'dryRun',
    component: DryRunComponent,
    data: {
      title: 'Dry run standing orders',
      hasPermission: { id: 'reporting_management', accessLevel: 'READ' }
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DepositAccountRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DepositAccountRoutingModule {}
