/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component} from '@angular/core';
import {UpdateProductDefinitionAction} from '../../store/checkings/checking.actions';
import * as fromDepositAccounts from '../../store/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {CheckingProductDefinition} from '../../../core/services/depositAccount/domain/definition/checking-product-definition.model';

@Component({
  templateUrl: './edit.form.component.html'
})
export class EditDepositCheckingsProductFormComponent {

  checking$: Observable<CheckingProductDefinition>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromDepositAccounts.State>) {
    this.checking$ = store.select(fromDepositAccounts.getSelectedCheckingProduct)
      .filter(product => !!product);
  }

  save(checking: CheckingProductDefinition): void {
    this.store.dispatch(new UpdateProductDefinitionAction({
      checking,
      activatedRoute: this.route
    }));
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
