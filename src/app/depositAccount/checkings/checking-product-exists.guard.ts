/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {LoadAction} from '../store/checkings/checking.actions';
import * as fromProducts from '../store';
import {Store} from '@ngrx/store';
import {DepositAccountService} from '../../core/services/depositAccount/deposit-account.service';
import {ExistsGuardService} from '../../core/services/guards/exists-guard';

@Injectable()
export class CheckingProductExistsGuard implements CanActivate {

  constructor(private store: Store<fromProducts.State>,
              private accountService: DepositAccountService,
              private existsGuardService: ExistsGuardService) {}

  hasProductInStore(id: string): Observable<boolean> {
    const timestamp$ = this.store.select(fromProducts.getCheckingProductsLoadedAt)
      .map(loadedAt => loadedAt[id]);

    return this.existsGuardService.isWithinExpiry(timestamp$);
  }

  hasProductInApi(id: string): Observable<boolean> {
    const getProduct = this.accountService.findCheckingProduct(id)
      .map(productEntity => new LoadAction({
        resource: productEntity
      }))
      .do((action: LoadAction) => this.store.dispatch(action))
      .map(product => !!product);

    return this.existsGuardService.routeTo404OnError(getProduct);
  }

  hasProduct(id: string): Observable<boolean> {
    return this.hasProductInStore(id)
      .switchMap(inStore => {
        if (inStore) {
          return of(inStore);
        }

        return this.hasProductInApi(id);
      });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.hasProduct(route.params['id']);
  }
}
