/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {TermProductDefinition} from '../../../core/services/depositAccount/domain/definition/term-product-definition.model';
import {FormGroup} from '@angular/forms';
import {Currency} from '../../../core/services/currency/domain/currency.model';
import {Action} from '../../../core/services/depositAccount/domain/definition/action.model';
import {Observable} from 'rxjs/Observable';
import {DepositProductDetails, DetailsFormService} from '../../form/services/details-form.service';
import {AdministrationFeeFormService} from '../../form/services/administration-fee-form.service';
import {ValueAddedTaxFormService} from '../../form/services/value-added-tax-form.service';
import {InstallmentFormService} from '../../form/services/installment-form.service';
import {TermsFormService} from '../../form/services/terms-form.service';
import {Type} from '../../../core/services/depositAccount/domain/type.model';

@Component({
  selector: 'aten-deposit-terms-form',
  templateUrl: './form.component.html'
})
export class DepositTermProductFormComponent implements OnChanges {

  @Input() term: TermProductDefinition;
  @Input() editMode = false;

  @Output() save: EventEmitter<TermProductDefinition> = new EventEmitter<TermProductDefinition>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() typeChanged: EventEmitter<Type> = new EventEmitter<Type>();

  actions$: Observable<Action[]>;
  currencies: Currency[];
  detailFormGroup: FormGroup;
  chargesFormGroup: FormGroup;
  administrationFormGroup: FormGroup;
  valueAddedTaxFormGroup: FormGroup;
  installmentFormGroup: FormGroup;
  termsFormGroup: FormGroup;

  constructor(private detailsFormService: DetailsFormService, private administrationFeeFormService: AdministrationFeeFormService,
              private valueAddedTaxFormService: ValueAddedTaxFormService, private installmentFormService: InstallmentFormService,
              private termsFormService: TermsFormService) {
    detailsFormService.fetchAndInitCurrencies().subscribe(currencies => this.currencies = currencies);

    this.detailFormGroup = detailsFormService.initDetailForm();
    this.chargesFormGroup = detailsFormService.initChargesForm();
    this.administrationFormGroup = administrationFeeFormService.init();
    this.valueAddedTaxFormGroup = valueAddedTaxFormService.init();
    this.installmentFormGroup = installmentFormService.init();
    this.termsFormGroup = termsFormService.init();

    this.actions$ = detailsFormService.fetchActions();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.term && this.term) {
      this.detailsFormService.setDepositProductDetails({
        shortCode: this.term.shortCode,
        name: this.term.name,
        description: this.term.description,
        minimumBalance: this.term.minimumBalance,
        equityLedger: this.term.equityLedger,
        currency: this.term.currency,
        charges: this.term.charges
      });

      this.administrationFeeFormService.setAdministrationFee(this.term.administrationFee);
      this.valueAddedTaxFormService.setValueAddedTax(this.term.valueAddedTax);
      this.installmentFormService.setInstallment(this.term.installment, this.term.inUse);
      this.termsFormService.setTerms({
        terms: this.term.terms,
        interestFactor: this.term.interestFactor,
        termRegression: this.term.termRegression
      });
    }
  }

  onSave(): void {
    const details: DepositProductDetails = this.detailsFormService.getDepositProductDetails();

    const termForm = this.termsFormService.getTermForm();

    const term: TermProductDefinition = {
      shortCode: details.shortCode,
      name: details.name,
      description: details.description,
      minimumBalance: details.minimumBalance,
      currency: details.currency,
      charges: details.charges,
      equityLedger: details.equityLedger,
      active: this.term.active,
      administrationFee: this.administrationFeeFormService.getAdministrationFee(),
      valueAddedTax: this.valueAddedTaxFormService.getValueAddedTax(),
      installment: this.installmentFormService.getInstallment(),
      terms: termForm.terms,
      termRegression: termForm.termRegression,
      interestFactor: termForm.interestFactor,
      inUse: this.term ? this.term.inUse : false
    };

    this.save.emit(term);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onTypeChanged(type: Type): void {
    this.typeChanged.emit(type);
  }

  onAddCharge(): void {
    this.detailsFormService.addCharge();
  }

  onRemoveCharge(index: number): void {
    this.detailsFormService.removeCharge(index);
  }

  onAddTerm(): void {
    this.termsFormService.addTerm();
  }

  onRemoveTerm(index: number): void {
    this.termsFormService.removeTerm(index);
  }

  isValid(): boolean {
    return this.detailsFormService.valid && this.administrationFormGroup.valid && this.valueAddedTaxFormGroup.valid
      && this.installmentFormGroup.valid && this.termsFormGroup.valid;
  }
}
