/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Currency} from '../../../core/services/currency/domain/currency.model';
import {ShareProductDefinition} from '../../../core/services/depositAccount/domain/definition/share-product-definition.model';
import {Action} from '../../../core/services/depositAccount/domain/definition/action.model';
import {Observable} from 'rxjs/Observable';
import {DepositProductDetails, DetailsFormService} from '../../form/services/details-form.service';
import {AdministrationFeeFormService} from '../../form/services/administration-fee-form.service';
import {ValueAddedTaxFormService} from '../../form/services/value-added-tax-form.service';
import {Type} from '../../../core/services/depositAccount/domain/type.model';

@Component({
  selector: 'aten-deposit-shares-form',
  templateUrl: './form.component.html'
})
export class DepositShareProductFormComponent implements OnChanges {

  @Input() share: ShareProductDefinition;
  @Input() editMode = false;

  @Output() save: EventEmitter<ShareProductDefinition> = new EventEmitter<ShareProductDefinition>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() typeChanged: EventEmitter<Type> = new EventEmitter<Type>();

  actions$: Observable<Action[]>;
  currencies: Currency[];
  detailFormGroup: FormGroup;
  valueAddedTaxFormGroup: FormGroup;
  administrationFormGroup: FormGroup;
  chargesFormGroup: FormGroup;

  constructor(private detailsFormService: DetailsFormService, private administrationFeeFormService: AdministrationFeeFormService,
              private valueAddedTaxFormService: ValueAddedTaxFormService) {
    detailsFormService.fetchAndInitCurrencies().subscribe(currencies => this.currencies = currencies);

    this.detailFormGroup = detailsFormService.initDetailForm();
    this.valueAddedTaxFormGroup = valueAddedTaxFormService.init();
    this.administrationFormGroup = administrationFeeFormService.init();
    this.chargesFormGroup = this.detailsFormService.initChargesForm();

    this.actions$ = this.detailsFormService.fetchActions();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.share && this.share) {
      this.detailsFormService.setDepositProductDetails({
        shortCode: this.share.shortCode,
        name: this.share.name,
        description: this.share.description,
        minimumBalance: this.share.minimumBalance,
        equityLedger: this.share.equityLedger,
        currency: this.share.currency,
        charges: this.share.charges
      });

      this.administrationFeeFormService.setAdministrationFee(this.share.administrationFee);
      this.valueAddedTaxFormService.setValueAddedTax(this.share.valueAddedTax);
    }
  }

  onSave(): void {
    const details: DepositProductDetails = this.detailsFormService.getDepositProductDetails();

    const share: ShareProductDefinition = {
      shortCode: details.shortCode,
      name: details.name,
      description: details.description,
      minimumBalance: details.minimumBalance,
      currency: details.currency,
      charges: details.charges,
      equityLedger: details.equityLedger,
      active: this.share.active,
      valueAddedTax: this.valueAddedTaxFormService.getValueAddedTax(),
      administrationFee: this.administrationFeeFormService.getAdministrationFee(),
      inUse: this.share ? this.share.inUse : false
    };

    this.save.emit(share);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  onTypeChanged(type: Type): void {
    this.typeChanged.emit(type);
  }

  onAddCharge(): void {
    this.detailsFormService.addCharge();
  }

  onRemoveCharge(index: number): void {
    this.detailsFormService.removeCharge(index);
  }

  isValid(): boolean {
    return this.detailsFormService.valid && this.valueAddedTaxFormGroup.valid && this.administrationFormGroup.valid;
  }
}
