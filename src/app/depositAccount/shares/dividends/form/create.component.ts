/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnInit} from '@angular/core';
import {CreateDividendDistributionAction} from '../../../store/dividends/dividend.actions';
import * as fromDepositAccounts from '../../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {DistributeDividendFormData} from './form.component';
import {Store} from '@ngrx/store';
import {ShareProductDefinition} from '../../../../core/services/depositAccount/domain/definition/share-product-definition.model';

@Component({
  templateUrl: './create.component.html'
})
export class CreateDividendFormComponent implements OnInit {

  shortCode$: Observable<string>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromDepositAccounts.State>) {}

  ngOnInit() {
    this.shortCode$ = this.store.select(fromDepositAccounts.getSelectedShareProduct)
      .filter(product => !!product)
      .map((product: ShareProductDefinition) => product.shortCode);
  }

  save(formData: DistributeDividendFormData): void {
    this.store.dispatch(new CreateDividendDistributionAction({
      shortCode: formData.shortCode,
      dividendDistribution: formData.dividendDistribution,
      activatedRoute: this.route
    }));
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
