/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {payableOptionList} from '../../../domain/payable-option-list.model';
import {DividendDistribution} from '../../../../core/services/depositAccount/domain/definition/dividend-distribution.model';
import {AccountingService} from '../../../../core/services/accounting/accounting.service';
import {todayAsISOString, toLongISOString} from '../../../../core/services/domain/date.converter';
import {FimsValidators} from '../../../../core/validator/validators';
import {accountExists} from '../../../../core/validator/account-exists.validator';

export interface DistributeDividendFormData {
  shortCode: string;
  dividendDistribution: DividendDistribution;
}

@Component({
  selector: 'aten-deposit-product-dividend-form',
  templateUrl: './form.component.html'
})
export class DividendFormComponent implements OnInit {

  @Input() shortCode: string;

  payableOptions = payableOptionList;
  form: FormGroup;

  @Output() save = new EventEmitter<DistributeDividendFormData>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      dueDate: [todayAsISOString(), [Validators.required]],
      amount: ['', [ Validators.required, FimsValidators.minValue(0)] ],
      payable: ['MONTHS', [Validators.required]],
      cashAccount: ['', [Validators.required], accountExists(this.accountingService)],
      expenseAccount: ['', [Validators.required], accountExists(this.accountingService)]
    });
  }

  onSave(): void {
    this.save.emit({
      shortCode: this.shortCode,
      dividendDistribution: {
        dueDate: toLongISOString(this.form.get('dueDate').value),
        amount: this.form.get('amount').value,
        payable: this.form.get('payable').value,
        cashAccount: this.form.get('cashAccount').value,
        expenseAccount: this.form.get('expenseAccount').value
      }
    });
  }

  onCancel(): void {
    this.cancel.emit();
  }
}
