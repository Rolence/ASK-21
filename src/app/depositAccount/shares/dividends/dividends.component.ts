/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import * as fromDepositAccounts from '../../store/index';
import {Observable} from 'rxjs/Observable';
import {LoadAllAction} from '../../store/dividends/dividend.actions';
import {Subscription} from 'rxjs/Subscription';
import {DatePipe} from '@angular/common';
import {Store} from '@ngrx/store';
import {TableData} from '../../../shared/data-table/data-table.component';
import {ShareProductDefinition} from '../../../core/services/depositAccount/domain/definition/share-product-definition.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  providers: [DatePipe],
  templateUrl: './dividends.component.html'
})
export class DepositProductDividendsComponent implements OnInit, OnDestroy {

  private loadAllSubscription: Subscription;

  dividendData$: Observable<TableData>;

  columns: any[] = [
    { name: 'dueDate', label: 'Due date', format: value => this.datePipe.transform(value, 'short') },
    { name: 'payable', label: 'Payable' },
    { name: 'amount', label: 'Dividend rate', format: value => value.toFixed(2) },
    { name: 'cashAccount', label: 'Cash account' },
    { name: 'expenseAccount', label: 'Expense account' },
    { name: 'createdBy', label: 'Created by' },
    { name: 'createdOn', label: 'Created on', format: value => this.datePipe.transform(value, 'short') }
  ];

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromDepositAccounts.State>, private datePipe: DatePipe) {}

  ngOnInit() {
    this.loadAllSubscription = this.store.select(fromDepositAccounts.getSelectedShareProduct)
      .filter(product => !!product)
      .map((product: ShareProductDefinition) => new LoadAllAction(product.shortCode))
      .subscribe(this.store);

    this.dividendData$ = this.store.select(fromDepositAccounts.getDividends)
      .map(dividends => ({
        data: dividends,
        totalElements: dividends.length,
        totalPages: 1
      }));
  }

  ngOnDestroy(): void {
    this.loadAllSubscription.unsubscribe();
  }

  distribute(): void {
    this.router.navigate(['distribute'], { relativeTo: this.route });
  }
}
