/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {SelectAction} from '../store/shares/share.actions';
import * as fromDepositAccounts from '../store';
import {Store} from '@ngrx/store';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';
import {ShareProductDefinition} from '../../core/services/depositAccount/domain/definition/share-product-definition.model';

@Component({
  templateUrl: './share.index.component.html'
})
export class DepositShareIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private productSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromDepositAccounts.State>,
              private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.productSubscription = this.store.select(fromDepositAccounts.getSelectedShareProduct)
      .filter(product => !!product)
      .subscribe((share: ShareProductDefinition) => this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, share.name));
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.productSubscription.unsubscribe();
  }
}
