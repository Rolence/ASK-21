/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {FimsValidators} from '../../../core/validator/validators';
import {accountExists} from '../../../core/validator/account-exists.validator';
import {AdministrationFee} from '../../../core/services/depositAccount/domain/definition/administration-fee.model';

@Injectable()
export class AdministrationFeeFormService {

  private _administrationFeeFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {
  }

  init(): FormGroup {
    this._administrationFeeFormGroup = this.buildAdministrationFeeForm();

    this._administrationFeeFormGroup.get('enabled').valueChanges
      .startWith(null)
      .subscribe(enabled => this.toggleEnabled(enabled));

    return this._administrationFeeFormGroup;
  }

  private buildAdministrationFeeForm(): FormGroup {
    return this.formBuilder.group({
      enabled: [false],
      amount: ['', [Validators.required, FimsValidators.minValue(0)]],
      period: ['MONTHS', [Validators.required]],
      incomeAccount: ['', [Validators.required], accountExists(this.accountingService)]
    });
  }

  getAdministrationFee(): AdministrationFee {
    if (this.administrationFormGroup.get('enabled').value) {
      return {
        amount: this.administrationFormGroup.get('amount').value,
        period: this.administrationFormGroup.get('period').value,
        incomeAccount: this.administrationFormGroup.get('incomeAccount').value
      };
    }
  }

  setAdministrationFee(administrationFee?: AdministrationFee): void {
    if (administrationFee) {
      this.administrationFormGroup.reset({
        enabled: true,
        amount: administrationFee.amount,
        period: administrationFee.period,
        incomeAccount: administrationFee.incomeAccount
      });
    }
  }

  get administrationFormGroup(): FormGroup {
    return this._administrationFeeFormGroup;
  }

  private toggleEnabled(enabled: boolean): void {
    const amountControl: FormControl = this.administrationFormGroup.get('amount') as FormControl;
    const periodControl: FormControl = this.administrationFormGroup.get('period') as FormControl;
    const incomeAccountControl: FormControl = this.administrationFormGroup.get('incomeAccount') as FormControl;

    if (enabled) {
      amountControl.enable();
      periodControl.enable();
      incomeAccountControl.enable();
    } else {
      amountControl.disable();
      periodControl.disable();
      incomeAccountControl.disable();
    }
  }
}
