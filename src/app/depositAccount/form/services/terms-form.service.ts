/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {InterestFactor} from '../../../core/services/depositAccount/domain/definition/term-product-definition.model';
import {Term} from '../../../core/services/depositAccount/domain/term.model';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {FimsValidators} from '../../../core/validator/validators';
import {accountExists} from '../../../core/validator/account-exists.validator';

export interface TermForm {
  terms: Term[];
  termRegression?: boolean;
  interestFactor?: InterestFactor;
}

@Injectable()
export class TermsFormService {

  private _termsFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {
  }

  init(): FormGroup {
    this._termsFormGroup = this.buildTermsFormGroup();

    this._termsFormGroup.get('termRegression').valueChanges
      .subscribe(termRegression => {
        const interestFactorControl = this.termsFormGroup.get('interestFactor');
        if (termRegression) {
          interestFactorControl.enable();
        } else {
          interestFactorControl.disable();
        }
      });

    return this._termsFormGroup;
  }

  private initTerms(terms: Term[]): FormArray {
    const formControls: FormGroup[] = [];
    terms.forEach(term => formControls.push(this.initTerm(term)));
    return this.formBuilder.array(formControls);
  }

  private initTerm(term?: Term): FormGroup {
    return this.formBuilder.group({
      identifier: [term ? term.identifier : '', Validators.required],
      length: [term ? term.length : 1, [
        Validators.required,
        FimsValidators.isNumber,
        FimsValidators.minValue(0),
        FimsValidators.maxScale(0) ]
      ],
      period: [term ? term.period : 'MONTHS', [Validators.required]],
      interestAmount: [term ? term.interest.amount : '0.00', [Validators.required, FimsValidators.minValue(0)]]
    });
  }

  addTerm(term?: Term): void {
    const terms: FormArray = this.termsFormGroup.get('terms') as FormArray;
    terms.push(this.initTerm(term));
  }

  removeTerm(index: number): void {
    const terms: FormArray = this.termsFormGroup.get('terms') as FormArray;
    terms.removeAt(index);
  }

  private buildTermsFormGroup(): FormGroup {
    return this.formBuilder.group({
      terms: this.initTerms([]),
      expenseAccount: [{value: '', disabled: true}, [Validators.required], accountExists(this.accountingService)],
      accrualAccount: [{value: '', disabled: true}, [], accountExists(this.accountingService)],
      termRegression: [false],
      interestFactor: ['PAYMENT']
    });
  }

  getTermForm(): TermForm {
    const termsFormArray = this.termsFormGroup.get('terms').value;
    const expenseAccount = this.termsFormGroup.get('expenseAccount').value;
    const accrualAccount = this.termsFormGroup.get('accrualAccount').value;

    const terms: Term[] = termsFormArray.map(term => ({
      identifier: term.identifier,
      length: term.length,
      period: term.period,
      interest: {
        amount: term.interestAmount,
        payable: 'ERAS',
        fixed: true,
        expenseAccount,
        accrualAccount
      }})
    );

    const termRegression: boolean = this.termsFormGroup.get('termRegression').value;

    return {
      terms,
      termRegression,
      interestFactor: termRegression ? this.termsFormGroup.get('interestFactor').value : undefined
    };
  }

  setTerms(termForm: TermForm = { terms: [] }): void {
    const terms = termForm.terms;
    let expenseAccount = '';
    let accrualAccount = '';

    if (terms.length > 0) {
      expenseAccount = terms[0].interest.expenseAccount;
      accrualAccount = terms[0].interest.accrualAccount;
    }

    this.termsFormGroup.reset({
      expenseAccount: { value: expenseAccount, disabled: false },
      accrualAccount: { value: accrualAccount, disabled: false },
      termRegression: termForm.termRegression,
      interestFactor: termForm.interestFactor ? termForm.interestFactor : 'PAYMENT'
    });

    terms.forEach(term => this.addTerm(term));
  }

  get termsFormGroup(): FormGroup {
    return this._termsFormGroup;
  }

}
