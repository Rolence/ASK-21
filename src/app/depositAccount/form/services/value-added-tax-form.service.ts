/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {FimsValidators} from '../../../core/validator/validators';
import {accountExists} from '../../../core/validator/account-exists.validator';
import {ValueAddedTax} from '../../../core/services/depositAccount/domain/definition/value-added-tax.model';

@Injectable()
export class ValueAddedTaxFormService {

  private _valueAddedTaxFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {
  }

  init(): FormGroup {
    this._valueAddedTaxFormGroup = this.buildValueAddedTaxFormGroup();

    this._valueAddedTaxFormGroup.get('enabled').valueChanges
      .startWith(null)
      .subscribe(enabled => this.toggleEnabled(enabled));

    return this._valueAddedTaxFormGroup;
  }

  private buildValueAddedTaxFormGroup(): FormGroup {
    return this.formBuilder.group({
      enabled: [false],
      name: ['', [Validators.required, Validators.maxLength(256)]],
      description: ['', [Validators.maxLength(4096)]],
      amount: ['', [Validators.required, FimsValidators.minValue(0)]],
      accountsPayable: ['', [Validators.required], accountExists(this.accountingService)]
    });
  }

  getValueAddedTax(): ValueAddedTax {
    if (this.valueAddedTaxFormGroup.get('enabled').value) {
      return {
        amount: this.valueAddedTaxFormGroup.get('amount').value,
        name: this.valueAddedTaxFormGroup.get('name').value,
        description: this.valueAddedTaxFormGroup.get('description').value,
        accountsPayable: this.valueAddedTaxFormGroup.get('accountsPayable').value
      };
    }
  }

  setValueAddedTax(valueAddedTax?: ValueAddedTax): void {
    if (valueAddedTax) {
      this.valueAddedTaxFormGroup.reset({
        enabled: true,
        amount: valueAddedTax.amount,
        name: valueAddedTax.name,
        description: valueAddedTax.description,
        accountsPayable: valueAddedTax.accountsPayable
      });
    }
  }

  get valueAddedTaxFormGroup(): FormGroup {
    return this._valueAddedTaxFormGroup;
  }

  private toggleEnabled(enabled: boolean): void {
    const nameControl: FormControl = this.valueAddedTaxFormGroup.get('name') as FormControl;
    const descriptionControl: FormControl = this.valueAddedTaxFormGroup.get('description') as FormControl;
    const amountControl: FormControl = this.valueAddedTaxFormGroup.get('amount') as FormControl;
    const accountsPayableControl: FormControl = this.valueAddedTaxFormGroup.get('accountsPayable') as FormControl;

    if (enabled) {
      nameControl.enable();
      descriptionControl.enable();
      amountControl.enable();
      accountsPayableControl.enable();
    } else {
      nameControl.disable();
      descriptionControl.disable();
      amountControl.disable();
      accountsPayableControl.disable();
    }
  }

}
