/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountingService} from '../../../core/services/accounting/accounting.service';
import {FimsValidators} from '../../../core/validator/validators';
import {accountExists} from '../../../core/validator/account-exists.validator';
import {Interest} from '../../../core/services/depositAccount/domain/interest.model';

@Injectable()
export class InterestFormService {

  private _interestFormGroup: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService) {
  }

  init(): FormGroup {
    this._interestFormGroup = this.buildInterestForm();

    this.interestFormGroup.get('enabled').valueChanges
      .startWith(null)
      .subscribe(enabled => this.toggleEnabled(enabled));

    return this._interestFormGroup;
  }

  private buildInterestForm(): FormGroup {
    const formGroup = this.formBuilder.group({
      enabled: [false],
      amount: ['', [Validators.required, FimsValidators.minValue(0)]],
      fixed: [false, [Validators.required]],
      payable: ['MONTHS', [Validators.required]],
      expenseAccount: ['', [Validators.required], accountExists(this.accountingService)],
      accrualAccount: [undefined, [], accountExists(this.accountingService)]
    });

    return formGroup;
  }

  getInterest(): Interest {
    if (this.interestFormGroup.get('enabled').value) {
      const accrualAccount = this.interestFormGroup.get('accrualAccount').value;

      return {
        amount: this.interestFormGroup.get('amount').value,
        payable: this.interestFormGroup.get('payable').value,
        fixed: this.interestFormGroup.get('fixed').value,
        expenseAccount: this.interestFormGroup.get('expenseAccount').value,
        accrualAccount: accrualAccount ? accrualAccount : undefined
      };
    }
  }

  setInterest(interest?: Interest, required: boolean = false): void {
    if (interest) {
      this.interestFormGroup.reset({
        enabled: { value: true, disabled: required },
        amount: interest.amount,
        payable: interest.payable,
        fixed: interest.fixed,
        expenseAccount: interest.expenseAccount,
        accrualAccount: interest.accrualAccount
      });
    }
  }

  get interestFormGroup(): FormGroup {
    return this._interestFormGroup;
  }

  private toggleEnabled(enabled: boolean): void {
    const amountControl: FormControl = this.interestFormGroup.get('amount') as FormControl;
    const fixedControl: FormControl = this.interestFormGroup.get('fixed') as FormControl;
    const payableControl: FormControl = this.interestFormGroup.get('payable') as FormControl;
    const expenseAccountControl: FormControl = this.interestFormGroup.get('expenseAccount') as FormControl;
    const accrualAccountControl: FormControl = this.interestFormGroup.get('accrualAccount') as FormControl;

    if (enabled) {
      amountControl.enable();
      fixedControl.enable();
      payableControl.enable();
      expenseAccountControl.enable();
      accrualAccountControl.enable();
    } else {
      amountControl.disable();
      fixedControl.disable();
      payableControl.disable();
      expenseAccountControl.disable();
      accrualAccountControl.disable();
    }

  }
}
