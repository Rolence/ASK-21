/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, PRIMARY_OUTLET, Router, UrlSegment} from '@angular/router';

export interface Breadcrumb {
  title: string;
  path: string;
  custom?: boolean;
}

@Injectable()
export class BreadCrumbService {

  private _crumbs: Breadcrumb[] = [];

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    if (this.router.events) {
      this.router.events
        .filter(event => event instanceof NavigationEnd)
        .subscribe(() => {
          const root: ActivatedRoute = this.activatedRoute.root;
          this._crumbs = this.getBreadcrumbs(root, '', []);
        });
    } else {
      console.warn('Could not subscribe to router events');
    }
  }

  public setCustomBreadCrumb(snapshot: ActivatedRouteSnapshot, title: string): void {
    const path = this.createRootUrl(snapshot);
    const foundCrumb = this.crumbs.find(crumb => crumb.path === path);

    if (!foundCrumb) {
      this.crumbs.push({
        path,
        title,
        custom: true
      });
    } else {
      foundCrumb.title = title;
      foundCrumb.custom = true;
    }
  }

  private getBreadcrumbs(route: ActivatedRoute, path: string, breadcrumbs: Breadcrumb[]): Breadcrumb[] {
    const children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      path = this.concatPath(path, this.buildPath(child.snapshot.url));

      let title = child.snapshot.data['title'];
      let custom = false;

      // if path existed before use title
      const previousCrumb = this.crumbs.find(crumb => crumb.path === path);
      if (previousCrumb && previousCrumb.custom) {
        title = previousCrumb.title;
        custom = previousCrumb.custom;
      }

      const breadCrumb = breadcrumbs.find(crumb => crumb.path === path);

      if (!breadCrumb) {
        breadcrumbs.push({
          title,
          path,
          custom
        });
      }

      if (breadCrumb) {
        breadCrumb.title = title;
        breadCrumb.custom = custom;
      }

      return this.getBreadcrumbs(child, path, breadcrumbs);
    }
  }

  private createRootUrl(snapshot: ActivatedRouteSnapshot) {
    let url = '';
    let next = snapshot.root;

    while (next.firstChild !== snapshot && next.firstChild !== null) {
      next = next.firstChild;

      if (next.routeConfig === null) { continue; }
      if (!next.routeConfig.path) { continue; }

      url = this.concatPath(url, this.buildPath(next.url));
    }

    url = this.concatPath(url, this.buildPath(snapshot.url));

    return url;
  }

  private concatPath(current: string, path: string): string {
    if (path && path.length > 0) {
      return current + `/${path}`;
    }

    return current;
  }

  private buildPath(urls: UrlSegment[]): string {
    return urls.map(segment => segment.path).join('/');
  }

  get crumbs(): Breadcrumb[] {
    return this._crumbs;
  }
}
