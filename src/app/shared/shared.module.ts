/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {
  CovalentChipsModule,
  CovalentCommonModule,
  CovalentDataTableModule,
  CovalentDialogsModule,
  CovalentFileModule,
  CovalentLoadingModule,
  CovalentMediaModule,
  CovalentMessageModule,
  CovalentPagingModule,
  CovalentSearchModule,
  CovalentStepsModule
} from '@covalent/core';
import {TranslateModule} from '@ngx-translate/core';
import {PermissionDirective} from './security/authz/permission.directive';
import {BreadcrumbComponent} from './bread-crumbs/breadcrumb.component';
import {FormCardComponent} from './forms/form-card.component';
import {HeaderComponent} from './header/header.component';
import {NavigationComponent} from './navigation/navigation.component';
import {PageComponent} from './page/page.component';
import {RouterModule} from '@angular/router';
import {DataTableComponent} from './data-table/data-table.component';
import {PagingComponent} from './data-table/paging.component';
import {TextInputComponent} from './text-input/text-input.component';
import {IdInputComponent} from './id-input/id-input.component';
import {BreadCrumbService} from './bread-crumbs/bread-crumb.service';
import {IndexComponent} from './util/index.component';
import {TextDetailComponent} from './text-detail/text-detail.component';
import {ContactDisplayComponent} from './contact-detail/contact-display.component';
import {HeaderSecondaryActionComponent} from './header/header-secondary-action.component';
import {HeaderPrimaryActionComponent} from './header/header-primary-action.component';
import {HeaderNavActionsComponent, HeaderPrimaryActionsComponent, HeaderSecondaryActionsComponent} from './header/header-actions.component';
import {SearchBoxComponent} from './search-box/search-box.component';
import {DisplayFimsFinancialNumber} from './number/fims-financial-number.pipe';
import {DisplayFimsNumber} from './number/fims-number.pipe';
import {NumberInputComponent} from './number-input/number-input.component';
import {AccountSelectComponent} from './account-select/account-select.component';
import {CsvUploadComponent} from './upload/csv-upload.component';
import {OfficeSelectComponent} from './office-select/office-select.component';
import {NumberDetailComponent} from './number-detail/number-detail.component';
import {StateDisplayComponent} from './state-display/state-display.component';
import {DownloadTransactionsDialogComponent} from './ach/download-transactions-dialog.component';
import {AchAccountFormComponent} from './ach/ach-account.form.component';
import {InstitutionSelectComponent} from './ach/institution-select.component';
import {ACHTransactionListComponent} from './ach/ach-transaction.list.component';
import {CommandDisplayComponent} from './command-display/command-display.component';
import {DateInputComponent} from './date-input/date-input.component';
import {CardPageComponent} from './page/card-page.component';
import {AddressDisplayComponent} from './address/address-display.component';
import {BooleanDetailComponent} from './boolean-detail/boolean-detail.component';
import {AddressFormComponent} from './address/address.component';
import {EmployeeAutoCompleteComponent} from './employee-autocomplete/employee-auto-complete.component';
import {LedgerSelectComponent} from './ledger-select/ledger-select.component';
import {FieldFormDialogComponent} from './custom-fields/field-dialog.component';
import {FieldFormComponent} from './custom-fields/field-form.component';
import {CustomFieldsFormComponent} from './custom-fields/custom-fields-form.component';
import {CustomFieldsTableComponent} from './custom-fields/field-table.component';
import {CustomValuesComponent} from './custom-fields/value.component';
import {LoanProductTableComponent} from './loan-product-list/loan-product-list.component';
import {MinMaxComponent} from './min-max/min-max.component';
import {AccountOutlineTableComponent} from './customer-account-list/account-outline-table.component';
import {CustomerAccountListComponent} from './customer-account-list/customer-account-list.component';
import {PaymentOptionsComponent} from './payment-options/payment-options.component';
import {PortraitComponent} from './portrait/portrait.component';
import {DisplayFimsDate} from './date/fims-date.pipe';
import {DepositAccountSelectComponent} from './deposit-account-select/deposit-account-select.component';
import {ImageComponent} from './image/image.component';
import {SelectListComponent} from './select-list/select-list.component';
import {FieldFormService} from './custom-fields/services/field-form.service';
import {DateDetailComponent} from './date-detail/date-detail.component';
import {HeaderNavActionComponent} from './header/header-nav-action.component';
import {SingleSelectComponent} from './single-select/single-select.component';
import {MultiSelectComponent} from './multi-select/multi-select.component';
import {CustomFieldAutocompleteComponent} from './custom-fields/custom-field-autocomplete.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,

    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatTabsModule,
    MatExpansionModule,

    CovalentCommonModule,
    CovalentLoadingModule,
    CovalentMediaModule,
    CovalentDialogsModule,
    CovalentDataTableModule,
    CovalentPagingModule,
    CovalentFileModule,
    CovalentChipsModule,
    CovalentMessageModule,
    CovalentStepsModule,
    CovalentSearchModule
  ],

  declarations: [
    PermissionDirective,
    DisplayFimsFinancialNumber,
    DisplayFimsNumber,
    DisplayFimsDate,
    BreadcrumbComponent,
    FormCardComponent,
    HeaderComponent,
    HeaderSecondaryActionComponent,
    HeaderNavActionComponent,
    HeaderPrimaryActionComponent,
    HeaderPrimaryActionsComponent,
    HeaderSecondaryActionsComponent,
    HeaderNavActionsComponent,
    NavigationComponent,
    PageComponent,
    CardPageComponent,
    DataTableComponent,
    PagingComponent,
    TextInputComponent,
    IdInputComponent,
    IndexComponent,
    TextDetailComponent,
    NumberDetailComponent,
    ContactDisplayComponent,
    SearchBoxComponent,
    NumberInputComponent,
    AccountSelectComponent,
    OfficeSelectComponent,
    CsvUploadComponent,
    StateDisplayComponent,
    DownloadTransactionsDialogComponent,
    AchAccountFormComponent,
    InstitutionSelectComponent,
    ACHTransactionListComponent,
    CommandDisplayComponent,
    DateInputComponent,
    AddressDisplayComponent,
    AddressFormComponent,
    BooleanDetailComponent,
    EmployeeAutoCompleteComponent,
    LedgerSelectComponent,
    FieldFormDialogComponent,
    CustomValuesComponent,
    CustomFieldsTableComponent,
    FieldFormComponent,
    SingleSelectComponent,
    MultiSelectComponent,
    CustomFieldAutocompleteComponent,
    CustomFieldsFormComponent,
    LoanProductTableComponent,
    MinMaxComponent,
    AccountOutlineTableComponent,
    CustomerAccountListComponent,
    PaymentOptionsComponent,
    PortraitComponent,
    DepositAccountSelectComponent,
    ImageComponent,
    SelectListComponent,
    DateDetailComponent
  ],

  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatTabsModule,
    MatExpansionModule,

    CovalentCommonModule,
    CovalentLoadingModule,
    CovalentMediaModule,
    CovalentDialogsModule,
    CovalentDataTableModule,
    CovalentPagingModule,
    CovalentFileModule,
    CovalentChipsModule,
    CovalentMessageModule,
    CovalentStepsModule,
    CovalentSearchModule,

    TranslateModule,

    PermissionDirective,
    DisplayFimsFinancialNumber,
    DisplayFimsNumber,
    DisplayFimsDate,

    BreadcrumbComponent,
    FormCardComponent,
    HeaderComponent,
    HeaderSecondaryActionComponent,
    HeaderNavActionComponent,
    HeaderPrimaryActionComponent,
    HeaderPrimaryActionsComponent,
    HeaderSecondaryActionsComponent,
    HeaderNavActionsComponent,
    NavigationComponent,
    PageComponent,
    CardPageComponent,
    DataTableComponent,
    PagingComponent,
    TextInputComponent,
    IdInputComponent,
    IndexComponent,
    TextDetailComponent,
    NumberDetailComponent,
    ContactDisplayComponent,
    SearchBoxComponent,
    NumberInputComponent,
    AccountSelectComponent,
    OfficeSelectComponent,
    CsvUploadComponent,
    StateDisplayComponent,
    DownloadTransactionsDialogComponent,
    AchAccountFormComponent,
    InstitutionSelectComponent,
    ACHTransactionListComponent,
    CommandDisplayComponent,
    DateInputComponent,
    AddressDisplayComponent,
    AddressFormComponent,
    BooleanDetailComponent,
    EmployeeAutoCompleteComponent,
    LedgerSelectComponent,
    FieldFormDialogComponent,
    CustomValuesComponent,
    CustomFieldsTableComponent,
    FieldFormComponent,
    SingleSelectComponent,
    MultiSelectComponent,
    CustomFieldAutocompleteComponent,
    CustomFieldsFormComponent,
    LoanProductTableComponent,
    MinMaxComponent,
    AccountOutlineTableComponent,
    CustomerAccountListComponent,
    PaymentOptionsComponent,
    PortraitComponent,
    DepositAccountSelectComponent,
    ImageComponent,
    SelectListComponent,
    DateDetailComponent
  ],
  entryComponents: [
    FieldFormDialogComponent,
    ImageComponent,
    DownloadTransactionsDialogComponent
  ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        BreadCrumbService,
        FieldFormService
      ]
    };
  }
}
