/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Component, Inject, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Field, FieldDataType} from '../../core/services/catalog/domain/field.model';
import {FieldFormService} from './services/field-form.service';

@Component({
  templateUrl: './field-dialog.component.html'
})
export class FieldFormDialogComponent {

  editMode = false;
  form: FormGroup;

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: Field, private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<FieldFormDialogComponent, Field>, private fieldFormService: FieldFormService) {
    this.editMode = !!data;

    this.form = fieldFormService.buildForm();

    fieldFormService.resetForm(this.form, data);

    this.form.get('dataType').valueChanges
      .subscribe((dataType) => this.toggleDataType(dataType));

    this.toggleDataType(this.form.get('dataType').value);
  }

  private toggleDataType(dataType: FieldDataType): void {
    const lengthControl = this.form.get('length');
    const precisionControl = this.form.get('precision');
    const minValueControl = this.form.get('minValue');
    const maxValueControl = this.form.get('maxValue');
    const optionsControl = this.form.get('options');

    lengthControl.disable();
    precisionControl.disable();
    minValueControl.disable();
    maxValueControl.disable();
    optionsControl.disable();

    switch (dataType) {
      case 'TEXT': {
        if (!this.editMode) {
          lengthControl.enable();
        }
        break;
      }

      case 'NUMBER': {
        if (!this.editMode) {
          precisionControl.enable();
          minValueControl.enable();
          maxValueControl.enable();
        }
        break;
      }

      case 'SINGLE_SELECTION':
      case 'MULTI_SELECTION': {
        optionsControl.enable();
        break;
      }

      default:
        break;
    }
  }

  save(): void {
    const field: Field = this.form.getRawValue();
    this.dialogRef.close(field);
  }

  addOption(): void {
    this.fieldFormService.addOption(this.form);
  }

  removeOption(index: number): void {
    this.fieldFormService.removeOption(this.form, index);
  }

  get options(): FormArray {
    return this.form.get('options') as FormArray;
  }

}
