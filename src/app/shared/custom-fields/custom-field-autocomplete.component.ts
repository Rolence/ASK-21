/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Option} from '../../core/services/catalog/domain/option.model';
import {Observable} from 'rxjs/Observable';
import {Field} from '../../core/services/catalog/domain/field.model';

@Component({
  selector: 'aten-custom-field-autocomplete',
  templateUrl: './custom-field-autocomplete.component.html'
})
export class CustomFieldAutocompleteComponent implements OnInit {

  filteredOptions$: Observable<Option[]>;

  @Input() form: FormGroup;
  @Input() field: Field;

  ngOnInit(): void {
    this.filteredOptions$ = this.form.get(this.field.identifier).valueChanges
      .startWith('')
      .map(searchValue => this.filter(searchValue));
  }

  filter(value: string | Option): Option[] {
    if (typeof value === 'string') {
      return this.field.options
        .filter(option => option.label.toLowerCase().includes(value.toLowerCase()))
        .slice(0, 10);
    }
    return this.field.options
      .filter(option => option.value === value.value)
      .slice(0, 10);
  }

  displayWith(option: Option): string {
    return option ? option.label : undefined;
  }

}
