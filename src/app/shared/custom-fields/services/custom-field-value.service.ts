/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Option} from '../../../core/services/catalog/domain/option.model';
import {Field} from '../../../core/services/catalog/domain/field.model';
import {Value} from '../../../core/services/catalog/domain/value.model';
import {addCurrentTime} from '../../../core/services/domain/date.converter';
import {FimsValidators} from '../../../core/validator/validators';
import {CustomFieldValidators} from './validators';

@Injectable()
export class CustomFieldValueService {

  private _valuesFormGroup: FormGroup = new FormGroup({});
  private fields: Field[] = [];

  constructor(private formBuilder: FormBuilder) {
  }

  initValuesForm(fields: Field[]): FormGroup {
    this.fields = fields;

    const group: FormGroup = this.formBuilder.group({});

    for (const field of fields) {
      const formControl: FormControl = new FormControl();

      const validators: ValidatorFn[] = [];

      switch (field.dataType) {
        case 'TEXT': {
          validators.push(...this.buildTextValidators(field));
          break;
        }

        case 'NUMBER': {
          validators.push(...this.buildNumberValidators(field));
          break;
        }

        case 'SINGLE_SELECTION': {
          validators.push(CustomFieldValidators.hasOption(field.options));
          break;
        }

        default:
          break;
      }

      if (field.mandatory) {
        validators.push(Validators.required);
      }

      formControl.setValidators(validators);

      group.addControl(field.identifier, formControl);
    }

    this._valuesFormGroup = group;

    return group;
  }

  setValues(values: Value[]): void {
    const data = {};

    values.forEach(value => {
      data[value.fieldIdentifier] = this.getValue(value);
    });

    this.valuesFormGroup.reset(data);
  }

  private getValue(value: Value): string | number | Option | number[] {
    const field = this.findField(value.fieldIdentifier);
    const valueString = value.value;

    switch (field.dataType) {
      case 'TEXT': {
        return valueString;
      }

      case 'NUMBER': {
        return valueString.length ? Number(valueString) : undefined;
      }

      case 'DATE': {
        return valueString.length ? valueString.substring(0, 10) : '';
      }

      case 'SINGLE_SELECTION': {
        const valueAsNumber: number = Number(valueString);
        return field.options.find(option => option.value === valueAsNumber);
      }

      case 'MULTI_SELECTION': {
        if (valueString.length) {
          return valueString
            .split(',')
            .map(valueNumber => Number(valueNumber));
        }
        return [];
      }

      default:
        console.warn('Type ' + field.dataType + ' unknown');
        break;
    }
  }

  getValues(catalogIdentifier: string): Value[] {
    const fields: any = this.valuesFormGroup.getRawValue();

    const values: Value[] = [];

    for (const fieldIdentifier in fields) {
      if (fields.hasOwnProperty(fieldIdentifier)) {
        let value = fields[fieldIdentifier];

        const field: Field = this.findField(fieldIdentifier);

        if (value == null || value.length === 0) {
          continue;
        }

        switch (field.dataType) {
          case 'NUMBER': {
            value = value.toString();
            break;
          }

          case 'DATE': {
            const date = new Date(value);
            value = addCurrentTime(date).toISOString();
            break;
          }

          case 'SINGLE_SELECTION': {
            let option: Option;
            if (typeof value === 'string') {
              const foundField = this.findField(fieldIdentifier);
              option = foundField.options.find(fieldOption => fieldOption.label === value);
            } else {
              option = value as Option;
            }
            value = option.value.toString();
            break;
          }

          case 'MULTI_SELECTION': {
            value = value.join(',');
            break;
          }
        }

        values.push({
          catalogIdentifier,
          fieldIdentifier,
          value
        });
      }
    }
    return values;
  }

  private findField(identifier: string): Field {
    return this.fields.find(field => field.identifier === identifier);
  }

  private buildTextValidators(field: Field): ValidatorFn[] {
    const validators: ValidatorFn[] = [];

    if (field.length != null) {
      validators.push(Validators.maxLength(field.length));
    }

    return validators;
  }

  private buildNumberValidators(field: Field): ValidatorFn[] {
    const validators: ValidatorFn[] = [];

    if (field.minValue != null) {
      validators.push(FimsValidators.minValue(field.minValue));
    }

    if (field.maxValue != null) {
      validators.push(FimsValidators.maxValue(field.maxValue));
    }

    if (field.precision != null) {
      validators.push(FimsValidators.maxScale(field.precision));
    }

    return validators;
  }

  get valuesFormGroup(): FormGroup {
    return this._valuesFormGroup;
  }
}
