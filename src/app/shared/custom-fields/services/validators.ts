import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {isEmptyInputValue} from '../../../core/validator/validators';
import {Option} from '../../../core/services/catalog/domain/option.model';

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

export class CustomFieldValidators {
  static hasOption(options: Option[]): ValidatorFn {
    return (c: AbstractControl): ValidationErrors | null => {
      if (isEmptyInputValue(c.value)) {
        return null;
      }

      let label: string;
      if (typeof c.value === 'string') {
        label = c.value;
      } else {
        const option: Option = c.value;
        label = option.label;
      }

      const foundOption = options.find(option => option.label === label);

      if (foundOption) {
        return null;
      }

      return {
        hasOption: false
      };
    };
  }
}
