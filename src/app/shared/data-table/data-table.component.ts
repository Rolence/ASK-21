/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ITdDataTableColumn, ITdDataTableSortChangeEvent, TdDataTableSortingOrder} from '@covalent/core';
import {TranslateService} from '@ngx-translate/core';
import {Page} from '../../core/services/domain/paging/page.model';
import {Sort} from '../../core/services/domain/paging/sort.model';

export interface TableData {
  data: any[];
  totalElements: number;
  totalPages: number;
}

export interface TableFetchRequest {
  page: Page;
  sort: Sort;
  searchTerm?: string;
}

@Component({
  selector: 'aten-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent {

  currentPage: Page = {
    pageIndex: 0,
    size: 10
  };

  currentSort: Sort = {
    sortColumn: 'identifier',
    sortDirection: 'ASC'
  };

  _columns: any[];

  @Input('data') data: TableData = {
    totalElements: 0,
    totalPages: 0,
    data: []
  };

  @Input() set columns(columns: ITdDataTableColumn[]) {
    columns.forEach((column) => {
      this.translate.get(column.label)
        .subscribe((value) => {
          column.label = value;
          column.tooltip = value;
        });
    });
    this._columns = columns;
  }

  @Input() sortable = false;
  @Input() set sortBy(sortBy: string) {
    this.currentSort.sortColumn = sortBy;
  }

  @Input() pageable = false;
  @Input() clickable = true;
  @Input() loading = false;
  @Input() search: boolean;
  @Input() searchTitle: string;

  @Output() onFetch: EventEmitter<TableFetchRequest> = new EventEmitter<TableFetchRequest>();
  @Output() onRowClick: EventEmitter<any> = new EventEmitter<any>();

  constructor(private translate: TranslateService) {}

  page(page: Page): void {
    this.currentPage = page;
    this.fetch();
  }

  sortChanged(event: ITdDataTableSortChangeEvent): void {
    this.currentSort = {
      sortDirection: event.order === TdDataTableSortingOrder.Ascending ? 'DESC' : 'ASC',
      sortColumn: event.name
    };
    this.fetch();
  }

  private fetch(searchTerm?: string) {
    const fetchRequest: TableFetchRequest = {
      page: this.currentPage,
      sort: this.currentSort,
      searchTerm
    };
    this.onFetch.emit(fetchRequest);
  }

  rowClick(row): void {
    if (this.clickable) {
      this.onRowClick.emit(row);
    }
  }

  get hasData(): boolean {
    return this.data && this.data.data && this.data.data.length > 0;
  }

  isBoolean(value: any): boolean {
    return typeof(value) === 'boolean';
  }
}
