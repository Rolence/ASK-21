/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, ContentChild, ElementRef, EventEmitter, Input, Output} from '@angular/core';
import {Breadcrumb, BreadCrumbService} from '../bread-crumbs/bread-crumb.service';
import {HeaderNavActionsComponent, HeaderSecondaryActionsComponent} from './header-actions.component';

export interface Action {
  title: string;
  identifier?: string;
  routerLink?: string;
  disabled?: boolean;
}

@Component({
  selector: 'aten-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @ContentChild(HeaderSecondaryActionsComponent) secondaryActions: ElementRef;
  @ContentChild(HeaderNavActionsComponent) navActions: ElementRef;

  constructor(private breadCrumbService: BreadCrumbService) {}

  get breadCrumbs(): Breadcrumb[] {
    const crumbs = this.breadCrumbService.crumbs;
    if (crumbs.length > 1) {
      return crumbs.slice(0, crumbs.length - 1);
    }

    return [];
  }

  get title(): string {
    const crumbs = this.breadCrumbService.crumbs;
    if (crumbs.length > 0) {
      return crumbs[crumbs.length - 1].title;
    }
  }

}
