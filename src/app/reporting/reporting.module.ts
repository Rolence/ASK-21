/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import {SharedModule} from '../shared/shared.module';
import {NgModule} from '@angular/core';
import {ReportingRoutingModule} from './reporting-routing.module';
import {CustomerIndexComponent} from './customers/customer-index.component';
import {CustomerListingComponent} from './customers/customerListing/customerListing.component';
import {DepositIndexComponent} from './deposits/deposit-index.component';
import {DepositAccountListingComponent} from './deposits/depositAccountListing/depositAccountListing.component';
import {StandingOrderListingComponent} from './deposits/standingOrderListing/standing-order-listing.component';
import {TermAccountListingComponent} from './deposits/termAccountListing/termAccountListing.component';
import {LoanIndexComponent} from './loans/loan-index.component';
import {LoanAccountListingComponent} from './loans/loanAccountListing/loanAccountListing.component';
import {FinanceIndexComponent} from './finance/finance-index.component';
import {FinancialConditionComponent} from './finance/financialCondition/financial-condition.component';
import {IncomeStatementComponent} from './finance/incomeStatement/income-statement.component';
import {TrialBalanceComponent} from './finance/trialBalance/trial-balance.component';

@NgModule({
  imports: [
    ReportingRoutingModule,
    SharedModule
  ],
  declarations: [
    CustomerIndexComponent,
    CustomerListingComponent,

    DepositIndexComponent,
    DepositAccountListingComponent,
    StandingOrderListingComponent,
    TermAccountListingComponent,

    LoanIndexComponent,
    LoanAccountListingComponent,

    FinanceIndexComponent,
    FinancialConditionComponent,
    IncomeStatementComponent,
    TrialBalanceComponent
  ]
})
export class ReportingModule {}
