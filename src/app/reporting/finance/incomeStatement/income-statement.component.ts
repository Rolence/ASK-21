/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IncomeStatement} from '../../../core/services/accounting/domain/income-statement.model';
import {IncomeStatementSection} from '../../../core/services/accounting/domain/income-statement-section.model';
import {ReportingService} from '../../../core/services/reporting/reporting.service';

@Component({
  templateUrl: './income-statement.component.html',
  styleUrls: ['./income-statement.component.scss']
})
export class IncomeStatementComponent {

  incomeStatement$: Observable<IncomeStatement>;
  income$: Observable<IncomeStatementSection>;
  expenses$: Observable<IncomeStatementSection>;

  constructor(private reportingService: ReportingService) {
    this.incomeStatement$ = reportingService.getIncomeStatement().share();

    this.income$ = this.incomeStatement$
      .map(statement => statement.incomeStatementSections
        .find(section => section.type === 'INCOME')
      );

    this.expenses$ = this.incomeStatement$
      .map(statement => statement.incomeStatementSections
        .find(section => section.type === 'EXPENSES')
      );
  }

  download(): void {
    this.reportingService.downloadIncomeStatement();
  }
}
