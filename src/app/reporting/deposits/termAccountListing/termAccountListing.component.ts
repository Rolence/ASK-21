/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DatePipe} from '@angular/common';
import {TableData} from '../../../shared/data-table/data-table.component';
import {ReportingService} from '../../../core/services/reporting/reporting.service';
import {DisplayFimsNumber} from '../../../shared/number/fims-number.pipe';

@Component({
  templateUrl: './termAccountListing.component.html',
  providers: [DatePipe, DisplayFimsNumber]
})
export class TermAccountListingComponent {

  termAccountListEntries$: Observable<TableData>;

  columns: any[] = [
    { name: 'customerNumber', label: 'Member Id' },
    { name: 'accountNumber', label: 'Account Identifier' },
    { name: 'balance', label: 'Balance', format: (v: any) => this.displayFimsNumber.transform(v) },
    { name: 'termDuration', label: 'Term Duration' },
    { name: 'openedOn', label: 'Opened on', format: (v: any) => {
      return this.datePipe.transform(v, 'shortDate'); }},
    {
      name: 'maturityDate', label: 'Maturity Date',  format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
  ];

  constructor(private reportingService: ReportingService, private datePipe: DatePipe, private displayFimsNumber: DisplayFimsNumber) {
    this.termAccountListEntries$ = this.reportingService.fetchTermDepositAccounts()
      .map(termAccountListEntries => ({
        data: termAccountListEntries,
        totalElements: termAccountListEntries.length,
        totalPages: 1
      })).share();
  }

  exportTermAccountList(): void {
   this.reportingService.exportTermAccountList().subscribe();
  }
}
