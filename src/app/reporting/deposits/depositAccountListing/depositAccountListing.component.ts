/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DatePipe} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DisplayFimsNumber} from '../../../shared/number/fims-number.pipe';
import {TableData} from '../../../shared/data-table/data-table.component';
import {OfficeService} from '../../../core/services/office/office.service';
import {ReportingService} from '../../../core/services/reporting/reporting.service';
import {Type} from '../../../core/services/depositAccount/domain/type.model';
import {todayAsISOString, toMonthAgo, toShortISOString} from '../../../core/services/domain/date.converter';
import {employeeExists} from '../../../core/validator/employee-exists.validator';
import {FimsValidators} from '../../../core/validator/validators';

@Component({
  templateUrl: './depositAccountListing.component.html',
  providers: [DisplayFimsNumber, DatePipe]
})
export class DepositAccountListingComponent {

  depositAccountListEntries$: Observable<TableData>;
  states: string[];
  types: Type[];
  form: FormGroup;

  columns: any[] = [
    { name: 'customerNumber', label: 'Member Id' },
    { name: 'customerName', label: 'Member Name' },
    { name: 'productShortCode', label: 'Product Short Name' },
    { name: 'productName', label: 'Product Name' },
    { name: 'accountNumber', label: 'Account Identifier' },
    { name: 'balance', label: 'Balance', format: (v: any) => this.displayFimsNumber.transform(v) },
    { name: 'openedOn', label: 'Opened on', format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
    { name: 'lastTransactionDate', label: 'Last Transaction Date', format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
    { name: 'createdBy', label: 'Created by' },
    {
      name: 'createdOn', label: 'Created on',  format: (v: any) => {
        return this.datePipe.transform(v, 'shortDate');
      }
    },
  ];

  constructor(private reportingService: ReportingService, private datePipe: DatePipe, private displayFimsNumber: DisplayFimsNumber,
              private formBuilder: FormBuilder, private officeService: OfficeService) {
    const date = new Date();
    const today = todayAsISOString();
    const monthAgo = toMonthAgo(date.toDateString());
    this.types = ['CHECKING', 'SAVINGS', 'SHARE', 'TERM'];
    this.states = ['ACTIVE', 'INACTIVE', 'PENDING', 'CLOSED'];
    this.form = this.formBuilder.group({
      'state': ['ACTIVE', Validators.required],
      'startDate': [monthAgo, [Validators.required]],
      'endDate': [today, [Validators.required]],
      'type': ['SAVINGS', Validators.required],
      'createdBy': [undefined, [], employeeExists(this.officeService)]
    }, {validator: FimsValidators.matchRange('startDate', 'endDate')});
  }

  fetchDepositAccounts(): void {
    const startDate = toShortISOString(this.form.get('startDate').value);
    const endDate = toShortISOString(this.form.get('endDate').value);
    const state = this.form.get('state').value;
    const type = this.form.get('type').value;
    const createdBy = this.form.get('createdBy').value;
    this.depositAccountListEntries$ = this.reportingService.fetchDepositAccounts(state, startDate, endDate, type, createdBy)
      .map(depositAccountListEntries => ({
        data: depositAccountListEntries,
        totalElements: depositAccountListEntries.length,
        totalPages: 1
    })).share();
  }

  exportDepositAccountList(): void {
    const startDate = toShortISOString(this.form.get('startDate').value);
    const endDate = toShortISOString(this.form.get('endDate').value);
    const state = this.form.get('state').value;
    const type = this.form.get('type').value;
    const createdBy = this.form.get('createdBy').value;
    this.reportingService.exportDepositAccountList(state, startDate, endDate, type, createdBy).subscribe();
  }
}
