/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ReportingService} from '../../../core/services/reporting/reporting.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoanAgreementListEntry} from '../../../core/services/loan/domain/agreement/loan-agreement-list-entry.model';
import {State} from '../../../core/services/loan/domain/agreement/loan-agreement.model';
import {TableData} from '../../../shared/data-table/data-table.component';

@Component({
  templateUrl: './loanAccountListing.component.html'
})
export class LoanAccountListingComponent {

  loanAgreementListEntries$: Observable<TableData>;
  matchingLoanAgreements$: Observable<LoanAgreementListEntry[]>;
  states: State[];
  form: FormGroup;

  columns: any[] = [
    { name: 'customerNumber', label: 'Member Id' },
    { name: 'customerName', label: 'Member Name' },
    { name: 'productShortName', label: 'Product Short Name' },
    { name: 'accountNumber', label: 'Account Identifier' },
    { name: 'loanType', label: 'Loan Type' },
    { name: 'termDuration', label: 'Term Duration'},
    { name: 'balance', label: 'Balance'},
    { name: 'delinquentAge', label: 'Delinquent days'},
  ];

  constructor(private reportingService: ReportingService, private formBuilder: FormBuilder) {
    this.states = ['PENDING', 'UNDERWRITING', 'APPROVED', 'DECLINED', 'DISBURSED', 'DELINQUENT', 'REPAID', 'RESTRUCTURE'];
    this.form = this.formBuilder.group({
      'state': ['DISBURSED', Validators.required],
    });
  }

  fetchLoanAccounts(): void {
    const state = this.form.get('state').value;
    this.loanAgreementListEntries$ = this.reportingService.fetchLoanAccounts(state)
      .map(loanAgreementListEntries => ({
        data: loanAgreementListEntries,
        totalElements: loanAgreementListEntries.length,
        totalPages: 1
    }));
  }

  exportLoanAccountList(): void {
    const state = this.form.get('state').value;
    this.reportingService.exportLoanAccountList(state).subscribe();
  }
}
