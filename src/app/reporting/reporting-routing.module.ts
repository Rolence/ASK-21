/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {IndexComponent} from '../shared/util/index.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CustomerListingComponent} from './customers/customerListing/customerListing.component';
import {CustomerIndexComponent} from './customers/customer-index.component';
import {DepositIndexComponent} from './deposits/deposit-index.component';
import {DepositAccountListingComponent} from './deposits/depositAccountListing/depositAccountListing.component';
import {TermAccountListingComponent} from './deposits/termAccountListing/termAccountListing.component';
import {StandingOrderListingComponent} from './deposits/standingOrderListing/standing-order-listing.component';
import {LoanIndexComponent} from './loans/loan-index.component';
import {LoanAccountListingComponent} from './loans/loanAccountListing/loanAccountListing.component';
import {FinanceIndexComponent} from './finance/finance-index.component';
import {FinancialConditionComponent} from './finance/financialCondition/financial-condition.component';
import {IncomeStatementComponent} from './finance/incomeStatement/income-statement.component';
import {TrialBalanceComponent} from './finance/trialBalance/trial-balance.component';

const ReportingRoutes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'Member'
    },
    children: [
      {
        path: '',
        component: CustomerIndexComponent
      },
      {
        path: 'customerListing',
        component: CustomerListingComponent,
        data: {
          title: 'Member list report'
        }
      }
    ]
  },
  {
    path: 'deposits',
    component: IndexComponent,
    data: {
      title: 'Deposit'
    },
    children: [
      {
        path: '',
        component: DepositIndexComponent
      },
      {
        path: 'depositAccountListing',
        component: DepositAccountListingComponent,
        data: {
          title: 'Deposit accounts report',
        }
      },
      {
        path: 'termAccountListing',
        component: TermAccountListingComponent,
        data: {
          title: 'Term accounts report',
        }
      },
      {
        path: 'standingOrderListing',
        component: StandingOrderListingComponent,
        data: {
          title: 'Standing order report',
        }
      }
    ]
  },
  {
    path: 'loans',
    component: IndexComponent,
    data: {
      title: 'Loan'
    },
    children: [
      {
        path: '',
        component: LoanIndexComponent
      },
      {
        path: 'loanAccountListing',
        component: LoanAccountListingComponent,
        data: {
          title: 'Loan agreement report'
        }
      }
    ]
  },
  {
    path: 'finance',
    component: IndexComponent,
    data: {
      title: 'Finance'
    },
    children: [
      {
        path: '',
        component: FinanceIndexComponent
      },
      {
        path: 'financialCondition',
        component: FinancialConditionComponent,
        data: {
          title: 'Financial condition'
        }
      },
      {
        path: 'incomeStatement',
        component: IncomeStatementComponent,
        data: {
          title: 'Income statement'
        }
      },
      {
        path: 'trialBalance',
        component: TrialBalanceComponent,
        data: {
          title: 'Trial balance'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ReportingRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ReportingRoutingModule {}
