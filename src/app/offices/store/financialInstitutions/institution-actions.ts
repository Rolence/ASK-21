/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {RoutePayload} from '../../../core/store/util/route-payload';
import {FinancialInstitution} from '../../../core/services/office/domain/financial-institution.model';

export enum InstitutionActionTypes {
  LOAD = '[Institution] Load',
  SELECT = '[Institution] Select',
  CREATE = '[Institution] Create',
  CREATE_SUCCESS = '[Institution] Create Success',
  CREATE_FAIL = '[Institution] Create Fail'
}

export interface InstitutionPayload extends RoutePayload {
  institution: FinancialInstitution;
}

export class LoadAction implements Action {
  readonly type = InstitutionActionTypes.LOAD;

  constructor(public payload: { institution: FinancialInstitution }) { }
}

export class SelectAction implements Action {
  readonly type = InstitutionActionTypes.SELECT;

  constructor(public payload: { code: string }) { }
}

export class CreateInstitutionAction implements Action {
  readonly type = InstitutionActionTypes.CREATE;

  constructor(public payload: InstitutionPayload) { }
}

export class CreateInstitutionSuccessAction implements Action {
  readonly type = InstitutionActionTypes.CREATE_SUCCESS;

  constructor(public payload: InstitutionPayload) { }
}

export class CreateInstitutionFailAction implements Action {
  readonly type = InstitutionActionTypes.CREATE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export type InstitutionActions
  = LoadAction
  | SelectAction
  | CreateInstitutionAction
  | CreateInstitutionSuccessAction
  | CreateInstitutionFailAction;
