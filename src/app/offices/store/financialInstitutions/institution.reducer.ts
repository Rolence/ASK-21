/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {InstitutionActions, InstitutionActionTypes} from './institution-actions';
import {createSelector} from 'reselect';
import {FinancialInstitution} from '../../../core/services/office/domain/financial-institution.model';

export const adapter: EntityAdapter<FinancialInstitution> = createEntityAdapter<FinancialInstitution>({
  selectId: institution => institution.code
});

export interface State extends EntityState<FinancialInstitution> {
  selectedId: string | null;
  loadedAt: { [id: string]: number };
}

export const initialState: State = adapter.getInitialState({
  selectedId: null,
  loadedAt: {}
});

export function reducer(state = initialState, action: InstitutionActions): State {

  switch (action.type) {

    case InstitutionActionTypes.LOAD: {
      const changes = action.payload.institution;

      const updatedState: State = {
        ...state,
        loadedAt: {
          ...state.loadedAt,
          ['code']: Date.now()
        }
      };

      return adapter.upsertOne({
        id: changes.code,
        changes
      }, updatedState);
    }

    case InstitutionActionTypes.SELECT: {
      return {
        ...state,
        selectedId: action.payload.code
      };
    }

    case InstitutionActionTypes.CREATE_SUCCESS: {
      const changes = action.payload.institution;
      return adapter.upsertOne({
        id: changes.code,
        changes
      }, state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectEntities: selectInstitutionEntities,
} = adapter.getSelectors();

export const getSelectedId = (state: State) => state.selectedId;
export const getLoadedAt = (state: State) => state.loadedAt;

export const getSelectedInstitution = createSelector(selectInstitutionEntities, getSelectedId, (entities, selectedId) => {
  return entities[selectedId];
});
