/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {SelectAction} from '../store/office.actions';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import * as fromOffices from '../store';
import {Store} from '@ngrx/store';
import {Office} from '../../core/services/office/domain/office.model';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';

@Component({
  templateUrl: './office.index.component.html',
})
export class OfficeIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private officeSubscription: Subscription;

  constructor(private store: Store<fromOffices.State>, private route: ActivatedRoute, private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.officeSubscription = this.store.select(fromOffices.getSelectedOffice)
      .filter(office => !!office)
      .subscribe((office: Office) =>
        this.breadCrumbService.setCustomBreadCrumb(
          this.route.snapshot, office.parentIdentifier ? office.name : `${office.name} (Headquarter)`
        )
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.officeSubscription.unsubscribe();
  }
}
