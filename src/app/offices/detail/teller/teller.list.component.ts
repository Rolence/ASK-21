/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {ITdDataTableColumn} from '@covalent/core';
import * as fromOffices from '../../store';
import {Store} from '@ngrx/store';
import {DisplayFimsNumber} from '../../../shared/number/fims-number.pipe';
import {TableData} from '../../../shared/data-table/data-table.component';
import {Teller} from '../../../core/services/teller/domain/teller.model';

@Component({
  templateUrl: './teller.list.component.html',
  providers: [DatePipe, DisplayFimsNumber]
})
export class OfficeTellerListComponent {

  tellerData$: Observable<TableData>;

  columns: ITdDataTableColumn[] = [
    {name: 'code', label: 'Number'},
    {name: 'cashdrawLimit', label: 'Cash withdrawal limit', format: v => this.displayFimsNumber.transform(v) },
    {name: 'assignedEmployee', label: 'Assigned employee'},
    {name: 'state', label: 'Status'},
    {
      name: 'lastOpenedOn', label: 'Last opened on', format: (v: any) => {
      return this.datePipe.transform(v, 'short');
    }}
  ];

  constructor(private store: Store<fromOffices.State>, private route: ActivatedRoute,
              private router: Router, private datePipe: DatePipe, private displayFimsNumber: DisplayFimsNumber) {
    this.tellerData$ = this.store.select(fromOffices.getAllTellerEntities)
      .map(tellers => ({
        data: tellers,
        totalElements: tellers.length,
        totalPages: 1
      }));
  }

  rowSelect(teller: Teller): void {
    this.router.navigate(['detail', teller.code], {relativeTo: this.route});
  }

  addTeller(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
