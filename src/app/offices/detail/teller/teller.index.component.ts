/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {SelectAction} from '../../store/teller/teller.actions';
import * as fromOffices from '../../store';
import {Store} from '@ngrx/store';
import {BreadCrumbService} from '../../../shared/bread-crumbs/bread-crumb.service';
import {Teller} from '../../../core/services/teller/domain/teller.model';

@Component({
  templateUrl: './teller.index.component.html',
})
export class OfficeTellerIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private tellerSubscription: Subscription;

  constructor(private store: Store<fromOffices.State>, private route: ActivatedRoute, private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['code']))
      .subscribe(this.store);

    this.tellerSubscription = this.store.select(fromOffices.getSelectedTeller)
      .filter(teller => !!teller)
      .subscribe((teller: Teller) => this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, teller.code));
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.tellerSubscription.unsubscribe();
  }
}
