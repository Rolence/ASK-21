/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnInit} from '@angular/core';
import * as fromOffices from '../../../store/index';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {Teller} from '../../../../core/services/teller/domain/teller.model';

@Component({
  templateUrl: './teller.detail.component.html'
})
export class OfficeTellerDetailComponent implements OnInit {

  teller$: Observable<Teller>;
  isClosed$: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromOffices.State>) {}

  ngOnInit(): void {
    this.teller$ = this.store.select(fromOffices.getSelectedTeller)
      .filter(teller => !!teller);

    this.isClosed$ = this.teller$
      .map(teller => teller.state === 'CLOSED');

  }

  editTeller(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  viewBalance(): void {
    this.router.navigate(['balance'], { relativeTo: this.route });
  }

  viewDenominations(): void {
    this.router.navigate(['denominations'], { relativeTo: this.route });
  }

}
