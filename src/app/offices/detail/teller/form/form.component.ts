/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Teller} from '../../../../core/services/teller/domain/teller.model';
import {FormComponent} from '../../../../shared/forms/form.component';
import {AccountingService} from '../../../../core/services/accounting/accounting.service';
import {FimsValidators} from '../../../../core/validator/validators';
import {accountExists} from '../../../../core/validator/account-exists.validator';

@Component({
  selector: 'aten-teller-form',
  templateUrl: './form.component.html'
})
export class OfficeTellerFormComponent extends FormComponent<Teller> implements OnChanges {

  @Input('teller') teller: Teller;
  @Input('editMode') editMode: boolean;

  @Output() save = new EventEmitter<Teller>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private accountService: AccountingService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.teller && this.teller) {
      this.prepareForm(this.teller);
    }
  }

  prepareForm(teller: Teller): void {
    this.form = this.formBuilder.group({
      code: [teller.code, [Validators.required, Validators.minLength(3), Validators.maxLength(32), FimsValidators.urlSafe]],
      password: [teller.password, [Validators.required, Validators.minLength(8), Validators.maxLength(4096)]],
      cashdrawLimit: [teller.cashdrawLimit, [Validators.required, FimsValidators.greaterThanValue(0)]],
      tellerAccountIdentifier: [teller.tellerAccountIdentifier, [Validators.required], accountExists(this.accountService)],
      vaultAccountIdentifier: [teller.vaultAccountIdentifier, [Validators.required], accountExists(this.accountService)],
      chequesReceivableAccount: [teller.chequesReceivableAccount, [Validators.required], accountExists(this.accountService)],
      chequesPayableAccount: [teller.chequesPayableAccount, [Validators.required], accountExists(this.accountService)],
      notesPayableAccount: [teller.notesPayableAccount, [Validators.required], accountExists(this.accountService)],
      achPayableAccount: [teller.achPayableAccount, [], accountExists(this.accountService)],
      cashOverShortAccount: [teller.cashOverShortAccount, [Validators.required], accountExists(this.accountService)],
      denominationRequired: [teller.denominationRequired]
    });
  }

  onSave(): void {
    const teller: Teller = {
      ...this.teller,
      code: this.form.get('code').value,
      password: this.form.get('password').value,
      cashdrawLimit: this.form.get('cashdrawLimit').value,
      tellerAccountIdentifier: this.form.get('tellerAccountIdentifier').value,
      vaultAccountIdentifier: this.form.get('vaultAccountIdentifier').value,
      chequesReceivableAccount: this.form.get('chequesReceivableAccount').value,
      chequesPayableAccount: this.form.get('chequesPayableAccount').value,
      notesPayableAccount: this.form.get('notesPayableAccount').value,
      achPayableAccount: this.form.get('achPayableAccount').value ? this.form.get('achPayableAccount').value : null,
      cashOverShortAccount: this.form.get('cashOverShortAccount').value,
      denominationRequired: this.form.get('denominationRequired').value
    };

    this.save.emit(teller);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  get formData(): Teller {
    // Not needed
    return null;
  }

  showCodeValidationError(): void {
    this.setError('code', 'unique', true);
  }
}
