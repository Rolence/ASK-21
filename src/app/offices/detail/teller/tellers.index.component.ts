/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {LoadTellerAction} from '../../store/teller/teller.actions';
import * as fromOffices from '../../store';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './tellers.index.component.html',
})
export class OfficeTellersIndexComponent implements OnDestroy {

  private loadTellerSubscription: Subscription;

  constructor(private store: Store<fromOffices.State>) {
    this.loadTellerSubscription = this.store.select(fromOffices.getSelectedOffice)
      .filter(office => !!office)
      .map(office => new LoadTellerAction(office.identifier))
      .subscribe(this.store);
  }

  ngOnDestroy(): void {
    this.loadTellerSubscription.unsubscribe();
  }
}
