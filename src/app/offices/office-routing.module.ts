/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {RouterModule, Routes} from '@angular/router';
import {OfficeComponent} from './office.component';
import {OfficeDetailComponent} from './detail/office.detail.component';
import {EditOfficeFormComponent} from './form/edit/edit.form.component';
import {CreateOfficeFormComponent} from './form/create/create.form.component';
import {HeadquarterNotFoundComponent} from './headquarter/headquarter-not-found.component';
import {HeadquarterGuard} from './headquarter/headquarter.guard';
import {OfficeExistsGuard} from './office-exists.guard';
import {OfficeIndexComponent} from './detail/office.index.component';
import {OfficeTellerListComponent} from './detail/teller/teller.list.component';
import {OfficeTellerIndexComponent} from './detail/teller/teller.index.component';
import {TellerExistsGuard} from './detail/teller/teller-exists.guard';
import {EditOfficeTellerFormComponent} from './detail/teller/form/edit.form.component';
import {CreateOfficeTellerFormComponent} from './detail/teller/form/create.form.component';
import {OfficeTellerDetailComponent} from './detail/teller/detail/teller.detail.component';
import {TellerBalanceComponent} from './detail/teller/detail/balance/balance.component';
import {OfficeTellerCommandComponent} from './detail/teller/detail/command/command.component';
import {TellerDenominationListComponent} from './detail/teller/detail/denomination/denomination.list.component';
import {CreateDenominationFormComponent} from './detail/teller/detail/denomination/form/create.form.component';
import {OfficeTellersIndexComponent} from './detail/teller/tellers.index.component';
import {FinancialInstitutionsListComponent} from './financialnstitutions/financial-institutions.list.component';
import {UploadFinancialInstitutionFormComponent} from './financialnstitutions/form/upload.form.component';
import {CreateFinancialInstitutionComponent} from './financialnstitutions/form/create.form.component';
import {FinancialInstitutionDetailComponent} from './financialnstitutions/financial-institution.detail.component';
import {FinancialInstitutionIndexComponent} from './financialnstitutions/financial-institution.index.component';
import {FinancialInstitutionExistsGuard} from './financialnstitutions/financial-institution-exists.guard';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../shared/util/index.component';

const OfficeRoutes: Routes = [
  {
    path: '',
    component: OfficeComponent,
    canActivate: [HeadquarterGuard],
    data: {
      title: 'All offices',
      hasPermission: {id: 'office_offices', accessLevel: 'READ'}
    }
  },
  {
    path: 'hqNotFound', component: HeadquarterNotFoundComponent, data: {title: 'Headquarter not found'}
  },
  {
    path: 'create',
    component: CreateOfficeFormComponent,
    data: {title: 'Add office', hasPermission: {id: 'office_offices', accessLevel: 'CHANGE'}}
  },
  {
    path: 'detail/:id',
    component: OfficeIndexComponent,
    canActivate: [OfficeExistsGuard],
    children: [
      {
        path: '',
        component: OfficeDetailComponent,
        data: {
          hasPermission: {id: 'office_offices', accessLevel: 'READ'}
        }
      },
      {
        path: 'edit',
        component: EditOfficeFormComponent,
        data: {
          title: 'Edit',
          hasPermission: {id: 'office_offices', accessLevel: 'CHANGE'}
        }
      },
      {
        path: 'tellers',
        component: OfficeTellersIndexComponent,
        data: {
          hasPermission: {id: 'office_offices', accessLevel: 'READ'},
          title: 'All teller'
        },
        children: [
          {
            path: '',
            component: OfficeTellerListComponent
          },
          {
            path: 'create',
            component: CreateOfficeTellerFormComponent,
            data: {title: 'Add teller', hasPermission: {id: 'teller_management', accessLevel: 'CHANGE'}}
          },
          {
            path: 'detail/:code',
            component: OfficeTellerIndexComponent,
            canActivate: [TellerExistsGuard],
            data: {hasPermission: {id: 'teller_management', accessLevel: 'READ'}},
            children: [
              {
                path: '',
                component: OfficeTellerDetailComponent,
              },
              {
                path: 'edit',
                component: EditOfficeTellerFormComponent,
                data: {title: 'Edit', hasPermission: {id: 'teller_management', accessLevel: 'CHANGE'}}
              },
              {
                path: 'command',
                component: OfficeTellerCommandComponent,
                data: { title: 'Open/close teller', hasPermission: {id: 'teller_management', accessLevel: 'CHANGE'}}
              },
              {
                path: 'balance',
                component: TellerBalanceComponent,
                data: { title: 'Balance'}
              },
              {
                path: 'denominations',
                data: { title: 'Denominations'},
                children: [
                  {
                    path: '',
                    component: TellerDenominationListComponent,
                  },
                  {
                    path: 'create',
                    component: CreateDenominationFormComponent,
                    data: { title: 'Add denomination'}
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        path: 'financialInstitutions',
        component: IndexComponent,
        data: {
          title: 'All financial institutions'
        },
        children: [
          {
            path: '',
            component: FinancialInstitutionsListComponent,
          },
          {
            path: 'upload',
            component: UploadFinancialInstitutionFormComponent,
            data: {
              title: 'Upload CSV'
            }
          },
          {
            path: 'create',
            component: CreateFinancialInstitutionComponent,
            data: {
              title: 'Add institution'
            }
          },
          {
            path: 'detail/:code',
            component: FinancialInstitutionIndexComponent,
            canActivate: [FinancialInstitutionExistsGuard],
            children: [
              {
                path: '',
                component: FinancialInstitutionDetailComponent,
              },
              {
                path: 'edit',
                component: CreateFinancialInstitutionComponent,
                data: {
                  title: 'Edit'
                }
              }
            ]
          }
        ]
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(OfficeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class OfficeRoutingModule {}
