/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {TableData} from '../../shared/data-table/data-table.component';
import {OfficeService} from '../../core/services/office/office.service';
import {FetchRequest} from '../../core/services/domain/paging/fetch-request.model';
import {FinancialInstitution} from '../../core/services/office/domain/financial-institution.model';

@Component({
  templateUrl: './financial-institutions.list.component.html'
})
export class FinancialInstitutionsListComponent {

  tableData$: Observable<TableData>;

  columns: any[] = [
    {name: 'code', label: 'Code'},
    {name: 'name', label: 'Name'},
    {name: 'network', label: 'Network'}
  ];

  constructor(private router: Router, private route: ActivatedRoute, private officeService: OfficeService) {
    this.fetchFinancialInstitutions();
  }

  fetchFinancialInstitutions(fetchRequest?: FetchRequest): void {
    this.tableData$ = this.officeService.fetchFinancialInstitutions(fetchRequest)
      .map(page => ({
        data: page.financialInstitutions,
        totalElements: page.totalElements,
        totalPages: page.totalPages
      }));
  }

  rowSelect(financialInstitution: FinancialInstitution): void {
    this.router.navigate(['detail', financialInstitution.code], {relativeTo: this.route});
  }

  upload(): void {
    this.router.navigate(['upload'], { relativeTo: this.route });
  }

  create(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
