/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Field} from '../../../../core/services/catalog/domain/field.model';
import {Option} from '../../../../core/services/catalog/domain/option.model';
import {FieldFormService} from '../../../../shared/custom-fields/services/field-form.service';

@Component({
  selector: 'aten-catalog-custom-field-form',
  templateUrl: './form.component.html'
})
export class CatalogFieldFormComponent implements OnChanges {

  form: FormGroup;

  @Input('field') field: Field;

  @Output() save = new EventEmitter<Field>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private fieldFormService: FieldFormService) {
    this.form = fieldFormService.buildForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.field) {
      this.fieldFormService.resetForm(this.form, this.field);
    }
  }

  onSave(): void {
    const field: Field = Object.assign({}, this.field, {
      label: this.form.get('label').value,
      hint: this.form.get('hint').value,
      description: this.form.get('description').value,
      mandatory: this.form.get('mandatory').value,
      options: this.form.get('options').value
    });

    this.save.emit(field);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  addOption(option?: Option): void {
    this.fieldFormService.addOption(this.form, option);
  }

  removeOption(index: number): void {
    this.fieldFormService.removeOption(this.form, index);
  }

}
