/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {CustomerComponent} from './customer.component';
import {CreateCustomerFormComponent} from './form/create/create.form.component';
import {CustomerDetailComponent} from './detail/customer.detail.component';
import {EditCustomerFormComponent} from './form/edit/edit.form.component';
import {CustomerActivityComponent} from './detail/activity/activity.component';
import {CustomerStatusComponent} from './detail/status/status.component';
import {CustomerIndexComponent} from './detail/customer.index.component';
import {CustomerExistsGuard} from './customer-exists.guard';
import {CustomerPortraitComponent} from './detail/portrait/portrait.component';
import {TaskListComponent} from './tasks/task.list.component';
import {TaskExistsGuard} from './tasks/task-exists.guard';
import {TaskEditFormComponent} from './tasks/form/edit.form.component';
import {TaskCreateFormComponent} from './tasks/form/create.form.component';
import {TaskIndexComponent} from './tasks/task.index.component';
import {TaskDetailComponent} from './tasks/task.detail.component';
import {PayrollExistsGuard} from './detail/payroll/payroll-exists.guard';
import {CustomerPayrollDetailComponent} from './detail/payroll/payroll.detail.component';
import {CreateCustomerPayrollFormComponent} from './detail/payroll/form/create.form.component';
import {CatalogDetailComponent} from './customFields/catalog.detail.component';
import {CatalogExistsGuard} from './customFields/catalog-exists.guard';
import {CreateCustomerCatalogFormComponent} from './customFields/form/create.form.component';
import {FieldIndexComponent} from './customFields/fields/field.index.component';
import {FieldExistsGuard} from './customFields/fields/field-exists.guard';
import {FieldDetailComponent} from './customFields/fields/field.detail.component';
import {EditCatalogFieldFormComponent} from './customFields/fields/form/edit.form.component';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../shared/util/index.component';
import {SettingsIndexComponent} from './settings/settings-index.component';
import {AccountNumberComponent} from './settings/accountNumber/account-number.component';
import {DoubletValidationComponent} from './settings/doubletValidation/doublet-validation.component';
import {CreateCustomerNumberFormatFormComponent} from './settings/accountNumber/form/create.form.component';
import {CustomerAccountNumberFormatExistsGuard} from './settings/accountNumber/account-number-format-exists.guard';
import {CustomerDoubletValidationFormatExistsGuard} from './settings/doubletValidation/doublet-validation-exists.guard';
import {CreateDoubletValidationFormComponent} from './settings/doubletValidation/form/create.form.component';
import {CustomerValidationGuard} from './customer-validation.guard';
import {CustomerValidationComponent} from './customer-validation.component';

const CustomerRoutes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'Manage members',
      hasPermission: {id: 'customer_customers', accessLevel: 'READ'}
    },
    canActivate: [ CatalogExistsGuard ],
    children: [
      {
        path: '',
        component: CustomerComponent
      },
      {
        path: 'create',
        component: CreateCustomerFormComponent,
        canActivate: [CustomerValidationGuard],
        data: {
          title: 'Add member',
          hasPermission: { id: 'customer_customers', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'validate',
        component: CustomerValidationComponent,
        data: {
          title: 'Validation'
        }
      },
      {
        path: 'detail/:id',
        component: CustomerIndexComponent,
        data: {
          hasPermission: { id: 'customer_customers', accessLevel: 'READ' }
        },
        canActivate: [ CustomerExistsGuard ],
        children: [
          {
            path: '',
            component: CustomerDetailComponent
          },
          {
            path: 'edit',
            component: EditCustomerFormComponent,
            data: {title: 'Edit', hasPermission: { id: 'customer_customers', accessLevel: 'CHANGE' }},
          },
          {
            path: 'tasks',
            component: CustomerStatusComponent,
            data: {title: 'All tasks'},
          },
          {
            path: 'activities',
            component: CustomerActivityComponent,
            data: {title: 'All activities'}
          },
          {
            path: 'portrait',
            component: CustomerPortraitComponent,
            data: {
              title: 'Upload portrait',
              hasPermission: { id: 'customer_portrait', accessLevel: 'READ' }
            }
          },
          {path: 'identifications', loadChildren: './detail/identityCard/identity-card.module#IdentityCardModule'},
          {path: 'loans', loadChildren: './loan-agreements/loan-agreement.module#LoanProductModule'},
          {path: 'deposits', loadChildren: './deposits/deposits.module#DepositsModule'},
          {
            path: 'payroll',
            component: IndexComponent,
            canActivate: [ PayrollExistsGuard ],
            data: {
              title: 'All payroll allocations',
              hasPermission: { id: 'payroll_configuration', accessLevel: 'READ' }
            },
            children: [
              {
                path: '',
                component: CustomerPayrollDetailComponent
              },
              {
                path: 'edit',
                component: CreateCustomerPayrollFormComponent,
                data: {
                  title: 'Edit',
                  hasPermission: { id: 'payroll_configuration', accessLevel: 'CHANGE' }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: 'tasks',
    component: IndexComponent,
    data: {
      hasPermission: { id: 'customer_tasks', accessLevel: 'READ' },
      title: 'All tasks'
    },
    children: [
      {
        path: '',
        component: TaskListComponent,
      },
      {
        path: 'detail/:id',
        canActivate: [ TaskExistsGuard ],
        component: TaskIndexComponent,
        data: {
          title: 'Add task',
          hasPermission: { id: 'customer_tasks', accessLevel: 'READ' }
        },
        children: [
          {
            path: '',
            component: TaskDetailComponent
          },
          {
            path: 'edit',
            component: TaskEditFormComponent,
            data: {
              title: 'Edit',
              hasPermission: { id: 'customer_tasks', accessLevel: 'CHANGE' }
            }
          }
        ]
      },
      {
        path: 'create',
        component: TaskCreateFormComponent,
        data: {
          title: 'Add task',
          hasPermission: { id: 'customer_tasks', accessLevel: 'CHANGE' }
        }
      }
    ]
  },
  {
    path: 'catalog/detail',
    component: IndexComponent,
    data: {
      hasPermission: { id: 'catalog_catalogs', accessLevel: 'READ' },
      title: 'Custom fields'
    },
    children: [
      {
        path: '',
        component: CatalogDetailComponent
      },
      {
        path: 'edit',
        component: CreateCustomerCatalogFormComponent,
        data: {
          title: 'Add custom fields',
          hasPermission: { id: 'catalog_catalogs', accessLevel: 'CHANGE' }
        }
      },
      {
        path: 'field/detail/:fieldId',
        component: FieldIndexComponent,
        canActivate: [ FieldExistsGuard ],
        children: [
          {
            path: '',
            component: FieldDetailComponent
          },
          {
            path: 'edit',
            component: EditCatalogFieldFormComponent,
            data: {
              title: 'Edit',
              hasPermission: { id: 'catalog_catalogs', accessLevel: 'CHANGE' }
            }
          }
        ]
      }
    ]
  },
  {
    path: 'settings',
    component: IndexComponent,
    data: {
      title: 'Settings',
      hasPermission: { id: 'customer_settings', accessLevel: 'READ' }
    },
    children: [
      {
        path: '',
        component: SettingsIndexComponent
      },
      {
        path: 'accountNumber',
        component: IndexComponent,
        canActivate: [CustomerAccountNumberFormatExistsGuard],
        data: {
          title: 'Account number formatting'
        },
        children: [
          {
            path: '',
            component: AccountNumberComponent
          },
          {
            path: 'edit',
            component: CreateCustomerNumberFormatFormComponent,
            data: {
              title: 'Edit',
              hasPermission: { id: 'customer_settings', accessLevel: 'CHANGE' }
            }
          }
        ]
      },
      {
        path: 'doubletValidation',
        component: IndexComponent,
        canActivate: [CustomerDoubletValidationFormatExistsGuard],
        data: {
          title: 'Doublet validation'
        },
        children: [
          {
            path: '',
            component: DoubletValidationComponent
          },
          {
            path: 'edit',
            component: CreateDoubletValidationFormComponent,
            data: {
              title: 'Edit',
              hasPermission: { id: 'customer_settings', accessLevel: 'CHANGE' }
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(CustomerRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CustomerRoutingModule {}
