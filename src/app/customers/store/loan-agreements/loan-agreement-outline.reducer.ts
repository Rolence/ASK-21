/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as outlineActions from './loan-agreement-outline.actions';
import * as agreementActions from './loan-agreement.actions';
import {ActionPayload, UnderwritePayload} from './loan-agreement.actions';
import {LoanAgreementOutline} from '../../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';

export interface State {
  entities: LoanAgreementOutline[];
}

export const initialState: State = {
  entities: [],
};

export function reducer(state = initialState, action: outlineActions.Actions | agreementActions.Actions): State {

  switch (action.type) {

    case outlineActions.LOAD_ALL: {
      return initialState;
    }

    case outlineActions.LOAD_ALL_COMPLETE: {
      return {
        entities: action.payload
      };
    }

    case agreementActions.CREATE_SUCCESS: {
      return {
        entities: [...state.entities, action.payload.resource]
      };
    }

    case agreementActions.UPDATE_SUCCESS: {
      const resource: LoanAgreement = action.payload.resource;
      const entities: LoanAgreementOutline[] = state.entities.map(entity => {
        if (entity.loanAccount !== resource.loanAccount) {
          return entity;
        }

        return {
          ...entity,
          guarantor: !!resource.guarantor,
          term: resource.term,
          disbursementMethod: resource.disbursementMethod
        };
      });

      return {
        entities
      };
    }

    case agreementActions.DELETE_SUCCESS: {
      const resource: any = action.payload.resource;
      const entities = state.entities.filter(entity => entity.loanAccount !== resource.loanAccount);

      return {
        entities
      };
    }

    case agreementActions.UNDERWRITE_SUCCESS: {
      const payload: UnderwritePayload = action.payload;
      const outline: LoanAgreementOutline = state.entities.find(entity => entity.loanAccount === payload.loanAccount);
      const entities = state.entities.filter(entity => entity.loanAccount !== payload.loanAccount);

      entities.push({
        ...outline,
        state: 'UNDERWRITING',
        hasPendingAssessments: false
      });

      return {
        entities
      };
    }

    case agreementActions.APPROVE_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const outline: LoanAgreementOutline = state.entities.find(entity => entity.loanAccount === payload.loanAccount);
      const entities = state.entities.filter(entity => entity.loanAccount !== payload.loanAccount);

      entities.push({
        ...outline,
        state: 'APPROVED',
        inRestructuring: false
      });

      return {
        entities
      };
    }

    case agreementActions.DECLINE_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const outline: LoanAgreementOutline = state.entities.find(entity => entity.loanAccount === payload.loanAccount);
      const entities = state.entities.filter(entity => entity.loanAccount !== payload.loanAccount);

      entities.push({
        ...outline,
        state: outline.inRestructuring ? 'DISBURSED' : 'DECLINED',
        inRestructuring: false
      });

      return {
        entities
      };
    }

    case agreementActions.RETURN_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const outline: LoanAgreementOutline = state.entities.find(entity => entity.loanAccount === payload.loanAccount);
      const entities = state.entities.filter(entity => entity.loanAccount !== payload.loanAccount);

      entities.push({
        ...outline,
        state: outline.inRestructuring ? 'RESTRUCTURE' : 'PENDING',
        hasPendingAssessments: true
      });

      return {
        entities
      };
    }

    case agreementActions.RESTRUCTURE_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const outline: LoanAgreementOutline = state.entities.find(entity => entity.loanAccount === payload.loanAccount);
      const entities = state.entities.filter(entity => entity.loanAccount !== payload.loanAccount);

      entities.push({
        ...outline,
        state: 'RESTRUCTURE',
        inRestructuring: true
      });

      return {
        entities
      };
    }

    default: {
      return state;
    }
  }
}

export const getEntities = (state: State) => state.entities;
