/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as actions from './loan-assessment.actions';
import {Assessment} from '../../../core/services/loan/domain/agreement/assessment.model';

export interface State {
  assessments: Assessment[];
}

export const initialState: State = {
  assessments: []
};

export function reducer(state = initialState, action: actions.Actions): State {

  switch (action.type) {

    case actions.LOAD_ALL_ASSESSMENTS: {
      return initialState;
    }

    case actions.LOAD_ALL_ASSESSMENTS_COMPLETE: {
      const assessments = action.payload;

      return {
        assessments
      };
    }

    default: {
      return state;
    }
  }
}

export const getAssessments = (state: State) => state.assessments;
