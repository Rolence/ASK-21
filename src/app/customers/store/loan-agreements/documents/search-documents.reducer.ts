/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as actions from './document.actions';
import {DocumentOutline} from '../../../../core/services/loan/domain/agreement/document-outline.model';
import {Document} from '../../../../core/services/loan/domain/agreement/document-meta-data.model';
import {SearchState} from '../../../../core/store/util/search.reducer';

export const initialState: SearchState = {
  entities: [],
  totalPages: 0,
  totalElements: 0,
  loading: false,
  fetchRequest: null
};

export function reducer(state = initialState, action: actions.Actions): SearchState {
  switch (action.type) {

    case actions.CREATE_SUCCESS: {
      const document: Document = action.payload.resource;

      const newOutline: DocumentOutline = {
        type: document.type,
        fileName: document.fileName,
        contentType: document.contentType,
        sizeInBytes: document.contentInBase64.length
      };

      const entities = state.entities.filter(outline => outline.fileName !== newOutline.fileName);
      entities.push(newOutline);

      return {
        ...state,
        entities
      };
    }

    case actions.DELETE_SUCCESS: {
      const document: Document = action.payload.resource;
      const entities = state.entities.filter(entity => entity.fileName !== document.fileName);
      return {
        ...state,
        entities
      };
    }

    default: {
      return state;
    }
  }
}
