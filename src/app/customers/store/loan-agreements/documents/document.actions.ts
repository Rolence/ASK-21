/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Action} from '@ngrx/store';
import {type} from '../../../../core/store/util';
import {RoutePayload} from '../../../../core/store/util/route-payload';
import {DocumentOutline} from '../../../../core/services/loan/domain/agreement/document-outline.model';
import {Document} from '../../../../core/services/loan/domain/agreement/document-meta-data.model';
import {SearchResult} from '../../../../core/store/util/search.reducer';
import {CreateResourceSuccessPayload, DeleteResourceSuccessPayload} from '../../../../core/store/util/resource.reducer';

export const SEARCH = type('[Loan Agreement Document] Search');
export const SEARCH_COMPLETE = type('[Loan Agreement Document] Search Complete');

export const CREATE = type('[Loan Agreement Document] Create');
export const CREATE_SUCCESS = type('[Loan Agreement Document] Create Success');
export const CREATE_FAIL = type('[Loan Agreement Document] Create Fail');

export const DELETE = type('[Loan Agreement Document] Delete');
export const DELETE_SUCCESS = type('[Loan Agreement Document] Delete Success');
export const DELETE_FAIL = type('[Loan Agreement Document] Delete Fail');

export const DOWNLOAD = type('[Loan Agreement Document] Download');
export const DOWNLOAD_SUCCESS = type('[Loan Agreement Document] Download Success');
export const DOWNLOAD_FAIL = type('[Loan Agreement Document] Download Fail');


export interface SearchDocumentPayload {
  customerNumber: string;
  loanAccount: string;
}

export interface UploadPayload extends RoutePayload {
  customerNumber: string;
  loanAccount: string;
  documents: Document[];
}

export interface DeletePayload extends RoutePayload {
  customerNumber: string;
  loanAccount: string;
  document: DocumentOutline;
}

export interface DownloadPayload {
  customerNumber: string;
  loanAccount: string;
  fileName: string;
}

export class SearchAction implements Action {
  readonly type = SEARCH;

  constructor(public payload: SearchDocumentPayload) { }
}

export class SearchCompleteAction implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: SearchResult) { }
}

export class CreateAction implements Action {
  readonly type = CREATE;

  constructor(public payload: UploadPayload) { }
}

export class CreateSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: CreateResourceSuccessPayload) { }
}

export class CreateFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: Error) { }
}

export class DeleteAction implements Action {
  readonly type = DELETE;

  constructor(public payload: DeletePayload) { }
}

export class DeleteSuccessAction implements Action {
  readonly type = DELETE_SUCCESS;

  constructor(public payload: DeleteResourceSuccessPayload) { }
}

export class DeleteFailAction implements Action {
  readonly type = DELETE_FAIL;

  constructor(public payload: Error) { }
}

export class DownloadAction implements Action {
  readonly type = DOWNLOAD;

  constructor(public payload: DownloadPayload) { }
}

export class DownloadSuccessAction implements Action {
  readonly type = DOWNLOAD_SUCCESS;

  constructor(public payload: DownloadPayload) { }
}

export class DownloadFailAction implements Action {
  readonly type = DOWNLOAD_FAIL;

  constructor(public payload: Error) { }
}

export type Actions
  = SearchAction
  | SearchCompleteAction
  | CreateAction
  | CreateSuccessAction
  | CreateFailAction
  | DeleteAction
  | DeleteSuccessAction
  | DeleteFailAction
  | DownloadAction
  | DownloadSuccessAction
  | DownloadFailAction;
