/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import {Actions, Effect} from '@ngrx/effects';
import {NotificationService} from '../../../../../core/services/notification/notification.service';
import * as actions from '../document.actions';
import {Injectable} from '@angular/core';

@Injectable()
export class LoanAgreementDocumentNotificationEffects {

  @Effect({ dispatch: false })
  createDocumentSuccess$ = this.actions$
    .ofType(actions.CREATE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Document is going to be uploaded'));

  @Effect({ dispatch: false })
  createDocumentFail$ = this.actions$
    .ofType(actions.CREATE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue uploading the document.'));

  @Effect({ dispatch: false })
  deleteDocumentSuccess$ = this.actions$
    .ofType(actions.DELETE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Document is going to be deleted'));

  @Effect({ dispatch: false })
  deleteDocumentFail$ = this.actions$
    .ofType(actions.DELETE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue deleting the document.'));

  constructor(private actions$: Actions, private notificationService: NotificationService) {}
}
