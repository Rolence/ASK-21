/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Error} from '../../../core/services/domain/error.model';
import {
  CreateResourceSuccessPayload,
  DeleteResourceSuccessPayload,
  LoadResourcePayload,
  SelectResourcePayload,
  UpdateResourceSuccessPayload
} from '../../../core/store/util/resource.reducer';

import {Action} from '@ngrx/store';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';
import {Assessment} from '../../../core/services/loan/domain/agreement/assessment.model';
import {type} from '../../../core/store/util';
import {RoutePayload} from '../../../core/store/util/route-payload';

export const LOAD = type('[Loan Agreement] Load');
export const SELECT = type('[Loan Agreement] Select');

export const CREATE = type('[Loan Agreement] Create');
export const CREATE_SUCCESS = type('[Loan Agreement] Create Success');
export const CREATE_FAIL = type('[Loan Agreement] Create Fail');

export const UPDATE = type('[Loan Agreement] Update');
export const UPDATE_SUCCESS = type('[Loan Agreement] Update Success');
export const UPDATE_FAIL = type('[Loan Agreement] Update Fail');

export const DELETE = type('[Loan Agreement] Delete');
export const DELETE_SUCCESS = type('[Loan Agreement] Delete Success');
export const DELETE_FAIL = type('[Loan Agreement] Delete Fail');

export const RESET_FORM = type('[Loan Agreement] Reset Form');

export const UNDERWRITE = type('[Loan Agreement] Underwrite');
export const UNDERWRITE_SUCCESS = type('[Loan Agreement] Underwrite Success');
export const UNDERWRITE_FAIL = type('[Loan Agreement] Underwrite Fail');

export const APPROVE = type('[Loan Agreement] Approve');
export const APPROVE_SUCCESS = type('[Loan Agreement] Approve Success');
export const APPROVE_FAIL = type('[Loan Agreement] Approve Fail');

export const DECLINE = type('[Loan Agreement] Decline');
export const DECLINE_SUCCESS = type('[Loan Agreement] Decline Success');
export const DECLINE_FAIL = type('[Loan Agreement] Decline Fail');

export const RETURN = type('[Loan Agreement] Return');
export const RETURN_SUCCESS = type('[Loan Agreement] Return Success');
export const RETURN_FAIL = type('[Loan Agreement] Return Fail');

export const RESTRUCTURE = type('[Loan Agreement] Restructure');
export const RESTRUCTURE_SUCCESS = type('[Loan Agreement] Restructure Success');
export const RESTRUCTURE_FAIL = type('[Loan Agreement] Restructure Fail');

export interface LoanAgreementRoutePayload extends RoutePayload {
  customerNumber: string;
  agreement: LoanAgreement;
}

export interface UnderwritePayload {
  customerNumber: string;
  loanAccount: string;
  note?: string;
  assessments: Assessment[];
}

export interface ReturnPayload {
  customerNumber: string;
  loanAccount: string;
  note: string;
  guarantorNeeded: boolean;
}

export interface ActionPayload {
  customerNumber: string;
  loanAccount: string;
  note: string;
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: LoadResourcePayload) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: SelectResourcePayload) { }
}

export class CreateLoanAgreementAction implements Action {
  readonly type = CREATE;

  constructor(public payload: LoanAgreementRoutePayload) { }
}

export class CreateLoanAgreementSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: CreateResourceSuccessPayload) { }
}

export class CreateLoanAgreementFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: Error) { }
}

export class UpdateLoanAgreementAction implements Action {
  readonly type = UPDATE;

  constructor(public payload: LoanAgreementRoutePayload) { }
}

export class UpdateLoanAgreementSuccessAction implements Action {
  readonly type = UPDATE_SUCCESS;

  constructor(public payload: UpdateResourceSuccessPayload) { }
}

export class UpdateLoanAgreementFailAction implements Action {
  readonly type = UPDATE_FAIL;

  constructor(public payload: Error) { }
}

export class DeleteLoanAgreementAction implements Action {
  readonly type = DELETE;

  constructor(public payload: LoanAgreementRoutePayload) { }
}

export class DeleteLoanAgreementSuccessAction implements Action {
  readonly type = DELETE_SUCCESS;

  constructor(public payload: DeleteResourceSuccessPayload) { }
}

export class DeleteLoanAgreementFailAction implements Action {
  readonly type = DELETE_FAIL;

  constructor(public payload: Error) { }
}

export class UnderwriteLoanAgreementAction implements Action {
  readonly type = UNDERWRITE;

  constructor(public payload: UnderwritePayload) { }
}

export class UnderwriteLoanAgreementSuccessAction implements Action {
  readonly type = UNDERWRITE_SUCCESS;

  constructor(public payload: UnderwritePayload) { }
}

export class UnderwriteLoanAgreementFailAction implements Action {
  readonly type = UNDERWRITE_FAIL;

  constructor(public payload: Error) { }
}

export class ApproveLoanAgreementAction implements Action {
  readonly type = APPROVE;

  constructor(public payload: ActionPayload) { }
}

export class ApproveLoanAgreementSuccessAction implements Action {
  readonly type = APPROVE_SUCCESS;

  constructor(public payload: ActionPayload) { }
}

export class ApproveLoanAgreementFailAction implements Action {
  readonly type = APPROVE_FAIL;

  constructor(public payload: Error) { }
}

export class DeclineLoanAgreementAction implements Action {
  readonly type = DECLINE;

  constructor(public payload: ActionPayload) { }
}

export class DeclineLoanAgreementSuccessAction implements Action {
  readonly type = DECLINE_SUCCESS;

  constructor(public payload: ActionPayload) { }
}

export class DeclineLoanAgreementFailAction implements Action {
  readonly type = DECLINE_FAIL;

  constructor(public payload: Error) { }
}

export class ReturnLoanAgreementAction implements Action {
  readonly type = RETURN;

  constructor(public payload: ReturnPayload) { }
}

export class ReturnLoanAgreementSuccessAction implements Action {
  readonly type = RETURN_SUCCESS;

  constructor(public payload: ReturnPayload) { }
}

export class ReturnLoanAgreementFailAction implements Action {
  readonly type = RETURN_FAIL;

  constructor(public payload: Error) { }
}

export class RestructureLoanAgreementAction implements Action {
  readonly type = RESTRUCTURE;

  constructor(public payload: ActionPayload) { }
}

export class RestructureLoanAgreementSuccessAction implements Action {
  readonly type = RESTRUCTURE_SUCCESS;

  constructor(public payload: ActionPayload) { }
}

export class RestructureLoanAgreementFailAction implements Action {
  readonly type = RESTRUCTURE_FAIL;

  constructor(public payload: Error) { }
}

export type Actions
  = LoadAction
  | SelectAction
  | CreateLoanAgreementAction
  | CreateLoanAgreementSuccessAction
  | CreateLoanAgreementFailAction
  | UpdateLoanAgreementAction
  | UpdateLoanAgreementSuccessAction
  | UpdateLoanAgreementFailAction
  | DeleteLoanAgreementAction
  | DeleteLoanAgreementSuccessAction
  | DeleteLoanAgreementFailAction
  | UnderwriteLoanAgreementAction
  | UnderwriteLoanAgreementSuccessAction
  | UnderwriteLoanAgreementFailAction
  | ApproveLoanAgreementAction
  | ApproveLoanAgreementSuccessAction
  | ApproveLoanAgreementFailAction
  | DeclineLoanAgreementAction
  | DeclineLoanAgreementSuccessAction
  | DeclineLoanAgreementFailAction
  | ReturnLoanAgreementAction
  | ReturnLoanAgreementSuccessAction
  | ReturnLoanAgreementFailAction
  | RestructureLoanAgreementAction
  | RestructureLoanAgreementSuccessAction
  | RestructureLoanAgreementFailAction;
