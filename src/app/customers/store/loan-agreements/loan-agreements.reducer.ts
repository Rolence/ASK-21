/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as actions from './loan-agreement.actions';
import {ActionPayload, UnderwritePayload} from './loan-agreement.actions';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';
import {ResourceState} from '../../../core/store/util/resource.reducer';

export const initialState: ResourceState = {
  ids: [],
  entities: {},
  loadedAt: {},
  selectedId: null,
};

export function reducer(state = initialState, action: actions.Actions): ResourceState {

  switch (action.type) {

    case actions.UNDERWRITE_SUCCESS: {
      const payload: UnderwritePayload = action.payload;
      const loanAgreement: LoanAgreement = state.entities[payload.loanAccount];

      return {
        ...state,
        entities: Object.assign({}, state.entities, {
          [loanAgreement.loanAccount]: {
            ...loanAgreement,
            state: 'UNDERWRITING',
            hasPendingAssessments: false
          }
        })
      };
    }

    case actions.APPROVE_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const loanAgreement: LoanAgreement = state.entities[payload.loanAccount];

      return {
        ...state,
        entities: Object.assign({}, state.entities, {
          [loanAgreement.loanAccount]: {
            ...loanAgreement,
            state: 'APPROVED',
            inRestructuring: false
          }
        })
      };
    }

    case actions.DECLINE_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const loanAgreement: LoanAgreement = state.entities[payload.loanAccount];

      return {
        ...state,
        entities: Object.assign({}, state.entities, {
          [loanAgreement.loanAccount]: {
            ...loanAgreement,
            state: loanAgreement.inRestructuring ? 'DISBURSED' : 'DECLINED',
            inRestructuring: false
          }
        })
      };
    }

    case actions.RETURN_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const loanAgreement: LoanAgreement = state.entities[payload.loanAccount];

      return {
        ...state,
        entities: Object.assign({}, state.entities, {
          [loanAgreement.loanAccount]: {
            ...loanAgreement,
            state: loanAgreement.inRestructuring ? 'RESTRUCTURE' : 'PENDING',
            hasPendingAssessments: true
          }
        })
      };
    }

    case actions.RESTRUCTURE_SUCCESS: {
      const payload: ActionPayload = action.payload;
      const loanAgreement: LoanAgreement = state.entities[payload.loanAccount];

      return {
        ...state,
        entities: Object.assign({}, state.entities, {
          [loanAgreement.loanAccount]: {
            ...loanAgreement,
            state: 'RESTRUCTURE',
            inRestructuring: true
          }
        })
      };
    }

    default: {
      return state;
    }
  }
}
