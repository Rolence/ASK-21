/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Action} from '@ngrx/store';
import {Injectable} from '@angular/core';
import * as agreementActions from '../loan-agreement.actions';
import * as outlineActions from '../loan-agreement-outline.actions';
import * as assessmentActions from '../loan-assessment.actions';
import {CustomerLoanService} from '../../../../core/services/loan/customer-loan.service';

@Injectable()
export class LoanAgreementApiEffects {

  @Effect()
  loadAllOutlines: Observable<Action> = this.actions$
    .ofType(outlineActions.LOAD_ALL)
    .map((action: outlineActions.LoadAllAction) => action.payload)
    .switchMap((customerNumber) =>
      this.customerLoanService.fetchAllLoanAgreements(customerNumber)
        .map(products => new outlineActions.LoadAllCompleteAction(products))
        .catch(() => of(new outlineActions.LoadAllCompleteAction([]))));

  @Effect()
  fetchAssessments: Observable<Action> = this.actions$
    .ofType(assessmentActions.LOAD_ALL_ASSESSMENTS)
    .map((action: assessmentActions.LoadAllAssessmentsAction) => action.payload)
    .switchMap(payload =>
      this.customerLoanService.fetchAssessments(payload.customerNumber, payload.loanAccount, false)
        .map(assessments => new assessmentActions.LoadAllAssessmentsCompleteAction(assessments))
        .catch(() => of(new assessmentActions.LoadAllAssessmentsCompleteAction([]))));

  @Effect()
  createLoanAgreement$: Observable<Action> = this.actions$
    .ofType(agreementActions.CREATE)
    .map((action: agreementActions.CreateLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.createLoanAgreement(payload.customerNumber, payload.agreement)
        .map(loanAccount => new agreementActions.CreateLoanAgreementSuccessAction({
          resource: Object.assign({}, payload.agreement, {
            loanAccount,
            state: 'PENDING',
            balance: '0'
          }),
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new agreementActions.CreateLoanAgreementFailAction(error)))
    );

  @Effect()
  updateLoanAgreement$: Observable<Action> = this.actions$
    .ofType(agreementActions.UPDATE)
    .map((action: agreementActions.UpdateLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.modifyLoanAgreement(payload.customerNumber, payload.agreement)
        .map(() => new agreementActions.UpdateLoanAgreementSuccessAction({
          resource: payload.agreement,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new agreementActions.UpdateLoanAgreementFailAction(error)))
    );

  @Effect()
  deleteLoanAgreement$: Observable<Action> = this.actions$
    .ofType(agreementActions.DELETE)
    .map((action: agreementActions.DeleteLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.deleteLoanAgreement(payload.customerNumber, payload.agreement.loanAccount)
        .map(() => new agreementActions.DeleteLoanAgreementSuccessAction({
          resource: payload.agreement,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new agreementActions.DeleteLoanAgreementFailAction(error)))
    );

  @Effect()
  underwrite$: Observable<Action> = this.actions$
    .ofType(agreementActions.UNDERWRITE)
    .map((action: agreementActions.UnderwriteLoanAgreementAction) => action.payload)
    .mergeMap(payload => {
      if (payload.assessments && payload.assessments.length > 0) {
        return this.customerLoanService.confirmAssessment(payload.customerNumber, payload.loanAccount, payload.assessments[0].sequence)
          .map(() => new agreementActions.UnderwriteLoanAgreementSuccessAction(payload))
          .catch((error) => of(new agreementActions.UnderwriteLoanAgreementFailAction(error)));
      }

      return this.customerLoanService.requestAssessment(payload.customerNumber, payload.loanAccount, {
        note: payload.note
      }).map(() => new agreementActions.UnderwriteLoanAgreementSuccessAction(payload))
        .catch((error) => of(new agreementActions.UnderwriteLoanAgreementFailAction(error)));
    });

  @Effect()
  approve$: Observable<Action> = this.actions$
    .ofType(agreementActions.APPROVE)
    .map((action: agreementActions.ApproveLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.confirm(payload.customerNumber, payload.loanAccount, {
        command: 'APPROVE',
        note: payload.note
      }).map(() => new agreementActions.ApproveLoanAgreementSuccessAction(payload))
        .catch((error) => of(new agreementActions.ApproveLoanAgreementFailAction(error)))
    );

  @Effect()
  decline$: Observable<Action> = this.actions$
    .ofType(agreementActions.DECLINE)
    .map((action: agreementActions.DeclineLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.confirm(payload.customerNumber, payload.loanAccount, {
        command: 'DECLINE',
        note: payload.note
      }).map(() => new agreementActions.DeclineLoanAgreementSuccessAction(payload))
        .catch((error) => of(new agreementActions.DeclineLoanAgreementFailAction(error)))
    );

  @Effect()
  return$: Observable<Action> = this.actions$
    .ofType(agreementActions.RETURN)
    .map((action: agreementActions.ReturnLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.confirm(payload.customerNumber, payload.loanAccount, {
        command: 'RETURN',
        note: payload.note,
        guarantorNeeded: payload.guarantorNeeded
      }).map(() => new agreementActions.ReturnLoanAgreementSuccessAction(payload))
        .catch((error) => of(new agreementActions.ReturnLoanAgreementFailAction(error)))
    );

  @Effect()
  restructure$: Observable<Action> = this.actions$
    .ofType(agreementActions.RESTRUCTURE)
    .map((action: agreementActions.RestructureLoanAgreementAction) => action.payload)
    .mergeMap(payload =>
      this.customerLoanService.restructure(payload.customerNumber, payload.loanAccount, {
        note: payload.note
      }).map(() => new agreementActions.RestructureLoanAgreementSuccessAction(payload))
        .catch((error) => of(new agreementActions.RestructureLoanAgreementFailAction(error)))
    );

  constructor(private actions$: Actions, private customerLoanService: CustomerLoanService) { }
}
