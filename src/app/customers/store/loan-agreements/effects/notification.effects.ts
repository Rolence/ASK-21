/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as actions from '../loan-agreement.actions';
import {NotificationService, NotificationType} from '../../../../core/services/notification/notification.service';

@Injectable()
export class LoanAgreementNotificationEffects {

  @Effect({ dispatch: false })
  createLoanAgreementSuccess$ = this.actions$
    .ofType(actions.CREATE_SUCCESS, actions.UPDATE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan agreement is going to be saved'));

  @Effect({ dispatch: false })
  createLoanAgreementFail$ = this.actions$
    .ofType(actions.CREATE_FAIL, actions.UPDATE_FAIL)
    .do(() => this.notificationService.send({
      type: NotificationType.ALERT,
      title: 'Loan agreement can\'t be saved',
      message: 'There was an issue saving the agreement.'
    }));

  @Effect({ dispatch: false })
  underwriteLoanAgreementSuccess$ = this.actions$
    .ofType(actions.UNDERWRITE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan agreement is going to be send to underwriting'));

  @Effect({ dispatch: false })
  underwriteLoanAgreementFail$ = this.actions$
    .ofType(actions.UNDERWRITE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue sending the agreement to underwrite'));

  @Effect({ dispatch: false })
  approveLoanAgreementSuccess$ = this.actions$
    .ofType(actions.APPROVE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan agreement is going to be approved'));

  @Effect({ dispatch: false })
  approveLoanAgreementFail$ = this.actions$
    .ofType(actions.APPROVE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue approving the agreement'));

  @Effect({ dispatch: false })
  declineLoanAgreementSuccess$ = this.actions$
    .ofType(actions.DECLINE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan agreement is going to be declined'));

  @Effect({ dispatch: false })
  declineLoanAgreementFail$ = this.actions$
    .ofType(actions.DECLINE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue declining the agreement'));

  @Effect({ dispatch: false })
  returnLoanAgreementSuccess$ = this.actions$
    .ofType(actions.RETURN_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan agreement is going to be returned'));

  @Effect({ dispatch: false })
  returnLoanAgreementFail$ = this.actions$
    .ofType(actions.RETURN_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue returning the agreement'));

  @Effect({ dispatch: false })
  restructureLoanAgreementSuccess$ = this.actions$
    .ofType(actions.RESTRUCTURE_SUCCESS)
    .do(() => this.notificationService.sendMessage('Loan agreement is going to be restructured'));

  @Effect({ dispatch: false })
  restructureLoanAgreementFail$ = this.actions$
    .ofType(actions.RESTRUCTURE_FAIL)
    .do(() => this.notificationService.sendAlert('There was an issue restructure the agreement'));

  constructor(private actions$: Actions, private notificationService: NotificationService) {}
}
