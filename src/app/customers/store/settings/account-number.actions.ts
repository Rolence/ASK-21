/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {RoutePayload} from '../../../core/store/util/route-payload';
import {CustomerNumberFormat} from '../../../core/services/customer/domain/customer-number-format.model';

export enum AccountNumberActionTypes {
  LOAD = '[Account Number] Load',

  UPDATE = '[Account Number] Update',
  UPDATE_SUCCESS = '[Account Number] Update Success',
  UPDATE_FAIL = '[Account Number] Update Fail',
}

export interface AccountNumberPayload extends RoutePayload {
  customerNumberFormat: CustomerNumberFormat;
}

export class LoadAction implements Action {
  readonly type = AccountNumberActionTypes.LOAD;

  constructor(public payload: { customerNumberFormat: CustomerNumberFormat }) { }
}

export class UpdateAccountNumberAction implements Action {
  readonly type = AccountNumberActionTypes.UPDATE;

  constructor(public payload: AccountNumberPayload) { }
}

export class UpdateAccountNumberSuccessAction implements Action {
  readonly type = AccountNumberActionTypes.UPDATE_SUCCESS;

  constructor(public payload: AccountNumberPayload) { }
}

export class UpdateAccountNumberFailAction implements Action {
  readonly type = AccountNumberActionTypes.UPDATE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export type AccountNumberActions
  = LoadAction
  | UpdateAccountNumberAction
  | UpdateAccountNumberSuccessAction
  | UpdateAccountNumberFailAction;
