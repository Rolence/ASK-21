/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {RoutePayload} from '../../../core/store/util/route-payload';
import {CustomerDoubletValidation} from '../../../core/services/customer/domain/customer-doublet-validation.model';

export enum DoubletValidationActionTypes {
  LOAD = '[Doublet Validation] Load',

  UPDATE = '[Doublet Validation] Update',
  UPDATE_SUCCESS = '[Doublet Validation] Update Success',
  UPDATE_FAIL = '[Doublet Validation] Update Fail',
}

export interface DoubletValidationPayload extends RoutePayload {
  doubletValidation: CustomerDoubletValidation;
}

export class LoadAction implements Action {
  readonly type = DoubletValidationActionTypes.LOAD;

  constructor(public payload: { doubletValidation: CustomerDoubletValidation }) { }
}

export class UpdateDoubletValidationAction implements Action {
  readonly type = DoubletValidationActionTypes.UPDATE;

  constructor(public payload: DoubletValidationPayload) { }
}

export class UpdateDoubletValidationSuccessAction implements Action {
  readonly type = DoubletValidationActionTypes.UPDATE_SUCCESS;

  constructor(public payload: DoubletValidationPayload) { }
}

export class UpdateDoubletValidationFailAction implements Action {
  readonly type = DoubletValidationActionTypes.UPDATE_FAIL;

  constructor(public payload: { error: Error }) { }
}

export type DoubletValidationActions
  = LoadAction
  | UpdateDoubletValidationAction
  | UpdateDoubletValidationSuccessAction
  | UpdateDoubletValidationFailAction;
