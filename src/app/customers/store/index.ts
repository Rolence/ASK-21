/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromRoot from '../../core/store';
import * as fromCustomers from './customers.reducer';
import * as fromCustomerTasks from './customerTasks/customer-tasks.reducer';
import * as fromCustomerIdentificationCards from './identityCards/identity-cards.reducer';
import * as fromCatalogs from './catalogs/catalog.reducer';
import * as fromCommands from './commands/commands.reducer';
import * as fromScans from './identityCards/scans/scans.reducer';
import * as fromTasks from './tasks/tasks.reducer';
import * as fromPayrollDistribution from './payroll/payroll.reducer';
import * as fromAccountNumerFormat from './settings/account-number.reducer';
import * as fromDoubletValidation from './settings/doublet-validation.reducer';

import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createSelector} from 'reselect';
import {
  createResourceReducer,
  getResourceAll,
  getResourceLoadedAt,
  getResourceSelected,
  ResourceState
} from '../../core/store/util/resource.reducer';
import {createFormReducer, FormState, getFormError} from '../../core/store/util/form.reducer';
import {createSearchReducer, getSearchEntities, SearchState} from '../../core/store/util/search.reducer';
import * as fromDocuments from './loan-agreements/documents/search-documents.reducer';
import * as fromDepositOutlines from './deposits/deposit-outline.reducer';
import * as fromStandingOrders from './deposits/standing-orders/standing-orders.reducer';
import * as fromAgreements from './loan-agreements/loan-agreements.reducer';
import * as fromAgreementOutlines from './loan-agreements/loan-agreement-outline.reducer';
import * as fromAssessments from './loan-agreements/loan-assessments.reducer';
import {LoanAgreementOutline} from '../../core/services/loan/domain/agreement/loan-agreement-outline.model';
import {ProductInstanceOutline} from '../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {InjectionToken} from '@angular/core';
import {CustomerAccountOutline} from '../../shared/customer-account-list/domain/customer-account-outline.model';

export interface CustomerState {
  customers: ResourceState;
  customerForm: FormState;
  tasks: ResourceState;
  taskForm: FormState;
  customerTasks: fromCustomerTasks.State;
  customerCatalog: fromCatalogs.State;
  customerCommands: fromCommands.State;
  customerIdentificationCards: ResourceState;
  customerIdentificationCardForm: FormState;
  customerIdentificationCardScans: ResourceState;
  customerIdentificationCardScanForm: FormState;
  customerPayrollDistribution: fromPayrollDistribution.State;
  customerAccountNumberFormat: fromAccountNumerFormat.State;
  customerDoubletValidation: fromDoubletValidation.State;
  deposits: ResourceState;
  depositSearch: fromDepositOutlines.State;
  depositStandingOrders: ResourceState;
  agreementsSearch: fromAgreementOutlines.State;
  agreements: ResourceState;
  documentsSearch: SearchState;
  assessments: fromAssessments.State;
}

export interface State extends fromRoot.State {
  customer: CustomerState;
}

export const reducers: ActionReducerMap<CustomerState> = {
  customers: createResourceReducer('Customer', fromCustomers.reducer),
  customerForm: createFormReducer('Customer'),
  tasks: createResourceReducer('Task', fromTasks.reducer),
  taskForm: createFormReducer('Task'),
  customerTasks: fromCustomerTasks.reducer,
  customerCatalog: fromCatalogs.reducer,
  customerCommands: fromCommands.reducer,
  customerIdentificationCards: createResourceReducer('Customer Identity Card', fromCustomerIdentificationCards.reducer, 'number'),
  customerIdentificationCardForm: createFormReducer('Customer Identity Card'),
  customerIdentificationCardScans: createResourceReducer('Customer Identity Card Scan', fromScans.reducer),
  customerIdentificationCardScanForm: createFormReducer('Customer Identity Card Scan'),
  customerPayrollDistribution: fromPayrollDistribution.reducer,
  customerAccountNumberFormat: fromAccountNumerFormat.reducer,
  customerDoubletValidation: fromDoubletValidation.reducer,
  deposits: createResourceReducer('Deposit', undefined, 'accountIdentifier'),
  depositSearch: fromDepositOutlines.reducer,
  depositStandingOrders: createResourceReducer('Deposit Standing Order', fromStandingOrders.reducer, 'sequence'),
  agreementsSearch: fromAgreementOutlines.reducer,
  agreements: createResourceReducer('Loan Agreement', fromAgreements.reducer, 'loanAccount'),
  documentsSearch: createSearchReducer('Loan Agreement Document', fromDocuments.reducer),
  assessments: fromAssessments.reducer
};

export const reducerToken = new InjectionToken<ActionReducerMap<CustomerState>>('Customer reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export const selectCustomerState = createFeatureSelector<CustomerState>('customer');

export const getCustomersState = createSelector(selectCustomerState, (state: CustomerState) => state.customers);

export const getCustomerFormState = createSelector(selectCustomerState, (state: CustomerState) => state.customerForm);
export const getCustomerFormError = createSelector(getCustomerFormState, getFormError);

export const getCustomerLoadedAt = createSelector(getCustomersState, getResourceLoadedAt);
export const getSelectedCustomer = createSelector(getCustomersState, getResourceSelected);
export const isCustomerActive = createSelector(getSelectedCustomer, (customer) => customer.currentState === 'ACTIVE');

/**
 * Task Selectors
 */
export const getTasksState = createSelector(selectCustomerState, (state: CustomerState) => state.tasks);

export const getAllTaskEntities = createSelector(getTasksState, getResourceAll);

export const getTaskLoadedAt = createSelector(getTasksState, getResourceLoadedAt);
export const getSelectedTask = createSelector(getTasksState, getResourceSelected);

/**
 * Customer Task Selectors
 */
export const getCustomerTaskCommandsState = createSelector(selectCustomerState, (state: CustomerState) => state.customerTasks);

export const getCustomerTaskProcessSteps = createSelector(getCustomerTaskCommandsState, fromCustomerTasks.getProcessSteps);


/**
 * Customer Command Selectors
 */

export const getCustomerCommandsState = createSelector(selectCustomerState, (state: CustomerState) => state.customerCommands);

export const getAllCustomerCommands = createSelector(getCustomerCommandsState, fromCommands.getCommands);

/**
 * Customer Identification Card Selectors
 */
export const getCustomerIdentificationCardsState =
  createSelector(selectCustomerState, (state: CustomerState) => state.customerIdentificationCards);

export const getAllCustomerIdentificationCardEntities = createSelector(getCustomerIdentificationCardsState, getResourceAll);

export const getCustomerIdentificationCardFormState =
  createSelector(selectCustomerState, (state: CustomerState) => state.customerIdentificationCardForm);
export const getCustomerIdentificationCardFormError = createSelector(getCustomerIdentificationCardFormState, getFormError);

export const getIdentificationCardLoadedAt = createSelector(getCustomerIdentificationCardsState, getResourceLoadedAt);
export const getSelectedIdentificationCard = createSelector(getCustomerIdentificationCardsState, getResourceSelected);

/**
 * Customer Identification Card Scan Selectors
 */
export const getIdentificationCardScansState =
  createSelector(selectCustomerState, (state: CustomerState) => state.customerIdentificationCardScans);

export const getAllIdentificationCardScanEntities = createSelector(getIdentificationCardScansState, getResourceAll);

export const getCustomerIdentificationCardScanFormState =
  createSelector(selectCustomerState, (state: CustomerState) => state.customerIdentificationCardScanForm);
export const getCustomerIdentificationCardScanFormError = createSelector(getCustomerIdentificationCardScanFormState, getFormError);

/**
 * Customer Payroll Distribution Selectors
 */
export const getPayrollDistributionState = createSelector(selectCustomerState, (state: CustomerState) => state.customerPayrollDistribution);

export const getPayrollDistribution = createSelector(getPayrollDistributionState, fromPayrollDistribution.getPayrollDistribution);
export const getPayrollDistributionLoadedAt = createSelector(getPayrollDistributionState,
  fromPayrollDistribution.getPayrollDistributionLoadedAt);

/**
 * Customer Catalog Selectors
 */
export const getCustomerCatalogState = createSelector(selectCustomerState, (state: CustomerState) => state.customerCatalog);

export const getCustomerCatalog = createSelector(getCustomerCatalogState, fromCatalogs.getCustomerCatalog);
export const getCustomerCatalogLoadedAt = createSelector(getCustomerCatalogState, fromCatalogs.getCustomerCatalogLoadedAt);
export const getSelectedField = createSelector(getCustomerCatalogState, fromCatalogs.getSelectedField);

/**
 * Customer deposit account selectors
 */
export const getDepositSearchState = createSelector(selectCustomerState, (state: CustomerState) => state.depositSearch);
export const getSearchDeposits = createSelector(getDepositSearchState, fromDepositOutlines.getEntities);
export const getActiveSearchDeposits = createSelector(getSearchDeposits, (outlines: ProductInstanceOutline[]) => {
  return outlines.filter(outline => outline.state === 'ACTIVE' && !outline.collateralAttached);
});

export const getDepositsState = createSelector(selectCustomerState, (state: CustomerState) => state.deposits);
export const getDepositsLoadedAt = createSelector(getDepositsState, getResourceLoadedAt);
export const getSelectedDepositInstance = createSelector(getDepositsState, getResourceSelected);

export const getStandingOrderSearchState = createSelector(selectCustomerState, (state: CustomerState) => state.depositStandingOrders);
export const getSearchStandingOrders = createSelector(getStandingOrderSearchState, getResourceAll);

export const getStandingOrdersState = createSelector(selectCustomerState, (state: CustomerState) => state.depositStandingOrders);
export const getStandingOrdersLoadedAt = createSelector(getStandingOrdersState, getResourceLoadedAt);
export const getSelectedStandingOrder = createSelector(getStandingOrdersState, getResourceSelected);

/**
 * Customer loan agreement selectors
 */
export const getAgreementSearchState = createSelector(selectCustomerState, (state: CustomerState) => state.agreementsSearch);
export const getSearchAgreements = createSelector(getAgreementSearchState, fromAgreementOutlines.getEntities);

export const getAgreementsState = createSelector(selectCustomerState, (state: CustomerState) => state.agreements);
export const getAgreementsLoadedAt = createSelector(getAgreementsState, getResourceLoadedAt);
export const getSelectedAgreement = createSelector(getAgreementsState, getResourceSelected);

export const canCreateAgreements = createSelector(fromRoot.getPermissions, isCustomerActive, (permissions, isActive) => {
  const hasChangePermission = permissions.filter(permission =>
    permission.id === 'loan_agreement' &&
    permission.accessLevel === 'CHANGE'
  ).length > 0;

  return hasChangePermission && isActive;
});

export const pendingAssessments = createSelector(getSearchAgreements, (loanAgreements: LoanAgreementOutline[]) => {
  return loanAgreements.filter(agreement => agreement.hasPendingAssessments).length;
});

/**
 * Customer account number format
 */
export const getCustomerAccountNumberFormatState = createSelector(selectCustomerState,
  (state: CustomerState) => state.customerAccountNumberFormat);

export const getCustomerNumberFormat = createSelector(getCustomerAccountNumberFormatState, fromAccountNumerFormat.getCustomerNumberFormat);
export const getCustomerNumberFormatLoadedAt = createSelector(getCustomerAccountNumberFormatState,
  fromAccountNumerFormat.getCustomerNumberFormatLoadedAt);

/**
 * Customer doublet validation
 */
export const getCustomerDoubletValidationState = createSelector(selectCustomerState,
  (state: CustomerState) => state.customerDoubletValidation);

export const getCustomerDoubletValidation = createSelector(getCustomerDoubletValidationState,
  fromDoubletValidation.getCustomerDoubletValidation);
export const getCustomerDoubletValidationLoadedAt = createSelector(getCustomerDoubletValidationState,
  fromDoubletValidation.getCustomerDoubletValidationLoadedAt);

/**
 * Document selectors
 */
export const getDocumentSearchState = createSelector(selectCustomerState, (state: CustomerState) => state.documentsSearch);
export const getSearchDocuments = createSelector(getDocumentSearchState, getSearchEntities);

export const getDepositAccountOutlines =
  createSelector(getSearchDeposits, (depositAccounts: ProductInstanceOutline[]): CustomerAccountOutline[] => {
    const deposits: CustomerAccountOutline[] = depositAccounts.map(account => ({
      type: account.type,
      productShortCode: account.productShortCode,
      accountIdentifier: account.accountIdentifier,
      balance: account.balance,
      amountOnHold: account.amountOnHold,
      collateralAmount: account.collateralAmount
    }));

    return deposits.sort((outlineA, outlineB) => {
      return outlineA.accountIdentifier.localeCompare(outlineB.accountIdentifier);
    });
});

export const getLoanAccountOutlines =
  createSelector(getSearchAgreements, (loanAgreements: LoanAgreementOutline[]): CustomerAccountOutline[] => {
    const agreements: CustomerAccountOutline[] = loanAgreements.map(account => ({
      type: account.loanType,
      productShortCode: account.productShortName,
      accountIdentifier: account.loanAccount,
      balance: account.balance,
      collateralAmount: account.collateralAmount,
      accruedInterest: account.accruedInterest
    }));

    return agreements.sort((outlineA, outlineB) => {
      return outlineA.accountIdentifier.localeCompare(outlineB.accountIdentifier);
    });
});

/**
 * Assessment Selectors
 */
export const getAssessmentsState = createSelector(selectCustomerState, (state: CustomerState) => state.assessments);
export const getAssessments = createSelector(getAssessmentsState, fromAssessments.getAssessments);

export const canDeleteAgreement = createSelector(fromRoot.getPermissions, getSelectedAgreement, getAssessments,
  (permissions, selectedAgreement,  assessments) => {
  if (!selectedAgreement) {
    return false;
  }

  const hasDeletePermission = permissions.filter(permission =>
    permission.id === 'loan_agreement' &&
    permission.accessLevel === 'DELETE'
  ).length > 0;

  const hasNoAssessments = assessments.length === 0;
  const isPending = selectedAgreement.state === 'PENDING';
  const noPendingAssessments = !selectedAgreement.hasPendingAssessments;

  return hasDeletePermission && isPending && hasNoAssessments && noPendingAssessments;
});

export const canChangeAgreement = createSelector(fromRoot.getPermissions, getSelectedAgreement, (permissions, selectedAgreement) => {
    if (!selectedAgreement) {
      return false;
    }

    const hasDeletePermission = permissions.filter(permission =>
      permission.id === 'loan_agreement' &&
      permission.accessLevel === 'CHANGE'
    ).length > 0;

    const hasState = selectedAgreement.state === 'PENDING' || selectedAgreement.state === 'RESTRUCTURE';
    return hasDeletePermission && hasState;
});

export const canUploadDocument = createSelector(fromRoot.getPermissions, getSelectedAgreement, (permissions, selectedAgreement) => {
  if (!selectedAgreement) {
    return false;
  }

  const hasDeletePermission = permissions.filter(permission =>
    permission.id === 'loan_agreement' &&
    permission.accessLevel === 'DELETE'
  ).length > 0;

  const hasState = selectedAgreement.state === 'PENDING'
    || selectedAgreement.state === 'RESTRUCTURE'
    || selectedAgreement.state === 'DISBURSED';
  return hasDeletePermission && hasState;
});

export const canDeleteDocument = createSelector(fromRoot.getPermissions, getSelectedAgreement, (permissions, selectedAgreement) => {
  if (!selectedAgreement) {
    return false;
  }

  const hasDeletePermission = permissions.filter(permission =>
    permission.id === 'loan_agreement' &&
    permission.accessLevel === 'DELETE'
  ).length > 0;

  const hasState = selectedAgreement.state === 'PENDING' || selectedAgreement.state === 'RESTRUCTURE';
  return hasDeletePermission && hasState;
});
