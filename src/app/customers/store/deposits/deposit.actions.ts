/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {
  CreateResourceSuccessPayload,
  LoadResourcePayload,
  SelectResourcePayload,
  UpdateResourceSuccessPayload
} from '../../../core/store/util/resource.reducer';
import {Action} from '@ngrx/store';
import {IssuingCount} from '../../../core/services/cheque/domain/issuing-count.model';
import {ProductInstanceAssignment} from '../../../core/services/depositAccount/domain/instance/product-instance-assignment.model';
import {Type} from '../../../core/services/depositAccount/domain/type.model';
import {ProductInstanceChangeSet} from '../../../core/services/depositAccount/domain/instance/product-instance-change-set.model';
import {ProductInstance} from '../../../core/services/depositAccount/domain/instance/product-instance.model';
import {type} from '../../../core/store/util';
import {RoutePayload} from '../../../core/store/util/route-payload';

export const LOAD = type('[Deposit] Load');
export const SELECT = type('[Deposit] Select');

export const CREATE = type('[Deposit] Create');
export const CREATE_SUCCESS = type('[Deposit] Create Success');
export const CREATE_FAIL = type('[Deposit] Create Fail');

export const UPDATE = type('[Deposit] Update');
export const UPDATE_SUCCESS = type('[Deposit] Update Success');
export const UPDATE_FAIL = type('[Deposit] Update Fail');

export const ISSUE_CHEQUES = type('[Deposit] Issue Cheques');
export const ISSUE_CHEQUES_SUCCESS = type('[Deposit] Issue Cheques Success');
export const ISSUE_CHEQUES_FAIL = type('[Deposit] Issue Cheques Fail');

export interface CreateDepositRoutePayload extends RoutePayload {
  type: Type;
  productInstance: ProductInstanceAssignment;
}

export interface ChangeDepositRoutePayload extends RoutePayload {
  accountIdentifier: string;
  productInstance: ProductInstance;
  productChangeSet: ProductInstanceChangeSet;
}

export interface IssueChequePayload extends RoutePayload {
  issuingCount: IssuingCount;
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: LoadResourcePayload) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: SelectResourcePayload) { }
}

export class CreateProductInstanceAction implements Action {
  readonly type = CREATE;

  constructor(public payload: CreateDepositRoutePayload) { }
}

export class CreateProductInstanceSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: CreateResourceSuccessPayload) { }
}

export class CreateProductInstanceFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: Error) { }
}

export class UpdateProductInstanceAction implements Action {
  readonly type = UPDATE;

  constructor(public payload: ChangeDepositRoutePayload) { }
}

export class UpdateProductInstanceSuccessAction implements Action {
  readonly type = UPDATE_SUCCESS;

  constructor(public payload: UpdateResourceSuccessPayload) { }
}

export class UpdateProductInstanceFailAction implements Action {
  readonly type = UPDATE_FAIL;

  constructor(public payload: Error) { }
}

export class IssueChequesAction implements Action {
  readonly type = ISSUE_CHEQUES;

  constructor(public payload: IssueChequePayload) { }
}

export class IssueChequesSuccessAction implements Action {
  readonly type = ISSUE_CHEQUES_SUCCESS;

  constructor(public payload: IssueChequePayload) { }
}

export class IssueChequesFailAction implements Action {
  readonly type = ISSUE_CHEQUES_FAIL;

  constructor(public payload: Error) { }
}

export type Actions
  = LoadAction
  | SelectAction
  | CreateProductInstanceAction
  | CreateProductInstanceSuccessAction
  | CreateProductInstanceFailAction
  | UpdateProductInstanceAction
  | UpdateProductInstanceSuccessAction
  | UpdateProductInstanceFailAction
  | IssueChequesAction
  | IssueChequesSuccessAction
  | IssueChequesFailAction;
