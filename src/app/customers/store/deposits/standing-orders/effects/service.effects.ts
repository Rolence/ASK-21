/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import * as standingOrderActions from '../standing-order.actions';
import {Action} from '@ngrx/store';
import {of} from 'rxjs/observable/of';
import {DepositAccountService} from '../../../../../core/services/depositAccount/deposit-account.service';
import {Injectable} from '@angular/core';

@Injectable()
export class DepositStandingOrderApiEffects {

  @Effect()
  loadAll$: Observable<Action> = this.actions$
    .ofType(standingOrderActions.LOAD_ALL)
    .map((action: standingOrderActions.LoadAllAction) => action.payload)
    .mergeMap(identifier =>
      this.depositService.fetchAllStandingOrders(identifier)
        .map(standingOrders => new standingOrderActions.LoadAllCompleteAction(standingOrders))
        .catch(() => of(new standingOrderActions.LoadAllCompleteAction([])))
    );

  @Effect()
  createStandingOrder$: Observable<Action> = this.actions$
    .ofType(standingOrderActions.CREATE)
    .map((action: standingOrderActions.CreateStandingOrderAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.createStandingOrder(payload.identifier, payload.standingOrder)
        .map((sequence: number) => new standingOrderActions.CreateStandingOrderSuccessAction({
          resource: {
            ...payload.standingOrder,
            sequence: sequence.toString()
          },
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new standingOrderActions.CreateStandingOrderFailAction(error)))
    );

  @Effect()
  updateStandingOrder$: Observable<Action> = this.actions$
    .ofType(standingOrderActions.UPDATE)
    .map((action: standingOrderActions.UpdateStandingOrderAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.updateStandingOrder(payload.identifier, payload.standingOrder)
        .map(() => new standingOrderActions.UpdateStandingOrderSuccessAction({
          resource: payload.standingOrder,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new standingOrderActions.UpdateStandingOrderFailAction(error)))
    );

  @Effect()
  deleteStandingOrder$: Observable<Action> = this.actions$
    .ofType(standingOrderActions.DELETE)
    .map((action: standingOrderActions.DeleteStandingOrderAction) => action.payload)
    .mergeMap(payload =>
      this.depositService.deleteStandingOrder(payload.identifier, payload.standingOrder.sequence)
        .map(() => new standingOrderActions.DeleteStandingOrderSuccessAction({
          resource: payload.standingOrder,
          activatedRoute: payload.activatedRoute
        }))
        .catch((error) => of(new standingOrderActions.DeleteStandingOrderFailAction(error)))
    );

  constructor(private actions$: Actions, private depositService: DepositAccountService) { }
}
