/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Action} from '@ngrx/store';
import {
  CreateResourceSuccessPayload,
  DeleteResourceSuccessPayload,
  LoadResourcePayload,
  SelectResourcePayload,
  UpdateResourceSuccessPayload
} from '../../../../core/store/util/resource.reducer';
import {RoutePayload} from '../../../../core/store/util/route-payload';
import {StandingOrder} from '../../../../core/services/depositAccount/domain/instance/standing-order.model';
import {type} from '../../../../core/store/util';

export const LOAD_ALL = type('[Deposit Standing Order] Load All');
export const LOAD_ALL_COMPLETE = type('[Deposit Standing Order] Load All Complete');

export const LOAD = type('[Deposit Standing Order] Load');
export const SELECT = type('[Deposit Standing Order] Select');

export const CREATE = type('[Deposit Standing Order] Create');
export const CREATE_SUCCESS = type('[Deposit Standing Order] Create Success');
export const CREATE_FAIL = type('[Deposit Standing Order] Create Fail');

export const UPDATE = type('[Deposit Standing Order] Update');
export const UPDATE_SUCCESS = type('[Deposit Standing Order] Update Success');
export const UPDATE_FAIL = type('[Deposit Standing Order] Update Fail');

export const DELETE = type('[Deposit Standing Order] Delete');
export const DELETE_SUCCESS = type('[Deposit Standing Order] Delete Success');
export const DELETE_FAIL = type('[Deposit Standing Order] Delete Fail');

export interface StandingOrderRoutePayload extends RoutePayload {
  identifier: string;
  standingOrder: StandingOrder;
}

export class LoadAllAction implements Action {
  readonly type = LOAD_ALL;

  constructor(public payload: string) { }
}

export class LoadAllCompleteAction implements Action {
  readonly type = LOAD_ALL_COMPLETE;

  constructor(public payload: StandingOrder[]) { }
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: LoadResourcePayload) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: SelectResourcePayload) { }
}

export class CreateStandingOrderAction implements Action {
  readonly type = CREATE;

  constructor(public payload: StandingOrderRoutePayload) { }
}

export class CreateStandingOrderSuccessAction implements Action {
  readonly type = CREATE_SUCCESS;

  constructor(public payload: CreateResourceSuccessPayload) { }
}

export class CreateStandingOrderFailAction implements Action {
  readonly type = CREATE_FAIL;

  constructor(public payload: Error) { }
}

export class UpdateStandingOrderAction implements Action {
  readonly type = UPDATE;

  constructor(public payload: StandingOrderRoutePayload) { }
}

export class UpdateStandingOrderSuccessAction implements Action {
  readonly type = UPDATE_SUCCESS;

  constructor(public payload: UpdateResourceSuccessPayload) { }
}

export class UpdateStandingOrderFailAction implements Action {
  readonly type = UPDATE_FAIL;

  constructor(public payload: Error) { }
}

export class DeleteStandingOrderAction implements Action {
  readonly type = DELETE;

  constructor(public payload: StandingOrderRoutePayload) { }
}

export class DeleteStandingOrderSuccessAction implements Action {
  readonly type = DELETE_SUCCESS;

  constructor(public payload: DeleteResourceSuccessPayload) { }
}

export class DeleteStandingOrderFailAction implements Action {
  readonly type = DELETE_FAIL;

  constructor(public payload: Error) { }
}

export type Actions
  = LoadAllAction
  | LoadAllCompleteAction
  | CreateStandingOrderAction
  | CreateStandingOrderSuccessAction
  | CreateStandingOrderFailAction
  | UpdateStandingOrderAction
  | UpdateStandingOrderSuccessAction
  | UpdateStandingOrderFailAction
  | DeleteStandingOrderAction
  | DeleteStandingOrderSuccessAction
  | DeleteStandingOrderFailAction;
