/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as outlineActions from './deposit-outline.actions';
import * as depositActions from './deposit.actions';
import {ProductInstanceOutline} from '../../../core/services/depositAccount/domain/instance/product-instance-outline.model';

export interface State {
  entities: ProductInstanceOutline[];
}

export const initialState: State = {
  entities: [],
};

export function reducer(state = initialState, action: outlineActions.Actions | depositActions.Actions): State {

  switch (action.type) {

    case outlineActions.LOAD_ALL: {
      return initialState;
    }

    case outlineActions.LOAD_ALL_COMPLETE: {
      return {
        entities: action.payload
      };
    }

    case depositActions.CREATE_SUCCESS: {
      return {
        entities: [...state.entities, action.payload.resource]
      };
    }

    default: {
      return state;
    }
  }
}

export const getEntities = (state: State) => state.entities;
