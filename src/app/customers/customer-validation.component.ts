/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SettingsService} from '../core/services/customer/settings.service';
import {CustomerVerification} from '../core/services/customer/domain/customer-verification.model';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomerDoubletValidationValue} from '../core/services/customer/domain/customer-doublet-validation-value.model';

@Component({
  templateUrl: './customer-validation.component.html',
  styleUrls: ['./customer-validation.component.scss']
})
export class CustomerValidationComponent {

  form: FormGroup;

  validated: boolean;
  valid: boolean;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private route: ActivatedRoute, private settingsService: SettingsService) {
    this.form = this.formBuilder.group({
      values: this.initValues([])
    });

    this.settingsService.getCustomerDoubletValidation()
      .subscribe(validation => validation.values.forEach(value => this.addValue(value)));
  }

  validate(): void {
    const valuesArray = this.form.get('values') as FormArray;
    const customerVerification: CustomerVerification = {
      entries: valuesArray.getRawValue().map(value => {
        if (value.field === 'DATE_OF_BIRTH') {
          value.value = value.value + 'Z';
        }

        return value;
      })
    };
    this.settingsService.verifyCustomer(customerVerification)
      .subscribe(valid => {
        this.valid = valid;
        this.validated = true;
      });
  }

  proceed(): void {
    this.router.navigate(['../create'], { relativeTo: this.route, queryParams: { validated: true } });
  }

  cancel(): void {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  private initValues(values: CustomerDoubletValidationValue[]): FormArray {
    const formControls: FormGroup[] = [];
    values.forEach(value => formControls.push(this.initValue(value)));
    return this.formBuilder.array(formControls);
  }

  initValue(value?: CustomerDoubletValidationValue): FormGroup {
    return this.formBuilder.group({
      field: [value.field, [Validators.required]],
      value: [null, [Validators.required]]
    });
  }

  addValue(value?: CustomerDoubletValidationValue): void {
    const values: FormArray = this.form.get('values') as FormArray;
    values.push(this.initValue(value));
  }

  get values(): AbstractControl[] {
    const values: FormArray = this.form.get('values') as FormArray;
    return values.controls;
  }

}
