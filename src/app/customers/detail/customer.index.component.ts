/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {SelectAction} from '../store/customer.actions';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {LoadAllAction as LoadAllDepositOutlineAction} from '../store/deposits/deposit-outline.actions';
import {LoadAllAction as LoadAllLoanOutlineAction} from '../store/loan-agreements/loan-agreement-outline.actions';
import * as fromCustomers from '../store';
import {Store} from '@ngrx/store';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';
import {Customer} from '../../core/services/customer/domain/customer.model';

@Component({
  templateUrl: './customer.index.component.html'
})
export class CustomerIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private customerSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromCustomers.State>, private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => params['id'])
      .subscribe(customerIdentifier => {
        this.store.dispatch(new SelectAction(customerIdentifier));
        this.store.dispatch(new LoadAllDepositOutlineAction(customerIdentifier));
        this.store.dispatch(new LoadAllLoanOutlineAction(customerIdentifier));
      });

    this.customerSubscription = this.store.select(fromCustomers.getSelectedCustomer)
      .filter(customer => !!customer)
      .subscribe((customer: Customer) =>
        this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, `${customer.givenName} ${customer.surname}`)
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.customerSubscription.unsubscribe();
  }
}
