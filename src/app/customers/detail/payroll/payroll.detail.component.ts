/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../store/index';
import {ITdDataTableColumn} from '@covalent/core';
import {Store} from '@ngrx/store';
import {TableData} from '../../../shared/data-table/data-table.component';
import {PayrollConfiguration} from '../../../core/services/payroll/domain/payroll-configuration.model';
import {Customer} from '../../../core/services/customer/domain/customer.model';
import {DisplayFimsNumber} from '../../../shared/number/fims-number.pipe';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './payroll.detail.component.html',
  providers: [DisplayFimsNumber]
})
export class CustomerPayrollDetailComponent {

  customer$: Observable<Customer>;
  distribution$: Observable<PayrollConfiguration>;
  allocationData: TableData;
  columns: ITdDataTableColumn[] = [
    { name: 'accountNumber', label: 'Account number' },
    { name: 'amount', label: 'Amount', format: value => this.displayFimsNumber.transform(value) },
    { name: 'proportional', label: 'Proportional?' }
  ];

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>, private displayFimsNumber: DisplayFimsNumber) {
    this.distribution$ = store.select(fromCustomers.getPayrollDistribution)
      .filter(distribution => !!distribution)
      .do(distribution => this.allocationData = {
        data: distribution.payrollAllocations,
        totalElements: distribution.payrollAllocations.length,
        totalPages: 1
      });

    this.customer$ = store.select(fromCustomers.getSelectedCustomer);
  }

  editPayroll(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
