/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component} from '@angular/core';
import {PayrollConfiguration} from '../../../../core/services/payroll/domain/payroll-configuration.model';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../../store/index';
import {ActivatedRoute, Router} from '@angular/router';
import {UpdatePayrollDistributionAction} from '../../../store/payroll/payroll.actions';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';
import {CustomerLoanService} from '../../../../core/services/loan/customer-loan.service';
import {PayrollAccountEntry} from './form.component';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './create.form.component.html'
})
export class CreateCustomerPayrollFormComponent {

  distribution$: Observable<PayrollConfiguration>;
  productInstances$: Observable<PayrollAccountEntry[]>;
  loanAgreements$: Observable<PayrollAccountEntry[]>;

  constructor(private store: Store<fromCustomers.State>, private router: Router, private route: ActivatedRoute,
              private depositService: DepositAccountService, private customerLoanService: CustomerLoanService) {
    this.distribution$ = store.select(fromCustomers.getPayrollDistribution);

    const selectedCustomer$ = this.store.select(fromCustomers.getSelectedCustomer);

    this.productInstances$ = selectedCustomer$
      .mergeMap(customer =>
        this.depositService.fetchProductInstancesByCustomer(customer.identifier)
          .map(instances => instances.filter(instance => instance.state === 'ACTIVE'))
          .map(instances => instances.map(instance => ({
            label: `${instance.accountIdentifier}(${instance.productShortCode})`,
            accountIdentifier: instance.accountIdentifier,
            accountType: 'DEPOSIT'
          })))
      );

    this.loanAgreements$ = selectedCustomer$
      .mergeMap(customer =>
        this.customerLoanService.fetchAllLoanAgreements(customer.identifier)
          .map(agreements => agreements.filter(agreement => agreement.state === 'DISBURSED' || agreement.state === 'DELINQUENT'))
          .map(agreements => agreements.map(agreement => ({
            label: `${agreement.loanAccount}(${agreement.productShortName})`,
            accountIdentifier: agreement.loanAccount,
            accountType: 'LOAN',
            nextPayment: agreement.nextPayment
          })))
      );
  }

  onSave(distribution: PayrollConfiguration): void {
    this.store.select(fromCustomers.getSelectedCustomer)
      .take(1)
      .map(customer => new UpdatePayrollDistributionAction({
        customerId: customer.identifier,
        distribution,
        activatedRoute: this.route
      }))
      .subscribe(this.store);
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
