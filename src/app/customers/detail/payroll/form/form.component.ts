/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {PayrollConfiguration} from '../../../../core/services/payroll/domain/payroll-configuration.model';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PayrollAllocation} from '../../../../core/services/payroll/domain/payroll-allocation.model';
import {accountUnique} from './validator/account-unique.validator';
import {AccountType} from '../../../../core/services/payroll/domain/account-type.model';
import {FimsValidators} from '../../../../core/validator/validators';

export interface PayrollAccountEntry {
  label: string;
  accountIdentifier: string;
  accountType: string;
  nextPayment?: string;
}

@Component({
  selector: 'aten-customer-payroll-form',
  templateUrl: './form.component.html'
})
export class CustomerPayrollFormComponent implements OnChanges {

  form: FormGroup;

  @Input('productInstances') productInstances: PayrollAccountEntry[];
  @Input('loanAgreements') loanAgreements: PayrollAccountEntry[];
  @Input('distribution') distribution: PayrollConfiguration;

  @Output() save = new EventEmitter<PayrollConfiguration>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      mainAccountNumber: ['', [Validators.required]],
      employeeId: [''],
      payrollAllocations: this.initAllocations([])
    }, { validator: accountUnique });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.distribution) {
      this.form.reset({
        mainAccountNumber: this.distribution.mainAccountNumber,
        employeeId: this.distribution.employeeId
      });
    }

    if (this.distribution && this.productInstances && this.loanAgreements) {
      this.distribution.payrollAllocations.forEach(allocation => this.addAllocation(allocation));
    }
  }

  onSave(): void {
    const formArray = this.form.get('payrollAllocations') as FormArray;
    const payrollAllocations: PayrollAllocation[] = formArray.getRawValue()
      .map(allocation => ({
        accountNumber: allocation.accountNumber,
        amount: allocation.amount,
        proportional: allocation.proportional,
        accountType: this.findAccountType(allocation.accountNumber)
      }));

    const distribution: PayrollConfiguration = {
      ...this.distribution,
      mainAccountNumber: this.form.get('mainAccountNumber').value,
      employeeId: this.form.get('employeeId').value,
      payrollAllocations
    };

    this.save.emit(distribution);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  private findAccountType(accountIdentifier: string): AccountType {
    const accountEntry = this.findAccount(accountIdentifier);
    return accountEntry.accountType as AccountType;
  }

  private findAccount(accountIdentifier): PayrollAccountEntry {
    const allEntries = this.productInstances.concat(this.loanAgreements);

    return allEntries
      .find(entry => entry.accountIdentifier === accountIdentifier);
  }

  private initAllocations(allocations: PayrollAllocation[]): FormArray {
    const formControls: FormGroup[] = [];
    allocations.forEach(allocation => formControls.push(this.initAllocation(allocation)));
    return this.formBuilder.array(formControls);
  }

  private initAllocation(allocation?: PayrollAllocation): FormGroup {
    const accountNumber = allocation ? allocation.accountNumber : '';
    const formGroup = this.formBuilder.group({
      accountNumber: [accountNumber, [Validators.required]],
      amount: [allocation ? allocation.amount : '', [
        Validators.required,
        FimsValidators.minValue(0.001),
        FimsValidators.maxValue(9999999999.99999)]
      ],
      proportional: [allocation ? allocation.proportional : false]
    });

    formGroup.get('accountNumber').valueChanges
      .startWith(accountNumber)
      .filter(accountNumberInput => !!accountNumberInput)
      .subscribe(accountNumberInput => {
        const account = this.findAccount(accountNumberInput);
        const amountControl = formGroup.get('amount');
        const proportionalControl = formGroup.get('proportional');

        if (!account) {
          amountControl.enable();
          proportionalControl.enable();
          return;
        }

        if (account.accountType === 'LOAN') {
          amountControl.setValue(account.nextPayment);
          amountControl.disable();
          proportionalControl.setValue(false);
          proportionalControl.disable();
        } else {
          amountControl.enable();
          proportionalControl.enable();
        }
      });

    return formGroup;
  }

  addAllocation(allocation?: PayrollAllocation): void {
    const allocations: FormArray = this.form.get('payrollAllocations') as FormArray;
    allocations.push(this.initAllocation(allocation));
  }

  removeAllocation(index: number): void {
    const allocations: FormArray = this.form.get('payrollAllocations') as FormArray;
    allocations.removeAt(index);
  }

  get allocations(): AbstractControl[] {
    const allocations: FormArray = this.form.get('payrollAllocations') as FormArray;
    return allocations.controls;
  }
}
