/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import {Component} from '@angular/core';
import {Customer} from '../../core/services/customer/domain/customer.model';
import {Catalog} from '../../core/services/catalog/domain/catalog.model';
import * as fromCustomers from '../store';
import {CustomerService} from '../../core/services/customer/customer.service';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {CustomerAccountOutline} from '../../shared/customer-account-list/domain/customer-account-outline.model';

@Component({
  templateUrl: './customer.detail.component.html',
  styleUrls: ['./customer.detail.component.scss']
})
export class CustomerDetailComponent {

  customer$: Observable<Customer>;
  portrait$: Observable<{} | Blob>;
  catalog$: Observable<Catalog>;
  depositOutlines$: Observable<CustomerAccountOutline[]>;
  loanOutlines$: Observable<CustomerAccountOutline[]>;
  pendingAssessments$: Observable<number>;

  constructor(private route: ActivatedRoute, private router: Router, private store: Store<fromCustomers.State>,
              private customerService: CustomerService) {
    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer)
      .filter(customer => !!customer);
    this.catalog$ = this.store.select(fromCustomers.getCustomerCatalog);

    this.portrait$ = this.customer$
      .switchMap(customer => this.customerService.getPortrait(customer.identifier));

    this.depositOutlines$ = this.store.select(fromCustomers.getDepositAccountOutlines);
    this.loanOutlines$ = this.store.select(fromCustomers.getLoanAccountOutlines);
    this.pendingAssessments$ = this.store.select(fromCustomers.pendingAssessments);
  }

  searchCustomer(term): void {
    if (term) {
      this.router.navigate(['../../../'], { queryParams: { term: term }, relativeTo: this.route });
    }
  }

  changePortrait(): void {
    this.router.navigate(['portrait'], { relativeTo: this.route });
  }

  editCustomer(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  goToTasks(): void {
    this.router.navigate(['tasks'], { relativeTo: this.route });
  }

  goToLoanAgreements(): void {
    this.router.navigate(['loans'], { relativeTo: this.route });
  }

  goToDepositAccounts(): void {
    this.router.navigate(['deposits'], { relativeTo: this.route });
  }

  goToIdentificationCards(): void {
    this.router.navigate(['identifications'], { relativeTo: this.route });
  }

  goToActivities(): void {
    this.router.navigate(['activities'], { relativeTo: this.route });
  }

  goToPayroll(): void {
    this.router.navigate(['payroll'], { relativeTo: this.route });
  }
}
