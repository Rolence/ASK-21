/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import * as fromCustomers from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {Customer} from '../../../core/services/customer/domain/customer.model';
import {NotificationService, NotificationType} from '../../../core/services/notification/notification.service';
import {CustomerService} from '../../../core/services/customer/customer.service';
import {DialogService} from '../../../core/services/dialog/dialog.service';

@Component({
  templateUrl: './portrait.component.html'
})
export class CustomerPortraitComponent implements OnInit, OnDestroy {

  private customerSubscription: Subscription;

  customer: Customer;
  fileSelectMsg = 'No file selected yet.';
  invalidSize = false;
  portrait: Blob | {};

  constructor(private router: Router, private route: ActivatedRoute, private customerService: CustomerService,
              private store: Store<fromCustomers.State>, private notificationService: NotificationService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.customerSubscription = this.store.select(fromCustomers.getSelectedCustomer)
      .do(customer => this.customer = customer)
      .flatMap(customer => this.customerService.getPortrait(customer.identifier))
      .subscribe(portrait => this.portrait = portrait);
  }

  ngOnDestroy(): void {
    this.customerSubscription.unsubscribe();
  }

  selectEvent(file: File): void {
    this.invalidSize = file.size > 524288;
    this.fileSelectMsg = file.name;
  }

  uploadEvent(file: File): void {
    if (this.invalidSize) {
      return;
    }

    this.customerService.uploadPortrait(this.customer.identifier, file).subscribe(() => {
      this.notificationService.send({
        type: NotificationType.MESSAGE,
        message: 'Portrait is going to be uploaded'
      });
      this.navigateAway();
    });
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete the portrait?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE PORTRAIT'
    });
  }

  deletePortrait(): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .flatMap(() => this.customerService.deletePortrait(this.customer.identifier))
      .subscribe(() => {
        this.notificationService.send({
          type: NotificationType.MESSAGE,
          message: 'Portrait is going to be deleted'
        });
        this.navigateAway();
      });
  }

  navigateAway(): void {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
