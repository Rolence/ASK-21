/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import * as fromCustomers from '../../store/index';
import {Observable} from 'rxjs/Observable';
import {LOAD_ALL} from '../../store/identityCards/identity-cards.actions';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {TableData} from '../../../shared/data-table/data-table.component';
import {IdentificationCard} from '../../../core/services/customer/domain/identification-card.model';

@Component({
  templateUrl: './identity-card.list.component.html'
})
export class CustomerIdentityCardListComponent {

  identityCardData$: Observable<TableData>;
  columns: any[] = [
    { name: 'number', label: 'Number' },
    { name: 'type', label: 'Type' },
    { name: 'issuer', label: 'Issuer' }
  ];

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>) {
    this.identityCardData$ = this.store.select(fromCustomers.getAllCustomerIdentificationCardEntities)
      .map(entities => ({
        data: entities,
        totalElements: entities.length,
        totalPages: 1
      }));

    this.store.select(fromCustomers.getSelectedCustomer)
      .take(1)
      .subscribe(customer => {
        this.store.dispatch({ type: LOAD_ALL, payload: customer.identifier});
      });
  }

  rowSelect(identificationCard: IdentificationCard): void {
    this.router.navigate(['detail', identificationCard.number], { relativeTo: this.route });
  }

  addIdentification(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
