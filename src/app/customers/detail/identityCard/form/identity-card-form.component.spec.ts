/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IdentityCardFormComponent} from './identity-card-form.component';
import {By} from '@angular/platform-browser';
import {Component, DebugElement} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from '../../../../shared/shared.module';
import {dateAsISOString, toFimsDate} from '../../../../core/services/domain/date.converter';
import {IdentificationCard} from '../../../../core/services/customer/domain/identification-card.model';
import {setValueByCssSelector} from '../../../../core/testing/input-fields';
import {RouterTestingModule} from '@angular/router/testing';

describe('Test identity card form component', () => {

  let fixture: ComponentFixture<TestComponent>;

  let formComponent: TestComponent;

  let oneDayAhead: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestComponent,
        IdentityCardFormComponent
      ],
      imports: [
        TranslateModule.forRoot(),
        SharedModule.forRoot(),
        RouterTestingModule,
        NoopAnimationsModule
      ]
    });

    fixture = TestBed.createComponent(TestComponent);
    formComponent = fixture.componentInstance;

    fixture.detectChanges();
  }));

  beforeEach(() => {
    const today = new Date();
    today.setDate(today.getDate() + 1);
    oneDayAhead = dateAsISOString(today);
  });

  function setValidValues(): void {
    setValueByCssSelector(fixture, '#number', 'test');
    setValueByCssSelector(fixture, '#type', 'test');
    setValueByCssSelector(fixture, '#expirationDate', oneDayAhead);
    setValueByCssSelector(fixture, '#issuer', 'test');
  }

  it('should disable/enable save button', () => {
    const button: DebugElement = fixture.debugElement.query(By.css('button[color="primary"]'));

    expect(button.properties['disabled']).toBeTruthy('Button should be disabled');

    setValidValues();

    fixture.detectChanges();

    expect(button.properties['disabled']).toBeFalsy('Button should be enabled');
  });

  it('should set the correct form values', () => {
    setValidValues();

    fixture.detectChanges();

    const button: DebugElement = fixture.debugElement.query(By.css('button[color="primary"]'));

    button.nativeElement.click();

    const expectedResult: IdentificationCard = {
      type: 'test',
      issuer: 'test',
      expirationDate: toFimsDate(oneDayAhead),
      number: 'test'
    };

    fixture.detectChanges();

    expect(expectedResult).toEqual(fixture.componentInstance.output);
  });

});

@Component({
  template: '<aten-identity-card-form [identificationCard]="input" (save)="onSave($event)"></aten-identity-card-form>'
})
class TestComponent {

  input: IdentificationCard = {
    type: '',
    number: '',
    expirationDate: null
  };

  output: IdentificationCard;

  onSave(identificationCard: IdentificationCard): void {
    this.output = identificationCard;
  }
}
