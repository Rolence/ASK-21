/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {CustomerLoanService} from '../../../core/services/loan/customer-loan.service';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {Restructuring} from '../../../core/services/loan/domain/agreement/restructuring.model';
import {Store} from '@ngrx/store';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';
import {TableData} from '../../../shared/data-table/data-table.component';

@Component({
  templateUrl: './restructuring.list.component.html',
  providers: [DatePipe]
})
export class LoanAgreementRestructuringListComponent {

  loanAgreement$: Observable<LoanAgreement>;
  restructuringsData$: Observable<TableData>;

  columns: any[] = [
    { name: 'note', label: 'Note' },
    { name: 'createdBy', label: 'Created by' },
    { name: 'createdOn', label: 'Created on', format: (v: any) => this.datePipe.transform(v, 'short') }
  ];

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>,
              private customerLoanService: CustomerLoanService, private datePipe: DatePipe) {
    this.loanAgreement$ = store.select(fromCustomers.getSelectedAgreement);
    this.restructuringsData$ = Observable.combineLatest(
      store.select(fromCustomers.getSelectedCustomer),
      this.loanAgreement$
    ).mergeMap(([customer, agreement]) => customerLoanService.fetchRestructurings(customer.identifier, agreement.loanAccount))
     .map(restructurings => ({
       data: restructurings,
       totalElements: restructurings.length,
       totalPages: 1
     }));
  }

  rowSelect(restructuring: Restructuring): void {
    this.router.navigate(['detail', restructuring.sequence], { relativeTo: this.route });
  }

}
