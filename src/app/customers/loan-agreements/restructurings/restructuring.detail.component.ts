/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {CustomerLoanService} from '../../../core/services/loan/customer-loan.service';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../store';
import {RestructureDetail} from '../../../core/services/loan/domain/agreement/restructure-detail.model';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {LoanAgreement} from '../../../core/services/loan/domain/agreement/loan-agreement.model';

@Component({
  templateUrl: './restructuring.detail.component.html'
})
export class LoanAgreementRestructuringDetailComponent {

  loanAgreement$: Observable<LoanAgreement>;
  restructureDetail$: Observable<RestructureDetail>;

  constructor(private route: ActivatedRoute, private store: Store<fromCustomers.State>, private customerLoanService: CustomerLoanService) {
    this.loanAgreement$ = store.select(fromCustomers.getSelectedAgreement);
    this.restructureDetail$ = Observable.combineLatest(
      store.select(fromCustomers.getSelectedCustomer),
      store.select(fromCustomers.getSelectedAgreement),
      route.params.map(params => params['sequence'])
    ).mergeMap(([customer, agreement, sequence]) =>
      customerLoanService.getRestructuring(customer.identifier, agreement.loanAccount, sequence)
    );
  }
}
