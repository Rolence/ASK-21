/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {DebtToIncomeEntry} from '../../../../core/services/loan/domain/agreement/debt-to-income-entry.model';
import {DTIConfig} from '../../../../core/services/loan/domain/definition/DTI-config.model';

@Component({
  selector: 'aten-debt-to-income-form',
  templateUrl: './debt-to-income.component.html'
})
export class DebtToIncomeFormComponent {

  @Input() dtiConfig: DTIConfig;
  @Input() formGroup: FormGroup;

  @Output() onAddMonthlyIncomeEntry = new EventEmitter<void>();
  @Output() onRemoveMonthlyEntry = new EventEmitter<number>();
  @Output() onAddObligationEntry = new EventEmitter<void>();
  @Output() onRemoveObligationEntry = new EventEmitter<number>();

  constructor() {}

  get ratio(): number {
    return this.formGroup.get('ratio').value;
  }

  addMonthlyIncomeEntry(): void {
    this.onAddMonthlyIncomeEntry.emit();
  }

  removeMonthlyIncomeEntry(index: number): void {
    this.onRemoveMonthlyEntry.emit(index);
  }

  addObligationEntry(): void {
    this.onAddObligationEntry.emit();
  }

  removeObligationEntry(index: number): void {
    this.onRemoveObligationEntry.emit(index);
  }

  get obligations(): FormArray {
    return this.formGroup.get('monthlyObligations') as FormArray;
  }

  get incomes(): FormArray {
    return this.formGroup.get('monthlyIncome') as FormArray;
  }

  get incomeTotal(): number {
    return this.sum(this.incomes.value);
  }

  get debtTotal(): number {
    return this.sum(this.obligations.value);
  }

  private sum(factors: DebtToIncomeEntry[]): number {
    return factors.reduce((acc, val) => {
      if (val.value) {
        return acc + parseFloat(val.value);
      }
      return 0;
    }, 0);
  }
}
