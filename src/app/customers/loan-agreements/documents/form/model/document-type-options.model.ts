/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Type} from '../../../../../core/services/loan/domain/agreement/document-meta-data.model';

export interface TypeOption {
  label: string;
  type: Type;
}

export const documentTypeOptionList: TypeOption[] = [
  { type: 'LOAN_AGREEMENT', label: 'Loan agreement' },
  { type: 'GUARANTOR_AGREEMENT', label: 'Guarantor agreement' },
  { type: 'INCOME_RECORD', label: 'Income record' },
  { type: 'OBLIGATION_RECORD', label: 'Obligation record' },
  { type: 'CREDIT_SCORE_RECORD', label: 'Credit score record' },
  { type: 'CERTIFICATE', label: 'Certificate' },
  { type: 'CUSTOM', label: 'Custom' }
];
