/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {documentTypeOptionList} from './model/document-type-options.model';
import {Document} from '../../../../core/services/loan/domain/agreement/document-meta-data.model';
import {FimsValidators} from '../../../../core/validator/validators';

@Component({
  selector: 'aten-loan-agreement-document-form',
  templateUrl: './form.component.html'
})
export class LoanAgreementDocumentFormComponent {

  formGroup: FormGroup;
  typeOptions = documentTypeOptionList;

  @Output() save = new EventEmitter<Document>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
      type: ['', [Validators.required]],
      file: ['', [Validators.required, FimsValidators.maxFileSize(1024), FimsValidators.mediaType(
        ['application/pdf', 'image/png', 'image/jpeg'])
      ]],
      description: [''],
      expirationDate: ['', [FimsValidators.afterToday]],
    });
  }

  onSave(): void {
    const file: File = this.formGroup.get('file').value;
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      const document: Document = {
        type: this.formGroup.get('type').value,
        fileName: file.name.substring(0, file.name.lastIndexOf('.')),
        contentType: file.type,
        contentInBase64: reader.result.split(',')[1],
        description: this.formGroup.get('description').value,
        expirationDate: this.formGroup.get('expirationDate').value
      };

      this.save.emit(document);
    };
  }

  onCancel(): void {
    this.cancel.emit();
  }
}
