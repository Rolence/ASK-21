/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {of} from 'rxjs/observable/of';
import * as fromCustomers from '../store';
import {Injectable} from '@angular/core';
import {LoadAction} from '../store/loan-agreements/loan-agreement.actions';
import {LoadAllAssessmentsAction} from '../store/loan-agreements/loan-assessment.actions';
import {Store} from '@ngrx/store';
import {ExistsGuardService} from '../../core/services/guards/exists-guard';
import {CustomerLoanService} from '../../core/services/loan/customer-loan.service';

@Injectable()
export class LoanAgreementExistsGuard implements CanActivate {

  constructor(private store: Store<fromCustomers.State>,
              private customerLoanService: CustomerLoanService,
              private existsGuardService: ExistsGuardService) {}

  hasAgreementInStore(loanAccount: string): Observable<boolean> {
    const timestamp$ = this.store.select(fromCustomers.getAgreementsLoadedAt)
      .map(loadedAt => loadedAt[loanAccount]);

    return this.existsGuardService.isWithinExpiry(timestamp$);
  }

  hasAgreementInApi(customerNumber: string, loanAccount: string): Observable<boolean> {
    const getProduct = this.customerLoanService.findLoanAgreement(customerNumber, loanAccount)
      .do(resource => this.store.dispatch(new LoadAction({ resource })))
      .do(() => this.store.dispatch(new LoadAllAssessmentsAction({
        customerNumber,
        loanAccount
      })))
      .map(product => !!product);

    return this.existsGuardService.routeTo404OnError(getProduct);
  }

  hasAgreement(customerNumber: string, loanAccount: string): Observable<boolean> {
    return this.hasAgreementInStore(loanAccount)
      .switchMap(inStore => {
        if (inStore) {
          return of(inStore);
        }

        return this.hasAgreementInApi(customerNumber, loanAccount);
      });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.hasAgreement(route.parent.parent.parent.params['id'], route.params['id']);
  }
}
