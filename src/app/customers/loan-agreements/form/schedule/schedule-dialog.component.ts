/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AmortizationSchedule} from '../../../../core/services/loan/domain/finance/amortization-schedule.model';
import {Observable} from 'rxjs/Observable';
import {ConsumerLoanService} from '../../../../core/services/loan/consumer-loan.service';
import {Term} from '../../../../core/services/loan/domain/agreement/term.model';

export interface CalculateScheduleData {
  shortName: string;
  term: Term;
}

@Component({
  templateUrl: './schedule-dialog.component.html'
})
export class LoanAgreementScheduleDialogComponent {

  amortizationSchedule$: Observable<AmortizationSchedule>;

  constructor(public dialogRef: MatDialogRef<LoanAgreementScheduleDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: CalculateScheduleData, private consumerLoanService: ConsumerLoanService) {
    this.amortizationSchedule$ = consumerLoanService.calculateAmortizationSchedule(data.shortName, data.term);
  }
}
