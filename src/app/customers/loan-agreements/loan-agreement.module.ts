/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {NgModule} from '@angular/core';
import {LoanAgreementExistsGuard} from './loan-agreement-exists.guard';
import {LoanAgreementListComponent} from './loan-agreement.list.component';
import {LoanAgreementRoutingModule} from './loan-agreement-routing.module';
import {LoanAgreementFormComponent} from './form/form.component';
import {LoanAgreementCreateComponent} from './form/create.component';
import {LoanAgreementDetailComponent} from './loan-agreement.detail.component';
import {LoanAgreementIndexComponent} from './loan-agreement.index.component';
import {LoanAgreementScheduleDialogComponent} from './form/schedule/schedule-dialog.component';
import {DebtToIncomeEntryFormComponent} from './components/debt-to-income/debt-to-income-entry.component';
import {DebtToIncomeFormComponent} from './components/debt-to-income/debt-to-income.component';
import {LoanAgreementDocumentListComponent} from './documents/document.list.component';
import {LoanAgreementDocumentFormComponent} from './documents/form/form.component';
import {CreateLoanAgreementDocumentComponent} from './documents/form/create.component';
import {LoanAgreementStateMessageComponent} from './components/state-message/state-message.component';
import {LoanAgreementConfirmationDialogComponent} from './components/confirmation/confirmation-dialog.component';
import {LoanAgreementStateIconComponent} from './components/state-icon/state-icon.component';
import {LoanAgreementChargesFormComponent} from './components/charges/charges.component';
import {DebtToIncomeFormService} from './form/services/deb-to-income.service';
import {LoanAgreementEditComponent} from './form/edit.component';
import {ChargesFormService} from './form/services/charges.service';
import {LoanAgreementRestructuringListComponent} from './restructurings/restructuring.list.component';
import {LoanAgreementRestructuringDetailComponent} from './restructurings/restructuring.detail.component';
import {PaymentPeriodLabelPipe} from './pipes/payment-period-label.pipe';
import {LoanAgreementCollateralFormComponent} from './components/collateral/collateral.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    LoanAgreementRoutingModule,
    SharedModule
  ],
  declarations: [
    LoanAgreementListComponent,
    LoanAgreementIndexComponent,
    LoanAgreementFormComponent,
    LoanAgreementCreateComponent,
    LoanAgreementEditComponent,
    LoanAgreementDetailComponent,
    LoanAgreementScheduleDialogComponent,
    DebtToIncomeFormComponent,
    DebtToIncomeEntryFormComponent,
    LoanAgreementDocumentListComponent,
    CreateLoanAgreementDocumentComponent,
    LoanAgreementDocumentFormComponent,
    LoanAgreementStateMessageComponent,
    LoanAgreementConfirmationDialogComponent,
    LoanAgreementStateIconComponent,
    LoanAgreementChargesFormComponent,
    LoanAgreementCollateralFormComponent,
    LoanAgreementRestructuringListComponent,
    LoanAgreementRestructuringDetailComponent,
    PaymentPeriodLabelPipe
  ],
  entryComponents: [
    LoanAgreementScheduleDialogComponent,
    LoanAgreementConfirmationDialogComponent
  ],
  providers: [
    LoanAgreementExistsGuard,
    ChargesFormService,
    DebtToIncomeFormService
  ]
})
export class LoanProductModule {}
