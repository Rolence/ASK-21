/**
 * Copyright 2018 The Mifos Initiative.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {SelectAction} from '../store/loan-agreements/loan-agreement.actions';
import * as fromCustomers from '../store';
import {Store} from '@ngrx/store';
import {LoanAgreement} from '../../core/services/loan/domain/agreement/loan-agreement.model';
import {BreadCrumbService} from '../../shared/bread-crumbs/bread-crumb.service';

@Component({
  templateUrl: './loan-agreement.index.component.html'
})
export class LoanAgreementIndexComponent implements OnInit, OnDestroy {

  private actionsSubscription: Subscription;
  private agreementSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromCustomers.State>, private breadCrumbService: BreadCrumbService) {}

  ngOnInit(): void {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.agreementSubscription = this.store.select(fromCustomers.getSelectedAgreement)
      .filter(agreement => !!agreement)
      .subscribe((agreement: LoanAgreement) =>
        this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, agreement.loanAccount)
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.agreementSubscription.unsubscribe();
  }
}
