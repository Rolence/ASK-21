/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import * as fromCustomers from '../../store/index';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Store} from '@ngrx/store';
import {ExistsGuardService} from '../../../core/services/guards/exists-guard';
import {SettingsService} from '../../../core/services/customer/settings.service';
import {LoadAction} from '../../store/settings/account-number.actions';

@Injectable()
export class CustomerAccountNumberFormatExistsGuard implements CanActivate {

  constructor(private store: Store<fromCustomers.State>,
              private settingsService: SettingsService,
              private existsGuardService: ExistsGuardService) {}

  hasCustomerFormatInStore(): Observable<boolean> {
    const timestamp$ = this.store.select(fromCustomers.getCustomerNumberFormatLoadedAt);
    return this.existsGuardService.isWithinExpiry(timestamp$);
  }

  loadCustomerFormatFromApi(): Observable<boolean> {
    const getCustomerFormat$ = this.settingsService.getCustomerNumberFormat(true)
      .catch(() => {
        return Observable.of({
          prefix: '',
          length: null
        });
      })
      .map(customerNumberFormat => new LoadAction({ customerNumberFormat }))
      .do((action: LoadAction) => this.store.dispatch(action))
      .map(customerNumberFormat => !!customerNumberFormat);

    return this.existsGuardService.routeTo404OnError(getCustomerFormat$);
  }

  hasCustomerFormat(): Observable<boolean> {
    return this.hasCustomerFormatInStore()
      .switchMap(inStore => {
        if (inStore) {
          return of(inStore);
        }
        return this.loadCustomerFormatFromApi();
      });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.hasCustomerFormat();
  }
}
