/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {CustomerNumberFormat} from '../../../../core/services/customer/domain/customer-number-format.model';
import {Component} from '@angular/core';
import * as fromCustomers from '../../../store';
import {UpdateAccountNumberAction} from '../../../store/settings/account-number.actions';
import {Observable} from 'rxjs/Observable';

@Component({
  templateUrl: './create.form.component.html'
})
export class CreateCustomerNumberFormatFormComponent {

  customerNumberFormat$: Observable<CustomerNumberFormat>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>) {
    this.customerNumberFormat$ = store.select(fromCustomers.getCustomerNumberFormat);
  }

  onSave(customerNumberFormat: CustomerNumberFormat): void {
    this.store.dispatch(new UpdateAccountNumberAction({
      customerNumberFormat,
      activatedRoute: this.route
    }));
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
