/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomerNumberFormat} from '../../../../core/services/customer/domain/customer-number-format.model';
import {FimsValidators} from '../../../../core/validator/validators';

@Component({
  selector: 'aten-customer-number-format-form',
  templateUrl: './form.component.html'
})
export class CustomerNumberFormatFormComponent implements OnChanges {

  form: FormGroup;

  @Input('customerNumberFormat') customerNumberFormat: CustomerNumberFormat;

  @Output() save = new EventEmitter<CustomerNumberFormat>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      prefix: ['', [Validators.minLength(0), Validators.maxLength(8)]],
      length: ['', [Validators.required, FimsValidators.minValue(3), FimsValidators.maxValue(32)]]
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.customerNumberFormat) {
      this.form.reset({
        prefix: this.customerNumberFormat.prefix,
        length: this.customerNumberFormat.length
      });
    }
  }

  onSave(): void {
    const format: CustomerNumberFormat = {
      prefix: this.form.get('prefix').value,
      length: this.form.get('length').value
    };

    this.save.emit(format);
  }

  onCancel(): void {
    this.cancel.emit();
  }

}
