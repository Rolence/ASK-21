/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {FormGroup, ValidationErrors} from '@angular/forms';
import {CustomerDoubletValidationValue} from '../../../../../core/services/customer/domain/customer-doublet-validation-value.model';

export function fieldUnique(group: FormGroup): ValidationErrors | null {
  const values: CustomerDoubletValidationValue[] = group.controls.values.value;

  const fields = values
    .filter(value => value.field)
    .map(value => value.field);

  const set = new Set();

  fields.forEach(field => set.add(field));

  if (set.size !== fields.length) {
    return {
      fieldUnique: true
    };
  }

  return null;
}
