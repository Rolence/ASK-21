/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../store';
import {CustomerDoubletValidation} from '../../../core/services/customer/domain/customer-doublet-validation.model';
import {ITdDataTableColumn} from '@covalent/core';
import {TableData} from '../../../shared/data-table/data-table.component';

@Component({
  templateUrl: './doublet-validation.component.html'
})
export class DoubletValidationComponent {

  doubletValidation$: Observable<CustomerDoubletValidation>;

  valueData$: Observable<TableData>;

  columns: ITdDataTableColumn[] = [
    { name: 'field', label: 'Field' },
    { name: 'verification', label: 'Verification' },
    { name: 'threshold', label: 'Threshold' }
  ];

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>) {
    this.doubletValidation$ = store.select(fromCustomers.getCustomerDoubletValidation);

    this.valueData$ = store.select(fromCustomers.getCustomerDoubletValidation)
      .map(doubletValidation => ({
        data: doubletValidation.values,
        totalElements: doubletValidation.values.length,
        totalPages: 1
      }));
  }

  edit(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

}
