/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Injectable} from '@angular/core';
import {Type} from '../../../../core/services/depositAccount/domain/type.model';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';
import {Observable} from 'rxjs/Observable';
import {AdministrationFee} from '../../../../core/services/depositAccount/domain/definition/administration-fee.model';
import {Installment} from '../../../../core/services/depositAccount/domain/definition/installment.model';
import {Term} from '../../../../core/services/depositAccount/domain/term.model';
import {CustomerService} from '../../../../core/services/customer/customer.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ChronoUnit} from '../../../../core/services/depositAccount/domain/definition/chrono-unit.model';
import {OfficeService} from '../../../../core/services/office/office.service';
import {FinancialInstitution} from '../../../../core/services/office/domain/financial-institution.model';
import {FimsValidators} from '../../../../core/validator/validators';
import {institutionExists} from '../../../../core/validator/instituion-exists.validator';
import {AccountingValidationService} from '../../../../core/validator/services/accounting-validation.service';

export interface DefinitionData {
  administrationFee: AdministrationFee;
  installment?: Installment;
  terms?: Term[];
}

@Injectable()
export class DepositFormService {

  constructor(private formBuilder: FormBuilder, private customerService: CustomerService,
              private depositService: DepositAccountService, private validationService: AccountingValidationService,
              private officeService: OfficeService) {
  }

  buildDetailForm(): FormGroup {
    const detailForm = this.formBuilder.group({
      productShortCode: ['', [Validators.required]],
      beneficiaries: [[]],
      termIdentifier: ['', [Validators.required]],
      administrationFeeReferenceAccount: ['', [], this.validationService.accountExists()],
      autoRenewal: [false],
      payoutMaturityAccount: ['']
    });

    detailForm.get('autoRenewal').valueChanges
      .startWith(false)
      .subscribe(enabled => {
        const control = detailForm.get('payoutMaturityAccount');
        if (enabled) {
          control.disable();
        } else {
          control.enable();
        }
      });

    return detailForm;
  }

  buildInstallmentForm(payable: ChronoUnit): FormGroup {
    const dueDayValidators = [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(1)];
    if (payable === 'WEEKS') {
      dueDayValidators.push(FimsValidators.maxValue(7));
    } else {
      dueDayValidators.push(FimsValidators.maxValue(31));
    }
    const installmentForm = this.formBuilder.group({
      enabled: [false],
      amount: ['0.00', [Validators.required, FimsValidators.isNumber, FimsValidators.minValue(0)]],
      referenceAccount: ['', [], this.validationService.accountExists()],
      dueDay: [1, dueDayValidators],
      payable: [{ value: payable, disabled: true }, [Validators.required]]
    });

    installmentForm.get('enabled').valueChanges
      .startWith(false)
      .subscribe( enabled => {
        const amountControl: FormControl = installmentForm.get('amount') as FormControl;
        const referenceAccountControl: FormControl = installmentForm.get('referenceAccount') as FormControl;
        const dueDayControl: FormControl = installmentForm.get('dueDay') as FormControl;

        if (enabled) {
          amountControl.enable();
          referenceAccountControl.enable();
          dueDayControl.enable();
        } else {
          amountControl.disable();
          referenceAccountControl.disable();
          dueDayControl.disable();
        }
      });

    return installmentForm;
  }

  public buildExternalAccountReferenceForm(): FormGroup {
    const externalAccountReferenceForm = this.formBuilder.group({
      enabled: [false],
      financialInstitution: ['', [Validators.required], institutionExists(this.officeService)],
      accountNumber: ['', [Validators.required]]
    });

    externalAccountReferenceForm.get('enabled').valueChanges
      .startWith(false)
      .subscribe( enabled => {
        const financialInstitutionControl: FormControl = externalAccountReferenceForm.get('financialInstitution') as FormControl;
        const accountNumberControl: FormControl = externalAccountReferenceForm.get('accountNumber') as FormControl;

        if (enabled) {
          financialInstitutionControl.enable();
          accountNumberControl.enable();
        } else {
          financialInstitutionControl.disable();
          accountNumberControl.disable();
        }
      });

    return externalAccountReferenceForm;
  }

  public getDefinitionData(shortCode: string, type: Type): Observable<DefinitionData> {
    switch (type) {
      case 'TERM': {
        return this.depositService.findTermProduct(shortCode)
          .map(termProduct => ({
            terms: termProduct.terms,
            administrationFee: termProduct.administrationFee,
            installment: termProduct.installment
          }));
      }

      case 'CHECKING': {
        return this.depositService.findCheckingProduct(shortCode)
          .map(checkingProduct => ({
            administrationFee: checkingProduct.administrationFee
          }));
      }

      case 'SAVINGS': {
        return this.depositService.findSavingsProduct(shortCode)
          .map(savingsProduct => ({
            administrationFee: savingsProduct.administrationFee,
            installment: savingsProduct.installment
          }));
      }

      case 'SHARE': {
        return this.depositService.findShareProduct(shortCode)
          .map(shareProduct => ({
            administrationFee: shareProduct.administrationFee
          }));
      }
    }
  }

  public filterCustomers(searchTerm: string): Observable<string[]> {
    return this.customerService.fetchCustomers({
      searchTerm
    }).map(customerPage => customerPage.customers.map(customer => customer.identifier));
  }

  public filterInstitutions(searchTerm: string): Observable<FinancialInstitution[]> {
    return this.officeService.fetchFinancialInstitutions({ searchTerm })
      .map(page => page.financialInstitutions);
  }

  public findInstitution(code: string): Observable<FinancialInstitution> {
    return this.officeService.findFinancialInstitution(code);
  }
}
