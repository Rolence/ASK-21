/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {CreateProductInstanceEvent, DepositCreateFormComponent} from './form.component';
import {Customer} from '../../../../core/services/customer/domain/customer.model';
import {ActivatedRoute, Router} from '@angular/router';
import {CreateProductInstanceAction} from '../../../store/deposits/deposit.actions';
import * as fromCustomers from '../../../store/index';
import {Observable} from 'rxjs/Observable';
import {ProductInstanceAssignment} from '../../../../core/services/depositAccount/domain/instance/product-instance-assignment.model';
import {ProductOutline} from '../../../../core/services/depositAccount/domain/definition/product-outline.model';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';
import {ProductInstanceOutline} from '../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './create.component.html'
})
export class DepositCreateComponent implements OnInit {

  @ViewChild('form') formComponent: DepositCreateFormComponent;

  productOutlines$: Observable<ProductOutline[]>;
  productInstances$: Observable<ProductInstanceOutline[]>;
  customer$: Observable<Customer>;

  productInstance: ProductInstanceAssignment = {
    customerIdentifier: '',
    productShortCode: '',
    currentBalance: '0.00'
  };

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>, private depositsService: DepositAccountService) {}

  ngOnInit(): void {
    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer)
      .filter(customer => !!customer);

    this.productOutlines$ = this.depositsService.fetchProductDefinitions()
      .map(productOutLines => productOutLines.filter(definition => definition.active));

    this.productInstances$ = this.store.select(fromCustomers.getActiveSearchDeposits);
  }

  save(event: CreateProductInstanceEvent): void {
    this.store.dispatch(new CreateProductInstanceAction({
      productInstance: event.productInstance,
      type: event.type,
      activatedRoute: this.route
    }));
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
