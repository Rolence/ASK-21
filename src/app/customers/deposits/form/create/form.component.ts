/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProductInstance} from '../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {Observable} from 'rxjs/Observable';
import {CustomerService} from '../../../../core/services/customer/customer.service';
import {ProductOutline} from '../../../../core/services/depositAccount/domain/definition/product-outline.model';
import {ProductInstanceAssignment} from '../../../../core/services/depositAccount/domain/instance/product-instance-assignment.model';
import {Term} from '../../../../core/services/depositAccount/domain/term.model';
import {AccountingService} from '../../../../core/services/accounting/accounting.service';
import {AdministrationFee} from '../../../../core/services/depositAccount/domain/definition/administration-fee.model';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';
import {Type} from '../../../../core/services/depositAccount/domain/type.model';
import {DepositFormService} from '../service/deposit-form.service';
import {FinancialInstitution} from '../../../../core/services/office/domain/financial-institution.model';
import {isObject} from '../../../../core/validator/validators';

export interface CreateProductInstanceEvent {
  type: Type;
  productInstance: ProductInstanceAssignment;
}

@Component({
  selector: 'aten-deposit-create-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class DepositCreateFormComponent {

  private _terms: Term[];

  filteredCustomers$: Observable<string[]>;
  filteredInstitutions$: Observable<FinancialInstitution[]>;
  detailForm: FormGroup;
  installmentForm: FormGroup;
  externalAccountReferenceForm: FormGroup;

  @Input() productOutlines: ProductOutline[] = [];
  @Input() customerId: string;
  @Input() productInstances: ProductInstance[];

  @Output() save = new EventEmitter<CreateProductInstanceEvent>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private accountingService: AccountingService,
              private depositService: DepositAccountService, private customerService: CustomerService,
              private depositFormService: DepositFormService) {

    this.detailForm = this.depositFormService.buildDetailForm();
    this.externalAccountReferenceForm = this.depositFormService.buildExternalAccountReferenceForm();
    this.detailForm.get('productShortCode').valueChanges
      .subscribe(productShortCode => this.outlineChange(productShortCode));

    this.filteredInstitutions$ = this.externalAccountReferenceForm.get('financialInstitution').valueChanges
      .distinctUntilChanged()
      .debounceTime(500)
      .filter(searchTerm => searchTerm)
      .map(searchTerm => isObject(searchTerm) ? searchTerm.code : searchTerm)
      .switchMap(searchTerm =>
        this.depositFormService.filterInstitutions(searchTerm)
      );
  }

  get isValid(): boolean {
    return this.detailForm.valid
      && this.installmentFormValid
      && this.externalAccountReferenceForm.valid;
  }

  onSave(): void {
    const productShortCode = this.detailForm.get('productShortCode').value;
    const adminReferenceAccount = this.detailForm.get('administrationFeeReferenceAccount').value;
    const termIdentifier = this.detailForm.get('termIdentifier').value;

    const productInstance: ProductInstanceAssignment = {
      customerIdentifier: this.customerId,
      productShortCode,
      beneficiaries: this.detailForm.get('beneficiaries').value,
      administrationFeeReferenceAccount: adminReferenceAccount && adminReferenceAccount.length > 0 ? adminReferenceAccount : undefined,
      termIdentifier: termIdentifier && termIdentifier.length > 0 ? termIdentifier : undefined,
      currentBalance: '0.00'
    };

    if (this.termEnabled) {
      const autoRenewal = this.detailForm.get('autoRenewal').value;
      productInstance.autoRenewal = autoRenewal;
      productInstance.payoutMaturityAccount = autoRenewal ? undefined : this.detailForm.get('payoutMaturityAccount').value;
    }

    if (this.installmentForm && this.installmentForm.get('enabled').value) {
      const referenceAccount = this.installmentForm.get('referenceAccount').value;
      productInstance.installmentInstance = {
        amount: this.installmentForm.get('amount').value,
        referenceAccount: referenceAccount && referenceAccount.length > 0 ? referenceAccount : undefined,
        dueDay: this.installmentForm.get('dueDay').value,
        payable: this.installmentForm.get('payable').value
      };
    }

    if (this.externalAccountReferenceForm.get('enabled').value) {
      const institution = this.externalAccountReferenceForm.get('financialInstitution').value as FinancialInstitution;
      productInstance.externalAccountReference = {
        financialInstitution: institution.name,
        branchSortCode: institution.code,
        accountNumber: this.externalAccountReferenceForm.get('accountNumber').value,
      };
    }

    const product: ProductOutline = this.productOutlines.find(outline => outline.shortCode === productShortCode);

    this.save.emit({
      productInstance,
      type: product.type
    });
  }

  onCancel(): void {
    this.cancel.emit();
  }

  fetchCustomers(searchTerm: string): void {
    this.filteredCustomers$ = this.depositFormService.filterCustomers(searchTerm);
  }

  private outlineChange(productShortCode: string): void {
    this.terms = [];
    this.toggleAdministrationFee();

    if (productShortCode && this.productOutlines) {
      const outline = this.productOutlines.find(productOutline => productOutline.shortCode === productShortCode);

      this.depositFormService.getDefinitionData(productShortCode, outline.type)
        .subscribe(data => {
          this.terms = data.terms;
          this.toggleAdministrationFee(data.administrationFee);

          if (data.installment) {
            // We need to have installment first before we can build up the form
            this.installmentForm = this.depositFormService.buildInstallmentForm(data.installment.payable);
          }
        });
    }
  }

  set terms(terms: Term[]) {
    this._terms = terms;

    const control = this.detailForm.get('termIdentifier');

    if (this.termEnabled) {
      control.enable();
    } else {
      control.disable();
    }
  }

  get terms(): Term[] {
    return this._terms;
  }

  private toggleAdministrationFee(administrationFee?: AdministrationFee) {
    const control = this.detailForm.get('administrationFeeReferenceAccount');

    if (administrationFee) {
      control.enable();
    } else {
      control.disable();
    }
  }

  get termEnabled(): boolean {
    return this.terms && this.terms.length > 0;
  }

  /**
   * Returns if installment form is valid considering that the form is only available for certain products
   * @returns {boolean}
   */
  get installmentFormValid(): boolean {
    if (!this.installmentForm) {
      return true;
    }

    if (this.installmentForm.get('enabled').value) {
      return this.installmentForm.valid;
    } else {
      return true;
    }
  }
}
