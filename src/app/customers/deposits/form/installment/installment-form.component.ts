/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {payableOptionList} from '../../../../depositAccount/domain/payable-option-list.model';
import {ProductInstanceOutline} from '../../../../core/services/depositAccount/domain/instance/product-instance-outline.model';

@Component({
  selector: 'aten-customer-deposit-installment-form',
  templateUrl: './installment-form.component.html'
})
export class CustomerDepositInstallmentFormComponent {

  payableOptions = payableOptionList;

  @Input() productInstances: ProductInstanceOutline[];
  @Input() formGroup: FormGroup;

}
