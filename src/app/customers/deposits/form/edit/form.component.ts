/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {ProductInstance} from '../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {CustomerService} from '../../../../core/services/customer/customer.service';
import {DepositFormService} from '../service/deposit-form.service';
import {ProductInstanceChangeSet} from '../../../../core/services/depositAccount/domain/instance/product-instance-change-set.model';
import {AdministrationFee} from '../../../../core/services/depositAccount/domain/definition/administration-fee.model';
import {FinancialInstitution} from '../../../../core/services/office/domain/financial-institution.model';
import {AccountingValidationService} from '../../../../core/validator/services/accounting-validation.service';
import {isObject} from '../../../../core/validator/validators';

@Component({
  selector: 'aten-deposit-edit-form',
  templateUrl: './form.component.html'
})
export class DepositEditFormComponent implements OnChanges {

  filteredCustomers$: Observable<string[]>;
  filteredInstitutions$: Observable<FinancialInstitution[]>;
  detailForm: FormGroup;
  installmentForm: FormGroup;
  externalAccountReferenceForm: FormGroup;

  @Input() productInstance: ProductInstance;
  @Input() productInstances: ProductInstance[];

  @Output() save = new EventEmitter<ProductInstanceChangeSet>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private validationService: AccountingValidationService,
              private customerService: CustomerService, private depositFormService: DepositFormService) {
    this.detailForm = this.formBuilder.group({
      beneficiaries: [[]],
      administrationFeeReferenceAccount: ['', [], this.validationService.accountExists()],
      autoRenewal: [false],
      payoutMaturityAccount: ['']
    });

    this.detailForm.get('autoRenewal').valueChanges
      .startWith(false)
      .subscribe(enabled => {
        const control = this.detailForm.get('payoutMaturityAccount');
        if (enabled) {
          control.disable();
        } else {
          control.enable();
        }
      });

    this.externalAccountReferenceForm = this.depositFormService.buildExternalAccountReferenceForm();

    this.filteredInstitutions$ = this.externalAccountReferenceForm.get('financialInstitution').valueChanges
      .distinctUntilChanged()
      .debounceTime(500)
      .filter(searchTerm => searchTerm)
      .map(searchTerm => isObject(searchTerm) ? searchTerm.code : searchTerm)
      .switchMap(searchTerm =>
        this.depositFormService.filterInstitutions(searchTerm)
      );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.productInstance && this.productInstance) {
      this.depositFormService.getDefinitionData(this.productInstance.productShortCode, this.productInstance.type)
        .subscribe(data => {
          const installmentInstance = this.productInstance.installment;

          if (installmentInstance) {
            this.installmentForm = this.depositFormService.buildInstallmentForm(installmentInstance.payable);

            this.installmentForm.reset({
              enabled: true,
              amount: installmentInstance.amount,
              referenceAccount: installmentInstance.referenceAccount,
              dueDay: installmentInstance.dueDay,
              payable: installmentInstance.payable
            });
          }

          if (!installmentInstance && data.installment) {
            this.installmentForm = this.depositFormService.buildInstallmentForm(data.installment.payable);
            this.installmentForm.reset({
              enabled: false,
              payable: data.installment.payable
            });
          }

          this.toggleAdministrationFee(data.administrationFee);
        });

      const values: any = {
        beneficiaries: this.productInstance.beneficiaries ? this.productInstance.beneficiaries : [],
        payoutMaturityAccount: this.productInstance.payoutMaturityAccount,
        autoRenewal: this.productInstance.autoRenewal
      };

      const feeInstance = this.productInstance.administrationFeeInstance;
      if (feeInstance) {
        values.administrationFeeReferenceAccount = feeInstance.referenceAccount;
      }

      this.detailForm.reset(values);

      const externalAccountReference = this.productInstance.externalAccountReference;
      if (externalAccountReference) {
        this.depositFormService.findInstitution(externalAccountReference.branchSortCode)
          .subscribe(financialInstitution => {
            this.externalAccountReferenceForm.reset({
              enabled: true,
              financialInstitution,
              accountNumber: externalAccountReference.accountNumber
            });
          });
      } else {
        this.externalAccountReferenceForm.reset({
          enabled: false
        });
      }
    }
  }

  get isValid(): boolean {
    return this.detailForm.valid
      && this.installmentFormValid;
  }

  onSave(): void {
    const adminReferenceAccount = this.detailForm.get('administrationFeeReferenceAccount').value;

    const productChangeSet: ProductInstanceChangeSet = {
      beneficiaries: this.detailForm.get('beneficiaries').value,
      administrationFeeReferenceAccount: adminReferenceAccount && adminReferenceAccount.length > 0 ? adminReferenceAccount : undefined
    };

    if (this.productInstance.type === 'TERM') {
      const autoRenewal = this.detailForm.get('autoRenewal').value;
      productChangeSet.autoRenewal = autoRenewal;
      productChangeSet.payoutMaturityAccount = autoRenewal ? undefined : this.detailForm.get('payoutMaturityAccount').value;
    }

    if (this.installmentForm && this.installmentForm.get('enabled').value) {
      const referenceAccount = this.installmentForm.get('referenceAccount').value;
      productChangeSet.installmentInstance = {
        amount: this.installmentForm.get('amount').value,
        referenceAccount: referenceAccount && referenceAccount.length > 0 ? referenceAccount : undefined,
        dueDay: this.installmentForm.get('dueDay').value,
        payable: this.installmentForm.get('payable').value
      };
    }

    if (this.externalAccountReferenceForm.get('enabled').value) {
      const institution = this.externalAccountReferenceForm.get('financialInstitution').value as FinancialInstitution;
      productChangeSet.externalAccountReference = {
        financialInstitution: institution.name,
        branchSortCode: institution.code,
        accountNumber: this.externalAccountReferenceForm.get('accountNumber').value,
      };
    }

    this.save.emit(productChangeSet);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  fetchCustomers(searchTerm: string): void {
    this.filteredCustomers$ = this.depositFormService.filterCustomers(searchTerm);
  }

  get termEnabled(): boolean {
    return this.productInstance && this.productInstance.type === 'TERM';
  }

  private toggleAdministrationFee(administrationFee?: AdministrationFee) {
    const control = this.detailForm.get('administrationFeeReferenceAccount');

    if (administrationFee) {
      control.enable();
    } else {
      control.disable();
    }
  }

  /**
   * Returns if installment form is valid considering that the form is only available for certain products
   * @returns {boolean}
   */
  get installmentFormValid(): boolean {
    if (!this.installmentForm) {
      return true;
    }

    if (this.installmentForm.get('enabled').value) {
      return this.installmentForm.valid;
    } else {
      return true;
    }
  }
}
