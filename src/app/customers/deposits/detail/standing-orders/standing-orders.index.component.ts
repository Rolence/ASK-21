/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Subscription} from 'rxjs/Subscription';
import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as fromCustomers from '../../../store';
import {LoadAllAction} from '../../../store/deposits/standing-orders/standing-order.actions';
import {ProductInstance} from '../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './standing-orders.index.component.html'
})
export class StandingOrdersIndexComponent implements OnDestroy {

  private actionsSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromCustomers.State>) {
    this.actionsSubscription = this.store.select(fromCustomers.getSelectedDepositInstance)
      .map((instance: ProductInstance) => new LoadAllAction(instance.accountIdentifier))
      .subscribe(this.store);
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
  }
}
