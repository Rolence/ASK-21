/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../../store';
import {DepositAccountService} from '../../../../core/services/depositAccount/deposit-account.service';
import {of} from 'rxjs/observable/of';
import {LoadAction} from '../../../store/deposits/standing-orders/standing-order.actions';
import {Store} from '@ngrx/store';
import {ExistsGuardService} from '../../../../core/services/guards/exists-guard';

@Injectable()
export class StandingOrderExistsGuard implements CanActivate {

  constructor(private store: Store<fromCustomers.State>,
              private depositService: DepositAccountService,
              private existsGuardService: ExistsGuardService) {}

  hasStandingOrderInStore(id: string): Observable<boolean> {
    const timestamp$ = this.store.select(fromCustomers.getStandingOrdersLoadedAt)
      .map(loadedAt => loadedAt[id]);

    return this.existsGuardService.isWithinExpiry(timestamp$);
  }

  hasStandingOrderInApi(id: string, sequence: number): Observable<boolean> {
    const getStandingOrder$ = this.depositService.getStandingOrder(id, sequence)
      .map(standingOrder => new LoadAction({
        resource: {
          ...standingOrder,
          sequence: standingOrder.sequence.toString()
        }
      }))
      .do((action: LoadAction) => this.store.dispatch(action))
      .map(standingOrder => !!standingOrder);

    return this.existsGuardService.routeTo404OnError(getStandingOrder$);
  }

  hasStandingOrder(id: string, sequence: number): Observable<boolean> {
    return this.hasStandingOrderInStore(id)
      .switchMap(inStore => {
        if (inStore) {
          return of(inStore);
        }

        return this.hasStandingOrderInApi(id, sequence);
      });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.hasStandingOrder(route.parent.parent.params['id'], route.params['sequence']);
  }
}
