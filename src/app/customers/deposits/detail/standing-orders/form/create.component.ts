/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {StandingOrder} from '../../../../../core/services/depositAccount/domain/instance/standing-order.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductInstance} from '../../../../../core/services/depositAccount/domain/instance/product-instance.model';
import * as fromCustomers from '../../../../store';
import {CreateStandingOrderAction} from '../../../../store/deposits/standing-orders/standing-order.actions';
import {todayAsDate} from '../../../../../core/services/domain/date.converter';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './create.component.html'
})
export class CreateStandingOrderComponent {

  standingOrder: StandingOrder = {
    payee: '',
    accountNumber: '',
    bankIdentifier: '',
    amount: '0.00',
    purpose: '',
    interval: 'WEEKS',
    firstPaymentDate: todayAsDate(),
    lastPaymentDate: ''
  };

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>) {}

  save(standingOrder: StandingOrder): void {
    this.store.select(fromCustomers.getSelectedDepositInstance)
      .take(1)
      .map((instance: ProductInstance) => new CreateStandingOrderAction({
        identifier: instance.accountIdentifier,
        standingOrder,
        activatedRoute: this.route
      }))
      .subscribe(this.store);
  }

  cancel(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
