/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {StandingOrder} from '../../../../../core/services/depositAccount/domain/instance/standing-order.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {intervalOptionList} from '../domain/interval-option-list.model';
import {dateAsISOString, toShortISOString} from '../../../../../core/services/domain/date.converter';
import {OfficeService} from '../../../../../core/services/office/office.service';
import {FinancialInstitution} from '../../../../../core/services/office/domain/financial-institution.model';
import {Observable} from 'rxjs/Observable';
import {FimsValidators, isObject} from '../../../../../core/validator/validators';

@Component({
  selector: 'aten-standing-order-form',
  templateUrl: './form.component.html'
})
export class StandingOrderFormComponent implements OnChanges {

  formGroup: FormGroup;
  optionList = intervalOptionList;
  filteredInstitutions$: Observable<FinancialInstitution[]>;

  @Input() standingOrder: StandingOrder;
  @Input() editMode = false;

  @Output() save = new EventEmitter<StandingOrder>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private officeService: OfficeService) {
    this.formGroup = formBuilder.group({
      payee: ['', [Validators.required, Validators.maxLength(256)]],
      accountNumber: ['', [Validators.required, Validators.maxLength(32)]],
      financialInstitution: ['', [Validators.required]],
      amount: ['', [Validators.required, FimsValidators.greaterThanValue(0)]],
      purpose: ['', [Validators.maxLength(2048)]],
      interval: ['', [Validators.required]],
      firstPaymentDate: ['', [Validators.required]],
      lastPaymentDate: ['', []],
    }, {validator: FimsValidators.matchRange('firstPaymentDate', 'lastPaymentDate')});

    this.filteredInstitutions$ = this.formGroup.get('financialInstitution').valueChanges
      .distinctUntilChanged()
      .debounceTime(500)
      .filter(searchTerm => searchTerm)
      .map(searchTerm => isObject(searchTerm) ? searchTerm.code : searchTerm)
      .switchMap(searchTerm =>
        this.officeService.fetchFinancialInstitutions({ searchTerm })
          .map(page => page.financialInstitutions)
      );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.standingOrder && this.standingOrder) {
      this.officeService.findFinancialInstitution(this.standingOrder.bankIdentifier, true)
        .subscribe(financialInstitution => {
          this.resetForm(this.standingOrder, financialInstitution);
        }, err => this.resetForm(this.standingOrder, this.standingOrder.bankIdentifier));
    }
  }

  private resetForm(standingOrder: StandingOrder, financialInstitution: FinancialInstitution | string): void {
    const lastPaymentDate = this.standingOrder.lastPaymentDate;

    this.formGroup.reset({
      payee: standingOrder.payee,
      accountNumber: standingOrder.accountNumber,
      financialInstitution,
      amount: standingOrder.amount,
      purpose: standingOrder.purpose,
      interval: standingOrder.interval,
      firstPaymentDate: dateAsISOString(new Date(standingOrder.firstPaymentDate)),
      lastPaymentDate: lastPaymentDate ? dateAsISOString(new Date(lastPaymentDate)) : null
    });
  }

  onSave(): void {
    const lastPaymentDate = this.formGroup.get('lastPaymentDate').value;

    let bankIdentifier: string;
    if (isObject(this.formGroup.get('financialInstitution').value)) {
      const institution = this.formGroup.get('financialInstitution').value as FinancialInstitution;
      bankIdentifier = institution.code;
    } else {
      bankIdentifier = this.formGroup.get('financialInstitution').value as string;
    }
    const standingOrder: StandingOrder = {
      ...this.standingOrder,
      payee: this.formGroup.get('payee').value,
      accountNumber: this.formGroup.get('accountNumber').value,
      bankIdentifier,
      amount: this.formGroup.get('amount').value,
      purpose: this.formGroup.get('purpose').value,
      interval: this.formGroup.get('interval').value,
      firstPaymentDate: toShortISOString(this.formGroup.get('firstPaymentDate').value),
      lastPaymentDate: lastPaymentDate ? toShortISOString(lastPaymentDate) : null
    };

    this.save.emit(standingOrder);
  }

  onCancel(): void {
    this.cancel.emit();
  }
}
