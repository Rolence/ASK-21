/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {StandingOrder} from '../../../../../core/services/depositAccount/domain/instance/standing-order.model';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCustomers from '../../../../store';
import {DeleteStandingOrderAction} from '../../../../store/deposits/standing-orders/standing-order.actions';
import {ProductInstance} from '../../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {DialogService} from '../../../../../core/services/dialog/dialog.service';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './detail.component.html'
})
export class StandingOrderDetailComponent {

  standingOrder$: Observable<StandingOrder>;
  depositInstance$: Observable<ProductInstance>;

  constructor(private router: Router, private route: ActivatedRoute,
              private store: Store<fromCustomers.State>, private dialogService: DialogService) {
    this.standingOrder$ = store.select(fromCustomers.getSelectedStandingOrder);
    this.depositInstance$ = store.select(fromCustomers.getSelectedDepositInstance);
  }

  deleteStandingOrder(standingOrder: StandingOrder): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .mergeMap(() =>
        this.depositInstance$.take(1)
      )
      .map((instance: ProductInstance) => new DeleteStandingOrderAction({
        identifier: instance.accountIdentifier,
        standingOrder,
        activatedRoute: this.route
      }))
      .subscribe(this.store);
  }

  private confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this standing order?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE STANDING ORDER',
    });
  }

  editStandingOrder(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
