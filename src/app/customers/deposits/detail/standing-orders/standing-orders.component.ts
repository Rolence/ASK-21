/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCustomers from '../../../store';
import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {StandingOrder} from '../../../../core/services/depositAccount/domain/instance/standing-order.model';
import {ITdDataTableColumn} from '@covalent/core';
import {Store} from '@ngrx/store';
import {ProductInstance} from '../../../../core/services/depositAccount/domain/instance/product-instance.model';
import {TableData} from '../../../../shared/data-table/data-table.component';
import {DisplayFimsNumber} from '../../../../shared/number/fims-number.pipe';

@Component({
  templateUrl: './standing-orders.component.html',
  providers: [DatePipe, DisplayFimsNumber]
})
export class StandingOrdersListComponent {

  depositInstance$: Observable<ProductInstance>;
  standingOrdersData$: Observable<TableData>;

  columns: ITdDataTableColumn[] = [
    { name: 'sequence', label: 'Order' },
    { name: 'payee', label: 'Payee' },
    { name: 'accountNumber', label: 'Account number' },
    { name: 'bankIdentifier', label: 'Bank ID' },
    { name: 'amount', label: 'Amount', format: v => this.displayFimsNumber.transform(v) },
    { name: 'interval', label: 'Interval' },
    { name: 'firstPaymentDate', label: 'Start', format: (v: any) => this.formatDate(v) },
    { name: 'lastPaymentDate', label: 'End', format: (v: any) => this.formatDate(v) }
  ];

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>,
              private datePipe: DatePipe, private displayFimsNumber: DisplayFimsNumber) {
    this.standingOrdersData$ = this.store.select(fromCustomers.getSearchStandingOrders)
      .map((standingOrders: StandingOrder[]) => ({
        totalElements: standingOrders.length,
        totalPages: 1,
        data: standingOrders
      }));

    this.depositInstance$ = store.select(fromCustomers.getSelectedDepositInstance);
  }

  rowSelect(standingOrder: StandingOrder): void {
    this.router.navigate(['detail', standingOrder.sequence], { relativeTo: this.route });
  }

  addStandingOrder(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

  private formatDate(value: any): string | null {
    return this.datePipe.transform(value, 'mediumDate');
  }
}
