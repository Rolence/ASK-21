/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IssuingCount} from '../../../../core/services/cheque/domain/issuing-count.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FimsValidators} from '../../../../core/validator/validators';

@Component({
  selector: 'aten-issue-cheque-form',
  templateUrl: './form.component.html'
})
export class IssueChequesFormComponent implements OnInit {

  form: FormGroup;

  @Input('accountIdentifier') accountIdentifier: string;

  @Output('onSave') onSave = new EventEmitter<IssuingCount>();
  @Output('onCancel') onCancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      start: [1, [FimsValidators.minValue(1)]],
      amount: [1, [Validators.required, FimsValidators.minValue(1)]]
    });
  }

  save(): void {
    const issuingCount: IssuingCount = {
      start: this.form.get('start').value,
      amount: this.form.get('amount').value,
      accountIdentifier: this.accountIdentifier
    };

    this.onSave.emit(issuingCount);
  }

  cancel(): void {
    this.onCancel.emit();
  }

}
