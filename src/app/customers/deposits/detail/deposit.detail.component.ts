/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnInit} from '@angular/core';
import {ProductInstance} from '../../../core/services/depositAccount/domain/instance/product-instance.model';
import {Observable} from 'rxjs/Observable';
import * as fromCustomers from '../../store/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Customer} from '../../../core/services/customer/domain/customer.model';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './deposit.detail.component.html'
})
export class DepositDetailComponent implements OnInit {

  depositInstance$: Observable<ProductInstance>;
  customer$: Observable<Customer>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>) {}

  ngOnInit(): void {
    this.depositInstance$ = this.store.select(fromCustomers.getSelectedDepositInstance);
    this.customer$ = this.store.select(fromCustomers.getSelectedCustomer);
  }

  issueCheques(): void {
    this.router.navigate(['cheques'], { relativeTo: this.route });
  }

  editInstance(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  goToStandingOrders(): void {
    this.router.navigate(['standingOrders'], { relativeTo: this.route });
  }
}
