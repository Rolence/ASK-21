/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Customer} from '../../../core/services/customer/domain/customer.model';
import * as fromCustomers from '../../store';
import {UPDATE} from '../../store/customer.actions';
import {Catalog} from '../../../core/services/catalog/domain/catalog.model';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './edit.form.component.html'
})
export class EditCustomerFormComponent {

  customer$: Observable<Customer>;

  catalog$: Observable<Catalog>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromCustomers.State>) {
    this.catalog$ = store.select(fromCustomers.getCustomerCatalog);
    this.customer$ = store.select(fromCustomers.getSelectedCustomer);
  }

  onSave(customer: Customer) {
    this.store.dispatch({ type: UPDATE, payload: {
      customer,
      activatedRoute: this.route
    } });
  }

  onCancel() {
    this.navigateAway();
  }

  navigateAway(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
