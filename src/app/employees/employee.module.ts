/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {NgModule} from '@angular/core';
import {EmployeeListComponent} from './employee-list.component';
import {EmployeeRoutingModule} from './employee-routing.module';
import {EmployeeFormComponent} from './form/form.component';
import {CreateEmployeeFormComponent} from './form/create/create.form.component';
import {EmployeeDetailComponent} from './detail/employee.detail.component';
import {EditEmployeeFormComponent} from './form/edit/edit.form.component';
import {UserResolver} from './user.resolver';
import {EmployeeExistsGuard} from './employee-exists.guard';
import {StoreModule} from '@ngrx/store';
import {reducerProvider, reducerToken} from './store/index';
import {EmployeeNotificationEffects} from './store/effects/notification.effects';
import {EffectsModule} from '@ngrx/effects';
import {EmployeeApiEffects} from './store/effects/service.effects';
import {EmployeeRouteEffects} from './store/effects/route.effects';
import {SharedModule} from '../shared/shared.module';
import {EmployeeIndexComponent} from './employee-index.component';

@NgModule({
  imports: [
    EmployeeRoutingModule,
    SharedModule,

    StoreModule.forFeature('employee', reducerToken),

    EffectsModule.forFeature([
      EmployeeApiEffects,
      EmployeeRouteEffects,
      EmployeeNotificationEffects
    ])
  ],
  declarations: [
    EmployeeListComponent,
    EmployeeFormComponent,
    CreateEmployeeFormComponent,
    EditEmployeeFormComponent,
    EmployeeDetailComponent,
    EmployeeIndexComponent
  ],
  providers: [
    UserResolver,
    EmployeeExistsGuard,
    reducerProvider
  ]
})
export class EmployeeModule {}
