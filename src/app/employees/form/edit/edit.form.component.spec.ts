/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {EditEmployeeFormComponent} from './edit.form.component';
import {EmployeeFormComponent} from '../form.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {UPDATE} from '../../store/employee.actions';
import * as fromEmployees from '../../store';
import * as fromEmployee from '../../store';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {User} from '../../../core/services/identity/domain/user.model';
import {Employee} from '../../../core/services/office/domain/employee.model';
import {SharedModule} from '../../../shared/shared.module';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {OfficeService} from '../../../core/services/office/office.service';

const userMock: User = {
  identifier: 'test',
  role: 'test'
};

const employeeMock: Employee = {
  identifier: 'test',
  assignedOffice: 'test',
  givenName: 'test',
  middleName: 'test',
  surname: 'test',
  contactDetails: [
    {
      group: 'BUSINESS',
      type: 'EMAIL',
      value: 'test',
      preferenceLevel: 1
    }
  ]
};

const activatedRoute = {
  parent: {
    data: Observable.of({
      user: userMock
    })
  }
};
let router: Router;

describe('Test employee form component', () => {

  let fixture: ComponentFixture<EditEmployeeFormComponent>;

  let testComponent: EditEmployeeFormComponent;

  beforeEach(() => {
    router = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      declarations: [
        EmployeeFormComponent,
        EditEmployeeFormComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        SharedModule.forRoot(),
        NoopAnimationsModule
      ],
      providers: [
        { provide: Router, useValue: router},
        { provide: ActivatedRoute, useValue: activatedRoute },
        {
          provide: Store, useClass: class {
            dispatch = jasmine.createSpy('dispatch');
            select = jasmine.createSpy('select').and.returnValue(Observable.empty());
          }
        },
        {
          provide: Store, useClass: class {
            dispatch = jasmine.createSpy('dispatch');
            select = jasmine.createSpy('select').and.callFake(selector => {
              if (selector === fromEmployees.getSelectedEmployee) {
                return Observable.of(employeeMock);
              }

              return Observable.empty();
            });
          }
        },
        {
          provide: IdentityService, useClass: class {
            listRoles = jasmine.createSpy('listRoles').and.returnValue(Observable.of([]));
          }
        },
        {
          provide: OfficeService, useClass: class {
            listOffices = jasmine.createSpy('listOffices').and.returnValue(Observable.of({
              offices: [],
              totalElements: 0,
              totalPages: 0
            }));
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });

    fixture = TestBed.createComponent(EditEmployeeFormComponent);
    testComponent = fixture.componentInstance;
  });

  it('should test if employee is updated', async(inject([Store], (store: Store<fromEmployee.State>) => {
    fixture.detectChanges();

    testComponent.formComponent.detailForm.get('password').setValue('newPassword');

    fixture.detectChanges();

    testComponent.formComponent.onSave();

    fixture.whenStable().then(() => {
      expect(store.dispatch).toHaveBeenCalledWith({ type: UPDATE, payload: {
        employee: employeeMock,
        contactDetails: employeeMock.contactDetails,
        role: userMock.role,
        password: 'newPassword',
        activatedRoute: activatedRoute
      }});
    });

  })));
});
