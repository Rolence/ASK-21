/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Observable, Subscribable} from 'rxjs/Observable';
import {Office} from '../../core/services/office/domain/office.model';
import {OfficePage} from '../../core/services/office/domain/office-page.model';
import {Employee} from '../../core/services/office/domain/employee.model';
import {BUSINESS, ContactDetail, EMAIL, MOBILE, PHONE} from '../../core/services/domain/contact/contact-detail.model';
import {Role} from '../../core/services/identity/domain/role.model';
import {User} from '../../core/services/identity/domain/user.model';
import {FimsValidators} from '../../core/validator/validators';
import {IdentityService} from '../../core/services/identity/identity.service';
import {OfficeService} from '../../core/services/office/office.service';
import 'rxjs/add/operator/toPromise';

export interface EmployeeFormData {
  user: User;
  employee: Employee;
}

export interface EmployeeSaveEvent {
  detailForm: {
    identifier: string;
    firstName: string;
    middleName: string;
    lastName: string;
    password: string;
    role: string;
    assignedOffice: string;
  };
  contactForm: {
    email: string;
    phone: string;
    mobile: string;
  };
}

@Component({
  selector: 'aten-employee-form',
  templateUrl: './form.component.html'
})
export class EmployeeFormComponent implements OnChanges {

  roles$: Observable<Role[]>;
  offices$: Observable<Office[]>;
  offices: any
  listOfBranches: Array<any>
  branches$: Observable<Office[]>

  detailForm: FormGroup;
  contactForm: FormGroup;

  @Input('editMode') editMode: boolean;
  @Input('formData') formData: EmployeeFormData;

  @Output() save = new EventEmitter<EmployeeSaveEvent>();
  @Output() cancel = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private identityService: IdentityService, private officeService: OfficeService) {
    this.detailForm = this.formBuilder.group({
      identifier: ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(32), FimsValidators.urlSafe ]],
      firstName: ['', [Validators.required, Validators.maxLength(256)]],
      middleName: ['', Validators.maxLength(256)],
      lastName: ['', [Validators.required, Validators.maxLength(256)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      role: ['', Validators.required],
      assignedOffice: ['']
    });

    this.contactForm = this.formBuilder.group({
      email: ['', [Validators.maxLength(256), FimsValidators.email]],
      phone: ['', Validators.maxLength(256)],
      mobile: ['', Validators.maxLength(256)]
    });

    this.roles$ = this.identityService.listRoles();
    
    this.offices$ = this.officeService.listOffices({
      page: {
        pageIndex: 0,
        size: 100
      }
    }).map(officePage => officePage.offices)

    this.offices = this.officeService.listOffices({
      page: {
        pageIndex: 0,
        size: 100
      }
    }).subscribe(result => {
      for(let i=0; i < result.offices.length; i++){
        if(result.totalElements != 0){ 
          this.getBranches(result.offices[i].identifier)
        }    
      }
    this.listOfBranches=[]
    })
  }

getBranches(identifier: string){
    this.officeService.listBranches(identifier)
    .subscribe(ressult => {
      for(let k = 0; k < ressult.offices.length; k++){
        this.listOfBranches.push(ressult.offices[k]);
        if(ressult.totalElements != 0){
          this.getBranches(ressult.offices[k].identifier)
        }
        console.log("Getting all branches ", this.listOfBranches)
      }
      return this.listOfBranches
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formData && this.formData) {
      const passwordValidators: ValidatorFn[] = [Validators.minLength(8)];

      if (!this.editMode) {
        passwordValidators.push(Validators.required);
      }

      this.detailForm.get('password').setValidators(passwordValidators);

      const employee = this.formData.employee;

      this.detailForm.reset({
        identifier: employee.identifier,
        firstName: employee.givenName,
        middleName: employee.middleName,
        lastName: employee.surname,
        password: '',
        role: this.formData.user ? this.formData.user.role : '',
        assignedOffice: employee.assignedOffice
      });

      let phone = '';
      let mobile = '';
      let email = '';

      const businessContacts: ContactDetail[] = employee.contactDetails.filter(contactDetail => contactDetail.group === BUSINESS);

      if (businessContacts.length) {
        phone = this.getFirstItemByType(businessContacts, PHONE);
        mobile = this.getFirstItemByType(businessContacts, MOBILE);
        email = this.getFirstItemByType(businessContacts, EMAIL);
      }

      this.contactForm.reset({
        email,
        phone,
        mobile
      });

    }
  }

  getFirstItemByType(contactDetails: ContactDetail[], type: string): string {
    const items = contactDetails.filter(contact => contact.type === type);
    return items.length ? items[0].value : '';
  }

  get formsInvalid(): boolean {
    return (!this.contactForm.pristine && this.contactForm.invalid)
      || this.detailForm.invalid;
  }

  onSave(): void {
    this.save.emit({
      detailForm: this.detailForm.value,
      contactForm: this.contactForm.value,
    });
  }

  onCancel(): void {
    this.cancel.emit();
  }

}
