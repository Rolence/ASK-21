/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromRoot from '../../core/store';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createSelector} from 'reselect';
import {createResourceReducer, getResourceLoadedAt, getResourceSelected, ResourceState} from '../../core/store/util/resource.reducer';
import {createFormReducer, FormState, getFormError} from '../../core/store/util/form.reducer';
import {InjectionToken} from '@angular/core';

export interface EmployeeState {
  employees: ResourceState;
  employeeForm: FormState;
}

export const reducers: ActionReducerMap<EmployeeState> = {
  employees: createResourceReducer('Employee'),
  employeeForm: createFormReducer('Employee')
};

export const reducerToken = new InjectionToken<ActionReducerMap<EmployeeState>>('Employee reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];

export interface State extends fromRoot.State {
  employee: EmployeeState;
}

export const selectEmployee = createFeatureSelector<EmployeeState>('employee');

export const getEmployeesState = createSelector(selectEmployee, (state: EmployeeState) => state.employees);

export const getEmployeeFormState = createSelector(selectEmployee, (state: EmployeeState) => state.employeeForm);
export const getEmployeeFormError = createSelector(getEmployeeFormState, getFormError);

export const getEmployeesLoadedAt = createSelector(getEmployeesState, getResourceLoadedAt);
export const getSelectedEmployee = createSelector(getEmployeesState, getResourceSelected);
