/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {EmployeeApiEffects} from './service.effects';
import {Observable} from 'rxjs/Observable';
import {UpdateEmployeeAction, UpdateEmployeeSuccessAction} from '../employee.actions';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {OfficeService} from '../../../core/services/office/office.service';
import {Employee} from '../../../core/services/office/domain/employee.model';

describe('Account Search Api Effects', () => {

  let actions: ReplaySubject<any>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
        EmployeeApiEffects,
        {
          provide: OfficeService,
          useValue: jasmine.createSpyObj('officeService', ['createEmployee', 'updateEmployee', 'setContactDetails'])
        },
        {
          provide: IdentityService,
          useValue: jasmine.createSpyObj('identityService', ['createUser', 'changeUserRole', 'changePassword'])
        },
        provideMockActions(() => actions)
      ]
    });

  });

  describe('updateEmployee$', () => {

    function setup() {
      const officeService = TestBed.get(OfficeService);

      officeService.updateEmployee.and.returnValue(Observable.of({}));

      return {
        officeService: officeService,
        identityService: TestBed.get(IdentityService),
        employeeEffects: TestBed.get(EmployeeApiEffects)
      };
    }

    it('should update employee', fakeAsync(() => {
      const { officeService, employeeEffects } = setup();

      const employee: Employee = {
        identifier: '',
        givenName: '',
        surname: '',
        contactDetails: []
      };

      const expectedResult = new UpdateEmployeeSuccessAction({
        resource: employee,
        activatedRoute: null
      });

      actions = new ReplaySubject(1);

      actions.next(new UpdateEmployeeAction({
        employee: employee,
        activatedRoute: null
      }));

      let result = null;
      employeeEffects.updateEmployee$.subscribe(_result => result = _result);

      tick();

      expect(result).toEqual(expectedResult);

      expect(officeService.updateEmployee).toHaveBeenCalled();
    }));

    it('should update contact details', fakeAsync(() => {
      const { officeService, employeeEffects } = setup();

      officeService.setContactDetails.and.returnValue(Observable.of({}));

      const employee: Employee = {
        identifier: '',
        givenName: '',
        surname: '',
        contactDetails: []
      };

      const expectedResult = new UpdateEmployeeSuccessAction({
        resource: employee,
        activatedRoute: null
      });

      actions = new ReplaySubject(1);

      actions.next(new UpdateEmployeeAction({
        employee: employee,
        contactDetails: [
          {
            type: 'EMAIL',
            group: 'BUSINESS',
            value: 'dont@call.me',
            preferenceLevel: 0
          }
        ],
        activatedRoute: null
      }));

      let result = null;
      employeeEffects.updateEmployee$.subscribe(_result => result = _result);

      tick();

      expect(result).toEqual(expectedResult);

      expect(officeService.setContactDetails).toHaveBeenCalled();
    }));

    it('should update password', fakeAsync(() => {
      const { identityService, employeeEffects } = setup();

      identityService.changePassword.and.returnValue(Observable.of({}));

      const employee: Employee = {
        identifier: '',
        givenName: '',
        surname: '',
        contactDetails: []
      };

      const expectedResult = new UpdateEmployeeSuccessAction({
        resource: employee,
        activatedRoute: null
      });

      actions = new ReplaySubject(1);

      actions.next(new UpdateEmployeeAction({
        employee: employee,
        password: 'test',
        activatedRoute: null
      }));

      let result = null;
      employeeEffects.updateEmployee$.subscribe(_result => result = _result);

      tick();

      expect(result).toEqual(expectedResult);

      expect(identityService.changePassword).toHaveBeenCalled();
    }));

    it('should update role', fakeAsync(() => {
      const { identityService, employeeEffects } = setup();

      identityService.changeUserRole.and.returnValue(Observable.of({}));

      const employee: Employee = {
        identifier: '',
        givenName: '',
        surname: '',
        contactDetails: []
      };

      const expectedResult = new UpdateEmployeeSuccessAction({
        resource: employee,
        activatedRoute: null
      });

      actions = new ReplaySubject(1);

      actions.next(new UpdateEmployeeAction({
        employee: employee,
        role: 'test',
        activatedRoute: null
      }));

      let result = null;
      employeeEffects.updateEmployee$.subscribe(_result => result = _result);

      tick();

      expect(result).toEqual(expectedResult);

      expect(identityService.changeUserRole).toHaveBeenCalled();
    }));
  });
});
