/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {NgModule} from '@angular/core';
import {RoleComponent} from './role.component';
import {RoleRoutingModule} from './role-routing.module';
import {RoleFormComponent} from './form/form.component';
import {CreateRoleFormComponent} from './form/create/create.form.component';
import {EditRoleFormComponent} from './form/edit/edit.form.component';
import {RoleExistsGuard} from './role-exists.guard';
import {reducerProvider, reducerToken} from './store/index';
import {StoreModule} from '@ngrx/store';
import {RoleNotificationEffects} from './store/effects/notification.effects';
import {EffectsModule} from '@ngrx/effects';
import {RoleRouteEffects} from './store/effects/route.effects';
import {RoleApiEffects} from './store/effects/service.effects';
import {RoleDetailComponent} from './detail/role.detail.component';
import {FormPermissionService} from './helper/form-permission.service';
import {PermissionListItemComponent} from './components/permission-list-item.component';
import {SharedModule} from '../../shared/shared.module';
import {RoleIndexComponent} from './role-index.component';

@NgModule({
  imports: [
    RoleRoutingModule,
    SharedModule,

    StoreModule.forFeature('role', reducerToken),

    EffectsModule.forFeature([
      RoleApiEffects,
      RoleRouteEffects,
      RoleNotificationEffects
    ])
  ],
  declarations: [
    RoleComponent,
    RoleIndexComponent,
    RoleFormComponent,
    CreateRoleFormComponent,
    EditRoleFormComponent,
    RoleDetailComponent,
    PermissionListItemComponent
  ],
  providers: [
    FormPermissionService,
    RoleExistsGuard,
    reducerProvider
  ],
  entryComponents: []
})
export class RoleModule {}
