/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as fromRoot from '../../../core/store/index';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {createSelector} from 'reselect';
import {createResourceReducer, getResourceLoadedAt, getResourceSelected, ResourceState} from '../../../core/store/util/resource.reducer';
import {createFormReducer, FormState, getFormError} from '../../../core/store/util/form.reducer';
import {InjectionToken} from '@angular/core';

export interface RoleState {
  roles: ResourceState;
  roleForm: FormState;
}

export const reducers: ActionReducerMap<RoleState> = {
  roles: createResourceReducer('Role'),
  roleForm: createFormReducer('Role'),
};

export const reducerToken = new InjectionToken<ActionReducerMap<RoleState>>('Role reducers');

export const reducerProvider = [
  { provide: reducerToken, useValue: reducers }
];


export interface State extends fromRoot.State {
  role: RoleState;
}

export const selectRole = createFeatureSelector<RoleState>('role');

export const getRolesState = createSelector(selectRole, (state: RoleState) => state.roles);

export const getRoleFormState = createSelector(selectRole, (state: RoleState) => state.roleForm);
export const getRoleFormError = createSelector(getRoleFormState, getFormError);

export const getRolesLoadedAt = createSelector(getRolesState, getResourceLoadedAt);
export const getSelectedRole = createSelector(getRolesState, getResourceSelected);
