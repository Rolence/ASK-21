/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Component, OnInit} from '@angular/core';
import * as fromRoles from '../store/index';
import {DELETE} from '../store/role.actions';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {FormPermissionService} from '../helper/form-permission.service';
import {FormPermissionGroup} from '../model/form-permission-group.model';
import {Store} from '@ngrx/store';
import {Role} from '../../../core/services/identity/domain/role.model';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {DialogService} from '../../../core/services/dialog/dialog.service';
import {PermittableGroup} from '../../../core/services/anubis/permittable-group.model';

@Component({
  templateUrl: './role.detail.component.html'
})
export class RoleDetailComponent implements OnInit {

  role$: Observable<Role>;
  permissionGroup$: Observable<FormPermissionGroup[]>;

  constructor(private router: Router, private route: ActivatedRoute,
              private identityService: IdentityService, private store: Store<fromRoles.State>,
              private formPermissionService: FormPermissionService, private dialogService: DialogService) {}

  ngOnInit(): void {
    this.role$ = this.store.select(fromRoles.getSelectedRole)
      .filter(role => !!role);

    this.permissionGroup$ = Observable.combineLatest(
      this.identityService.getPermittableGroups(),
      this.role$,
      (groups: PermittableGroup[], role: Role) => this.formPermissionService.mapToFormPermissions(groups, role.permissions)
    );
  }

  confirmDeletion(): Observable<boolean> {
    return this.dialogService.openConfirm({
      message: 'Do you want to delete this role?',
      title: 'Confirm deletion',
      acceptButton: 'DELETE ROLE',
    });
  }

  deleteRole(role: Role): void {
    this.confirmDeletion()
      .filter(accept => accept)
      .subscribe(() => {
        this.store.dispatch({ type: DELETE, payload: {
          role,
          activatedRoute: this.route
        } });
      });
  }

  goToEditPage(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
