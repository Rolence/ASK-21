/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {RouterModule, Routes} from '@angular/router';
import {EmployeeListComponent} from './employee-list.component';
import {CreateEmployeeFormComponent} from './form/create/create.form.component';
import {EmployeeDetailComponent} from './detail/employee.detail.component';
import {EditEmployeeFormComponent} from './form/edit/edit.form.component';
import {UserResolver} from './user.resolver';
import {EmployeeExistsGuard} from './employee-exists.guard';
import {NgModule} from '@angular/core';
import {IndexComponent} from '../shared/util/index.component';
import {EmployeeIndexComponent} from './employee-index.component';

const EmployeeRoutes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'All employees',
      hasPermission: {id: 'office_employees', accessLevel: 'READ'}
    },
    children: [
      {
        path: '',
        component: EmployeeListComponent
      },
      {
        path: 'create',
        component: CreateEmployeeFormComponent,
        data: {
          title: 'Add employee',
          hasPermission: {id: 'office_employees', accessLevel: 'CHANGE'}
        }
      },
      {
        path: 'detail/:id',
        component: EmployeeIndexComponent,
        canActivate: [EmployeeExistsGuard],
        resolve: {
          user: UserResolver
        },
        children: [
          {
            path: '',
            component: EmployeeDetailComponent,
          },
          {
            path: 'edit',
            component: EditEmployeeFormComponent,
            data: {
              title: 'Edit',
              hasPermission: {id: 'office_employees', accessLevel: 'CHANGE'}
            }
          }
        ]
      }
    ]
  },
  { path: 'roles', loadChildren: './roles/role.module#RoleModule' },
];

@NgModule({
  imports: [
    RouterModule.forChild(EmployeeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeeRoutingModule {}
