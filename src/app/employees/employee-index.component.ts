/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {Component, OnDestroy} from '@angular/core';
import {SelectAction} from './store/employee.actions';
import * as fromEmployees from './store';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';
import {BreadCrumbService} from '../shared/bread-crumbs/bread-crumb.service';
import {Employee} from '../core/services/office/domain/employee.model';

@Component({
  templateUrl: './employee-index.component.html'
})
export class EmployeeIndexComponent implements OnDestroy {

  private actionsSubscription: Subscription;
  private employeeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private store: Store<fromEmployees.State>, private breadCrumbService: BreadCrumbService) {
    this.actionsSubscription = this.route.params
      .map(params => new SelectAction(params['id']))
      .subscribe(this.store);

    this.employeeSubscription = this.store.select(fromEmployees.getSelectedEmployee)
      .filter(employee => !!employee)
      .subscribe((employee: Employee) =>
        this.breadCrumbService.setCustomBreadCrumb(this.route.snapshot, `${employee.givenName} ${employee.surname}`)
      );
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
    this.employeeSubscription.unsubscribe();
  }

}
